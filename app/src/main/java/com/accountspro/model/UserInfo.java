package com.accountspro.model;

public class UserInfo {
    String mobileno = "",pin = "",otp = "",deviceinfo = "",regi = "",demo = "";

    public UserInfo(String mobileno, String pin, String otp, String deviceinfo, String regi, String demo) {
        this.mobileno = mobileno;
        this.pin = pin;
        this.otp = otp;
        this.deviceinfo = deviceinfo;
        this.regi = regi;
        this.demo = demo;
    }

    public String getRegi() {
        return regi;
    }

    public String getDemo() {
        return demo;
    }

    public String getMobileno() {
        return mobileno;
    }

    public String getPin() {
        return pin;
    }

    public String getOtp() {
        return otp;
    }

    public String getDeviceinfo() {
        return deviceinfo;
    }
}
