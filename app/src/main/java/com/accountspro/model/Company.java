package com.accountspro.model;

public class Company {
    String compcode,compname,acyear,yearcode,orgreg,address,modulecode,billwisereqd,regionreqd,payrollreqd,batchreqd,legacyscan,salesstatementreqd;

    public Company(String compcode, String compname, String acyear, String yearcode, String orgreg,String address,String modulecode,String billwisereqd,String regionreqd,String payrollreqd,String batchreqd,String legacyscan,String salesstatementreqd) {
        this.compcode = compcode;
        this.compname = compname;
        this.acyear = acyear;
        this.yearcode = yearcode;
        this.orgreg = orgreg;
        this.address = address;
        this.modulecode = modulecode;
        this.billwisereqd = billwisereqd;
        this.regionreqd = regionreqd;
        this.payrollreqd = payrollreqd;
        this.batchreqd = batchreqd;
        this.legacyscan = legacyscan;
        this.salesstatementreqd = salesstatementreqd;
    }

    public String getAddress() {
        return address;
    }

    public String getModulecode() {
        return modulecode;
    }

    public String getBillwisereqd() {
        return billwisereqd;
    }

    public String getRegionreqd() {
        return regionreqd;
    }

    public String getPayrollreqd() {
        return payrollreqd;
    }

    public String getBatchreqd() {
        return batchreqd;
    }

    public String getLegacyscan() {
        return legacyscan;
    }

    public String getSalesstatementreqd() {
        return salesstatementreqd;
    }

    public String getOrgreg() {
        return orgreg;
    }

    public String getCompcode() {
        return compcode;
    }

    public String getCompname() {
        return compname;
    }

    public String getAcyear() {
        return acyear;
    }

    public String getYearcode() {
        return yearcode;
    }
}
