package com.accountspro.model;

public class Organization {
    String licencekey, organization_name, regkey;

    public Organization() {
    }

    public Organization(String regkey, String organization_name, String licencekey) {
        this.licencekey = licencekey;
        this.organization_name = organization_name;
        this.regkey = regkey;
    }

    public String getLicencekey() {
        return licencekey;
    }

    public String getOrganization_name() {
        return organization_name;
    }

    public String getRegkey() {
        return regkey;
    }
}
