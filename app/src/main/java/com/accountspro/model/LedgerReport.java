package com.accountspro.model;

public class LedgerReport {
    String trdate2,particulars,vchno,debit,credit,vchcode,vouchertypecode,narration = "",amount = "";

    public LedgerReport() {
    }

    public LedgerReport(String trdate2, String particulars, String vchno, String debit, String credit, String vchcode, String vouchertypecode, String narration, String amount) {
        this.trdate2 = trdate2;
        this.particulars = particulars;
        this.vchno = vchno;
        this.debit = debit;
        this.credit = credit;
        this.vchcode = vchcode;
        this.vouchertypecode = vouchertypecode;
        this.narration = narration;
        this.amount = amount;
    }

    public String getAmount() {
        return amount;
    }

    public String getTrdate2() {
        return trdate2;
    }

    public String getParticulars() {
        return particulars;
    }

    public String getVchno() {
        return vchno;
    }

    public String getDebit() {
        return debit;
    }

    public String getCredit() {
        return credit;
    }

    public String getVchcode() {
        return vchcode;
    }

    public String getVouchertypecode() {
        return vouchertypecode;
    }

    public String getNarration() {
        return narration;
    }
}
