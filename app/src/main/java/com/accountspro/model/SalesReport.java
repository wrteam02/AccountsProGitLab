package com.accountspro.model;

public class SalesReport {
    String trdate2, ledgercode, cashdebit, billtype, vchno, vchcode, vouchertypecode, amount = "";
    String partyname,typename,name;
    int no = 0;
    double doubleamount = 0;

    public SalesReport(String name) {
        this.name = name;
    }

    public SalesReport(String trdate2, String ledgercode, String cashdebit, String billtype, String vchno, String vchcode, String amount, String vouchertypecode, String partyname, String typename) {
        this.trdate2 = trdate2;
        this.ledgercode = ledgercode;
        this.cashdebit = cashdebit;
        this.billtype = billtype;
        this.vchno = vchno;
        this.vchcode = vchcode;
        this.vouchertypecode = vouchertypecode;
        this.amount = amount;
        this.partyname = partyname;
        this.typename = typename;
    }

    public SalesReport(String name,int no,double doubleamount) {
        this.name = name;
        this.no = no;
        this.doubleamount = doubleamount;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public void setDoubleamount(double doubleamount) {
        this.doubleamount = doubleamount;
    }

    public double getDoubleamount() {
        return doubleamount;
    }

    public String getName() {
        return name;
    }

    public int getNo() {
        return no;
    }

    public String getPartyname() {
        return partyname;
    }

    public String getTypename() {
        return typename;
    }

    public String getTrdate2() {
        return trdate2;
    }

    public String getLedgercode() {
        return ledgercode;
    }

    public String getCashdebit() {
        return cashdebit;
    }

    public String getBilltype() {
        return billtype;
    }

    public String getVchno() {
        return vchno;
    }

    public String getVchcode() {
        return vchcode;
    }

    public String getVouchertypecode() {
        return vouchertypecode;
    }

    public String getAmount() {
        return amount;
    }
}
