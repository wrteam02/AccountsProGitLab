package com.accountspro.model;

import java.util.ArrayList;

public class StockStatus {
    String groupingcode, minqty, itemcode, qty, unitcode, rate, itemname, groupname, amount,unitname;
    ArrayList<StockStatus> stockStatusArrayList;

    double doubleqty, doubleamount;

    public StockStatus(String groupingcode, String minqty, String itemcode, String qty, String unitcode, String rate, String itemname, String amount) {
        this.groupingcode = groupingcode;
        this.minqty = minqty;
        this.itemcode = itemcode;
        this.qty = qty;
        this.unitcode = unitcode;
        this.rate = rate;
        this.itemname = itemname;
        this.amount = amount;
    }

    public StockStatus(String groupingcode, String groupname, double doubleqty, double doubleamount, ArrayList<StockStatus> stockStatusArrayList) {
        this.groupingcode = groupingcode;
        this.groupname = groupname;
        this.doubleqty = doubleqty;
        this.doubleamount = doubleamount;
        this.stockStatusArrayList = stockStatusArrayList;
    }

    public String getAmount() {
        return amount;
    }

    public String getUnitname() {
        return unitname;
    }

    public void setGroupingcode(String groupingcode) {
        this.groupingcode = groupingcode;
    }

    public void setStockStatusArrayList(ArrayList<StockStatus> stockStatusArrayList) {
        this.stockStatusArrayList = stockStatusArrayList;
    }

    public void setDoubleqty(double doubleqty) {
        this.doubleqty = doubleqty;
    }

    public void setDoubleamount(double doubleamount) {
        this.doubleamount = doubleamount;
    }

    public String getGroupname() {
        return groupname;
    }

    public ArrayList<StockStatus> getStockStatusArrayList() {
        return stockStatusArrayList;
    }

    public double getDoubleqty() {
        return doubleqty;
    }

    public double getDoubleamount() {
        return doubleamount;
    }

    public String getGroupingcode() {
        return groupingcode;
    }

    public String getMinqty() {
        return minqty;
    }

    public String getItemcode() {
        return itemcode;
    }

    public String getQty() {
        return qty;
    }

    public String getUnitcode() {
        return unitcode;
    }

    public String getRate() {
        return rate;
    }

    public String getItemname() {
        return itemname;
    }
}
