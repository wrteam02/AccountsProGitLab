package com.accountspro.model;

public class VoucherInfo {
    String dtype,dtl1,dtl2,dtl3,dtl4,dtl5,dtl6,dtl7,compcode,orgcode,vchno,vchcode,vchtypecode,disgstrate,stockitemname,particulars,displayamount;

    public VoucherInfo(String dtype, String dtl1, String dtl2, String dtl3, String dtl4, String dtl5, String dtl6, String dtl7) {
        this.dtype = dtype;
        this.dtl1 = dtl1;
        this.dtl2 = dtl2;
        this.dtl3 = dtl3;
        this.dtl4 = dtl4;
        this.dtl5 = dtl5;
        this.dtl6 = dtl6;
        this.dtl7 = dtl7;
    }
    public VoucherInfo(String dtype, String dtl1, String dtl2, String dtl3, String dtl4, String dtl5, String dtl6, String dtl7,String disgstrate,String stockitemname) {
        this.dtype = dtype;
        this.dtl1 = dtl1;
        this.dtl2 = dtl2;
        this.dtl3 = dtl3;
        this.dtl4 = dtl4;
        this.dtl5 = dtl5;
        this.dtl6 = dtl6;
        this.dtl7 = dtl7;
        this.disgstrate = disgstrate;
        this.stockitemname = stockitemname;
    }
    public VoucherInfo(String dtype, String dtl1, String dtl2, String dtl3, String dtl4, String dtl5, String dtl6, String dtl7,String particulars,String displayamount,String temp) {
        this.dtype = dtype;
        this.dtl1 = dtl1;
        this.dtl2 = dtl2;
        this.dtl3 = dtl3;
        this.dtl4 = dtl4;
        this.dtl5 = dtl5;
        this.dtl6 = dtl6;
        this.dtl7 = dtl7;
        this.particulars = particulars;
        this.displayamount = displayamount;
    }

    public String getDisplayamount() {
        return displayamount;
    }

    public String getParticulars() {
        return particulars;
    }

    public String getStockitemname() {
        return stockitemname;
    }

    public String getDisgstrate() {
        return disgstrate;
    }


    public String getVchno() {
        return vchno;
    }

    public String getVchcode() {
        return vchcode;
    }

    public String getVchtypecode() {
        return vchtypecode;
    }

    public String getDtype() {
        return dtype;
    }

    public String getDtl1() {
        return dtl1;
    }

    public String getDtl2() {
        return dtl2;
    }

    public String getDtl3() {
        return dtl3;
    }

    public String getDtl4() {
        return dtl4;
    }

    public String getDtl5() {
        return dtl5;
    }

    public String getDtl6() {
        return dtl6;
    }

    public String getDtl7() {
        return dtl7;
    }

    public String getCompcode() {
        return compcode;
    }

    public String getOrgcode() {
        return orgcode;
    }
}
