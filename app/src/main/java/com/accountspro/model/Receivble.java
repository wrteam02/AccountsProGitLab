package com.accountspro.model;

import java.util.ArrayList;

public class Receivble {
    String mobileno, trdate2, ledgercode, regioncode, vchno, vchcode, vouchertypecode, billamt, due, dc, duedate2, partyname, regionname, duedays;
    ArrayList<Receivble> receivbleArrayList;
    double duedouble;

    public Receivble(String trdate2, String ledgercode, String regioncode, String vchno, String vccode, String vouchertypecode, String billamt, String due, String dc, String duedate2, String duedays, String regionname, String partyname, String mobileno) {
        this.trdate2 = trdate2;
        this.ledgercode = ledgercode;
        this.regioncode = regioncode;
        this.vchno = vchno;
        this.vchcode = vccode;
        this.vouchertypecode = vouchertypecode;
        this.billamt = billamt;
        this.due = due;
        this.dc = dc;
        this.duedate2 = duedate2;
        this.duedays = duedays;
        this.regionname = regionname;
        this.partyname = partyname;
        this.mobileno = mobileno;
    }

    public Receivble(String ledgercode, String partyname, String regionname, String due, String mobileno, ArrayList<Receivble> receivbleArrayList) {
        this.ledgercode = ledgercode;
        this.due = due;
        this.partyname = partyname;
        this.regionname = regionname;
        this.mobileno = mobileno;
        this.receivbleArrayList = receivbleArrayList;
    }
    public Receivble(String ledgercode, String partyname, String regionname, double duedouble, String mobileno, ArrayList<Receivble> receivbleArrayList) {
        this.ledgercode = ledgercode;
        this.duedouble = duedouble;
        this.partyname = partyname;
        this.regionname = regionname;
        this.mobileno = mobileno;
        this.receivbleArrayList = receivbleArrayList;
    }

    public Receivble(String ledgercode, String partyname, String regionname, String due, String mobileno) {
        this.ledgercode = ledgercode;
        this.due = due;
        this.partyname = partyname;
        this.regionname = regionname;
        this.mobileno = mobileno;
    }

    public void setReceivbleArrayList(ArrayList<Receivble> receivbleArrayList) {
        this.receivbleArrayList = receivbleArrayList;
    }

    public double getDuedouble() {
        return duedouble;
    }

    public void setDue(String due) {
        this.due = due;
    }

    public void setTrdate2(String trdate2) {
        this.trdate2 = trdate2;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setDuedate2(String duedate2) {
        this.duedate2 = duedate2;
    }

    public void setBillamt(String billamt) {
        this.billamt = billamt;
    }

    public ArrayList<Receivble> getReceivbleArrayList() {
        return receivbleArrayList;
    }

    public String getRegionname() {
        return regionname;
    }

    public String getDuedays() {
        return duedays;
    }

    public String getPartyname() {
        return partyname;
    }

    public String getTrdate2() {
        return trdate2;
    }

    public String getLedgercode() {
        return ledgercode;
    }

    public String getRegioncode() {
        return regioncode;
    }

    public String getVchno() {
        return vchno;
    }

    public String getVchcode() {
        return vchcode;
    }

    public String getVouchertypecode() {
        return vouchertypecode;
    }

    public String getBillamt() {
        return billamt;
    }

    public String getDue() {
        return due;
    }

    public String getDc() {
        return dc;
    }

    public String getDuedate2() {
        return duedate2;
    }
}
