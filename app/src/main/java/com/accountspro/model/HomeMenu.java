package com.accountspro.model;

import android.graphics.drawable.Drawable;

public class HomeMenu {
    String name, funname;
    int image;

    public HomeMenu(String name, String funname, int image) {
        this.name = name;
        this.funname = funname;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public String getFunname() {
        return funname;
    }

    public int getImage() {
        return image;
    }
}
