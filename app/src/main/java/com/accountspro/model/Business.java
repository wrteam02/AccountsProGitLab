package com.accountspro.model;

import android.graphics.drawable.Drawable;

public class Business {
    String dtype,dtl1,dtl2,masttype,namedisplay,maindtl2;


    public Business(String dtype, String dtl1, String dtl2, String masttype,String namedisplay,String maindtl2) {
        this.dtype = dtype;
        this.dtl1 = dtl1;
        this.dtl2 = dtl2;
        this.masttype = masttype;
        this.namedisplay = namedisplay;
        this.maindtl2 = maindtl2;
    }



    public String getMaindtl2() {
        return maindtl2;
    }

    public String getNamedisplay() {
        return namedisplay;
    }

    public String getDtype() {
        return dtype;
    }

    public String getDtl1() {
        return dtl1;
    }

    public String getDtl2() {
        return dtl2;
    }

    public String getMasttype() {
        return masttype;
    }
}
