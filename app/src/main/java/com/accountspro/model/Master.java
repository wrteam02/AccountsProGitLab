package com.accountspro.model;

public class Master {
    String reccode,datatype,mastercode,mastername,dtl1,dtl2,dtl3,dtl4,dtl5,compcode,orgcode;

    public Master(String reccode, String datatype, String mastercode, String mastername, String dtl1, String dtl2, String dtl3, String dtl4, String dtl5, String compcode,String orgcode) {
        this.reccode = reccode;
        this.datatype = datatype;
        this.mastercode = mastercode;
        this.mastername = mastername;
        this.dtl1 = dtl1;
        this.dtl2 = dtl2;
        this.dtl3 = dtl3;
        this.dtl4 = dtl4;
        this.dtl5 = dtl5;
        this.compcode = compcode;
        this.orgcode = orgcode;
    }

    public Master(String mastercode, String mastername) {
        this.mastercode = mastercode;
        this.mastername = mastername;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public String toString() {
        return getMastername().trim();
    }

    public String getReccode() {
        return reccode;
    }

    public String getDatatype() {
        return datatype;
    }

    public String getMastercode() {
        return mastercode;
    }

    public String getMastername() {
        return mastername;
    }

    public String getDtl1() {
        return dtl1;
    }

    public String getDtl2() {
        return dtl2;
    }

    public String getDtl3() {
        return dtl3;
    }

    public String getDtl4() {
        return dtl4;
    }

    public String getDtl5() {
        return dtl5;
    }

    public String getCompcode() {
        return compcode;
    }
}
