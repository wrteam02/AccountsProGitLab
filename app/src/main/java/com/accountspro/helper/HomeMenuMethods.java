package com.accountspro.helper;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import com.accountspro.activity.BusinessViewActivity;
import com.accountspro.activity.LedgerReportActivity;
import com.accountspro.activity.OwnerHomeActivity;
import com.accountspro.activity.ReceivbleActivity;
import com.accountspro.activity.SalesReportActivity;
import com.accountspro.activity.StockStatusActivity;
import com.accountspro.activity.VoucherInfoActivity;

public class HomeMenuMethods {

    Activity activity;
    UserSessionManager session;
    String orgcode, compcode,subject;
    DatabaseHelper databaseHelper;
    View view;

    public HomeMenuMethods(Activity activity,UserSessionManager session,String orgcode,String compcode,DatabaseHelper databaseHelper,String subject,View view) {
        this.activity = activity;
        this.session = session;
        this.orgcode = orgcode;
        this.compcode = compcode;
        this.databaseHelper = databaseHelper;
        this.subject = subject;
        this.view = view;
    }

    public void OnUpdateMasterClick() {

        session.OnUpdateMasterClick(new VolleyCallback() {
            @Override
            public void onSuccess(boolean result) {
               OwnerHomeActivity.setGridDetails();
            }
            public void onSuccessWithMsg(boolean result, String message) {}
        }, activity, session, orgcode, compcode, databaseHelper, subject,view);
    }


    public void OnBusinessviewClick() {
        activity.startActivity(new Intent(activity, BusinessViewActivity.class).putExtra("from", "home"));
    }

    public void OnReceivbleClick() {
        activity.startActivity(new Intent(activity, ReceivbleActivity.class).putExtra("from", "home"));
    }

    public void OnLedgerReportClick() {
        activity.startActivity(new Intent(activity, LedgerReportActivity.class).putExtra("from", "home"));
    }

    public void OnSalesReportClick() {
        activity.startActivity(new Intent(activity, SalesReportActivity.class).putExtra("from", "home"));
    }

    public void OnStockClick() {
        activity.startActivity(new Intent(activity, StockStatusActivity.class).putExtra("from", "home"));
    }

    public void OnVoucherInfoClick() {
        activity.startActivity(new Intent(activity, VoucherInfoActivity.class).putExtra("from", "home").putExtra("code", "0"));
    }
}
