package com.accountspro.helper;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.accountspro.R;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import java.io.File;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

public class Constant {


    static SimpleDateFormat month_date = new SimpleDateFormat("MMM");
    static SimpleDateFormat date_name = new SimpleDateFormat("EEEE");
    static SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
    static SimpleDateFormat inFormatdate = new SimpleDateFormat("yyyyMMdd");


    public static String BaseUrl = "http://netcomsoftware.co.in/android/app.php?";
    public static String MobRegiUrl = BaseUrl + "req=mobnoregi&mobno=";
    public static String PINSETUrl = BaseUrl + "req=mobnopininfo&mobno=";//9825730217&pin=123456";
    public static String GETORGANIZATIONUrl = BaseUrl + "req=orglist&mobno=";//9825730217&demo=Yes";
    public static String GETCOMPANYNUrl = BaseUrl + "req=req_complist&regkey=";//101&devicecode=1";
    public static String GETMASTERNUrl = BaseUrl + "req=req_downloaddata&ledger=Yes&item=Yes&regkey=";//111&compcode=00008;
    public static String OTPUrl = "http://sms.netcomsoftware.co.in/submitsms.jsp?user=NetcomSF&key=c3ff8d2de7XX&senderid=NETCOM&accusage=1&";//mobile=8200727077&message=test
    public static String SETDEVICEINFO = BaseUrl + "req=mobnoregiinfo&mobno=";//9825730217&imei=1111&deviceinfo=samsungj7, OS:Android 7 etc";
    public static String BusinessUrl = BaseUrl + "req=req_busview&regkey=";//336&compcode=00008&demo=Yes&date1=20190401&date2=20190415";
    public static String ReceivbleUrl = BaseUrl + "req=req_receivble&regkey=";//336&compcode=00008&demo=Yes&date2=20190521&searchby=Ledger Region&searchValue=3";
    public static String DemoReqCountUrl = BaseUrl + "req=demotext&mobno=";//9825730217";
    public static String DEACTIVATEDEVICE = BaseUrl + "req=devicedeactive&devicecode=";//2
    public static String VoucherInfoUrl = BaseUrl + "req=req_vchinfo&regkey=";//1&devicecode=1&compcode=00001&demo=Yes&vouchertypecode=6&vchno=10&vchcode=0";
    public static String LedgerReportUrl = BaseUrl + "req=req_ledreport&regkey=";//111&devicecode=1&compcode=00008&demo=Yes&date1=20190401&date2=20190521&ledgercode=212&narr=No;
    public static String SalesReportUrl = BaseUrl + "req=req_salereport&regkey=";//336&devicecode=1&%20compcode=00008&demo=Yes&date1=20190401&date2=20190521&vouchertypecode=101&mobno=8200727077&imei=358344089891766&devicecode=2";
    public static String ErrorReportUrl = BaseUrl + "req=errorlog&errdesc=";//TestDesc&subject=TestSubject";
    public static String StockStatusUrl = BaseUrl + "req=req_stockstatus&regkey=";//111&devicecode=1&compcode=00008&demo=Yes&date2=20190521&searchby=Item Group&searchValue=1&reorder=No";
    public static String UserInfoUrl = BaseUrl + "req=deviceinfo&devicecode=";

    public static String SMSHashCode = "+OEyXjFe7QE";
    public static int SalesReport_PAGELIMIT = 150;

    public static String LedgerReportSessionName = "ledgereport";
    public static String SalesReportSessionName = "salesreport";
    public static String DefaultLedgerGroupSearchValue = "Customers";
    public static String VoucherInfoSessionName = "vinfo_";
    public static String ERRORID = "APP";
    public static String ERRORNOCOMP = "NO COMP";
    public static String ERRORNOORG = "NO ORG";
    public static String STOCKSTATUSSESSIONNAME = "stockstatus";
    static NumberFormat indianFormat = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
    static DecimalFormat decimalFormat = new DecimalFormat("#.##");
    public static int ReqReadPermission = 1;
    public static int ReqWritePermission = 2;


    public static String QuickSpinnerType(String item) {
        String type = "";
        if (item.equals("Today")) {
            type = "today";
        } else if (item.equals("Yesterday")) {
            type = "yesterday";
        } else if (item.equals("Current Week")) {
            type = "week";
        } else if (item.equals("Current Month")) {
            type = "cmonth";
        } else if (item.equals("Last Month")) {
            type = "lmonth";
        } else if (item.equals("Current Quarter")) {
            type = "cqua";
        } else if (item.equals("Last Quarter")) {
            type = "lqua";
        } else if (item.equals("Current A/c Year")) {
            type = "cyear";
        } else if (item.equals("Last A/c Year")) {
            type = "lyear";
        }
        return type;
    }

    public static String SetDateData(Calendar calendar, String day, TextView txtfromdt, TextView txttodt, TextView txtfrom, TextView txtto) {

        String dateString = "", monthString = "", currentdate = "", day_name = "", month_name = "", yearString = "";
        String tdateString = "", tmonthString = "", tcurrentdate = "", tday_name = "", tmonth_name = "", tyearString = "";
        String startdate = "", enddate = "";
        try {
            if (day.equals("today")) {
                dateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                tdateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                monthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                tmonthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                yearString = String.valueOf(calendar.get(Calendar.YEAR));
                tyearString = String.valueOf(calendar.get(Calendar.YEAR));
                if (monthString.length() == 1) {
                    monthString = "0" + monthString;
                    tmonthString = "0" + tmonthString;
                }
                if (dateString.length() == 1) {
                    dateString = "0" + dateString;
                    tdateString = "0" + tdateString;
                }

                month_name = month_date.format(calendar.getTime());
                tmonth_name = month_date.format(calendar.getTime());
                day_name = date_name.format(calendar.getTime());
                tday_name = date_name.format(calendar.getTime());

                currentdate = "  " + month_name + " " + calendar.get(Calendar.YEAR) + "\n  " + day_name;
                tcurrentdate = "  " + tmonth_name + " " + calendar.get(Calendar.YEAR) + "\n  " + tday_name;

            } else if (day.equals("yesterday")) {
                calendar.add(Calendar.DATE, -1);

                dateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                tdateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                monthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                tmonthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                yearString = String.valueOf(calendar.get(Calendar.YEAR));
                tyearString = String.valueOf(calendar.get(Calendar.YEAR));
                if (monthString.length() == 1) {
                    monthString = "0" + monthString;
                    tmonthString = "0" + tmonthString;
                }
                if (dateString.length() == 1) {
                    dateString = "0" + dateString;
                    tdateString = "0" + tdateString;
                }

                month_name = month_date.format(calendar.getTime());
                tmonth_name = month_date.format(calendar.getTime());
                day_name = date_name.format(calendar.getTime());
                tday_name = date_name.format(calendar.getTime());

                currentdate = "  " + month_name + " " + calendar.get(Calendar.YEAR) + "\n  " + day_name;
                tcurrentdate = "  " + tmonth_name + " " + calendar.get(Calendar.YEAR) + "\n  " + tday_name;

            } else if (day.equals("week")) {

                tdateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                tmonthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                tyearString = String.valueOf(calendar.get(Calendar.YEAR));
                if (tmonthString.length() == 1) {
                    tmonthString = "0" + tmonthString;
                }
                if (tdateString.length() == 1) {
                    tdateString = "0" + tdateString;
                }


                tmonth_name = month_date.format(calendar.getTime());
                tday_name = date_name.format(calendar.getTime());

                tcurrentdate = "  " + tmonth_name + " " + calendar.get(Calendar.YEAR) + "\n  " + tday_name;


                calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

                dateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                monthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                yearString = String.valueOf(calendar.get(Calendar.YEAR));
                if (monthString.length() == 1) {
                    monthString = "0" + monthString;
                }
                if (dateString.length() == 1) {
                    dateString = "0" + dateString;
                }
                startdate = calendar.get(Calendar.YEAR) + monthString + dateString;
                month_name = month_date.format(calendar.getTime());
                day_name = date_name.format(calendar.getTime());

                currentdate = "  " + month_name + " " + calendar.get(Calendar.YEAR) + "\n  " + day_name;

                txtfromdt.setText(dateString);

            /*for (int i = 0; i < 6; i++) {
                calendar.add(Calendar.DATE, 1);
            }

            tdateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
            tmonthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
            tyearString = String.valueOf(calendar.get(Calendar.YEAR));
            if (tmonthString.length() == 1) {
                tmonthString = "0" + tmonthString;
            }
            if (tdateString.length() == 1) {
                tdateString = "0" + tdateString;
            }


            tmonth_name = month_date.format(calendar.getTime());
            tday_name = date_name.format(calendar.getTime());

            tcurrentdate = "  " + tmonth_name + " " + calendar.get(Calendar.YEAR) + "\n  " + tday_name;*/

            } else if (day.equals("cmonth")) {

                tdateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                tmonthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                tyearString = String.valueOf(calendar.get(Calendar.YEAR));
                if (tmonthString.length() == 1) {
                    tmonthString = "0" + tmonthString;
                }
                if (tdateString.length() == 1) {
                    tdateString = "0" + tdateString;
                }

                tmonth_name = month_date.format(calendar.getTime());
                tday_name = date_name.format(calendar.getTime());

                tcurrentdate = "  " + tmonth_name + " " + calendar.get(Calendar.YEAR) + "\n  " + tday_name;


                calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
                dateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                monthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                yearString = String.valueOf(calendar.get(Calendar.YEAR));
                if (monthString.length() == 1) {
                    monthString = "0" + monthString;
                }
                if (dateString.length() == 1) {
                    dateString = "0" + dateString;
                }

                month_name = month_date.format(calendar.getTime());
                day_name = date_name.format(calendar.getTime());

                currentdate = "  " + month_name + " " + calendar.get(Calendar.YEAR) + "\n  " + day_name;


            /*calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            tdateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
            tmonthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
            tyearString = String.valueOf(calendar.get(Calendar.YEAR));
            if (tmonthString.length() == 1) {
                tmonthString = "0" + tmonthString;
            }
            if (tdateString.length() == 1) {
                tdateString = "0" + tdateString;
            }

            tmonth_name = month_date.format(calendar.getTime());
            tday_name = date_name.format(calendar.getTime());

            tcurrentdate = "  " + tmonth_name + " " + calendar.get(Calendar.YEAR) + "\n  " + tday_name;*/

            } else if (day.equals("lmonth")) {
                calendar.add(Calendar.MONTH, -1);
                calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
                dateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                monthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                yearString = String.valueOf(calendar.get(Calendar.YEAR));
                if (monthString.length() == 1) {
                    monthString = "0" + monthString;
                }
                if (dateString.length() == 1) {
                    dateString = "0" + dateString;
                }

                month_name = month_date.format(calendar.getTime());
                day_name = date_name.format(calendar.getTime());

                currentdate = "  " + month_name + " " + calendar.get(Calendar.YEAR) + "\n  " + day_name;


                calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                tdateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                tmonthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                tyearString = String.valueOf(calendar.get(Calendar.YEAR));
                if (tmonthString.length() == 1) {
                    tmonthString = "0" + tmonthString;
                }
                if (tdateString.length() == 1) {
                    tdateString = "0" + tdateString;
                }

                tmonth_name = month_date.format(calendar.getTime());
                tday_name = date_name.format(calendar.getTime());

                tcurrentdate = "  " + tmonth_name + " " + calendar.get(Calendar.YEAR) + "\n  " + tday_name;

            } else if (day.equals("cyear")) {
                tdateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                tmonthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                tyearString = String.valueOf(calendar.get(Calendar.YEAR));
                if (tmonthString.length() == 1) {
                    tmonthString = "0" + tmonthString;
                }
                if (tdateString.length() == 1) {
                    tdateString = "0" + tdateString;
                }

                tmonth_name = month_date.format(calendar.getTime());
                tday_name = date_name.format(calendar.getTime());

                tcurrentdate = "  " + tmonth_name + " " + calendar.get(Calendar.YEAR) + "\n  " + tday_name;


                calendar.set(Calendar.MONTH, Calendar.APRIL);
                calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
                //calendar.set(Calendar.DAY_OF_YEAR, 1);
                dateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                monthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                yearString = String.valueOf(calendar.get(Calendar.YEAR));
                if (monthString.length() == 1) {
                    monthString = "0" + monthString;
                }
                if (dateString.length() == 1) {
                    dateString = "0" + dateString;
                }

                month_name = month_date.format(calendar.getTime());
                day_name = date_name.format(calendar.getTime());

                currentdate = "  " + month_name + " " + calendar.get(Calendar.YEAR) + "\n  " + day_name;


            /*calendar.add(Calendar.YEAR, 1);
            calendar.set(Calendar.MONTH, Calendar.MARCH);
            calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));

            tdateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
            tmonthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
            tyearString = String.valueOf(calendar.get(Calendar.YEAR));
            if (tmonthString.length() == 1) {
                tmonthString = "0" + tmonthString;
            }
            if (tdateString.length() == 1) {
                tdateString = "0" + tdateString;
            }

            tmonth_name = month_date.format(calendar.getTime());
            tday_name = date_name.format(calendar.getTime());

            tcurrentdate = "  " + tmonth_name + " " + calendar.get(Calendar.YEAR) + "\n  " + tday_name;*/

            } else if (day.equals("lyear")) {
                calendar.add(Calendar.YEAR, -1);
                calendar.set(Calendar.MONTH, Calendar.APRIL);
                calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));

                dateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                monthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                yearString = String.valueOf(calendar.get(Calendar.YEAR));
                if (monthString.length() == 1) {
                    monthString = "0" + monthString;
                }
                if (dateString.length() == 1) {
                    dateString = "0" + dateString;
                }

                month_name = month_date.format(calendar.getTime());
                day_name = date_name.format(calendar.getTime());

                currentdate = "  " + month_name + " " + calendar.get(Calendar.YEAR) + "\n  " + day_name;

            /*calendar.add(Calendar.MONTH, 11);
            calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));*/
                calendar.add(Calendar.YEAR, 1);
                calendar.set(Calendar.MONTH, Calendar.MARCH);
                calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));

                tdateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                tmonthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                tyearString = String.valueOf(calendar.get(Calendar.YEAR));
                if (tmonthString.length() == 1) {
                    tmonthString = "0" + tmonthString;
                }
                if (tdateString.length() == 1) {
                    tdateString = "0" + tdateString;
                }

                tmonth_name = month_date.format(calendar.getTime());
                tday_name = date_name.format(calendar.getTime());

                tcurrentdate = "  " + tmonth_name + " " + calendar.get(Calendar.YEAR) + "\n  " + tday_name;

            } else if (day.equals("cqua")) {

                tdateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                tmonthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                tyearString = String.valueOf(calendar.get(Calendar.YEAR));
                if (tmonthString.length() == 1) {
                    tmonthString = "0" + tmonthString;
                }
                if (tdateString.length() == 1) {
                    tdateString = "0" + tdateString;
                }

                tmonth_name = month_date.format(calendar.getTime());
                tday_name = date_name.format(calendar.getTime());

                tcurrentdate = "  " + tmonth_name + " " + calendar.get(Calendar.YEAR) + "\n  " + tday_name;


                int month = calendar.get(Calendar.MONTH);
                calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));

            /*double thisMonth = (double) month;
            String quarter = thisMonth / 3 <= 1 ? "Quarter 1" : thisMonth / 3 <= 2 ? "Quarter 2" : thisMonth / 3 <= 3 ? "Quarter 3" : "Quarter 4";
            //System.out.println("======== = = q- " + quarter);
            if (month % 3 == 2) {
                month = month - 1;
            } else if (month % 3 == 0) {
                month = month - 2;
            }
            calendar.set(Calendar.MONTH, month - 1);*/

                calendar.set(Calendar.MONTH, QuarterInRange(month));

                dateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                monthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                yearString = String.valueOf(calendar.get(Calendar.YEAR));
                if (monthString.length() == 1) {
                    monthString = "0" + monthString;
                }
                if (dateString.length() == 1) {
                    dateString = "0" + dateString;
                }

                month_name = month_date.format(calendar.getTime());
                day_name = date_name.format(calendar.getTime());

                currentdate = "  " + month_name + " " + calendar.get(Calendar.YEAR) + "\n  " + day_name;


            /*calendar.add(Calendar.MONTH, 2);
            calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
            tdateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
            tmonthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
            tyearString = String.valueOf(calendar.get(Calendar.YEAR));
            if (tmonthString.length() == 1) {
                tmonthString = "0" + tmonthString;
            }
            if (tdateString.length() == 1) {
                tdateString = "0" + tdateString;
            }

            tmonth_name = month_date.format(calendar.getTime());
            tday_name = date_name.format(calendar.getTime());

            tcurrentdate = "  " + tmonth_name + " " + calendar.get(Calendar.YEAR) + "\n  " + tday_name;*/

            } else if (day.equals("lqua")) {
                calendar.add(Calendar.MONTH, -3);
                int month = calendar.get(Calendar.MONTH);
                calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));

           /* double thisMonth = (double) month;
            String quarter = thisMonth / 3 <= 1 ? "Quarter 1" : thisMonth / 3 <= 2 ? "Quarter 2" : thisMonth / 3 <= 3 ? "Quarter 3" : "Quarter 4";
            //System.out.println("======== = = q- " + quarter);
            if (month % 3 == 2) {
                month = month - 1;
            } else if (month % 3 == 0) {
                month = month - 2;
            }
            calendar.set(Calendar.MONTH, month - 1);*/

                calendar.set(Calendar.MONTH, QuarterInRange(month));

                dateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                monthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                yearString = String.valueOf(calendar.get(Calendar.YEAR));
                if (monthString.length() == 1) {
                    monthString = "0" + monthString;
                }
                if (dateString.length() == 1) {
                    dateString = "0" + dateString;
                }

                month_name = month_date.format(calendar.getTime());
                day_name = date_name.format(calendar.getTime());

                currentdate = "  " + month_name + " " + calendar.get(Calendar.YEAR) + "\n  " + day_name;


                calendar.add(Calendar.MONTH, 2);
                calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                tdateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                tmonthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                tyearString = String.valueOf(calendar.get(Calendar.YEAR));
                if (tmonthString.length() == 1) {
                    tmonthString = "0" + tmonthString;
                }
                if (tdateString.length() == 1) {
                    tdateString = "0" + tdateString;
                }

                tmonth_name = month_date.format(calendar.getTime());
                tday_name = date_name.format(calendar.getTime());

                tcurrentdate = "  " + tmonth_name + " " + calendar.get(Calendar.YEAR) + "\n  " + tday_name;
            }

            txtfromdt.setText(dateString);
            txttodt.setText(tdateString);

            final SpannableString ss = new SpannableString(currentdate);
            ss.setSpan(new ForegroundColorSpan(Color.BLACK), 0, currentdate.indexOf(day_name), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            final SpannableString tss = new SpannableString(tcurrentdate);
            tss.setSpan(new ForegroundColorSpan(Color.BLACK), 0, tcurrentdate.indexOf(tday_name), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            txtfrom.setText(ss);
            txtto.setText(tss);
            startdate = yearString + monthString + dateString;
            enddate = tyearString + tmonthString + tdateString;
            //System.out.println("====== = =**" + startdate + " = " + enddate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (startdate + ";" + enddate);
    }

    private static int QuarterInRange(int number) {
        //0-2,3-5,6-8,9-11
        return number <= 2 ? 0 : number <= 5 ? 3 : number <= 8 ? 6 : 9;
    }

    public static double ConvertToDouble(String number) {
        try {
            return Double.parseDouble(number);
        } catch (NumberFormatException e) {
            return 0.00;
        }
    }

    public static String ConvertToIndianRupeeFormat(double item, boolean desimalformat, boolean replacerupee, boolean replacecomma) {
        String value = "";
        try {
            if (desimalformat)
                value = indianFormat.format(new BigDecimal(decimalFormat.format(item)));
            else if (replacerupee)
                value = indianFormat.format(new BigDecimal(item)).replaceAll("₹", "").trim();
            else
                value = indianFormat.format(new BigDecimal(item));

            value = value.replace("-", "- ");

            if (replacecomma)
                value = value.replace(",", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public static String FormateDate(SimpleDateFormat format, Date date) {
        try {
            return format.format(date);
        } catch (Exception e) {
            return "";
        }
    }

    public static Date FormateDate(SimpleDateFormat format, String date, String subject, DatabaseHelper databaseHelper) {
        try {
            return format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            databaseHelper.addParseERROR(e, subject);
        } catch (Exception e) {
            e.printStackTrace();
            databaseHelper.addExceptionERROR(e, subject);
        }
        return null;
    }

    public static String VolleyErrorMessage(VolleyError error) {
        String message = "";
        try {
            if (error instanceof NetworkError) {
                message = "Cannot connect to Internet...Please check your connection!";
            } else if (error instanceof ServerError) {
                message = "The server could not be found. Please try again after some time!!";
            } else if (error instanceof AuthFailureError) {
                message = "Cannot connect to Internet...Please check your connection!";
            } else if (error instanceof ParseError) {
                message = "Parsing error! Please try again after some time!!";
            } else if (error instanceof NoConnectionError) {
                message = "Cannot connect to Internet...Please check your connection!";
            } else if (error instanceof TimeoutError) {
                message = "Connection TimeOut! Please check your internet connection.";
            } else
                message = "";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }

    public static void openPdfExcel(File file, String type, Activity activity) {
        try {
            Uri uri = FileProvider.getUriForFile(activity, activity.getResources().getString(R.string.authority), file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            if (type.equals("pdf"))
                intent.setDataAndType(uri, "application/pdf");
            else
                intent.setDataAndType(uri, "application/vnd.ms-excel");

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            activity.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            if (type.equals("pdf"))
                Toast.makeText(activity, "PDF Viewer not found", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(activity, "Excel Viewer not found", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean ResponseErrorCheck(Activity activity, String response, boolean istoast) {
        boolean iserror = false;
        if (response.contains(activity.getResources().getString(R.string.noresponse))) {
            if (istoast)
                Toast.makeText(activity, activity.getResources().getString(R.string.notconnectedserver), Toast.LENGTH_SHORT).show();
            iserror = true;
        } else if (response.contains(activity.getResources().getString(R.string.multiuseword))) {
            if (istoast)
                Toast.makeText(activity, activity.getResources().getString(R.string.multiuse), Toast.LENGTH_SHORT).show();
            iserror = true;
        } else if (response.contains(activity.getResources().getString(R.string.qstringerr))) {
            if (istoast)
                Toast.makeText(activity, activity.getResources().getString(R.string.trylater), Toast.LENGTH_SHORT).show();
            iserror = true;
        } else if (response.contains(activity.getResources().getString(R.string.expireactivation))) {
            if (istoast)
                Toast.makeText(activity, activity.getResources().getString(R.string.expiremsg), Toast.LENGTH_SHORT).show();
            iserror = true;
        }
        return iserror;
    }

    public static boolean CheckValidation(Activity activity, SimpleDateFormat inFormatdate, String startdate, String enddate, String subject, DatabaseHelper databaseHelper) {
        boolean iserr = false;
        try {
            Date date_end = Constant.FormateDate(inFormatdate, enddate, subject, databaseHelper);
            Date date_start = Constant.FormateDate(inFormatdate, startdate, subject, databaseHelper);

            if (date_start.after(date_end)) {
                Toast.makeText(activity, activity.getResources().getString(R.string.fromtodatevalidation), Toast.LENGTH_SHORT).show();
                iserr = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
            iserr = true;
            databaseHelper.addExceptionERROR(e, subject);
        }
        return iserr;
    }

    public static void SetViewVisiblility(ImageView imgdown, LinearLayout lyttop) {
        imgdown.setVisibility(View.VISIBLE);
        if (lyttop.getVisibility() == View.GONE) {
            imgdown.setImageResource(R.drawable.ic_bup);
            lyttop.setVisibility(View.VISIBLE);
        } else {
            imgdown.setImageResource(R.drawable.ic_bdown);
            lyttop.setVisibility(View.GONE);
        }
    }

    public static void BtnEnableDisable(boolean isenable, Button btnsync, Button btnshow) {
        btnsync.setEnabled(isenable);
        btnshow.setEnabled(isenable);
    }

    public static boolean CheckReadWritePermissionGranted(Activity activity) {
        boolean ispermissiongranted = false;
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, ReqReadPermission);
        } /*else if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, ReqWritePermission);
        }*/ else {
            ispermissiongranted = true;
        }
        return ispermissiongranted;
    }

    public static File FilePathPdfExcel(boolean isdelete, String type, Activity activity, String filename) {

        File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + activity.getResources().getString(R.string.app_name) + "/");

        if (!folder.exists()) {
            boolean success = folder.mkdir();
        }

        String path = folder.getAbsolutePath();
        path = path + "/" + filename + type;
        File filepath = new File(path);
        if (isdelete && filepath.exists()) {
            filepath.delete();
        }
        return filepath;
    }

    public static String GetMonthDateString(Calendar calendar) {
        String dateString, monthString;
        dateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        monthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
        if (monthString.length() == 1) {
            monthString = "0" + monthString;
        }
        if (dateString.length() == 1) {
            dateString = "0" + dateString;
        }

        return (dateString + ";" + monthString);
    }

    public static String GetMonthDateOfDatePicker(int selectedMonth,int selectedDay) {
        String monthString = String.valueOf(selectedMonth + 1);
        String dateString = String.valueOf(selectedDay);
        if (monthString.length() == 1) {
            monthString = "0" + monthString;
        }
        if (dateString.length() == 1) {
            dateString = "0" + dateString;
        }

        return (dateString + ";" + monthString);
    }
}
