package com.accountspro.helper;

import android.content.Context;

import com.accountspro.R;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.IOException;

public class HeaderFooterPageEvent extends PdfPageEventHelper {

    String compname, address, datetext, from, txtledgername, txtaddress, txtmobno, txtemail, txtopeningbal;
    BaseFont urName = null;
    Font normalFont, mainFont;
    private PdfTemplate t;
    private Image total;
    Context context;
    Rectangle rect;
    PdfPTable table1;
    protected float tableHeight;

    public HeaderFooterPageEvent(String compname, String address, String datetext, Context context, String from) {
        this.compname = compname;
        this.address = address;
        this.datetext = datetext;
        this.context = context;
        this.from = from;

        try {
            urName = BaseFont.createFont("assets/montserratregular.otf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        } catch (
                DocumentException e) {
            e.printStackTrace();
        } catch (
                IOException e) {
            e.printStackTrace();
        }
        normalFont = new Font(urName, 10, Font.NORMAL, BaseColor.BLACK);
        mainFont = new Font(urName, 10, Font.BOLD, BaseColor.BLACK);

        addHeader();
    }

    public HeaderFooterPageEvent(String compname, String address, String datetext, String txtledgername, String txtaddress, String txtmobno, String txtemail, String txtopeningbal, Context context, String from) {
        this.compname = compname;
        this.address = address;
        this.datetext = datetext;
        this.context = context;
        this.from = from;
        this.txtledgername = txtledgername;
        this.txtaddress = txtaddress;
        this.txtmobno = txtmobno;
        this.txtemail = txtemail;
        this.txtopeningbal = txtopeningbal;

        try {
            urName = BaseFont.createFont("assets/montserratregular.otf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        } catch (
                DocumentException e) {
            e.printStackTrace();
        } catch (
                IOException e) {
            e.printStackTrace();
        }
        normalFont = new Font(urName, 10, Font.NORMAL, BaseColor.BLACK);
        mainFont = new Font(urName, 10, Font.BOLD, BaseColor.BLACK);

        addHeader();
    }

    public float getTableHeight() {
        return tableHeight;
    }

    public void onOpenDocument(PdfWriter writer, Document document) {
        rect = document.getPageSize();
        t = writer.getDirectContent().createTemplate(30, 16);
        try {
            total = Image.getInstance(t);
            total.setRole(PdfName.ARTIFACT);

        } catch (DocumentException de) {
            throw new ExceptionConverter(de);
        }
    }

    private void addHeader() {

        try {

            int noofcolumn = 7;

            if (from.equalsIgnoreCase("ledgerreport")) {
                noofcolumn = 4;
                table1 = new PdfPTable(new float[]{(float) 0.5, (float) 1.8, (float) 0.5, (float) 1.2});
            }else if (from.equalsIgnoreCase("stockstatus")) {
                noofcolumn = 4;
                table1 = new PdfPTable(new float[]{(float) 1.5, (float) 0.9, (float) 0.8, (float) 0.8});
            }else
                table1 = new PdfPTable(noofcolumn);

            table1.setWidthPercentage(100);
            table1.setSpacingBefore(0f);
            table1.setSpacingAfter(0f);
            table1.setTotalWidth(555);
            table1.setLockedWidth(true);


            PdfPCell company = new PdfPCell(new Phrase(compname, new Font(urName, 20, Font.BOLD, BaseColor.BLACK)));
            company.setColspan(noofcolumn);
            company.setHorizontalAlignment(Element.ALIGN_CENTER);
            company.setPaddingTop(20.0f);
            company.setBorder(Rectangle.NO_BORDER);

            PdfPCell cell = new PdfPCell(new Phrase(address, normalFont));
            cell.setColspan(noofcolumn);
            cell.setBorder(Rectangle.NO_BORDER);
            cell.setPaddingBottom(20.0f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);

            table1.addCell(company);
            table1.addCell(cell);

            if (!from.equals("voucher")) {
                PdfPCell datecell = new PdfPCell(new Phrase(datetext, normalFont));
                datecell.setColspan(noofcolumn);
                datecell.setBorder(Rectangle.TOP);
                datecell.setPaddingBottom(20.0f);
                datecell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(datecell);
            }

            if (from.equals("receivable")) {
                PdfPCell[] cellsheader = new PdfPCell[noofcolumn];
                cellsheader[0] = new PdfPCell(new Phrase(context.getResources().getString(R.string.date), mainFont));
                cellsheader[1] = new PdfPCell(new Phrase(context.getResources().getString(R.string.vch_no), mainFont));
                cellsheader[2] = new PdfPCell(new Phrase(context.getResources().getString(R.string.bill_amt), mainFont));
                cellsheader[3] = new PdfPCell(new Phrase(context.getResources().getString(R.string.due_amt), mainFont));
                cellsheader[4] = new PdfPCell(new Phrase(context.getResources().getString(R.string.dr_cr), mainFont));
                cellsheader[5] = new PdfPCell(new Phrase(context.getResources().getString(R.string.due_date), mainFont));
                cellsheader[6] = new PdfPCell(new Phrase(context.getResources().getString(R.string.due_days), mainFont));

                for (int j = 0; j < cellsheader.length; j++) {
                    cellsheader[j].setPadding(10.0f);
                    cellsheader[j].setBorder(Rectangle.BOX);
                    cellsheader[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                    cellsheader[j].setBackgroundColor(new BaseColor(245, 200, 169));
                    table1.addCell(cellsheader[j]);
                }
            } else if (from.equals("ledgerreport")) {
                PdfPCell[] cellsheaderdata = new PdfPCell[noofcolumn];
                cellsheaderdata[0] = new PdfPCell(new Phrase(context.getResources().getString(R.string.ledger), mainFont));
                cellsheaderdata[0].setColspan(1);
                cellsheaderdata[0].setBorder(Rectangle.BOX);
                cellsheaderdata[0].setPadding(10.0f);
                cellsheaderdata[0].setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell(cellsheaderdata[0]);
                cellsheaderdata[1] = new PdfPCell(new Phrase(txtledgername, normalFont));
                cellsheaderdata[1].setColspan(3);
                cellsheaderdata[1].setBorder(Rectangle.BOX);
                cellsheaderdata[1].setPadding(10.0f);
                cellsheaderdata[1].setHorizontalAlignment(Element.ALIGN_MIDDLE);
                table1.addCell(cellsheaderdata[1]);

                if (txtaddress.length() != 0) {
                    //PdfPCell[] cellsheadernewdata = new PdfPCell[noofcolumn];
                    cellsheaderdata[0] = new PdfPCell(new Phrase(context.getResources().getString(R.string.address), mainFont));
                    cellsheaderdata[0].setColspan(1);
                    cellsheaderdata[0].setBorder(Rectangle.BOX);
                    cellsheaderdata[0].setPadding(10.0f);
                    cellsheaderdata[0].setHorizontalAlignment(Element.ALIGN_CENTER);
                    table1.addCell(cellsheaderdata[0]);
                    cellsheaderdata[1] = new PdfPCell(new Phrase(txtaddress, normalFont));
                    cellsheaderdata[1].setColspan(3);
                    cellsheaderdata[1].setBorder(Rectangle.BOX);
                    cellsheaderdata[1].setPadding(10.0f);
                    cellsheaderdata[1].setHorizontalAlignment(Element.ALIGN_MIDDLE);
                    table1.addCell(cellsheaderdata[1]);
                }
                if (txtmobno.length() != 0 || txtemail.length() != 0) {

                    //PdfPCell[] cellsheadernewdata = new PdfPCell[noofcolumn];
                    if (txtmobno.length() != 0 && txtemail.length() != 0) {
                        cellsheaderdata[0] = new PdfPCell(new Phrase(txtmobno, normalFont));
                        cellsheaderdata[0].setColspan(1);
                        cellsheaderdata[0].setBorder(Rectangle.BOX);
                        cellsheaderdata[0].setPadding(10.0f);
                        cellsheaderdata[0].setHorizontalAlignment(Element.ALIGN_CENTER);
                        table1.addCell(cellsheaderdata[0]);

                        cellsheaderdata[1] = new PdfPCell(new Phrase(txtemail, normalFont));
                        cellsheaderdata[1].setColspan(3);
                        cellsheaderdata[1].setBorder(Rectangle.BOX);
                        cellsheaderdata[1].setPadding(10.0f);
                        cellsheaderdata[1].setVerticalAlignment(Element.ALIGN_MIDDLE);
                        table1.addCell(cellsheaderdata[1]);
                    }else if(txtmobno.length() != 0){
                        //cellsheaderdata[0] = new PdfPCell(new Phrase(txtmobno, normalFont));
                        cellsheaderdata[0].setColspan(4);
                        cellsheaderdata[0].setBorder(Rectangle.BOX);
                        cellsheaderdata[0].setPadding(10.0f);
                        cellsheaderdata[0].setHorizontalAlignment(Element.ALIGN_CENTER);
                        table1.addCell(cellsheaderdata[0]);

                    }else if(txtemail.length() != 0){
                        //cellsheadernewdata[0] = new PdfPCell(new Phrase(txtemail, mainFont));
                        cellsheaderdata[0].setColspan(4);
                        cellsheaderdata[0].setBorder(Rectangle.BOX);
                        cellsheaderdata[0].setPadding(10.0f);
                        cellsheaderdata[0].setHorizontalAlignment(Element.ALIGN_CENTER);
                        table1.addCell(cellsheaderdata[0]);
                    }
                }

                //PdfPCell[] cellsheader = new PdfPCell[noofcolumn];
                cellsheaderdata[0] = new PdfPCell(new Phrase(context.getResources().getString(R.string.date), mainFont));
                cellsheaderdata[1] = new PdfPCell(new Phrase(context.getResources().getString(R.string.particulars), mainFont));
                cellsheaderdata[2] = new PdfPCell(new Phrase(context.getResources().getString(R.string.vch_no), mainFont));
                cellsheaderdata[3] = new PdfPCell(new Phrase(context.getResources().getString(R.string.amount), mainFont));

                for (int j = 0; j < cellsheaderdata.length; j++) {
                    cellsheaderdata[j].setPadding(10.0f);
                    cellsheaderdata[j].setBorder(Rectangle.BOX);
                    cellsheaderdata[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                    cellsheaderdata[j].setBackgroundColor(new BaseColor(190, 215, 245));
                    table1.addCell(cellsheaderdata[j]);
                }

            }else if (from.equalsIgnoreCase("stockstatus")) {
                PdfPCell[] cellsheader = new PdfPCell[noofcolumn];
                cellsheader[0] = new PdfPCell(new Phrase(context.getResources().getString(R.string.particulars), mainFont));
                cellsheader[1] = new PdfPCell(new Phrase(context.getResources().getString(R.string.qty), mainFont));
                cellsheader[2] = new PdfPCell(new Phrase(context.getResources().getString(R.string.rate), mainFont));
                cellsheader[3] = new PdfPCell(new Phrase(context.getResources().getString(R.string.value), mainFont));

                for (int j = 0; j < cellsheader.length; j++) {
                    cellsheader[j].setPadding(10.0f);
                    cellsheader[j].setBorder(Rectangle.BOX);
                    cellsheader[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                    cellsheader[j].setBackgroundColor(new BaseColor(146, 208, 80));
                    table1.addCell(cellsheader[j]);
                }
            }

            tableHeight = table1.getTotalHeight();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onEndPage(PdfWriter writer, Document document) {
        table1.writeSelectedRows(0, -1,
                document.left(),
                document.top() + ((document.topMargin() + tableHeight) / 2),
                writer.getDirectContent());
    }

}
