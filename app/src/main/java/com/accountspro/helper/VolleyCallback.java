package com.accountspro.helper;

public interface VolleyCallback{
    void onSuccess(boolean result);

    void onSuccessWithMsg(boolean result,String message);

}
