package com.accountspro.helper;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.view.View;
import android.widget.Toast;

import com.accountspro.R;
import com.accountspro.activity.LoginActivity;
import com.accountspro.model.Business;
import com.accountspro.model.Company;
import com.accountspro.model.LedgerReport;
import com.accountspro.model.Master;
import com.accountspro.model.Receivble;
import com.accountspro.model.SalesReport;
import com.accountspro.model.StockStatus;
import com.accountspro.model.VoucherInfo;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Font;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

public class UserSessionManager {

    SharedPreferences pref;
    public SharedPreferences.Editor editor;
    static Context _context;
    int PRIVATE_MODE = 0;


    //static NumberFormat indianFormat = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
    private static final String PREFER_NAME = "AccountsProPref";
    public static String KEY_IMEI = "keyimei";
    public static final String IS_USER_LOGIN = "IsUserLoggedIn";
    public static final String KEY_LASTIMG = "lastimg";
    public static final String KEY_Mobile = "mobileno";
    public static final String KEY_REGI = "registered";
    public static final String KEY_DEMO = "demo";
    public static final String KEY_VERIFIED = "verified";
    public static final String KEY_DEVICEINFO = "deviceinfo";
    public static final String KEY_OTPSEND = "otpsend";
    public static final String KEY_OTP = "otp";
    public static final String KEY_PINSET = "pinset";
    public static final String KEY_PIN = "pinlogin";
    public static final String KEY_LoginType = "logintype";
    public static final String OWNER = "owner";
    public static final String VEMDOR = "vendor";
    public static final String KEY_ORG_LICENCE = "licencekey";
    public static final String KEY_ORG_NAME = "organization_name";
    public static final String KEY_ORG_REGKEY = "regkey";
    public static final String KEY_ORGSET = "orgset";
    public static final String KEY_COM_CODE = "compcode";
    public static final String KEY_COM_NAME = "compname";
    public static final String KEY_COM_ACYEAR = "acyear";
    public static final String KEY_COM_YEARCODE = "yearcode";
    public static final String KEY_COM_ADDRESS = "address";
    public static final String KEY_COM_MODULECODE = "modulecode";
    public static final String KEY_COM_BILLWISEREQD = "billwisereqd";
    public static final String KEY_COM_REGIONREQD = "regionreqd";
    public static final String KEY_COM_PAYROLLREQD = "payrollreqd";
    public static final String KEY_COM_BATCHREQD = "batchreqd";
    public static final String KEY_COM_LEGACYSCAN = "legacyscan";
    public static final String KEY_COM_SALESSTATEMENTREQD = "salesstatementreqd";
    public static final String KEY_COMSET = "comset";

    public static final String KEY_BSTARTDATE = "bstartdate";
    public static final String KEY_BENDDATE = "benddate";
    public static final String KEY_CheckBusinessSort = "checkbusort";
    public static final String KEY_LastLogin = "lastlogin";
    public static final String KEY_DEMOREQCOUNT = "demoreqcount";
    public static final String KEY_DEMOREQTOTAL = "demoreqtotal";
    public static final String KEY_DEVICECODE = "devicecode";

    public static final String KEY_USERNAME = "username";
    public static final String KEY_STARTDATE = "startdate";
    public static final String KEY_ENDDATE = "enddate";
    public static final String KEY_DEVICEIMEI = "deviceimei";
    public static final String KEY_YEARLYFEE = "yearlyfee";


    static BaseFont urName = null;


    public UserSessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();

        try {
            urName = BaseFont.createFont("assets/montserratregular.otf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void setBooleanData(String key, boolean val) {
        editor.putBoolean(key, val);
        editor.commit();
    }

    public void setIntData(String key, int val) {
        editor.putInt(key, val);
        editor.commit();
    }

    public void setData(String key, String val) {
        editor.putString(key, val);
        editor.commit();
    }

    public void setCompanyData(Company company, String orgreg) {
        editor.putString(orgreg + KEY_COM_CODE, company.getCompcode());
        editor.putString(orgreg + KEY_COM_NAME, company.getCompname());
        editor.putString(orgreg + KEY_COM_ACYEAR, company.getAcyear());
        editor.putString(orgreg + KEY_COM_YEARCODE, company.getYearcode());
        editor.putString(orgreg + KEY_COM_ADDRESS, company.getAddress());
        editor.putString(orgreg + KEY_COM_MODULECODE, company.getModulecode());
        editor.putString(orgreg + KEY_COM_BILLWISEREQD, company.getBillwisereqd());
        editor.putString(orgreg + KEY_COM_REGIONREQD, company.getRegionreqd());
        editor.putString(orgreg + KEY_COM_PAYROLLREQD, company.getPayrollreqd());
        editor.putString(orgreg + KEY_COM_BATCHREQD, company.getBatchreqd());
        editor.putString(orgreg + KEY_COM_LEGACYSCAN, company.getLegacyscan());
        editor.putString(orgreg + KEY_COM_SALESSTATEMENTREQD, company.getSalesstatementreqd());

        editor.commit();
    }

    public String getData(String id) {
        return pref.getString(id, "");
    }

    public String getData(String id, String defaultval) {
        return pref.getString(id, defaultval);
    }

    public boolean getBooleanData(String id) {
        return pref.getBoolean(id, false);
    }

    public int getIntData(String id) {
        return pref.getInt(id, 0);
    }

    public void createUserLoginSession() {
        editor.putBoolean(IS_USER_LOGIN, true);
        editor.commit();
    }


    public static void setLogin(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(IS_USER_LOGIN, true);
        editor.apply();
    }

    public static boolean isLogin(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(IS_USER_LOGIN, false);
    }

    public void ClearUserSession() {
        editor.clear();
        editor.commit();
    }

    public void logoutUser() {
        setBooleanData(IS_USER_LOGIN, false);
        Intent i = new Intent(_context, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        _context.startActivity(i);
    }

    public boolean isUserLoggedIn() {
        return pref.getBoolean(IS_USER_LOGIN, false);
    }

    public String getBusinessSort() {
        return pref.getString(KEY_CheckBusinessSort, "None_");
    }


    public static void CreateBusinessViewPdf(String filename, String address, String compname, String datetext, boolean salesvisibility, String s1, String s2, String s3, boolean purchasevisibility, String pu1, String pu2, String pu3, boolean receiptvisibility, boolean receiptmainvisibility, String rec1, String rec2, String rec3, ArrayList<Business> receiptlist0, ArrayList<Business> receiptlist1, boolean paymentvisibility, boolean paymentmainvisibility, String pay1, String pay2, String pay3, ArrayList<Business> paylist0, ArrayList<Business> paylist1, boolean cashvisibility, boolean cashmainvisibility, String cash1, String cash2, ArrayList<Business> cashlist, boolean bankvisibility, boolean bankmainvisibility, String bank1, String bank2, ArrayList<Business> banklist, boolean stockvisibility, String stock1, String stock2) {

        String file = Environment.getExternalStorageDirectory().toString() + "/" + _context.getResources().getString(R.string.app_name) + "/" + filename + "_" + System.currentTimeMillis() + ".pdf";
        HeaderFooterPageEvent event = new HeaderFooterPageEvent(compname, address, datetext, _context, "business");
        try {
            Document document = new Document(PageSize.A4, 20, 20, 20 + event.getTableHeight(), 25);
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));

            writer.setPageEvent(event);

            document.open();

            final PdfPTable table1 = new PdfPTable(1);
            table1.setWidthPercentage(100);
            table1.setSpacingBefore(0f);
            table1.setSpacingAfter(0f);

            Font mainFont = new Font(urName, 10, Font.BOLD, BaseColor.BLACK);
            Font titleFont = new Font(urName, 10, Font.BOLD, new BaseColor(121, 102, 254));

            /*PdfPCell company = new PdfPCell(new Phrase(compname, new Font(urName, 20, Font.BOLD, BaseColor.BLACK)));
            company.setColspan(6);
            company.setHorizontalAlignment(Element.ALIGN_CENTER);
            //company.setPaddingTop(20.0f);
            company.setBorder(Rectangle.NO_BORDER);

            PdfPCell cell = new PdfPCell(new Phrase(address, mainFont));
            cell.setColspan(6);
            cell.setBorder(Rectangle.BOTTOM);
            cell.setPaddingBottom(20.0f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);

            PdfPCell datecell = new PdfPCell(new Phrase(datetext, mainFont));
            datecell.setColspan(6);
            datecell.setBorder(Rectangle.NO_BORDER);
            datecell.setPaddingBottom(20.0f);
            datecell.setHorizontalAlignment(Element.ALIGN_CENTER);

            table1.addCell(company);
            table1.addCell(cell);
            table1.addCell(datecell);
            document.add(table1);*/

            if (salesvisibility) {
                PdfPTable table2 = new PdfPTable(1);
                table2.setWidthPercentage(100);
                table2.setSpacingBefore(20f);

                PdfPCell invoice = new PdfPCell(new Phrase(_context.getResources().getString(R.string.sales_figure), mainFont));
                invoice.setColspan(6);
                invoice.setHorizontalAlignment(Element.ALIGN_CENTER);
                invoice.setBackgroundColor(new BaseColor(146, 208, 80));
                invoice.setBorder(Rectangle.BOX);
                invoice.setPadding(8f);
                table2.addCell(invoice);
                document.add(table2);

                PdfPTable table = new PdfPTable(new float[]{(float) 0.5, 1, 1});

                table.setWidthPercentage(100);
                table.addCell(new Phrase(s1, mainFont));
                table.addCell(new Phrase(s2, mainFont));
                table.addCell(new Phrase(s3, mainFont));

                PdfPCell[] cells = table.getRow(0).getCells();
                for (int j = 0; j < cells.length; j++) {
                    cells[j].setPadding(10.0f);
                    cells[j].setBorder(Rectangle.BOX);
                    cells[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                }

                document.add(table);

            }
            if (purchasevisibility) {
                PdfPTable table2 = new PdfPTable(1);
                table2.setWidthPercentage(100);
                table2.setSpacingBefore(20f);


                PdfPCell invoice = new PdfPCell(new Phrase(_context.getResources().getString(R.string.purchase_figure), mainFont));
                invoice.setColspan(6);
                invoice.setHorizontalAlignment(Element.ALIGN_CENTER);
                invoice.setBackgroundColor(new BaseColor(250, 192, 144));
                invoice.setBorder(Rectangle.BOX);
                invoice.setPadding(8f);
                table2.addCell(invoice);
                document.add(table2);

                PdfPTable table = new PdfPTable(new float[]{(float) 0.5, 1, 1});

                table.setWidthPercentage(100);
                table.addCell(new Phrase(pu1, mainFont));
                table.addCell(new Phrase(pu2, mainFont));
                table.addCell(new Phrase(pu3, mainFont));

                PdfPCell[] cells = table.getRow(0).getCells();
                for (int j = 0; j < cells.length; j++) {
                    cells[j].setPadding(10.0f);
                    cells[j].setBorder(Rectangle.BOX);
                    cells[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                }

                document.add(table);
            }
            if (receiptvisibility) {
                PdfPTable table2 = new PdfPTable(1);
                table2.setWidthPercentage(100);
                table2.setSpacingBefore(20f);


                PdfPCell invoice = new PdfPCell(new Phrase(_context.getResources().getString(R.string.receipt_figure), mainFont));
                invoice.setColspan(6);
                invoice.setHorizontalAlignment(Element.ALIGN_CENTER);
                invoice.setBackgroundColor(new BaseColor(155, 187, 89));
                invoice.setBorder(Rectangle.BOX);
                invoice.setPadding(8f);
                table2.addCell(invoice);
                document.add(table2);

                PdfPTable table = new PdfPTable(new float[]{(float) 0.5, 1, 1});

                table.setWidthPercentage(100);
                table.addCell(new Phrase(rec1, mainFont));
                table.addCell(new Phrase(rec2, mainFont));
                table.addCell(new Phrase(rec3, mainFont));

                PdfPCell[] cells = table.getRow(0).getCells();
                for (int j = 0; j < cells.length; j++) {
                    cells[j].setPadding(10.0f);
                    cells[j].setBorder(Rectangle.BOX);
                    cells[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                    //cells[j].setVerticalAlignment(Element.ALIGN_CENTER);
                }
                document.add(table);

                if (receiptmainvisibility) {
                    if (receiptlist0.size() != 0 || receiptlist1.size() != 0) {
                        PdfPTable rectable = new PdfPTable(new float[]{(float) 1, 1});
                        rectable.setWidthPercentage(100);
                        rectable.addCell(new Phrase(_context.getResources().getString(R.string.customer_party), titleFont));
                        rectable.addCell(new Phrase(_context.getResources().getString(R.string.cash_bank), titleFont));

                        PdfPCell[] rcells = rectable.getRow(0).getCells();
                        for (int j = 0; j < rcells.length; j++) {
                            rcells[j].setPadding(10.0f);
                            rcells[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                            rcells[j].setBorder(Rectangle.BOX);
                            //rcells[j].setVerticalAlignment(Element.ALIGN_CENTER);
                        }
                        document.add(rectable);
                    }


                    PdfPTable reclist = new PdfPTable(new float[]{(float) 1, 1, 1, 1});
                    reclist.setWidthPercentage(100);


                    for (Business model : receiptlist0) {
                   /* String data = model.getDtl2().trim();
                    if (data != null && !data.equals("") && data.matches("[0-9.]+")) {
                        data = indianFormat.format(new BigDecimal(Double.parseDouble(model.getDtl2())));
                    }*/

                        reclist.addCell(new Phrase(model.getDtl2(), mainFont));
                        reclist.addCell(new Phrase(model.getNamedisplay(), mainFont));
                        reclist.addCell(new Phrase("", mainFont));
                        reclist.addCell(new Phrase("", mainFont));

                    }

                    for (int i = 0; i < receiptlist0.size(); i++) {
                        PdfPCell[] recells = reclist.getRow(i).getCells();
                        for (int j = 0; j < recells.length; j++) {
                            recells[j].setPadding(10.0f);
                            recells[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                            //recells[j].setVerticalAlignment(Element.ALIGN_CENTER);
                            //recells[j].setBorder(Rectangle.BOX);

                            if (j < 2)
                                recells[j].setBorder(Rectangle.BOX);
                            else
                                recells[j].setBorder(Rectangle.NO_BORDER);
                        }
                    }


                    for (int i = 0; i < receiptlist1.size(); i++) {
                        Business model = receiptlist1.get(i);

                   /* String data = model.getDtl2().trim();
                    if (data != null && !data.equals("") && data.matches("[0-9.]+")) {
                        data = indianFormat.format(new BigDecimal(Double.parseDouble(model.getDtl2())));
                    }*/

                        if (reclist.getRow(i) != null) {
                            PdfPCell[] recells = reclist.getRow(i).getCells();
                            recells[2].setPhrase(new Phrase(model.getDtl2(), mainFont));
                            recells[3].setPhrase(new Phrase(model.getNamedisplay(), mainFont));

                            recells[2].setPadding(10.0f);
                            recells[2].setHorizontalAlignment(Element.ALIGN_CENTER);
                            recells[2].setBorder(Rectangle.BOX);
                            recells[3].setPadding(10.0f);
                            recells[3].setHorizontalAlignment(Element.ALIGN_CENTER);
                            recells[3].setBorder(Rectangle.BOX);
                        } else {
                            reclist.addCell(new Phrase("", mainFont));
                            reclist.addCell(new Phrase("", mainFont));
                            reclist.addCell(new Phrase(model.getDtl2(), mainFont));
                            reclist.addCell(new Phrase(model.getNamedisplay(), mainFont));

                            PdfPCell[] recells = reclist.getRow(i).getCells();
                            for (int j = 0; j < recells.length; j++) {
                                recells[j].setPadding(10.0f);
                                recells[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                                if (j < 2)
                                    recells[j].setBorder(Rectangle.NO_BORDER);
                                else
                                    recells[j].setBorder(Rectangle.BOX);
                            }
                        }

                    }


                    /*for (int i = 0; i < receiptlist1.size(); i++) {
                        PdfPCell[] recells = reclist.getRow(i).getCells();
                        for (int j = 0; j < recells.length; j++) {
                            recells[j].setPadding(10.0f);
                            recells[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                            //recells[j].setVerticalAlignment(Element.ALIGN_CENTER);
                            recells[j].setBorder(Rectangle.BOX);
                        }
                    }*/

                    document.add(reclist);
                }

            }

            if (paymentvisibility) {
                PdfPTable table2 = new PdfPTable(1);
                table2.setWidthPercentage(100);
                table2.setSpacingBefore(20f);


                PdfPCell invoice = new PdfPCell(new Phrase(_context.getResources().getString(R.string.payment_figure), mainFont));
                invoice.setColspan(6);
                invoice.setHorizontalAlignment(Element.ALIGN_CENTER);
                invoice.setBackgroundColor(new BaseColor(250, 192, 144));
                invoice.setBorder(Rectangle.BOX);
                invoice.setPadding(8f);
                table2.addCell(invoice);
                document.add(table2);

                PdfPTable table = new PdfPTable(new float[]{(float) 0.5, 1, 1});

                table.setWidthPercentage(100);
                table.addCell(new Phrase(pay1, mainFont));
                table.addCell(new Phrase(pay2, mainFont));
                table.addCell(new Phrase(pay3, mainFont));

                PdfPCell[] cells = table.getRow(0).getCells();
                for (int j = 0; j < cells.length; j++) {
                    cells[j].setPadding(10.0f);
                    cells[j].setBorder(Rectangle.BOX);
                    cells[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                    //cells[j].setVerticalAlignment(Element.ALIGN_CENTER);
                }
                document.add(table);

                if (paymentmainvisibility) {
                    if (paylist0.size() != 0 || paylist1.size() != 0) {
                        PdfPTable rectable = new PdfPTable(new float[]{(float) 1, 1});
                        rectable.setWidthPercentage(100);
                        rectable.addCell(new Phrase(_context.getResources().getString(R.string.payment_a_c), titleFont));
                        rectable.addCell(new Phrase(_context.getResources().getString(R.string.cash_bank), titleFont));

                        PdfPCell[] rcells = rectable.getRow(0).getCells();
                        for (int j = 0; j < rcells.length; j++) {
                            rcells[j].setPadding(10.0f);
                            rcells[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                            rcells[j].setBorder(Rectangle.BOX);
                            //rcells[j].setVerticalAlignment(Element.ALIGN_CENTER);
                        }
                        document.add(rectable);
                    }


                    PdfPTable reclist = new PdfPTable(new float[]{(float) 1, 1, 1, 1});
                    reclist.setWidthPercentage(100);


                    for (Business model : paylist0) {
                   /* String data = model.getDtl2().trim();
                    if (data != null && !data.equals("") && data.matches("[0-9.]+")) {
                        data = indianFormat.format(new BigDecimal(Double.parseDouble(model.getDtl2())));
                    }*/

                        reclist.addCell(new Phrase(model.getDtl2(), mainFont));
                        reclist.addCell(new Phrase(model.getNamedisplay(), mainFont));
                        reclist.addCell(new Phrase("", mainFont));
                        reclist.addCell(new Phrase("", mainFont));

                    }

                    for (int i = 0; i < paylist0.size(); i++) {
                        PdfPCell[] recells = reclist.getRow(i).getCells();
                        for (int j = 0; j < recells.length; j++) {
                            recells[j].setPadding(10.0f);
                            recells[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                            //recells[j].setVerticalAlignment(Element.ALIGN_CENTER);
                            if (j < 2)
                                recells[j].setBorder(Rectangle.BOX);
                            else
                                recells[j].setBorder(Rectangle.NO_BORDER);
                        }
                    }


                    for (int i = 0; i < paylist1.size(); i++) {
                        Business model = paylist1.get(i);

                    /*String data = model.getDtl2().trim();
                    if (data != null && !data.equals("") && data.matches("[0-9.]+")) {
                        data = indianFormat.format(new BigDecimal(Double.parseDouble(model.getDtl2())));
                    }*/

                        if (reclist.getRow(i) != null) {
                            PdfPCell[] recells = reclist.getRow(i).getCells();
                            recells[2].setPhrase(new Phrase(model.getDtl2(), mainFont));
                            recells[3].setPhrase(new Phrase(model.getNamedisplay(), mainFont));

                            recells[2].setPadding(10.0f);
                            recells[2].setHorizontalAlignment(Element.ALIGN_CENTER);
                            recells[2].setBorder(Rectangle.BOX);
                            recells[3].setPadding(10.0f);
                            recells[3].setHorizontalAlignment(Element.ALIGN_CENTER);
                            recells[3].setBorder(Rectangle.BOX);
                        } else {
                            reclist.addCell(new Phrase("", mainFont));
                            reclist.addCell(new Phrase("", mainFont));
                            reclist.addCell(new Phrase(model.getDtl2(), mainFont));
                            reclist.addCell(new Phrase(model.getNamedisplay(), mainFont));

                            PdfPCell[] recells = reclist.getRow(i).getCells();
                            for (int j = 0; j < recells.length; j++) {
                                recells[j].setPadding(10.0f);
                                recells[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                                if (j < 2)
                                    recells[j].setBorder(Rectangle.NO_BORDER);
                                else
                                    recells[j].setBorder(Rectangle.BOX);
                            }
                        }

                    }


                    /*for (int i = 0; i < paylist1.size(); i++) {
                        PdfPCell[] recells = reclist.getRow(i).getCells();
                        for (int j = 0; j < recells.length; j++) {
                            recells[j].setPadding(10.0f);
                            recells[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                            //recells[j].setVerticalAlignment(Element.ALIGN_CENTER);
                            recells[j].setBorder(Rectangle.BOX);
                        }
                    }*/

                    document.add(reclist);
                }
            }


            if (cashvisibility) {
                PdfPTable table2 = new PdfPTable(1);
                table2.setWidthPercentage(100);
                table2.setSpacingBefore(20f);


                PdfPCell invoice = new PdfPCell(new Phrase(_context.getResources().getString(R.string.cash_balance), mainFont));
                invoice.setColspan(6);
                invoice.setHorizontalAlignment(Element.ALIGN_CENTER);
                invoice.setBackgroundColor(new BaseColor(197, 217, 241));
                invoice.setBorder(Rectangle.BOX);
                invoice.setPadding(8f);
                table2.addCell(invoice);
                document.add(table2);

                PdfPTable table = new PdfPTable(new float[]{(float) 1, 1});

                table.setWidthPercentage(100);
                table.addCell(new Phrase(cash1, mainFont));
                table.addCell(new Phrase(cash2, mainFont));

                PdfPCell[] cells = table.getRow(0).getCells();
                for (int j = 0; j < cells.length; j++) {
                    cells[j].setPadding(10.0f);
                    cells[j].setBorder(Rectangle.BOX);
                    cells[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                    // cells[j].setVerticalAlignment(Element.ALIGN_CENTER);
                }
                document.add(table);

                if (cashmainvisibility) {
                    if (cashlist.size() != 0) {
                        PdfPTable rectable = new PdfPTable(new float[]{(float) 1, 1});
                        rectable.setWidthPercentage(100);
                        rectable.addCell(new Phrase(_context.getResources().getString(R.string.ledger_name), titleFont));
                        rectable.addCell(new Phrase(_context.getResources().getString(R.string.balance), titleFont));

                        PdfPCell[] rcells = rectable.getRow(0).getCells();
                        for (int j = 0; j < rcells.length; j++) {
                            rcells[j].setPadding(10.0f);
                            rcells[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                            rcells[j].setBorder(Rectangle.BOX);
                            //rcells[j].setVerticalAlignment(Element.ALIGN_CENTER);
                        }
                        document.add(rectable);
                    }

                    PdfPTable reclist = new PdfPTable(new float[]{(float) 1, 1});
                    reclist.setWidthPercentage(100);


                    for (Business model : cashlist) {
                    /*String data = model.getDtl2().trim();
                    if (data != null && !data.equals("") && data.matches("[0-9.]+")) {
                        data = indianFormat.format(new BigDecimal(Double.parseDouble(model.getDtl2())));
                    }*/
                        //System.out.println("============ ex - " + model.getNamedisplay());
                        reclist.addCell(new Phrase(model.getNamedisplay(), mainFont));
                        reclist.addCell(new Phrase(model.getDtl2(), mainFont));

                    }

                    for (int i = 0; i < cashlist.size(); i++) {
                        PdfPCell[] recells = reclist.getRow(i).getCells();
                        for (int j = 0; j < recells.length; j++) {
                            recells[j].setPadding(10.0f);
                            recells[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                            //recells[j].setVerticalAlignment(Element.ALIGN_CENTER);
                            recells[j].setBorder(Rectangle.BOX);
                        }
                    }

                    document.add(reclist);
                }

            }


            if (bankvisibility) {
                PdfPTable table2 = new PdfPTable(1);
                table2.setWidthPercentage(100);
                table2.setSpacingBefore(20f);


                PdfPCell invoice = new PdfPCell(new Phrase(_context.getResources().getString(R.string.bank_balance), mainFont));
                invoice.setColspan(6);
                invoice.setHorizontalAlignment(Element.ALIGN_CENTER);
                invoice.setBackgroundColor(new BaseColor(83, 142, 213));
                invoice.setBorder(Rectangle.BOX);
                invoice.setPadding(8f);
                table2.addCell(invoice);
                document.add(table2);

                PdfPTable table = new PdfPTable(new float[]{(float) 1, 1});

                table.setWidthPercentage(100);
                table.addCell(new Phrase(bank1, mainFont));
                table.addCell(new Phrase(bank2, mainFont));

                PdfPCell[] cells = table.getRow(0).getCells();
                for (int j = 0; j < cells.length; j++) {
                    cells[j].setPadding(10.0f);
                    cells[j].setBorder(Rectangle.BOX);
                    cells[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                    //cells[j].setVerticalAlignment(Element.ALIGN_CENTER);
                }
                document.add(table);

                if (bankmainvisibility) {
                    if (banklist.size() != 0) {
                        PdfPTable rectable = new PdfPTable(new float[]{(float) 1, 1});
                        rectable.setWidthPercentage(100);
                        rectable.addCell(new Phrase(_context.getResources().getString(R.string.ledger_name), titleFont));
                        rectable.addCell(new Phrase(_context.getResources().getString(R.string.balance), titleFont));

                        PdfPCell[] rcells = rectable.getRow(0).getCells();
                        for (int j = 0; j < rcells.length; j++) {
                            rcells[j].setPadding(10.0f);
                            rcells[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                            rcells[j].setBorder(Rectangle.BOX);
                            //rcells[j].setVerticalAlignment(Element.ALIGN_CENTER);
                        }
                        document.add(rectable);
                    }

                    PdfPTable reclist = new PdfPTable(new float[]{(float) 1, 1});
                    reclist.setWidthPercentage(100);


                    for (Business model : cashlist) {
                   /* String data = model.getDtl2().trim();
                    if (data != null && !data.equals("") && data.matches("[0-9.]+")) {
                        data = indianFormat.format(new BigDecimal(Double.parseDouble(model.getDtl2())));
                    }*/

                        reclist.addCell(new Phrase(model.getNamedisplay(), mainFont));
                        reclist.addCell(new Phrase(model.getDtl2(), mainFont));

                    }

                    for (int i = 0; i < cashlist.size(); i++) {
                        PdfPCell[] recells = reclist.getRow(i).getCells();
                        for (int j = 0; j < recells.length; j++) {
                            recells[j].setPadding(10.0f);
                            recells[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                            //recells[j].setVerticalAlignment(Element.ALIGN_CENTER);
                            recells[j].setBorder(Rectangle.BOX);
                        }
                    }

                    document.add(reclist);
                }

            }

            if (stockvisibility) {
                PdfPTable table2 = new PdfPTable(1);
                table2.setWidthPercentage(100);
                table2.setSpacingBefore(20f);


                PdfPCell invoice = new PdfPCell(new Phrase(_context.getResources().getString(R.string.stock_valuation), mainFont));
                invoice.setColspan(6);
                invoice.setHorizontalAlignment(Element.ALIGN_CENTER);
                invoice.setBackgroundColor(new BaseColor(255, 82, 82));
                invoice.setBorder(Rectangle.BOX);
                invoice.setPadding(8f);
                table2.addCell(invoice);
                document.add(table2);

                PdfPTable table = new PdfPTable(new float[]{(float) 1, 1});

                table.setWidthPercentage(100);
                table.addCell(new Phrase(stock1, mainFont));
                table.addCell(new Phrase(stock2, mainFont));

                PdfPCell[] cells = table.getRow(0).getCells();
                for (int j = 0; j < cells.length; j++) {
                    cells[j].setPadding(10.0f);
                    cells[j].setBorder(Rectangle.BOX);
                    cells[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                    // cells[j].setVerticalAlignment(Element.ALIGN_CENTER);
                }
                document.add(table);
            }


            document.close();

            final String FILE1 = Environment.getExternalStorageDirectory().getAbsolutePath()
                    + "/" + _context.getResources().getString(R.string.app_name) + "/" + filename + ".pdf";
            manipulatePdf(file, FILE1);

            File file1 = new File(file);
            file1.delete();

        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void CreateVoucherInfoPdf(String filename, String address, String compname, String datetext, String vchno, String vchdate, String refno, String type, String party, String vehicleno, String createdby, String editedby, String inventorytotal, ArrayList<VoucherInfo> inventroylist, String othertotal, ArrayList<VoucherInfo> otherchargelist, ArrayList<VoucherInfo> transactionlist) {
        String file = Environment.getExternalStorageDirectory().toString() + "/" + _context.getResources().getString(R.string.app_name) + "/" + filename + "_" + System.currentTimeMillis() + ".pdf";
        HeaderFooterPageEvent event = new HeaderFooterPageEvent(compname, address, datetext, _context, "voucher");

        try {

            Document document = new Document(PageSize.A4, 20, 20, 20 + event.getTableHeight(), 25);
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));

            writer.setPageEvent(event);

            document.open();

            final PdfPTable table1 = new PdfPTable(1);
            table1.setWidthPercentage(100);
            table1.setSpacingBefore(0f);
            table1.setSpacingAfter(0f);

            Font mainFont = new Font(urName, 10, Font.BOLD, BaseColor.BLACK);
            Font titleFont = new Font(urName, 10, Font.BOLD, new BaseColor(121, 102, 254));
            Font normalFont = new Font(urName, 10, Font.NORMAL, BaseColor.BLACK);

            PdfPTable table = new PdfPTable(new float[]{(float) 0.5, 1, (float) 0.5, 1});
            table.setWidthPercentage(100);
            table.setSpacingBefore(20f);

            PdfPCell invoice = new PdfPCell(new Phrase(_context.getResources().getString(R.string.voucher_info), mainFont));
            invoice.setColspan(4);
            invoice.setHorizontalAlignment(Element.ALIGN_CENTER);
            invoice.setBackgroundColor(new BaseColor(146, 208, 80));
            invoice.setBorder(Rectangle.BOX);
            invoice.setPadding(8f);
            table.addCell(invoice);
            //document.add(table);

            //PdfPTable table = new PdfPTable(new float[]{(float) 0.5, 1, (float) 0.5, 1});

            table.setWidthPercentage(100);

            table.addCell(new Phrase(_context.getResources().getString(R.string.vch_no), mainFont));
            table.addCell(new Phrase(vchno, normalFont));
            table.addCell(new Phrase(_context.getResources().getString(R.string.date), mainFont));
            table.addCell(new Phrase(vchdate, normalFont));

            if (inventroylist.size() != 0) {
                table.addCell(new Phrase(_context.getResources().getString(R.string.ref_no), mainFont));
                table.addCell(new Phrase(refno, normalFont));
                table.addCell(new Phrase(_context.getResources().getString(R.string.type), mainFont));
                table.addCell(new Phrase(type, normalFont));
            }

            table.addCell(new Phrase(_context.getResources().getString(R.string.party), mainFont));
            //table.addCell(new Phrase(party, normalFont));
            if (vehicleno.length() != 0) {
                table.addCell(new Phrase(party, normalFont));
                table.addCell(new Phrase(_context.getResources().getString(R.string.vehi_no), mainFont));
                table.addCell(new Phrase(vehicleno, normalFont));
            } else {
                PdfPCell c1 = new PdfPCell(new Phrase(party, normalFont));
                c1.setColspan(3);
                c1.setPadding(8f);
                c1.setBorder(Rectangle.BOX);
                c1.setVerticalAlignment(Element.ALIGN_CENTER);
                table.addCell(c1);
            }

            PdfPCell created = new PdfPCell(new Phrase(createdby, mainFont));
            if (editedby.length() == 0)
                created.setColspan(4);
            else
                created.setColspan(2);
            created.setPadding(8f);
            created.setBorder(Rectangle.BOX);
            created.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(created);

            if (editedby.length() != 0) {
                PdfPCell edited = new PdfPCell(new Phrase(editedby, mainFont));
                edited.setColspan(2);
                edited.setPadding(8f);
                edited.setBorder(Rectangle.BOX);
                edited.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(edited);
            }

            int rowno = 3;
            if (inventroylist.size() == 0)
                rowno = 2;

            for (int i = 1; i <= rowno; i++) {

                PdfPCell[] cells = table.getRow(i).getCells();
                for (int j = 0; j < cells.length; j++) {
                    //System.out.println("========= = = " + i + " = " + j);
                    if (i == rowno && j == 1 && vehicleno.length() == 0)
                        break;

                    cells[j].setPadding(8f);
                    cells[j].setBorder(Rectangle.BOX);
                    cells[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                }
            }

            document.add(table);

            if (inventroylist.size() != 0) {


                PdfPTable table3 = new PdfPTable(new float[]{(float) 1, (float) 0.5, (float) 0.5, (float) 0.8});
                table3.setWidthPercentage(100);
                table3.setSpacingBefore(20f);

                PdfPCell inventoryheader = new PdfPCell(new Phrase(_context.getResources().getString(R.string.inventory_info), mainFont));
                inventoryheader.setColspan(4);
                inventoryheader.setHorizontalAlignment(Element.ALIGN_CENTER);
                inventoryheader.setBackgroundColor(new BaseColor(250, 192, 144));
                inventoryheader.setBorder(Rectangle.BOX);
                inventoryheader.setPadding(8f);

                table3.addCell(inventoryheader);

                table3.addCell(new Phrase(_context.getResources().getString(R.string.stock_item), mainFont));
                table3.addCell(new Phrase(_context.getResources().getString(R.string.qty), mainFont));
                table3.addCell(new Phrase(_context.getResources().getString(R.string.rate), mainFont));
                table3.addCell(new Phrase(_context.getResources().getString(R.string.total), mainFont));

                PdfPCell[] cells = table3.getRow(1).getCells();
                for (int j = 0; j < cells.length; j++) {
                    cells[j].setPadding(8f);
                    cells[j].setBorder(Rectangle.BOX);
                    cells[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                }

                int i = 1;
                for (VoucherInfo voucherInfo : inventroylist) {
                    PdfPCell c11 = new PdfPCell(new Phrase(i + ". " + voucherInfo.getStockitemname(), normalFont));
                    c11.setPadding(8f);
                    c11.setBorder(Rectangle.BOX);
                    c11.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    table3.addCell(c11);

                    PdfPCell c22 = new PdfPCell(new Phrase(voucherInfo.getDtl2(), normalFont));
                    c22.setPadding(8f);
                    c22.setBorder(Rectangle.BOX);
                    c22.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c22.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table3.addCell(c22);

                    PdfPCell c33 = new PdfPCell(new Phrase(voucherInfo.getDtl3(), normalFont));
                    c33.setPadding(8f);
                    c33.setBorder(Rectangle.BOX);
                    c33.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c33.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table3.addCell(c33);


                    PdfPCell ctotal = new PdfPCell(new Phrase(voucherInfo.getDtl6(), normalFont));
                    ctotal.setRowspan(2);
                    ctotal.setPadding(8f);
                    ctotal.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    ctotal.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    table3.addCell(ctotal);


                    String disc = voucherInfo.getDtl4().trim(), gst = voucherInfo.getDtl5().trim();

                    if ((disc.length() != 0 || !disc.equals("0")) && (gst.length() != 0 || !gst.equals("0"))) {
                        PdfPCell c1 = new PdfPCell(new Phrase("Disc: " + disc + "%, GST: " + gst + "%", normalFont));
                        c1.setColspan(2);
                        c1.setPadding(8f);
                        c1.setBorder(Rectangle.BOX);
                        c1.setVerticalAlignment(Element.ALIGN_CENTER);
                        c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        table3.addCell(c1);

                        PdfPCell c2 = new PdfPCell(new Phrase(voucherInfo.getDisgstrate(), normalFont));
                        c2.setColspan(1);
                        c2.setPadding(8f);
                        c2.setBorder(Rectangle.BOX);
                        c2.setVerticalAlignment(Element.ALIGN_CENTER);
                        c2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        table3.addCell(c2);
                    }

                    if (voucherInfo.getDtl7().trim().length() != 0) {
                        PdfPCell inventorylast = new PdfPCell(new Phrase(voucherInfo.getDtl7(), normalFont));
                        inventorylast.setColspan(4);
                        inventorylast.setHorizontalAlignment(Element.ALIGN_CENTER);
                        inventorylast.setBorder(Rectangle.BOX);
                        inventorylast.setPadding(8f);
                        table3.addCell(inventorylast);
                    }

                    i++;
                }

                PdfPCell inventorylast = new PdfPCell(new Phrase(_context.getResources().getString(R.string.total), mainFont));
                inventorylast.setColspan(2);
                inventorylast.setHorizontalAlignment(Element.ALIGN_CENTER);
                inventorylast.setBorder(Rectangle.BOX);
                inventorylast.setPadding(8f);
                table3.addCell(inventorylast);

                PdfPCell inventorymaintotal = new PdfPCell(new Phrase(inventorytotal, mainFont));
                inventorymaintotal.setColspan(2);
                inventorymaintotal.setHorizontalAlignment(Element.ALIGN_RIGHT);
                inventorymaintotal.setVerticalAlignment(Element.ALIGN_MIDDLE);
                inventorymaintotal.setBorder(Rectangle.BOX);
                inventorymaintotal.setPadding(8f);
                table3.addCell(inventorymaintotal);

                document.add(table3);
            }

            if (otherchargelist.size() != 0) {
                PdfPTable table3 = new PdfPTable(new float[]{(float) 1, 1});
                table3.setWidthPercentage(100);
                table3.setSpacingBefore(20f);

                PdfPCell inventoryheader = new PdfPCell(new Phrase(_context.getResources().getString(R.string.other_charge), mainFont));
                inventoryheader.setColspan(2);
                inventoryheader.setHorizontalAlignment(Element.ALIGN_CENTER);
                inventoryheader.setBackgroundColor(new BaseColor(83, 142, 213));
                inventoryheader.setBorder(Rectangle.BOX);
                inventoryheader.setPadding(8f);

                table3.addCell(inventoryheader);

                table3.addCell(new Phrase(_context.getResources().getString(R.string.particulars), mainFont));
                table3.addCell(new Phrase(_context.getResources().getString(R.string.amount), mainFont));

                PdfPCell[] cellsmain = table3.getRow(1).getCells();
                for (int j = 0; j < cellsmain.length; j++) {
                    cellsmain[j].setPadding(8f);
                    cellsmain[j].setBorder(Rectangle.BOX);
                    cellsmain[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                }

                for (int i = 0; i < otherchargelist.size(); i++) {
                    VoucherInfo voucherInfo = otherchargelist.get(i);
                    table3.addCell(new Phrase(voucherInfo.getDtl1(), normalFont));
                    table3.addCell(new Phrase(voucherInfo.getDtl3(), normalFont));

                    PdfPCell[] cells = table3.getRow(i + 2).getCells();
                    for (int j = 0; j < cells.length; j++) {
                        cells[j].setPadding(8f);
                        cells[j].setBorder(Rectangle.BOX);
                        cells[j].setVerticalAlignment(Element.ALIGN_MIDDLE);
                        if (j == 1) {
                            cells[j].setHorizontalAlignment(Element.ALIGN_RIGHT);
                        }
                    }
                }

                PdfPCell totalcell = new PdfPCell(new Phrase(_context.getResources().getString(R.string.total_invoice_amount), mainFont));
                totalcell.setPadding(8f);
                totalcell.setBorder(Rectangle.BOX);
                totalcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table3.addCell(totalcell);

                PdfPCell totalcellval = new PdfPCell(new Phrase(othertotal, mainFont));
                totalcellval.setPadding(8f);
                totalcellval.setBorder(Rectangle.BOX);
                totalcellval.setHorizontalAlignment(Element.ALIGN_RIGHT);
                totalcellval.setVerticalAlignment(Element.ALIGN_MIDDLE);
                table3.addCell(totalcellval);

                document.add(table3);

            }

            if (transactionlist.size() != 0) {
                PdfPTable table3 = new PdfPTable(new float[]{(float) 1, 1});
                table3.setWidthPercentage(100);
                table3.setSpacingBefore(20f);

                PdfPCell inventoryheader = new PdfPCell(new Phrase(_context.getResources().getString(R.string.accounting_transactions), mainFont));
                inventoryheader.setColspan(2);
                inventoryheader.setHorizontalAlignment(Element.ALIGN_CENTER);
                inventoryheader.setBackgroundColor(new BaseColor(255, 82, 82));
                inventoryheader.setBorder(Rectangle.BOX);
                inventoryheader.setPadding(8f);

                table3.addCell(inventoryheader);

                table3.addCell(new Phrase(_context.getResources().getString(R.string.particulars), mainFont));
                table3.addCell(new Phrase(_context.getResources().getString(R.string.amount), mainFont));

                PdfPCell[] cellsmain = table3.getRow(1).getCells();
                for (int j = 0; j < cellsmain.length; j++) {
                    cellsmain[j].setPadding(8f);
                    cellsmain[j].setBorder(Rectangle.BOX);
                    cellsmain[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                }

                for (int i = 0; i < transactionlist.size(); i++) {
                    VoucherInfo voucherInfo = transactionlist.get(i);
                    table3.addCell(new Phrase(voucherInfo.getParticulars(), normalFont));
                    table3.addCell(new Phrase(voucherInfo.getDisplayamount(), normalFont));

                    PdfPCell[] cells = table3.getRow(i + 2).getCells();
                    for (int j = 0; j < cells.length; j++) {
                        cells[j].setPadding(8f);
                        cells[j].setBorder(Rectangle.BOX);
                        cells[j].setVerticalAlignment(Element.ALIGN_MIDDLE);
                        if (j == 1) {
                            cells[j].setHorizontalAlignment(Element.ALIGN_RIGHT);
                        }
                    }
                }

                document.add(table3);
            }


            document.close();

            final String FILE1 = Environment.getExternalStorageDirectory().getAbsolutePath()
                    + "/" + _context.getResources().getString(R.string.app_name) + "/" + filename + ".pdf";
            manipulatePdf(file, FILE1);

            File file1 = new File(file);
            file1.delete();

        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static void CreateReceivblePdf(String filename, String address, String compname, String datetext, ArrayList<Receivble> entrylist, RecyclerView recyclerView, String totaldb, String totalcr, String netrec) {

        String file = Environment.getExternalStorageDirectory().toString() + "/" + _context.getResources().getString(R.string.app_name) + "/" + filename + "_" + System.currentTimeMillis() + ".pdf";
        HeaderFooterPageEvent event = new HeaderFooterPageEvent(compname, address, datetext, _context, "receivable");
        try {
            Document document = new Document(PageSize.A4, 20, 20, event.getTableHeight(), 25);
            //Document document = new Document(PageSize.A4, 36, 36, 90, 36);
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));

            writer.setPageEvent(event);

            document.open();

            final PdfPTable table1 = new PdfPTable(1);
            table1.setWidthPercentage(100);
            table1.setSpacingBefore(0f);
            table1.setSpacingAfter(0f);


            Font mainFont = new Font(urName, 10, Font.BOLD, BaseColor.BLACK);
            Font normalFont = new Font(urName, 10, Font.NORMAL, BaseColor.BLACK);


            /*PdfPCell company = new PdfPCell(new Phrase(compname, new Font(urName, 20, Font.BOLD, BaseColor.BLACK)));
            company.setColspan(6);
            company.setHorizontalAlignment(Element.ALIGN_CENTER);
            //company.setPaddingTop(20.0f);
            company.setBorder(Rectangle.NO_BORDER);

            PdfPCell cell = new PdfPCell(new Phrase(address, normalFont));
            cell.setColspan(6);
            cell.setBorder(Rectangle.BOTTOM);
            cell.setPaddingBottom(20.0f);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);

            PdfPCell datecell = new PdfPCell(new Phrase(datetext, normalFont));
            datecell.setColspan(6);
            datecell.setBorder(Rectangle.NO_BORDER);
            datecell.setPaddingBottom(20.0f);
            datecell.setHorizontalAlignment(Element.ALIGN_CENTER);

            table1.addCell(company);
            table1.addCell(cell);
            table1.addCell(datecell);
            document.add(table1);

            PdfPTable tableheader = new PdfPTable(new float[]{(float) 1, 1, 1, 1, 1, 1, 1});

            tableheader.setWidthPercentage(100);
            tableheader.addCell(new Phrase(_context.getResources().getString(R.string.date), mainFont));
            tableheader.addCell(new Phrase(_context.getResources().getString(R.string.vch_no), mainFont));
            tableheader.addCell(new Phrase(_context.getResources().getString(R.string.bill_amt), mainFont));
            tableheader.addCell(new Phrase(_context.getResources().getString(R.string.due_amt), mainFont));
            tableheader.addCell(new Phrase(_context.getResources().getString(R.string.dr_cr), mainFont));
            tableheader.addCell(new Phrase(_context.getResources().getString(R.string.due_date), mainFont));
            tableheader.addCell(new Phrase(_context.getResources().getString(R.string.due_days), mainFont));

            PdfPCell[] cellsheader = tableheader.getRow(0).getCells();
            for (int j = 0; j < cellsheader.length; j++) {
                cellsheader[j].setPadding(10.0f);
                cellsheader[j].setBorder(Rectangle.BOX);
                cellsheader[j].setHorizontalAlignment(Element.ALIGN_CENTER);
            }
            document.add(tableheader);*/


            for (int i = 0; i < entrylist.size(); i++) {
                Receivble receivble = entrylist.get(i);

                String data;
                String mobile = receivble.getMobileno().trim();
                if (mobile.length() != 0)
                    mobile = "M." + mobile;

                String region = receivble.getRegionname().trim();
                if (mobile.length() != 0)
                    region = region + "\n" + mobile;

                if (receivble.getRegionname().contains("NOT APPLICABLE"))
                    data = mobile;
                else
                    data = region;

                PdfPTable table = new PdfPTable(new float[]{(float) 1, 1, 1, 1, 1, 1, 1});

                table.setWidthPercentage(100);
                BaseColor baseColor;
                if (i % 2 == 0)
                    baseColor = new BaseColor(222, 206, 255);
                else
                    baseColor = new BaseColor(210, 220, 255);


                PdfPCell c1 = new PdfPCell(new Phrase(receivble.getPartyname(), mainFont));
                c1.setColspan(3);
                c1.setPadding(8f);
                c1.setBorder(Rectangle.BOX);
                c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                c1.setBackgroundColor(baseColor);

                PdfPCell c2 = new PdfPCell(new Phrase(data, mainFont));
                c2.setColspan(2);
                c2.setPadding(8f);
                c2.setBorder(Rectangle.BOX);
                c2.setHorizontalAlignment(Element.ALIGN_CENTER);
                c2.setBackgroundColor(baseColor);

                PdfPCell c3 = new PdfPCell(new Phrase(receivble.getDue(), mainFont));
                c3.setColspan(2);
                c3.setPadding(8f);
                c3.setBorder(Rectangle.BOX);
                c3.setHorizontalAlignment(Element.ALIGN_CENTER);
                c3.setBackgroundColor(baseColor);

                table.addCell(c1);
                table.addCell(c2);
                table.addCell(c3);

                //table.addCell(new Phrase(receivble.getPartyname(), mainFont));
                //table.addCell(new Phrase(receivble.getDue(), mainFont));
                //table.addCell(new Phrase(receivble.getDue(), mainFont));


                /*PdfPCell[] cells = table.getRow(0).getCells();

                for (int j = 0; j < cells.length; j++) {
                    cells[j].setPadding(10.0f);
                    cells[j].setBorder(Rectangle.BOX);
                    cells[j].setHorizontalAlignment(Element.ALIGN_CENTER);

                    if (i % 2 == 0)
                        cells[j].setBackgroundColor(new BaseColor(222, 206, 255));
                    else
                        cells[j].setBackgroundColor(new BaseColor(210, 220, 255));

                }*/
                document.add(table);

                View row = recyclerView.getLayoutManager().findViewByPosition(i);
                RecyclerView recycleviewdata = row.findViewById(R.id.recycleview);

                if (recycleviewdata.getVisibility() == View.VISIBLE) {
                    PdfPTable tabledata = new PdfPTable(new float[]{(float) 1, 1, 1, 1, 1, 1, 1});
                    tabledata.setWidthPercentage(100);
                    ArrayList<Receivble> datalist = receivble.getReceivbleArrayList();
                    for (int j = 0; j < datalist.size(); j++) {
                        Receivble receivbledata = datalist.get(j);
                        tabledata.addCell(new Phrase(receivbledata.getTrdate2(), normalFont));
                        tabledata.addCell(new Phrase(receivbledata.getVchno(), normalFont));
                        tabledata.addCell(new Phrase(receivbledata.getBillamt(), normalFont));
                        tabledata.addCell(new Phrase(receivbledata.getDue(), normalFont));
                        tabledata.addCell(new Phrase(receivbledata.getDc(), normalFont));
                        tabledata.addCell(new Phrase(receivbledata.getDuedate2(), normalFont));
                        tabledata.addCell(new Phrase(receivbledata.getDuedays(), normalFont));

                        PdfPCell[] cellsdata = tabledata.getRow(j).getCells();
                        for (int k = 0; k < cellsdata.length; k++) {
                            cellsdata[k].setPadding(8f);
                            cellsdata[k].setBorder(Rectangle.BOX);
                            cellsdata[k].setHorizontalAlignment(Element.ALIGN_CENTER);
                        }
                    }
                    document.add(tabledata);
                }

            }

            //Font titleFont = new Font(urName, 20, Font.BOLD, BaseColor.BLACK);
            PdfPTable tablefooter = new PdfPTable(new float[]{(float) 1, 1});
            tablefooter.setWidthPercentage(100);
            for (int i = 0; i < 3; i++) {

                if (i == 0) {
                    tablefooter.addCell(new Phrase(_context.getString(R.string.total_debit).trim(), mainFont));
                    tablefooter.addCell(new Phrase(totaldb, mainFont));
                } else if (i == 1) {
                    tablefooter.addCell(new Phrase(_context.getString(R.string.total_credit).trim(), mainFont));
                    tablefooter.addCell(new Phrase(totalcr, mainFont));
                } else {
                    tablefooter.addCell(new Phrase(_context.getString(R.string.net_receivble).trim(), mainFont));
                    tablefooter.addCell(new Phrase(netrec, mainFont));
                }


                PdfPCell[] cellsfooter = tablefooter.getRow(i).getCells();
                for (int j = 0; j < cellsfooter.length; j++) {
                    cellsfooter[j].setPadding(8f);
                    cellsfooter[j].setBorder(Rectangle.BOX);
                    cellsfooter[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                }

            }
            document.add(tablefooter);

            document.close();

            final String FILE1 = Environment.getExternalStorageDirectory().toString()
                    + "/" + _context.getResources().getString(R.string.app_name) + "/" + filename + ".pdf";
            manipulatePdf(file, FILE1);

            File file1 = new File(file);
            file1.delete();

        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void CreateLedgerReportPdf(String filename, String address, String compname, String datetext, ArrayList<LedgerReport> ledgerReportArrayList, String txtledgername, String txtaddress, String txtmobno, String txtemail, String totaldr, String totalcr, String txtopeningbalance, String txtclosingbalance) {
        String file = Environment.getExternalStorageDirectory().toString() + "/" + _context.getResources().getString(R.string.app_name) + "/" + filename + "_" + System.currentTimeMillis() + ".pdf";
        HeaderFooterPageEvent event = new HeaderFooterPageEvent(compname, address, datetext, txtledgername, txtaddress, txtmobno, txtemail, txtopeningbalance, _context, "ledgerreport");
        try {
            Document document = new Document(PageSize.A4, 20, 20, event.getTableHeight(), 25);
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));

            writer.setPageEvent(event);

            document.open();

            final PdfPTable table1 = new PdfPTable(1);
            table1.setWidthPercentage(100);
            table1.setSpacingBefore(0f);
            table1.setSpacingAfter(0f);


            PdfPTable table = new PdfPTable(new float[]{(float) 0.5, (float) 1.8, (float) 0.5, (float) 1.2});
            table.setWidthPercentage(100);

            Font mainFont = new Font(urName, 10, Font.BOLD, BaseColor.BLACK);
            Font normalFont = new Font(urName, 10, Font.NORMAL, BaseColor.BLACK);


            PdfPCell obal = new PdfPCell(new Phrase(_context.getResources().getString(R.string.opening_balance), mainFont));
            obal.setPadding(8f);
            obal.setColspan(3);
            obal.setHorizontalAlignment(Element.ALIGN_CENTER);
            obal.setBorder(Rectangle.BOX);
            table.addCell(obal);

            PdfPCell obalval = new PdfPCell(new Phrase(txtopeningbalance, mainFont));
            obalval.setPadding(8f);
            obalval.setHorizontalAlignment(Element.ALIGN_RIGHT);
            obalval.setVerticalAlignment(Element.ALIGN_MIDDLE);
            obalval.setBorder(Rectangle.BOX);
            table.addCell(obalval);

            if (ledgerReportArrayList.size() != 0) {

                int i = 1;
                for (LedgerReport model : ledgerReportArrayList) {
                    table.addCell(new Phrase(model.getTrdate2(), normalFont));
                    table.addCell(new Phrase(model.getParticulars(), normalFont));
                    table.addCell(new Phrase(model.getVchno(), normalFont));
                    table.addCell(new Phrase(String.valueOf(Html.fromHtml(model.getAmount())), normalFont));

                    PdfPCell[] cells = table.getRow(i).getCells();
                    for (int j = 0; j < cells.length; j++) {

                        if (j == 0 || j == 2) {
                            cells[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                        } else if (j == 1)
                            cells[j].setVerticalAlignment(Element.ALIGN_MIDDLE);
                        else {
                            cells[j].setVerticalAlignment(Element.ALIGN_MIDDLE);
                            cells[j].setHorizontalAlignment(Element.ALIGN_RIGHT);
                        }

                        cells[j].setPadding(8f);
                        cells[j].setBorder(Rectangle.BOX);
                    }

                    String narration = model.getNarration().trim();
                    if (narration.length() != 0) {
                        PdfPCell pdfPCell = new PdfPCell(new Phrase(narration, normalFont));
                        pdfPCell.setPadding(8f);
                        pdfPCell.setColspan(4);
                        pdfPCell.setBorder(Rectangle.BOX);
                        pdfPCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table.addCell(pdfPCell);

                        i += 1;
                    }
                    i += 1;
                }
            }

            PdfPCell c33 = new PdfPCell(new Phrase(txtledgername, mainFont));
            c33.setPadding(8f);
            c33.setRowspan(2);
            c33.setColspan(2);
            c33.setBorder(Rectangle.BOX);
            c33.setHorizontalAlignment(Element.ALIGN_CENTER);
            c33.setVerticalAlignment(Element.ALIGN_MIDDLE);
            c33.setBackgroundColor(new BaseColor(231, 255, 206));
            table.addCell(c33);


            PdfPCell dtotal = new PdfPCell(new Phrase(_context.getResources().getString(R.string.total_dr), mainFont));
            dtotal.setPadding(8f);
            dtotal.setHorizontalAlignment(Element.ALIGN_CENTER);
            dtotal.setBorder(Rectangle.BOX);
            dtotal.setBackgroundColor(new BaseColor(231, 255, 206));
            table.addCell(dtotal);

            PdfPCell dtotalval = new PdfPCell(new Phrase(totaldr, mainFont));
            dtotalval.setPadding(8f);
            dtotalval.setHorizontalAlignment(Element.ALIGN_RIGHT);
            dtotalval.setVerticalAlignment(Element.ALIGN_MIDDLE);
            dtotalval.setBorder(Rectangle.BOX);
            dtotalval.setBackgroundColor(new BaseColor(231, 255, 206));
            table.addCell(dtotalval);

            PdfPCell ctotal = new PdfPCell(new Phrase(_context.getResources().getString(R.string.total_cr), mainFont));
            ctotal.setPadding(8f);
            ctotal.setHorizontalAlignment(Element.ALIGN_CENTER);
            ctotal.setBorder(Rectangle.BOX);
            ctotal.setBackgroundColor(new BaseColor(231, 255, 206));
            table.addCell(ctotal);

            PdfPCell ctotalval = new PdfPCell(new Phrase(totalcr, mainFont));
            ctotalval.setPadding(8f);
            ctotalval.setHorizontalAlignment(Element.ALIGN_RIGHT);
            ctotalval.setVerticalAlignment(Element.ALIGN_MIDDLE);
            ctotalval.setBorder(Rectangle.BOX);
            ctotalval.setBackgroundColor(new BaseColor(231, 255, 206));
            table.addCell(ctotalval);


            PdfPCell cbal = new PdfPCell(new Phrase(_context.getResources().getString(R.string.closing_balance), mainFont));
            cbal.setPadding(8f);
            cbal.setColspan(3);
            cbal.setHorizontalAlignment(Element.ALIGN_CENTER);
            cbal.setBorder(Rectangle.BOX);
            table.addCell(cbal);

            PdfPCell cbalval = new PdfPCell(new Phrase(txtclosingbalance, mainFont));
            cbalval.setPadding(8f);
            cbalval.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cbalval.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cbalval.setBorder(Rectangle.BOX);
            table.addCell(cbalval);


            document.add(table);
            document.close();

            final String FILE1 = Environment.getExternalStorageDirectory().toString()
                    + "/" + _context.getResources().getString(R.string.app_name) + "/" + filename + ".pdf";
            manipulatePdf(file, FILE1);

            File file1 = new File(file);
            file1.delete();

        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    public static void CreateSalesReportPdf(String filename, String address, String compname, String datetext, ArrayList<SalesReport> salesReportArrayList, ArrayList<SalesReport> vList, ArrayList<SalesReport> pList) {
        String file = Environment.getExternalStorageDirectory().toString() + "/" + _context.getResources().getString(R.string.app_name) + "/" + filename + "_" + System.currentTimeMillis() + ".pdf";
        HeaderFooterPageEvent event = new HeaderFooterPageEvent(compname, address, datetext, _context, "salesreport");
        try {
            Document document = new Document(PageSize.A4, 20, 20, event.getTableHeight(), 25);
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));

            writer.setPageEvent(event);

            document.open();

            Font mainFont = new Font(urName, 10, Font.BOLD, BaseColor.BLACK);
            Font titleFont = new Font(urName, 10, Font.BOLD, new BaseColor(121, 102, 254));
            Font normalFont = new Font(urName, 10, Font.NORMAL, BaseColor.BLACK);


            if (salesReportArrayList.size() != 0) {
                PdfPTable table2 = new PdfPTable(new float[]{(float) 0.8, (float) 1.5, 1, (float) 0.7, (float) 1});
                table2.setWidthPercentage(100);
                table2.setSpacingBefore(20f);
                table2.addCell(new Phrase(_context.getResources().getString(R.string.date), mainFont));
                table2.addCell(new Phrase(_context.getResources().getString(R.string.party_name), mainFont));
                table2.addCell(new Phrase(_context.getResources().getString(R.string.type), mainFont));
                table2.addCell(new Phrase(_context.getResources().getString(R.string.vch_no), mainFont));
                table2.addCell(new Phrase(_context.getResources().getString(R.string.amount), mainFont));

                PdfPCell[] cells = table2.getRow(0).getCells();
                for (int j = 0; j < cells.length; j++) {
                    cells[j].setPadding(8f);
                    cells[j].setBorder(Rectangle.BOX);
                    cells[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                    cells[j].setBackgroundColor(new BaseColor(146, 208, 80));
                }
                for (int i = 0; i < salesReportArrayList.size(); i++) {
                    SalesReport salesReport = salesReportArrayList.get(i);
                    table2.addCell(new Phrase(salesReport.getTrdate2(), normalFont));
                    table2.addCell(new Phrase(salesReport.getPartyname(), normalFont));
                    table2.addCell(new Phrase(salesReport.getTypename(), normalFont));
                    table2.addCell(new Phrase(salesReport.getVchno(), normalFont));
                    table2.addCell(new Phrase(salesReport.getAmount(), normalFont));

                    PdfPCell[] cellstable2 = table2.getRow(i + 1).getCells();
                    for (int j = 0; j < cells.length; j++) {
                        cellstable2[j].setPadding(8f);
                        cellstable2[j].setBorder(Rectangle.BOX);
                        cellstable2[j].setVerticalAlignment(Element.ALIGN_MIDDLE);
                        if (j == 1) {
                            cellstable2[j].setHorizontalAlignment(Element.ALIGN_LEFT);
                        } else if (j == 4) {
                            cellstable2[j].setHorizontalAlignment(Element.ALIGN_RIGHT);
                        } else {
                            cellstable2[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                        }
                    }
                }
                document.add(table2);

            }

            if (pList.size() != 0) {
                PdfPTable table2 = new PdfPTable(new float[]{1, 1, 1});
                table2.setWidthPercentage(100);
                table2.setSpacingBefore(20f);

                PdfPCell paymentheader = new PdfPCell(new Phrase(_context.getResources().getString(R.string.paymentwise_summary), mainFont));
                paymentheader.setColspan(3);
                paymentheader.setHorizontalAlignment(Element.ALIGN_CENTER);
                paymentheader.setBackgroundColor(new BaseColor(190, 215, 245));
                paymentheader.setBorder(Rectangle.BOX);
                paymentheader.setPadding(8f);
                table2.addCell(paymentheader);

                table2.addCell(new Phrase(_context.getResources().getString(R.string.payment), mainFont));
                table2.addCell(new Phrase(_context.getResources().getString(R.string.no_s), mainFont));
                table2.addCell(new Phrase(_context.getResources().getString(R.string.amount), mainFont));

                PdfPCell[] cells = table2.getRow(1).getCells();
                for (int j = 0; j < cells.length; j++) {
                    cells[j].setPadding(8f);
                    cells[j].setBorder(Rectangle.BOX);
                    cells[j].setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cells[j].setBackgroundColor(new BaseColor(225, 238, 255));

                    if (j == 0) {
                        cells[j].setHorizontalAlignment(Element.ALIGN_LEFT);
                    } else {
                        cells[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                    }
                }


                for (int i = 0; i < pList.size(); i++) {

                    Font font = normalFont;
                    if (i == pList.size() - 1)
                        font = mainFont;

                    SalesReport salesReport = pList.get(i);
                    table2.addCell(new Phrase(salesReport.getName(), font));
                    table2.addCell(new Phrase(salesReport.getNo() + "", font));
                    table2.addCell(new Phrase(Constant.ConvertToIndianRupeeFormat(salesReport.getDoubleamount(), false, true, false), font));


                    PdfPCell[] cellstable2 = table2.getRow(i + 2).getCells();
                    for (int j = 0; j < cells.length; j++) {
                        cellstable2[j].setPadding(8f);
                        cellstable2[j].setBorder(Rectangle.BOX);
                        cellstable2[j].setVerticalAlignment(Element.ALIGN_MIDDLE);
                        if (j == 0) {
                            cells[j].setHorizontalAlignment(Element.ALIGN_LEFT);
                        } else if (j == 1) {
                            cells[j].setHorizontalAlignment(Element.ALIGN_RIGHT);
                        } else {
                            cells[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                        }

                    }

                }

                document.add(table2);
            }

            if (vList.size() != 0) {
                PdfPTable table2 = new PdfPTable(new float[]{1, 1, 1});
                table2.setWidthPercentage(100);
                table2.setSpacingBefore(20f);

                PdfPCell paymentheader = new PdfPCell(new Phrase(_context.getResources().getString(R.string.voucher_info), mainFont));
                paymentheader.setColspan(3);
                paymentheader.setHorizontalAlignment(Element.ALIGN_CENTER);
                paymentheader.setBackgroundColor(new BaseColor(250, 192, 144));
                paymentheader.setBorder(Rectangle.BOX);
                paymentheader.setPadding(8f);
                table2.addCell(paymentheader);

                table2.addCell(new Phrase(_context.getResources().getString(R.string.payment), mainFont));
                table2.addCell(new Phrase(_context.getResources().getString(R.string.no_s), mainFont));
                table2.addCell(new Phrase(_context.getResources().getString(R.string.amount), mainFont));


                PdfPCell[] cells = table2.getRow(1).getCells();
                for (int j = 0; j < cells.length; j++) {
                    cells[j].setPadding(8f);
                    cells[j].setBorder(Rectangle.BOX);
                    cells[j].setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cells[j].setBackgroundColor(new BaseColor(225, 234, 217));

                    if (j == 0) {
                        cells[j].setHorizontalAlignment(Element.ALIGN_LEFT);
                    } else if (j == 1) {
                        cells[j].setHorizontalAlignment(Element.ALIGN_RIGHT);
                    } else {
                        cells[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                    }
                }


                for (int i = 0; i < vList.size(); i++) {

                    Font font = normalFont;
                    if (i == vList.size() - 1)
                        font = mainFont;

                    SalesReport salesReport = vList.get(i);
                    table2.addCell(new Phrase(salesReport.getName(), font));
                    table2.addCell(new Phrase(salesReport.getNo() + "", font));
                    table2.addCell(new Phrase(Constant.ConvertToIndianRupeeFormat(salesReport.getDoubleamount(), false, true, false), font));


                    PdfPCell[] cellstable2 = table2.getRow(i + 2).getCells();
                    for (int j = 0; j < cells.length; j++) {
                        cellstable2[j].setPadding(8f);
                        cellstable2[j].setBorder(Rectangle.BOX);
                        cellstable2[j].setVerticalAlignment(Element.ALIGN_MIDDLE);
                        if (j == 0) {
                            cells[j].setHorizontalAlignment(Element.ALIGN_LEFT);
                        } else {
                            cells[j].setHorizontalAlignment(Element.ALIGN_CENTER);
                        }


                    }

                }

                document.add(table2);
            }


            document.close();

            final String FILE1 = Environment.getExternalStorageDirectory().toString() + "/" + _context.getResources().getString(R.string.app_name) + "/" + filename + ".pdf";
            manipulatePdf(file, FILE1);

            File file1 = new File(file);
            file1.delete();

        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void CreateStockStatusPdf(String filename, String address, String compname, String datetext, ArrayList<StockStatus> stockstauslist, RecyclerView recyclerView, String total, String txttotalitem) {
        String file = Environment.getExternalStorageDirectory().toString() + "/" + _context.getResources().getString(R.string.app_name) + "/" + filename + "_" + System.currentTimeMillis() + ".pdf";
        HeaderFooterPageEvent event = new HeaderFooterPageEvent(compname, address, datetext, _context, "stockstatus");
        try {
            Document document = new Document(PageSize.A4, 20, 20, event.getTableHeight(), 25);
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));

            writer.setPageEvent(event);

            document.open();

            Font mainFont = new Font(urName, 10, Font.BOLD, BaseColor.BLACK);
            Font titleFont = new Font(urName, 10, Font.BOLD, new BaseColor(121, 102, 254));
            Font normalFont = new Font(urName, 10, Font.NORMAL, BaseColor.BLACK);


            if (stockstauslist.size() != 0) {

                PdfPTable table2 = new PdfPTable(new float[]{(float) 1.5, (float) 0.9, (float) 0.8, (float) 0.8});
                table2.setWidthPercentage(100);
                table2.setSpacingBefore(20f);

                for (int i = 0; i < stockstauslist.size(); i++) {

                    BaseColor baseColor;
                    if (i % 2 == 0)
                        baseColor = new BaseColor(210, 220, 255);
                    else
                        baseColor = new BaseColor(222, 206, 255);


                    StockStatus stockStatus = stockstauslist.get(i);

                    PdfPCell c11 = new PdfPCell(new Phrase(stockStatus.getGroupname(), mainFont));
                    c11.setPadding(8f);
                    c11.setBorder(Rectangle.BOX);
                    c11.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c11.setHorizontalAlignment(Element.ALIGN_LEFT);
                    c11.setBackgroundColor(baseColor);
                    table2.addCell(c11);

                    PdfPCell c12 = new PdfPCell(new Phrase(String.valueOf(stockStatus.getDoubleqty()), mainFont));
                    c12.setPadding(8f);
                    c12.setBorder(Rectangle.BOX);
                    c12.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c12.setHorizontalAlignment(Element.ALIGN_CENTER);
                    c12.setBackgroundColor(baseColor);
                    table2.addCell(c12);

                    PdfPCell c13 = new PdfPCell(new Phrase(Constant.ConvertToIndianRupeeFormat(stockStatus.getDoubleamount(), false, true, false), mainFont));
                    c13.setPadding(8f);
                    c13.setBorder(Rectangle.BOX);
                    c13.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    c13.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    c13.setBackgroundColor(baseColor);
                    c13.setColspan(2);
                    table2.addCell(c13);


                    View row = recyclerView.getLayoutManager().findViewByPosition(i);
                    RecyclerView recycleviewdata = row.findViewById(R.id.recycleview);

                    if (recycleviewdata.getVisibility() == View.VISIBLE) {
                        ArrayList<StockStatus> datalist = stockStatus.getStockStatusArrayList();
                        for (int j = 0; j < datalist.size(); j++) {
                            StockStatus stockstatussub = datalist.get(j);

                            PdfPCell c1 = new PdfPCell(new Phrase(stockstatussub.getItemname(), normalFont));
                            c1.setPadding(8f);
                            c1.setBorder(Rectangle.BOX);
                            c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
                            c1.setHorizontalAlignment(Element.ALIGN_LEFT);
                            table2.addCell(c1);

                            PdfPCell c2 = new PdfPCell(new Phrase(stockstatussub.getQty(), normalFont));
                            c2.setPadding(8f);
                            c2.setBorder(Rectangle.BOX);
                            c2.setVerticalAlignment(Element.ALIGN_MIDDLE);
                            c2.setHorizontalAlignment(Element.ALIGN_CENTER);
                            table2.addCell(c2);

                            PdfPCell c3 = new PdfPCell(new Phrase(stockstatussub.getRate(), normalFont));
                            c3.setPadding(8f);
                            c3.setBorder(Rectangle.BOX);
                            c3.setVerticalAlignment(Element.ALIGN_MIDDLE);
                            c3.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            table2.addCell(c3);

                            PdfPCell c4 = new PdfPCell(new Phrase(stockstatussub.getAmount(), normalFont));
                            c4.setPadding(8f);
                            c4.setBorder(Rectangle.BOX);
                            c4.setVerticalAlignment(Element.ALIGN_MIDDLE);
                            c4.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            table2.addCell(c4);
                        }

                    }
                }

                BaseColor totalcolor = new BaseColor(255, 252, 168);
                PdfPCell c13 = new PdfPCell(new Phrase(txttotalitem, mainFont));
                c13.setPadding(8f);
                c13.setBorder(Rectangle.BOX);
                c13.setVerticalAlignment(Element.ALIGN_MIDDLE);
                c13.setHorizontalAlignment(Element.ALIGN_LEFT);
                c13.setColspan(2);
                c13.setBackgroundColor(totalcolor);
                table2.addCell(c13);

                PdfPCell c14 = new PdfPCell(new Phrase(total, mainFont));
                c14.setPadding(8f);
                c14.setBorder(Rectangle.BOX);
                c14.setVerticalAlignment(Element.ALIGN_MIDDLE);
                c14.setHorizontalAlignment(Element.ALIGN_RIGHT);
                c14.setColspan(2);
                c14.setBackgroundColor(totalcolor);
                table2.addCell(c14);

                document.add(table2);
            }

            document.close();

            final String FILE1 = Environment.getExternalStorageDirectory().toString() + "/" + _context.getResources().getString(R.string.app_name) + "/" + filename + ".pdf";
            manipulatePdf(file, FILE1);

            File file1 = new File(file);
            file1.delete();

        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void manipulatePdf(String src, String dest) throws IOException, DocumentException {
        PdfReader reader = new PdfReader(src);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
        int n = reader.getNumberOfPages();
        PdfContentByte canvas;
        Rectangle pageSize;
        float a, b, y, x;
        for (int p = 1; p <= n; p++) {
            pageSize = reader.getPageSizeWithRotation(p);

            a = pageSize.getBottom();
            b = pageSize.getRight();
            canvas = stamper.getOverContent(p);
            ColumnText.showTextAligned(canvas, Element.ALIGN_RIGHT,
                    new Phrase("Page " + p + " of " + n), b - 10, a + 10, 0);
        }
        stamper.close();
    }

    public void OnUpdateMasterClick(final VolleyCallback callback, Activity activity, UserSessionManager session, String orgcode, String companycode, DatabaseHelper databaseHelper, String subject, View view) {
        final boolean[] isupdate = {false};
        if (AppController.isConnected(activity, view)) {
            final AlertDialog.Builder alertdialog = new AlertDialog.Builder(activity);
            alertdialog.setTitle("Update Masters");
            alertdialog.setMessage("Update your Master to view Entries");

            alertdialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    callback.onSuccess(false);
                    dialog.dismiss();
                }
            });
            alertdialog.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    final ProgressDialog progressDialog = new ProgressDialog(activity);
                    progressDialog.setCancelable(false);
                    progressDialog.setMessage("Updating Masters...");
                    progressDialog.show();
                    System.out.println("============= =url =  " + Constant.GETMASTERNUrl + session.getData(UserSessionManager.KEY_ORG_REGKEY) + "&mobno=" + session.getData(UserSessionManager.KEY_Mobile) + "&imei=" + session.getData(UserSessionManager.KEY_IMEI) + "&compcode=" + session.getData(orgcode + UserSessionManager.KEY_COM_CODE) + "&devicecode=" + session.getData(UserSessionManager.KEY_DEVICECODE) + "&demo=" + session.getData(UserSessionManager.KEY_DEMO));
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.GETMASTERNUrl + session.getData(UserSessionManager.KEY_ORG_REGKEY) + "&compcode=" + session.getData(orgcode + UserSessionManager.KEY_COM_CODE) + "&mobno=" + session.getData(UserSessionManager.KEY_Mobile) + "&imei=" + session.getData(UserSessionManager.KEY_IMEI) + "&devicecode=" + session.getData(UserSessionManager.KEY_DEVICECODE) + "&demo=" + session.getData(UserSessionManager.KEY_DEMO), new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if (response.contains("\n"))
                                response = response.replace("\n", "").trim();

                            if (response.contains(activity.getResources().getString(R.string.noresponse))) {
                                progressDialog.dismiss();
                                callback.onSuccess(false);
                                Toast.makeText(activity, activity.getResources().getString(R.string.notconnectedserver), Toast.LENGTH_SHORT).show();
                            } else if (response.contains(activity.getResources().getString(R.string.multiuseword))) {
                                progressDialog.dismiss();
                                callback.onSuccess(false);
                                Toast.makeText(activity, activity.getResources().getString(R.string.multiuse), Toast.LENGTH_SHORT).show();
                            } else if (response.contains(activity.getResources().getString(R.string.qstringerr))) {
                                progressDialog.dismiss();
                                callback.onSuccess(false);
                                Toast.makeText(activity, activity.getResources().getString(R.string.trylater), Toast.LENGTH_SHORT).show();
                            } else {

                                databaseHelper.DeleteMasterData(companycode, orgcode);
                                try {
                                    JSONArray jsonArray = new JSONArray(response);
                                    //System.out.println("========== = len - " + jsonArray.toString().getBytes() + " = " + jsonArray.toString().getBytes().length);

                                    for (int i = 1; i < jsonArray.length() - 1; i++) {
                                        JSONArray masterarray = jsonArray.getJSONArray(i);
                                        Master master = new Master(masterarray.getString(0), masterarray.getString(1), masterarray.getString(2), masterarray.getString(3), masterarray.getString(4), masterarray.getString(5), masterarray.getString(6), masterarray.getString(7), masterarray.getString(8), companycode, orgcode);
                                        databaseHelper.addMasterdata(master);
                                    }

                                    progressDialog.dismiss();

                                    callback.onSuccess(true);

                                    Toast.makeText(activity, "Masters Successfully Updated", Toast.LENGTH_SHORT).show();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    callback.onSuccess(false);
                                    progressDialog.dismiss();
                                    if (e.toString().contains(activity.getResources().getString(R.string.noresponse)))
                                        Toast.makeText(activity, activity.getResources().getString(R.string.notconnectedserver), Toast.LENGTH_SHORT).show();
                                    else {
                                        databaseHelper.addJSONERROR(e, subject);
                                    }

                                }
                            }

                        }

                    },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    progressDialog.dismiss();
                                    callback.onSuccess(false);
                                    String message = Constant.VolleyErrorMessage(error);
                                    if (!message.equals(""))
                                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                                }
                            });
                    AppController.getInstance().getRequestQueue().getCache().clear();
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    AppController.getInstance().addToRequestQueue(stringRequest);
                }
            });
            final AlertDialog dialog = alertdialog.create();
            dialog.show();
        }

    }


}
