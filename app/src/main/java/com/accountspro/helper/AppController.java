package com.accountspro.helper;


import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;

import com.google.android.material.snackbar.Snackbar;

import androidx.multidex.MultiDex;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatDelegate;

import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Toast;

import com.accountspro.R;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;


public class AppController extends Application {

    public static final String TAG = AppController.class
            .getSimpleName();

    private RequestQueue mRequestQueue;
    private static AppController mInstance;
    private com.android.volley.toolbox.ImageLoader mImageLoader;
    SharedPreferences sharedPref;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        sharedPref = this.getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);

        FontsOverride.setDefaultFont(this, "DEFAULT", "ralewayregular.ttf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "ralewayregular.ttf");
        FontsOverride.setDefaultFont(this, "SERIF", "ralewayregular.ttf");
        FontsOverride.setDefaultFont(this, "SANS_SERIF", "ralewayregular.ttf");

        /*AppSignatureHelper appSignatureHelper = new AppSignatureHelper(this);
        System.out.println("=====Application -> "+appSignatureHelper.getAppSignatures());*/
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }


    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    /*public com.android.volley.toolbox.ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,new BitmapCache());
        }
        return this.mImageLoader;
    }*/


    public static Boolean isConnected(final Activity activity,View viewdata) {
        Boolean check = false;
        try {
            ConnectivityManager ConnectionManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnected() == true) {
                check = true;
            } else {
                Toast.makeText(activity, "Internet Not Connected, You are in Offline Mode", Toast.LENGTH_SHORT).show();

                /*if(viewdata == null)
                    viewdata = activity.findViewById(android.R.id.content);

                Snackbar snackbar = Snackbar
                        .make(viewdata, "Offline Mode", Snackbar.LENGTH_INDEFINITE)
                        .setAction("Connect ?", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = activity.getIntent();
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                activity.startActivity(intent);
                                activity.finish();
                            }
                        });
                snackbar.show();*/
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return check;
    }

    public static Boolean isNetConnected(final Activity activity) {
        Boolean check = false;
        try {
            ConnectivityManager ConnectionManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = ConnectionManager.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnected() == true) {
                check = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return check;
    }

    public String readToken() {
        return sharedPref.getString("TOKEN", "");
    }

    public void saveToken(String fcm) {
        sharedPref.edit().putString("TOKEN", fcm).apply();
    }

    public static String getUniqueIMEIId(UserSessionManager session, Activity activity) {
        String imei = "";
        try {
            TelephonyManager telephonyManager = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);

            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                return "";
            }
            imei = telephonyManager.getDeviceId();
            //Log.e("imei", "=" + imei);
            /*if (imei.equals("") && imei == null && imei.isEmpty()) {
                imei = android.os.Build.SERIAL;
            }*/
            session.setData(UserSessionManager.KEY_IMEI, imei);

            DisplayMetrics metrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);


            imei = "&imei=" + imei + "&deviceinfo=Brand:" + Build.BRAND + " , Manufacturer:" + Build.MANUFACTURER + " , Model:" + Build.MODEL + " , Version:" + Build.VERSION.RELEASE + " ,displayheight: " + metrics.heightPixels + " ,displaywidth: " + metrics.widthPixels + " ,xdpi: " + metrics.xdpi + " ,ydpi: " + metrics.ydpi;
            //System.out.println("=============  === imei> "+imei);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imei;
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


}
