package com.accountspro.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.accountspro.model.Company;
import com.accountspro.model.Master;
import com.accountspro.model.Organization;
import com.accountspro.model.UserInfo;
import com.accountspro.model.VoucherInfo;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String LOG = "DatabaseHelper";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "dbaccountspro";


    public static final String TABLE_USERINFO = "tbluserinfo";
    public static final String USER_MOBILENO = "umobileno";
    public static final String USER_DEVICEINFO = "udeviceinfo";
    public static final String USER_PIN = "userpin";
    public static final String USER_OTP = "userotp";
    public static final String USER_REGI = "userregi";
    public static final String USER_DEMO = "userdemo";

    public static final String TABLE_ORGANIZATION = "tblorganization";
    public static final String ORG_LIECEKEY = "licencekey";
    public static final String ORG_NAME = "organization_name";
    public static final String ORG_REGKEY = "regkey";

    public static final String TABLE_ERROR = "tblerror";
    public static final String ERROR_INFO = "errinfo";
    public static final String ERROR_SUBJECT = "errsubject";

    public static final String TABLE_COMPANY = "tblcompany";
    public static final String COM_CODE = "compcode";
    public static final String COM_NAME = "compname";
    public static final String COM_ACYEAR = "acyear";
    public static final String COM_YEARCODE = "yearcode";
    public static final String COM_ORGREG = "orgreg";
    public static final String COM_ADDRESS = "address";
    public static final String COM_MODULECODE = "modulecode";
    public static final String COM_BILLWISEREQD = "billwisereqd";
    public static final String COM_REGIONREQD = "regionreqd";
    public static final String COM_PAYROLLREQD = "payrollreqd";
    public static final String COM_BATCHREQD = "batchreqd";
    public static final String COM_LEGACYSCAN = "legacyscan";
    public static final String COM_SALESSTATEMENTREQD = "salesstatementreqd";

    public static final String TABLE_MASTER = "tblmaster";
    public static final String MASTER_RECCODE = "mreccode";
    public static final String MASTER_DATATYPE = "mdatatype";
    public static final String MASTER_MASTERCODE = "mmastercode";
    public static final String MASTER_MASTERNAME = "mmastername";
    public static final String MASTER_DTL1 = "mdtl1";
    public static final String MASTER_DTL2 = "mdtl2";
    public static final String MASTER_DTL3 = "mdtl3";
    public static final String MASTER_DTL4 = "mdtl4";
    public static final String MASTER_DTL5 = "mdtl5";
    public static final String MASTER_COMPANYCODE = "mcompcode";
    public static final String MASTER_ORGREG = "morgcode";

    public static final String TABLE_BUSINESS = "tblbusiness";
    public static final String BUSINESS_STARTDATE = "bstdate";
    public static final String BUSINESS_ENDTDATE = "benddate";
    public static final String BUSINESS_COMPANYCODE = "bcompcode";
    public static final String BUSINESS_ORGCODE = "borgpcode";
    public static final String BUSINESS_MAINDATA = "bdatalist";

    public static final String TABLE_RECEIVABLE = "tblreceivable";
    public static final String RECEIVABLE_ASDATE = "asdate";
    public static final String RECEIVABLE_SEARCHBY = "searchby";
    public static final String RECEIVABLE_SEARCHVALUE = "searchvalue";
    public static final String RECEIVABLE_MAINDATA = "maindata";
    public static final String RECEIVABLE_COMPANYCODE = "rcompcode";
    public static final String RECEIVABLE_ORGCODE = "rorgcode";

    public static final String TABLE_STOCKSTATUS = "tblstockstatus";
    public static final String STOCKSTATUS_ASDATE = "stasdate";
    public static final String STOCKSTATUS_SEARCHBY = "searchby";
    public static final String STOCKSTATUS_SEARCHVALUE = "searchvalue";
    public static final String STOCKSTATUS_MAINDATA = "maindata";
    public static final String STOCKSTATUS_COMPANYCODE = "stcompcode";
    public static final String STOCKSTATUS_ORGCODE = "storgcode";
    public static final String STOCKSTATUS_REORDER = "streorder";


    public static final String TABLE_VOUCHERINFO = "tblvoucherinfo";
    public static final String VOUCHERINFO_MAINDATA = "vchmaindata";
    public static final String VOUCHERINFO_COMPANYCODE = "vccompcode";
    public static final String VOUCHERINFO_ORGREG = "vcorgcode";
    public static final String VOUCHERINFO_VCHNO = "vchno";
    public static final String VOUCHERINFO_VCHCODE = "vchcode";
    public static final String VOUCHERINFO_VCHTYPECODE = "vchtypecode";

    public static final String TABLE_LEDGERREPORT = "tblledgerreport";
    public static final String LEDGERREPORT_MAINDATA = "lrmaindata";
    public static final String LEDGERREPORT_COMPANYCODE = "lrcompcode";
    public static final String LEDGERREPORT_ORGREG = "lrorgcode";
    public static final String LEDGERREPORT_STARTDATE = "lrstdate";
    public static final String LEDGERREPORT_ENDDATE = "lrenddate";
    public static final String LEDGERREPORT_NARRATION = "lrnarration";
    public static final String LEDGERREPORT_LEDGERNAMECODE = "lrnamecode";

    public static final String TABLE_SALESREPORT = "tblsalesreport";
    public static final String SALESREPORT_MAINDATA = "srmaindata";
    public static final String SALESREPORT_COMPANYCODE = "srcompcode";
    public static final String SALESREPORT_ORGREG = "srorgcode";
    public static final String SALESREPORT_STARTDATE = "srstdate";
    public static final String SALESREPORT_ENDDATE = "srenddate";
    public static final String SALESREPORT_VCHTYPECODE = "srvchtypecode";


    Context mContext;

    //table and column creation
    private static String userinfo = TABLE_USERINFO + "(" + USER_MOBILENO + " TEXT," + USER_DEVICEINFO + " TEXT ," + USER_PIN + " TEXT," + USER_REGI + " TEXT," + USER_DEMO + " TEXT," + USER_OTP + " TEXT" + ")";
    private static String organization = TABLE_ORGANIZATION + "(" + ORG_LIECEKEY + " TEXT," + ORG_NAME + " TEXT ," + ORG_REGKEY + " TEXT" + ")";
    private static String company = TABLE_COMPANY + "(" + COM_CODE + " TEXT," + COM_NAME + " TEXT ," + COM_ACYEAR + " TEXT ," + COM_YEARCODE + " TEXT ," + COM_ORGREG + " TEXT," + COM_ADDRESS + " TEXT," + COM_MODULECODE + " TEXT," + COM_BILLWISEREQD + " TEXT," + COM_REGIONREQD + " TEXT," + COM_PAYROLLREQD + " TEXT," + COM_BATCHREQD + " TEXT," + COM_LEGACYSCAN + " TEXT," + COM_SALESSTATEMENTREQD + " TEXT" + ")";
    private static String master = TABLE_MASTER + "(" + MASTER_RECCODE + " TEXT," + MASTER_DATATYPE + " TEXT ," + MASTER_MASTERCODE + " TEXT ," + MASTER_MASTERNAME + " TEXT," + MASTER_DTL1 + " TEXT," + MASTER_DTL2 + " TEXT," + MASTER_DTL3 + " TEXT," + MASTER_DTL4 + " TEXT," + MASTER_DTL5 + " TEXT," + MASTER_COMPANYCODE + " TEXT," + MASTER_ORGREG + " TEXT" + ")";
    private static String business = TABLE_BUSINESS + "(" + BUSINESS_ORGCODE + " TEXT," + BUSINESS_COMPANYCODE + " TEXT," + BUSINESS_STARTDATE + " TEXT ," + BUSINESS_ENDTDATE + " TEXT ," + BUSINESS_MAINDATA + " TEXT" + ")";
    private static String receivable = TABLE_RECEIVABLE + "(" + RECEIVABLE_ASDATE + " TEXT," + RECEIVABLE_SEARCHBY + " TEXT ," + RECEIVABLE_SEARCHVALUE + " TEXT ," + RECEIVABLE_MAINDATA + " TEXT," + RECEIVABLE_ORGCODE + " TEXT," + RECEIVABLE_COMPANYCODE + " TEXT" + ")";
    private static String voucher = TABLE_VOUCHERINFO + "(" + VOUCHERINFO_VCHTYPECODE + " TEXT," + VOUCHERINFO_VCHCODE + " TEXT," + VOUCHERINFO_VCHNO + " TEXT," + VOUCHERINFO_MAINDATA + " TEXT," + VOUCHERINFO_COMPANYCODE + " TEXT," + VOUCHERINFO_ORGREG + " TEXT" + ")";
    private static String ledgerreport = TABLE_LEDGERREPORT + "(" + LEDGERREPORT_LEDGERNAMECODE + " TEXT," + LEDGERREPORT_NARRATION + " TEXT," + LEDGERREPORT_COMPANYCODE + " TEXT," + LEDGERREPORT_ORGREG + " TEXT," + LEDGERREPORT_STARTDATE + " TEXT," + LEDGERREPORT_ENDDATE + " TEXT," + LEDGERREPORT_MAINDATA + " TEXT" + ")";
    private static String salesreport = TABLE_SALESREPORT + "(" + SALESREPORT_ORGREG + " TEXT," + SALESREPORT_COMPANYCODE + " TEXT," + SALESREPORT_STARTDATE + " TEXT," + SALESREPORT_ENDDATE + " TEXT," + SALESREPORT_VCHTYPECODE + " TEXT," + SALESREPORT_MAINDATA + " TEXT" + ")";
    private static String errordata = TABLE_ERROR + "(" + ERROR_SUBJECT + " TEXT," + ERROR_INFO + " TEXT" + ")";
    private static String stockstatus = TABLE_STOCKSTATUS + "(" + STOCKSTATUS_ASDATE + " TEXT," + STOCKSTATUS_SEARCHBY + " TEXT ," + STOCKSTATUS_SEARCHVALUE + " TEXT ," + STOCKSTATUS_MAINDATA + " TEXT," + STOCKSTATUS_COMPANYCODE + " TEXT," + STOCKSTATUS_ORGCODE + " TEXT," + STOCKSTATUS_REORDER + " TEXT" + ")";


    // Tag table create statement
    private static final String CREATE_TABLE_USERINFO = "CREATE TABLE " + userinfo;
    private static final String CREATE_TABLE_ORGANIZATION = "CREATE TABLE " + organization;
    private static final String CREATE_TABLE_COMPANY = "CREATE TABLE " + company;
    private static final String CREATE_TABLE_MASTER = "CREATE TABLE " + master;
    private static final String CREATE_TABLE_BUSINESS = "CREATE TABLE " + business;
    private static final String CREATE_TABLE_RECEIVABLE = "CREATE TABLE " + receivable;
    private static final String CREATE_TABLE_VOUCHERINFO = "CREATE TABLE " + voucher;
    private static final String CREATE_TABLE_LEDGERREPORT = "CREATE TABLE " + ledgerreport;
    private static final String CREATE_TABLE_SALESREPORT = "CREATE TABLE " + salesreport;
    private static final String CREATE_TABLE_ERROR = "CREATE TABLE " + errordata;
    private static final String CREATE_TABLE_STOCKSTATUS = "CREATE TABLE " + stockstatus;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_USERINFO);
        db.execSQL(CREATE_TABLE_ORGANIZATION);
        db.execSQL(CREATE_TABLE_COMPANY);
        db.execSQL(CREATE_TABLE_MASTER);
        db.execSQL(CREATE_TABLE_BUSINESS);
        db.execSQL(CREATE_TABLE_RECEIVABLE);
        db.execSQL(CREATE_TABLE_VOUCHERINFO);
        db.execSQL(CREATE_TABLE_LEDGERREPORT);
        db.execSQL(CREATE_TABLE_SALESREPORT);
        db.execSQL(CREATE_TABLE_ERROR);
        db.execSQL(CREATE_TABLE_STOCKSTATUS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        /*db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERINFO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ORGANIZATION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COMPANY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MASTER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BUSINESS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECEIVABLE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_VOUCHERINFO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LEDGERREPORT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SALESREPORT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ERROR);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STOCKSTATUS);
        onCreate(db);*/

        //System.out.println("=================  ==version - " + oldVersion + " == " + newVersion);

        replaceDataToNewTable(db, TABLE_USERINFO, userinfo);
        replaceDataToNewTable(db, TABLE_ORGANIZATION, organization);
        replaceDataToNewTable(db, TABLE_COMPANY, company);
        replaceDataToNewTable(db, TABLE_MASTER, master);
        replaceDataToNewTable(db, TABLE_BUSINESS, business);
        replaceDataToNewTable(db, TABLE_RECEIVABLE, receivable);
        replaceDataToNewTable(db, TABLE_VOUCHERINFO, voucher);
        replaceDataToNewTable(db, TABLE_LEDGERREPORT, ledgerreport);
        replaceDataToNewTable(db, TABLE_SALESREPORT, salesreport);
        replaceDataToNewTable(db, TABLE_ERROR, errordata);
        replaceDataToNewTable(db, TABLE_STOCKSTATUS, stockstatus);

    }


    private void replaceDataToNewTable(SQLiteDatabase db, String tableName, String tableString) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + tableString);

        List<String> columns = getColumns(db, tableName);
        db.execSQL("ALTER TABLE " + tableName + " RENAME TO temp_" + tableName);
        db.execSQL("CREATE TABLE " + tableString);

        columns.retainAll(getColumns(db, tableName));
        String cols = join(columns, ",");
        db.execSQL(String.format("INSERT INTO %s (%s) SELECT %s from temp_%s",
                tableName, cols, cols, tableName));
        db.execSQL("DROP TABLE temp_" + tableName);
    }

    private List<String> getColumns(SQLiteDatabase db, String tableName) {
        List<String> ar = null;
        Cursor c = null;
        try {
            c = db.rawQuery("select * from " + tableName + " limit 1", null);
            if (c != null) {
                ar = new ArrayList<String>(Arrays.asList(c.getColumnNames()));
            }
        } catch (Exception e) {
            addExceptionERROR(e, Constant.ERRORID + " | DATABASE ERROR | " + tableName);
            e.printStackTrace();
        } finally {
            if (c != null)
                c.close();
        }
        return ar;
    }

    private String join(List<String> list, String divider) {
        StringBuilder buf = new StringBuilder();
        int num = list.size();
        for (int i = 0; i < num; i++) {
            if (i != 0)
                buf.append(divider);
            buf.append(list.get(i));
        }
        return buf.toString();
    }


    public long addUserdata(String mobileno, String deviceinfo, String pin, String otp, String regi, String demo) {
        long desc_id = 0;
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(USER_MOBILENO, mobileno);
            values.put(USER_DEVICEINFO, deviceinfo);
            values.put(USER_PIN, pin);
            values.put(USER_OTP, otp);
            values.put(USER_REGI, regi);
            values.put(USER_DEMO, demo);

            // insert row
            desc_id = db.insert(TABLE_USERINFO, null, values);


            db.close();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            addIllegalStateERROR(e, Constant.ERRORID + " | DATABASE ERROR | ADDUSERDATA");
        } catch (Exception e) {
            e.printStackTrace();
            addExceptionERROR(e, Constant.ERRORID + " | DATABASE ERROR | ADDUSERDATA");
        }
        return desc_id;
    }

    public void addParseERROR(ParseException e, String subject) {

        //String errorinfo = "";
        StringBuilder errorinfo = new StringBuilder();
        StackTraceElement[] stackTrace = e.getStackTrace();

        errorinfo.append("\n").append(e.getMessage()).append("\n");
        for (int i = 0; i < stackTrace.length; i++) {
            errorinfo.append("\n").append(stackTrace[i].getLineNumber()).append(" ").append(stackTrace[i].getMethodName())/*.append(" in class ").append(stackTrace[i].getClassName())*/.append(" of ").append(stackTrace[i].getFileName());
        }

        AddERROR(subject, errorinfo.toString());
    }

    public void addExceptionERROR(Exception e, String subject) {
        StringBuilder errorinfo = new StringBuilder();
        StackTraceElement[] stackTrace = e.getStackTrace();
        errorinfo.append("\n").append(e.getMessage()).append("\n");
        for (int i = 0; i < stackTrace.length; i++) {
            errorinfo.append("\n").append(stackTrace[i].getLineNumber()).append(" ").append(stackTrace[i].getMethodName())/*.append(" in class ").append(stackTrace[i].getClassName())*/.append(" of ").append(stackTrace[i].getFileName());
        }

        AddERROR(subject, errorinfo.toString());
    }

    public void addIllegalStateERROR(IllegalStateException e, String subject) {
        StringBuilder errorinfo = new StringBuilder();
        StackTraceElement[] stackTrace = e.getStackTrace();
        errorinfo.append("\n").append(e.getMessage()).append("\n");
        for (int i = 0; i < stackTrace.length; i++) {
            errorinfo.append("\n").append(stackTrace[i].getLineNumber()).append(" ").append(stackTrace[i].getMethodName())/*.append(" in class ").append(stackTrace[i].getClassName())*/.append(" of ").append(stackTrace[i].getFileName());
        }

        AddERROR(subject, errorinfo.toString());
    }

    public void addJSONERROR(JSONException e, String subject) {
        StringBuilder errorinfo = new StringBuilder();
        StackTraceElement[] stackTrace = e.getStackTrace();
        errorinfo.append("\n").append(e.getMessage()).append("\n");
        for (int i = 0; i < stackTrace.length; i++) {
            errorinfo.append("\n").append(stackTrace[i].getLineNumber()).append(" ").append(stackTrace[i].getMethodName())/*.append(" in class ").append(stackTrace[i].getClassName())*/.append(" of ").append(stackTrace[i].getFileName());
        }

        AddERROR(subject, errorinfo.toString());
    }

    public void addUnSuEncodingERROR(UnsupportedEncodingException e, String subject) {
        StringBuilder errorinfo = new StringBuilder();
        StackTraceElement[] stackTrace = e.getStackTrace();
        errorinfo.append("\n").append(e.getMessage()).append("\n");
        for (int i = 0; i < stackTrace.length; i++) {
            errorinfo.append("\n").append(stackTrace[i].getLineNumber()).append(" ").append(stackTrace[i].getMethodName())/*.append(" in class ").append(stackTrace[i].getClassName())*/.append(" of ").append(stackTrace[i].getFileName());
        }

        AddERROR(subject, errorinfo.toString());
    }

    public void addArithmeticERROR(ArithmeticException e, String subject) {
        StringBuilder errorinfo = new StringBuilder();
        StackTraceElement[] stackTrace = e.getStackTrace();
        errorinfo.append("\n").append(e.getMessage()).append("\n");
        for (int i = 0; i < stackTrace.length; i++) {
            errorinfo.append("\n").append(stackTrace[i].getLineNumber()).append(" ").append(stackTrace[i].getMethodName())/*.append(" in class ").append(stackTrace[i].getClassName())*/.append(" of ").append(stackTrace[i].getFileName());
        }

        AddERROR(subject, errorinfo.toString());
    }

    public void addNumberERROR(NumberFormatException e, String subject) {
        StringBuilder errorinfo = new StringBuilder();
        StackTraceElement[] stackTrace = e.getStackTrace();
        errorinfo.append("\n").append(e.getMessage()).append("\n");
        for (int i = 0; i < stackTrace.length; i++) {
            errorinfo.append("\n").append(stackTrace[i].getLineNumber()).append(" ").append(stackTrace[i].getMethodName())/*.append(" in class ").append(stackTrace[i].getClassName())*/.append(" of ").append(stackTrace[i].getFileName());
        }

        AddERROR(subject, errorinfo.toString());
    }

    public void addIOERROR(IOException e, String subject) {
        StringBuilder errorinfo = new StringBuilder();
        StackTraceElement[] stackTrace = e.getStackTrace();
        errorinfo.append("\n").append(e.getMessage()).append("\n");
        for (int i = 0; i < stackTrace.length; i++) {
            errorinfo.append("\n").append(stackTrace[i].getLineNumber()).append(" ").append(stackTrace[i].getMethodName())/*.append(" in class ").append(stackTrace[i].getClassName())*/.append(" of ").append(stackTrace[i].getFileName());
        }

        AddERROR(subject, errorinfo.toString());
    }

    public void addFileNotFoundERROR(FileNotFoundException e, String subject) {
        StringBuilder errorinfo = new StringBuilder();
        StackTraceElement[] stackTrace = e.getStackTrace();

        errorinfo.append("\n").append(e.getMessage()).append("\n");
        for (int i = 0; i < stackTrace.length; i++) {
            errorinfo.append("\n").append(stackTrace[i].getLineNumber()).append(" ").append(stackTrace[i].getMethodName())/*.append(" in class ").append(stackTrace[i].getClassName())*/.append(" of ").append(stackTrace[i].getFileName());
        }

        AddERROR(subject, errorinfo.toString());
    }

    public void AddERROR(String subject, String errorinfo) {
        try {
            Date c = Calendar.getInstance().getTime();
            SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm a");

            errorinfo = df.format(c) + errorinfo;

            //System.out.println("===============err add " + subject + " == " + errorinfo);
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(ERROR_INFO, errorinfo);
            values.put(ERROR_SUBJECT, subject);

            long desc_id = db.insert(TABLE_ERROR, null, values);

            db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public HashMap<String, String> getERROR() {
        HashMap<String, String> translst = new HashMap<>();
        try {
            String selectQuery = "SELECT  * FROM " + TABLE_ERROR;

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);
            //System.out.println("===============err get - ");
            c.moveToFirst();
            if (c.moveToFirst()) {
                do {
                    //System.out.println("===============err get - " + c.getString(c.getColumnIndex(ERROR_SUBJECT)) + " == " + c.getString(c.getColumnIndex(ERROR_INFO)));
                    translst.put(c.getString(c.getColumnIndex(ERROR_SUBJECT)), c.getString(c.getColumnIndex(ERROR_INFO)));
                } while (c.moveToNext());
            }

            if (c.getCount() != 0)
                c.close();
            db.close();

        } catch (IllegalStateException e) {
            e.printStackTrace();
            addIllegalStateERROR(e, Constant.ERRORID + " | DATABASE ERROR | GETERROR");
        } catch (Exception e) {
            e.printStackTrace();
            addExceptionERROR(e, Constant.ERRORID + " | DATABASE ERROR | GETERROR");
        }
        return translst;
    }

    public long UpdateUserdata(String mobileno, String pin) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(USER_PIN, pin);
        return db.update(TABLE_USERINFO, values, USER_MOBILENO + " = ?", new String[]{mobileno});
    }

    public long addOrgdata(Organization organization) {
        long desc_id = 0;
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(ORG_LIECEKEY, organization.getLicencekey());
            values.put(ORG_NAME, organization.getOrganization_name());
            values.put(ORG_REGKEY, organization.getRegkey());

            // insert row
            desc_id = db.insert(TABLE_ORGANIZATION, null, values);

            db.close();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            addIllegalStateERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        } catch (Exception e) {
            e.printStackTrace();
            addExceptionERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        }
        return desc_id;
    }

    public long addCompanydata(Company company) {
        long desc_id = 0;
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COM_CODE, company.getCompcode());
            values.put(COM_ACYEAR, company.getAcyear());
            values.put(COM_NAME, company.getCompname());
            values.put(COM_YEARCODE, company.getYearcode());
            values.put(COM_ORGREG, company.getOrgreg());
            values.put(COM_ADDRESS, company.getAddress());
            values.put(COM_MODULECODE, company.getModulecode());
            values.put(COM_BILLWISEREQD, company.getBillwisereqd());
            values.put(COM_REGIONREQD, company.getRegionreqd());
            values.put(COM_PAYROLLREQD, company.getPayrollreqd());
            values.put(COM_BATCHREQD, company.getBatchreqd());
            values.put(COM_LEGACYSCAN, company.getLegacyscan());
            values.put(COM_SALESSTATEMENTREQD, company.getSalesstatementreqd());

            // insert row
            desc_id = db.insert(TABLE_COMPANY, null, values);


            db.close();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            addIllegalStateERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        } catch (Exception e) {
            e.printStackTrace();
            addExceptionERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        }
        return desc_id;
    }

    public long addMasterdata(Master master) {
        long desc_id = 0;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(MASTER_RECCODE, master.getReccode());
            values.put(MASTER_DATATYPE, master.getDatatype());
            values.put(MASTER_MASTERCODE, master.getMastercode());
            values.put(MASTER_MASTERNAME, master.getMastername());
            values.put(MASTER_DTL1, master.getDtl1());
            values.put(MASTER_DTL2, master.getDtl2());
            values.put(MASTER_DTL3, master.getDtl3());
            values.put(MASTER_DTL4, master.getDtl4());
            values.put(MASTER_DTL5, master.getDtl5());
            values.put(MASTER_COMPANYCODE, master.getCompcode());
            values.put(MASTER_ORGREG, master.getOrgcode());

            // insert row
            desc_id = db.insert(TABLE_MASTER, null, values);


            db.close();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            addIllegalStateERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        } catch (Exception e) {
            e.printStackTrace();
            addExceptionERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        }
        return desc_id;
    }

    public long addReceivabledata(String ccode, String asdate, String searchby, String searchvalue, String maindata, String orgcode) {

        long desc_id = 0;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(RECEIVABLE_COMPANYCODE, ccode);
            values.put(RECEIVABLE_ASDATE, asdate);
            values.put(RECEIVABLE_SEARCHBY, searchby);
            values.put(RECEIVABLE_SEARCHVALUE, searchvalue);
            values.put(RECEIVABLE_MAINDATA, maindata);
            values.put(RECEIVABLE_ORGCODE, orgcode);

            // insert row
            desc_id = db.insert(TABLE_RECEIVABLE, null, values);

            db.close();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            addIllegalStateERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        } catch (Exception e) {
            e.printStackTrace();
            addExceptionERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        }
        return desc_id;
    }

    public String getReceivableData(String orgcode, String ccode, String asdate, String searchby, String searchvalue) {
        String value = "";
        try {
            //System.out.println("========== = = =" + orgcode + " = " + ccode + " = " + asdate + " = " + searchby + " = " + searchvalue);
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery("SELECT * FROM " + TABLE_RECEIVABLE + " WHERE " + RECEIVABLE_ORGCODE + " = ? AND " + RECEIVABLE_COMPANYCODE + " = ? AND " + RECEIVABLE_ASDATE + " = ? AND " + RECEIVABLE_SEARCHBY + " = ? AND " + RECEIVABLE_SEARCHVALUE + " = ?", new String[]{orgcode, ccode, asdate, searchby, searchvalue});

            c.moveToFirst();
            if (c != null && c.getCount() != 0)
                value = c.getString(c.getColumnIndex(RECEIVABLE_MAINDATA));

            if (c.getCount() != 0)
                c.close();
            db.close();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            addIllegalStateERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        } catch (Exception e) {
            e.printStackTrace();
            addExceptionERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        }
        return value;
    }

    public boolean UpdateReceivableData(String orgcode, String ccode, String asdate, String searchby, String searchvalue, String maindata) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(RECEIVABLE_MAINDATA, maindata);

        db.update(TABLE_RECEIVABLE, values, RECEIVABLE_ORGCODE + " = ?" + " AND " + RECEIVABLE_COMPANYCODE + " = ?" + " AND " + RECEIVABLE_ASDATE + " = ?" + " AND " + RECEIVABLE_SEARCHBY + " = ?" + " AND " + RECEIVABLE_SEARCHVALUE + " = ?", new String[]{orgcode, ccode, asdate, searchby, searchvalue});
        db.close();
        return true;
    }

    public long addBusinessdata(String sdate, String edate, String ccode, String maindata, String orgcode) {

        long desc_id = 0;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(BUSINESS_STARTDATE, sdate);
            values.put(BUSINESS_ENDTDATE, edate);
            values.put(BUSINESS_COMPANYCODE, ccode);
            values.put(BUSINESS_MAINDATA, maindata);
            values.put(BUSINESS_ORGCODE, orgcode);

            desc_id = db.insert(TABLE_BUSINESS, null, values);

            db.close();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            addIllegalStateERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        } catch (Exception e) {
            e.printStackTrace();
            addExceptionERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        }
        return desc_id;
    }

    public boolean UpdateBusinessData(String sdate, String edate, String ccode, String maindata, String orgcode) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(BUSINESS_MAINDATA, maindata);
        db.update(TABLE_BUSINESS, values, BUSINESS_ORGCODE + " = ?" + " AND " + BUSINESS_COMPANYCODE + " = ?" + " AND " + BUSINESS_STARTDATE + " = ?" + " AND " + BUSINESS_ENDTDATE + " = ?", new String[]{orgcode, ccode, sdate, edate});
        db.close();
        return true;
    }

    public String getBusinessData(String sdate, String edate, String ccode, String orgkey) {
        String value = "";
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery("SELECT * FROM " + TABLE_BUSINESS + " WHERE " + BUSINESS_ORGCODE + " = ? AND " + BUSINESS_COMPANYCODE + " = ? AND " + BUSINESS_STARTDATE + " = ? AND " + BUSINESS_ENDTDATE + " = ?", new String[]{orgkey, ccode, sdate, edate});

            c.moveToFirst();
            if (c != null && c.getCount() != 0)
                value = c.getString(c.getColumnIndex(BUSINESS_MAINDATA));

            if (c.getCount() != 0)
                c.close();
            db.close();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            addIllegalStateERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        } catch (Exception e) {
            e.printStackTrace();
            addExceptionERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        }
        return value;
    }

    public void DeleteMasterData(String compnaycode, String orgcode) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL("DELETE FROM " + TABLE_MASTER + " WHERE " + MASTER_ORGREG + " = ? AND " + MASTER_COMPANYCODE + " = ?", new String[]{orgcode, compnaycode});
        //database.execSQL("DELETE FROM " + TABLE_MASTER + " WHERE " + MASTER_COMPANYCODE + "= '" + compnaycode + "'");
        database.close();
    }

    public void DeleteData(String Tablename, String orgreg) {
        SQLiteDatabase database = this.getWritableDatabase();
        if (orgreg.equals(""))
            database.execSQL("DELETE FROM " + Tablename);
        else
            database.execSQL("DELETE FROM " + Tablename + " WHERE " + COM_ORGREG + " = ?", new String[]{orgreg});


        database.close();
    }

    public boolean checkForTableExists(String table) {
        boolean returndata = false;
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String sql = "SELECT name FROM sqlite_master WHERE type='table' AND name='" + table + "'";
            Cursor mCursor = db.rawQuery(sql, null);
            if (mCursor.getCount() > 0) {
                returndata = true;
            }
            if (mCursor.getCount() != 0)
                mCursor.close();
            db.close();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            addIllegalStateERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        } catch (Exception e) {
            e.printStackTrace();
            addExceptionERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        }
        return returndata;
    }

    public UserInfo getAllUserData() {
        UserInfo userInfo = null;

        try {
            String selectQuery = "SELECT  * FROM " + TABLE_USERINFO;

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);

            c.moveToFirst();
            if (c.moveToFirst()) {
                do {
                    userInfo = new UserInfo(c.getString(c.getColumnIndex(USER_MOBILENO)), c.getString(c.getColumnIndex(USER_PIN)), c.getString(c.getColumnIndex(USER_OTP)), c.getString(c.getColumnIndex(USER_DEVICEINFO)), c.getString(c.getColumnIndex(USER_REGI)), c.getString(c.getColumnIndex(USER_DEMO)));
                } while (c.moveToNext());
            }

            if (c.getCount() != 0)
                c.close();
            db.close();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            addIllegalStateERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        } catch (Exception e) {
            e.printStackTrace();
            addExceptionERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        }
        return userInfo;
    }

    public ArrayList<Organization> getAllOrganization() {
        ArrayList<Organization> translst = new ArrayList<Organization>();

        try {
            String selectQuery = "SELECT  * FROM " + TABLE_ORGANIZATION;

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);

            c.moveToFirst();
            if (c.moveToFirst()) {
                do {
                    translst.add(new Organization(c.getString(c.getColumnIndex(ORG_REGKEY)), c.getString(c.getColumnIndex(ORG_NAME)), c.getString(c.getColumnIndex(ORG_LIECEKEY))));
                } while (c.moveToNext());
            }

            if (c.getCount() != 0)
                c.close();
            db.close();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            addIllegalStateERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        } catch (Exception e) {
            e.printStackTrace();
            addExceptionERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        }
        return translst;
    }

    public boolean IsMasterExists(String compnaycode, String orgcode) {
        boolean isexist = false;
        try {
            Cursor cursor = null;
            //String sql = "SELECT " + MASTER_COMPANYCODE + " FROM " + TABLE_MASTER + " WHERE " + MASTER_COMPANYCODE + "= ?" ,new String[]{compnaycode};
            SQLiteDatabase db = this.getReadableDatabase();
            cursor = db.rawQuery("SELECT * FROM " + TABLE_MASTER + " WHERE " + MASTER_ORGREG + " = ? AND " + MASTER_COMPANYCODE + " = ?", new String[]{orgcode, compnaycode});
            //cursor = db.rawQuery("SELECT * FROM " + TABLE_MASTER + " WHERE " + MASTER_COMPANYCODE + " = ?" ,new String[]{compnaycode});

            if (cursor.getCount() > 0) {
                isexist = true;
            } else {
                isexist = false;
            }

            if (cursor.getCount() != 0)
                cursor.close();
            db.close();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            addIllegalStateERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        } catch (Exception e) {
            e.printStackTrace();
            addExceptionERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        }
        return isexist;
    }

    public ArrayList<Master> getAllMasterData(String compnaycode, String orgcode) {
        ArrayList<Master> translst = new ArrayList<Master>();

        try {
            //String selectQuery = "SELECT  * FROM " + TABLE_MASTER+" WHERE "+MASTER_COMPANYCODE+"="+compnaycode;
            //String selectQuery = "SELECT  * FROM " + TABLE_MASTER + " WHERE " + MASTER_COMPANYCODE + " = '" + compnaycode + "'";
            SQLiteDatabase db = this.getReadableDatabase();
            //Cursor c = db.rawQuery(selectQuery, null);
            Cursor c = db.rawQuery("SELECT * FROM " + TABLE_MASTER + " WHERE " + MASTER_ORGREG + " = ? AND " + MASTER_COMPANYCODE + " = ?", new String[]{orgcode, compnaycode});
            c.moveToFirst();
            if (c.moveToFirst()) {
                do {
                    //String reccode, String datatype, String mastercode, String mastername, String dtl1, String dtl2, String dtl3, String dtl4, String dtl5, String compcode
                    translst.add(new Master(c.getString(c.getColumnIndex(MASTER_RECCODE)), c.getString(c.getColumnIndex(MASTER_DATATYPE)), c.getString(c.getColumnIndex(MASTER_MASTERCODE)), c.getString(c.getColumnIndex(MASTER_MASTERNAME)), c.getString(c.getColumnIndex(MASTER_DTL1)), c.getString(c.getColumnIndex(MASTER_DTL2)), c.getString(c.getColumnIndex(MASTER_DTL3)), c.getString(c.getColumnIndex(MASTER_DTL4)), c.getString(c.getColumnIndex(MASTER_DTL5)), c.getString(c.getColumnIndex(MASTER_COMPANYCODE)), c.getString(c.getColumnIndex(MASTER_ORGREG))));
                } while (c.moveToNext());
            }

            if (c.getCount() != 0)
                c.close();
            db.close();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            addIllegalStateERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        } catch (Exception e) {
            e.printStackTrace();
            addExceptionERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        }
        return translst;
    }

    public ArrayList<Master> getMasterRowData(String compnaycode, String datatype, String orgcode) {
        ArrayList<Master> translst = new ArrayList<Master>();

        try {
            //String selectQuery = "SELECT  * FROM " + TABLE_MASTER + " WHERE " + MASTER_COMPANYCODE + " = '" + compnaycode + "' AND "+ MASTER_DATATYPE +" = '"+datatype+"'"+ "' AND "+ MASTER_ORGREG +" = '"+orgcode+"'";
            SQLiteDatabase db = this.getReadableDatabase();
            //Cursor c = db.rawQuery(selectQuery, null);
            Cursor c = db.rawQuery("SELECT * FROM " + TABLE_MASTER + " WHERE " + MASTER_ORGREG + " = ? AND " + MASTER_COMPANYCODE + " = ? AND " + MASTER_DATATYPE + " = ?", new String[]{orgcode, compnaycode, datatype});
            c.moveToFirst();
            if (c.moveToFirst()) {
                do {
                    //System.out.println("============row - "+c.getString(c.getColumnIndex(MASTER_MASTERCODE))+" = "+ c.getString(c.getColumnIndex(MASTER_DATATYPE))+" = "+c.getString(c.getColumnIndex(MASTER_MASTERNAME)));
                    translst.add(new Master(c.getString(c.getColumnIndex(MASTER_RECCODE)), c.getString(c.getColumnIndex(MASTER_DATATYPE)), c.getString(c.getColumnIndex(MASTER_MASTERCODE)), c.getString(c.getColumnIndex(MASTER_MASTERNAME)), c.getString(c.getColumnIndex(MASTER_DTL1)), c.getString(c.getColumnIndex(MASTER_DTL2)), c.getString(c.getColumnIndex(MASTER_DTL3)), c.getString(c.getColumnIndex(MASTER_DTL4)), c.getString(c.getColumnIndex(MASTER_DTL5)), c.getString(c.getColumnIndex(MASTER_COMPANYCODE)), c.getString(c.getColumnIndex(MASTER_ORGREG))));
                } while (c.moveToNext());
            }

            if (c.getCount() != 0)
                c.close();
            db.close();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            addIllegalStateERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        } catch (Exception e) {
            e.printStackTrace();
            addExceptionERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        }
        return translst;
    }

    public ArrayList<Master> getMasterRowData(String compnaycode, String datatype, String orgcode, String colname, String colval) {
        ArrayList<Master> translst = new ArrayList<Master>();

        try {
            //String selectQuery = "SELECT  * FROM " + TABLE_MASTER + " WHERE " + MASTER_COMPANYCODE + " = '" + compnaycode + "' AND "+ MASTER_DATATYPE +" = '"+datatype+"'"+ "' AND "+ MASTER_ORGREG +" = '"+orgcode+"'";
            SQLiteDatabase db = this.getReadableDatabase();
            //Cursor c = db.rawQuery(selectQuery, null);
            Cursor c = db.rawQuery("SELECT * FROM " + TABLE_MASTER + " WHERE " + MASTER_ORGREG + " = ? AND " + MASTER_COMPANYCODE + " = ? AND " + MASTER_DATATYPE + " = ? AND " + colname + " = ?", new String[]{orgcode, compnaycode, datatype, colval});
            c.moveToFirst();
            if (c.moveToFirst()) {
                do {
                    translst.add(new Master(c.getString(c.getColumnIndex(MASTER_RECCODE)), c.getString(c.getColumnIndex(MASTER_DATATYPE)), c.getString(c.getColumnIndex(MASTER_MASTERCODE)), c.getString(c.getColumnIndex(MASTER_MASTERNAME)), c.getString(c.getColumnIndex(MASTER_DTL1)), c.getString(c.getColumnIndex(MASTER_DTL2)), c.getString(c.getColumnIndex(MASTER_DTL3)), c.getString(c.getColumnIndex(MASTER_DTL4)), c.getString(c.getColumnIndex(MASTER_DTL5)), c.getString(c.getColumnIndex(MASTER_COMPANYCODE)), c.getString(c.getColumnIndex(MASTER_ORGREG))));
                } while (c.moveToNext());
            }

            if (c.getCount() != 0) {
                translst.add(new Master("0", "All"));
                c.close();
            }
            db.close();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            addIllegalStateERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        } catch (Exception e) {
            e.printStackTrace();
            addExceptionERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        }
        return translst;
    }

    public String getMasterValue(String compnaycode, String colname, String colvalue, String datatype, String retcolname, String orgcode) {
        String value = null;

        try {
            //String selectQuery = "SELECT  * FROM " + TABLE_MASTER + " WHERE " + MASTER_COMPANYCODE + " = '" + compnaycode + "' AND " + colname + " = '" + colvalue + "' AND " + MASTER_DATATYPE + " = '" + datatype + "'";

            SQLiteDatabase db = this.getReadableDatabase();
            //Cursor c = db.rawQuery(selectQuery, null);
            Cursor c = db.rawQuery("SELECT * FROM " + TABLE_MASTER + " WHERE " + MASTER_ORGREG + " = ? AND " + MASTER_COMPANYCODE + " = ? AND " + MASTER_DATATYPE + " = ? AND " + colname + " = ?", new String[]{orgcode, compnaycode, datatype, colvalue});
            c.moveToFirst();

            DatabaseUtils.dumpCursorToString(c);

            if (c.moveToFirst()) {
                do {
                    //System.out.println("============rowval - "+c.getString(c.getColumnIndex(MASTER_RECCODE))+" = "+ c.getString(c.getColumnIndex(MASTER_DATATYPE))+" = "+c.getString(c.getColumnIndex(MASTER_MASTERNAME)));
                    value = c.getString(c.getColumnIndex(retcolname));
                } while (c.moveToNext());
            }

            if (c.getCount() != 0)
                c.close();
            db.close();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            addIllegalStateERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        } catch (Exception e) {
            e.printStackTrace();
            addExceptionERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        }


        return value;
    }


    public ArrayList<Company> getAllCompany(String orgreg) {
        ArrayList<Company> translst = new ArrayList<Company>();

        try {
            //String selectQuery = "SELECT  * FROM " + TABLE_COMPANY;

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery("SELECT * FROM " + TABLE_COMPANY + " WHERE " + COM_ORGREG + " = ?", new String[]{orgreg});

            c.moveToFirst();
            if (c.moveToFirst()) {
                do {
                    translst.add(new Company(c.getString(c.getColumnIndex(COM_CODE)), c.getString(c.getColumnIndex(COM_NAME)), c.getString(c.getColumnIndex(COM_ACYEAR)), c.getString(c.getColumnIndex(COM_YEARCODE)), c.getString(c.getColumnIndex(COM_ORGREG)), c.getString(c.getColumnIndex(COM_ADDRESS)), c.getString(c.getColumnIndex(COM_MODULECODE)), c.getString(c.getColumnIndex(COM_BILLWISEREQD)), c.getString(c.getColumnIndex(COM_REGIONREQD)), c.getString(c.getColumnIndex(COM_PAYROLLREQD)), c.getString(c.getColumnIndex(COM_BATCHREQD)), c.getString(c.getColumnIndex(COM_LEGACYSCAN)), c.getString(c.getColumnIndex(COM_SALESSTATEMENTREQD))));
                } while (c.moveToNext());
            }

            if (c.getCount() != 0)
                c.close();
            db.close();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            addIllegalStateERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        } catch (Exception e) {
            e.printStackTrace();
            addExceptionERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        }
        return translst;
    }


    public long addVoucherInfodata(String compcode, String orgcode, String maindata, String vchno, String vchcode, String vchtypecode) {
        long desc_id = 0;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(VOUCHERINFO_COMPANYCODE, compcode);
            values.put(VOUCHERINFO_ORGREG, orgcode);
            values.put(VOUCHERINFO_MAINDATA, maindata);
            values.put(VOUCHERINFO_VCHNO, vchno);
            values.put(VOUCHERINFO_VCHCODE, vchcode);
            values.put(VOUCHERINFO_VCHTYPECODE, vchtypecode);


            desc_id = db.insert(TABLE_VOUCHERINFO, null, values);


            db.close();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            addIllegalStateERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        } catch (Exception e) {
            e.printStackTrace();
            addExceptionERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        }
        return desc_id;
    }

    public boolean UpdateVoucherInfoData(String compcode, String orgcode, String maindata, String vchno, String vchcode, String vchtypecode) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(VOUCHERINFO_MAINDATA, maindata);
        db.update(TABLE_VOUCHERINFO, values, VOUCHERINFO_ORGREG + " = ?" + " AND " + VOUCHERINFO_COMPANYCODE + " = ?" + " AND " + VOUCHERINFO_VCHNO + " = ?" + " AND " + VOUCHERINFO_VCHCODE + " = ?" + " AND " + VOUCHERINFO_VCHTYPECODE + " = ?", new String[]{orgcode, compcode, vchno, vchcode, vchtypecode});
        db.close();
        return true;
    }

    public String getVoucherInfoData(String compcode, String orgcode, String vchno, String vchcode, String vchtypecode) {
        //System.out.println("================ = = " + compcode + " = " + orgcode + " = " + vchno + " = " + vchcode + " = " + vchtypecode);
        String value = "";
        try {
            SQLiteDatabase db = this.getReadableDatabase();

            Cursor c = db.rawQuery("SELECT * FROM " + TABLE_VOUCHERINFO + " WHERE " + VOUCHERINFO_ORGREG + " = ? AND " + VOUCHERINFO_COMPANYCODE + " = ? AND " + VOUCHERINFO_VCHNO + " = ? AND " + VOUCHERINFO_VCHCODE + " = ? AND " + VOUCHERINFO_VCHTYPECODE + " = ?", new String[]{orgcode, compcode, vchno, vchcode, vchtypecode});

            c.moveToFirst();
            if (c != null && c.getCount() != 0)
                value = c.getString(c.getColumnIndex(VOUCHERINFO_MAINDATA));

            if (c.getCount() != 0)
                c.close();
            db.close();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            addIllegalStateERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        } catch (Exception e) {
            e.printStackTrace();
            addExceptionERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        }
        return value;
    }

    public long addLedgerReportdata(String compcode, String orgcode, String maindata, String ledgernamecode, String startdate, String enddate, String narration) {

        long desc_id = 0;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(LEDGERREPORT_COMPANYCODE, compcode);
            values.put(LEDGERREPORT_ORGREG, orgcode);
            values.put(LEDGERREPORT_MAINDATA, maindata);
            values.put(LEDGERREPORT_LEDGERNAMECODE, ledgernamecode);
            values.put(LEDGERREPORT_STARTDATE, startdate);
            values.put(LEDGERREPORT_ENDDATE, enddate);
            values.put(LEDGERREPORT_NARRATION, narration);


            desc_id = db.insert(TABLE_LEDGERREPORT, null, values);

            db.close();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            addIllegalStateERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        } catch (Exception e) {
            e.printStackTrace();
            addExceptionERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        }
        return desc_id;
    }

    public boolean UpdateLedgerReportData(String compcode, String orgcode, String maindata, String ledgernamecode, String startdate, String enddate, String narration) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(LEDGERREPORT_MAINDATA, maindata);
        db.update(TABLE_LEDGERREPORT, values, LEDGERREPORT_ORGREG + " = ? AND " + LEDGERREPORT_COMPANYCODE + " = ? AND " + LEDGERREPORT_LEDGERNAMECODE + " = ? AND " + LEDGERREPORT_STARTDATE + " = ? AND " + LEDGERREPORT_ENDDATE + " = ? AND " + LEDGERREPORT_NARRATION + " = ?", new String[]{orgcode, compcode, ledgernamecode, startdate, enddate, narration});
        db.close();
        return true;
    }

    public String getLedgerReportData(String compcode, String orgcode, String ledgernamecode, String startdate, String enddate, String narration) {
        String value = "";
        try {
            SQLiteDatabase db = this.getReadableDatabase();

            if (compcode == null) compcode = "0";
            if (orgcode == null) orgcode = "0";
            if (ledgernamecode == null) ledgernamecode = "0";
            if (startdate == null) startdate = "0";
            if (enddate == null) enddate = "0";
            if (narration == null) narration = "No";

            Cursor c = db.rawQuery("SELECT * FROM " + TABLE_LEDGERREPORT + " WHERE " + LEDGERREPORT_ORGREG + " = ? AND " + LEDGERREPORT_COMPANYCODE + " = ? AND " + LEDGERREPORT_LEDGERNAMECODE + " = ? AND " + LEDGERREPORT_STARTDATE + " = ? AND " + LEDGERREPORT_ENDDATE + " = ? AND " + LEDGERREPORT_NARRATION + " = ?", new String[]{orgcode, compcode, ledgernamecode, startdate, enddate, narration});

            c.moveToFirst();
            if (c != null && c.getCount() != 0)
                value = c.getString(c.getColumnIndex(LEDGERREPORT_MAINDATA));

            if (c.getCount() != 0)
                c.close();
            db.close();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            addIllegalStateERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        } catch (Exception e) {
            e.printStackTrace();
            addExceptionERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        }
        return value;
    }

    public long addSalesReportdata(String compcode, String orgcode, String maindata, String vouchertypecode, String startdate, String enddate) {

        long desc_id = 0;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(SALESREPORT_COMPANYCODE, compcode);
            values.put(SALESREPORT_ORGREG, orgcode);
            values.put(SALESREPORT_MAINDATA, maindata);
            values.put(SALESREPORT_VCHTYPECODE, vouchertypecode);
            values.put(SALESREPORT_STARTDATE, startdate);
            values.put(SALESREPORT_ENDDATE, enddate);

            desc_id = db.insert(TABLE_SALESREPORT, null, values);


            db.close();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            addIllegalStateERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        } catch (Exception e) {
            e.printStackTrace();
            addExceptionERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        }
        return desc_id;
    }

    public boolean UpdateSalesReportData(String compcode, String orgcode, String maindata, String vouchertypecode, String startdate, String enddate) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SALESREPORT_MAINDATA, maindata);
        db.update(TABLE_SALESREPORT, values, SALESREPORT_ORGREG + " = ? AND " + SALESREPORT_COMPANYCODE + " = ? AND " + SALESREPORT_VCHTYPECODE + " = ? AND " + SALESREPORT_STARTDATE + " = ? AND " + SALESREPORT_ENDDATE + " = ?", new String[]{orgcode, compcode, vouchertypecode, startdate, enddate});
        db.close();
        return true;
    }

    public String getSalesReportData(String compcode, String orgcode, String vouchertypecode, String startdate, String enddate) {
        String value = "";

        try {
            SQLiteDatabase db = this.getReadableDatabase();

            Cursor c = db.rawQuery("SELECT * FROM " + TABLE_SALESREPORT + " WHERE " + SALESREPORT_ORGREG + " = ? AND " + SALESREPORT_COMPANYCODE + " = ? AND " + SALESREPORT_VCHTYPECODE + " = ? AND " + SALESREPORT_STARTDATE + " = ? AND " + SALESREPORT_ENDDATE + " = ?", new String[]{orgcode, compcode, vouchertypecode, startdate, enddate});

            c.moveToFirst();
            if (c != null && c.getCount() != 0)
                value = c.getString(c.getColumnIndex(SALESREPORT_MAINDATA));

            if (c.getCount() != 0)
                c.close();
            db.close();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            addIllegalStateERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        } catch (Exception e) {
            e.printStackTrace();
            addExceptionERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        }
        return value;
    }

    public long addStockStatusdata(String ccode, String asdate, String searchby, String searchvalue, String maindata, String orgcode, String reorder) {

        long desc_id = 0;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(STOCKSTATUS_COMPANYCODE, ccode);
            values.put(STOCKSTATUS_ASDATE, asdate);
            values.put(STOCKSTATUS_SEARCHBY, searchby);
            values.put(STOCKSTATUS_SEARCHVALUE, searchvalue);
            values.put(STOCKSTATUS_MAINDATA, maindata);
            values.put(STOCKSTATUS_ORGCODE, orgcode);
            values.put(STOCKSTATUS_REORDER, reorder);

            // insert row
            desc_id = db.insert(TABLE_STOCKSTATUS, null, values);

            db.close();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            addIllegalStateERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        } catch (Exception e) {
            e.printStackTrace();
            addExceptionERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        }
        return desc_id;
    }

    public String getStockStatusData(String orgcode, String ccode, String asdate, String searchby, String searchvalue, String reorder) {
        String value = "";
        try {
            //System.out.println("========== = = =" + orgcode + " = " + ccode + " = " + asdate + " = " + searchby + " = " + searchvalue);
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery("SELECT * FROM " + TABLE_STOCKSTATUS + " WHERE " + STOCKSTATUS_ORGCODE + " = ? AND " + STOCKSTATUS_COMPANYCODE + " = ? AND " + STOCKSTATUS_ASDATE + " = ? AND " + STOCKSTATUS_SEARCHBY + " = ? AND " + STOCKSTATUS_SEARCHVALUE + " = ? AND " + STOCKSTATUS_REORDER + " = ?", new String[]{orgcode, ccode, asdate, searchby, searchvalue, reorder});

            c.moveToFirst();
            if (c != null && c.getCount() != 0)
                value = c.getString(c.getColumnIndex(STOCKSTATUS_MAINDATA));

            if (c.getCount() != 0)
                c.close();
            db.close();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            addIllegalStateERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        } catch (Exception e) {
            e.printStackTrace();
            addExceptionERROR(e, Constant.ERRORID + " | " + Constant.ERRORNOCOMP + " | " + Constant.ERRORNOORG);
        }
        return value;
    }

    public boolean UpdateStockStatusData(String orgcode, String ccode, String asdate, String searchby, String searchvalue, String maindata, String reorder) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(STOCKSTATUS_MAINDATA, maindata);

        db.update(TABLE_STOCKSTATUS, values, STOCKSTATUS_ORGCODE + " = ? AND " + STOCKSTATUS_COMPANYCODE + " = ? AND " + STOCKSTATUS_ASDATE + " = ? AND " + STOCKSTATUS_SEARCHBY + " = ? AND " + STOCKSTATUS_SEARCHVALUE + " = ? AND " + STOCKSTATUS_REORDER + " = ?", new String[]{orgcode, ccode, asdate, searchby, searchvalue, reorder});
        db.close();
        return true;
    }


    public void deleteAllTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_USERINFO, null, null);
        db.delete(TABLE_ORGANIZATION, null, null);
        db.delete(TABLE_COMPANY, null, null);
        db.delete(TABLE_MASTER, null, null);
        db.delete(TABLE_BUSINESS, null, null);
        db.delete(TABLE_RECEIVABLE, null, null);
        db.delete(TABLE_VOUCHERINFO, null, null);
        db.delete(TABLE_LEDGERREPORT, null, null);
        db.delete(TABLE_SALESREPORT, null, null);
        db.delete(TABLE_STOCKSTATUS, null, null);
        db.delete(TABLE_ERROR, null, null);
    }

    public void ClearAllData() {
        SQLiteDatabase database = this.getWritableDatabase();

        database.execSQL("DELETE FROM " + TABLE_BUSINESS);
        database.execSQL("DELETE FROM " + TABLE_RECEIVABLE);
        database.execSQL("DELETE FROM " + TABLE_VOUCHERINFO);
        database.execSQL("DELETE FROM " + TABLE_LEDGERREPORT);
        database.execSQL("DELETE FROM " + TABLE_SALESREPORT);
        database.execSQL("DELETE FROM " + TABLE_STOCKSTATUS);

        database.close();
    }

}
