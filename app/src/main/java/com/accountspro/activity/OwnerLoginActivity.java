package com.accountspro.activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.accountspro.R;
import com.accountspro.helper.AppController;
import com.accountspro.helper.Constant;
import com.accountspro.helper.DatabaseHelper;
import com.accountspro.helper.PinView;
import com.accountspro.helper.UserSessionManager;
import com.accountspro.model.Company;
import com.accountspro.model.Master;
import com.accountspro.model.Organization;
import com.accountspro.model.UserInfo;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class OwnerLoginActivity extends AppCompatActivity {

    UserSessionManager session;
    PinView edtpin;
    DatabaseHelper databaseHelper;
    ProgressBar progressbar;
    LinearLayout lytbottom;
    public final int ReadPhoneStatemain = 0;
    public int ReqReadPermission = 1;
    public int ReqWritePermission = 2;
    Button btnsubmit;
    AsyncTask<String, String, String> fetchtask;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(fetchtask != null)
            fetchtask.cancel(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_owner_login);
        session = new UserSessionManager(OwnerLoginActivity.this);

        databaseHelper = new DatabaseHelper(OwnerLoginActivity.this);
        edtpin = (PinView) findViewById(R.id.edtpin);
        edtpin.requestFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        lytbottom = (LinearLayout) findViewById(R.id.lytbottom);
        btnsubmit = (Button) findViewById(R.id.btnsubmit);

        if (session.getData(UserSessionManager.KEY_REGI).equals("Yes"))
            lytbottom.setVisibility(View.GONE);

        edtpin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 4)
                    OnSubmitClick(edtpin);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        /*if (ContextCompat.checkSelfPermission(OwnerLoginActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(OwnerLoginActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, ReqReadPermission);
        } else if (ContextCompat.checkSelfPermission(OwnerLoginActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(OwnerLoginActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, ReqWritePermission);
        }*/
        if (Constant.CheckReadWritePermissionGranted(OwnerLoginActivity.this))

        if (ContextCompat.checkSelfPermission(OwnerLoginActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(OwnerLoginActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, ReadPhoneStatemain);
        } else {
            if (session.getData(UserSessionManager.KEY_IMEI).equals(""))
                AppController.getUniqueIMEIId(session, OwnerLoginActivity.this);
        }
    }

    public void OnRegisterClick(View view) {
        startActivity(new Intent(OwnerLoginActivity.this, RegisterMobileActivity.class).putExtra("fromlogin", false));
    }

    public void OnLoginClick(View view) {
        startActivity(new Intent(OwnerLoginActivity.this, RegisterMobileActivity.class).putExtra("fromlogin", true));
    }

    public void OnSubmitClick(View view) {

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edtpin.getWindowToken(), 0);

        UserInfo userInfo = null;
        if (databaseHelper.checkForTableExists(DatabaseHelper.TABLE_USERINFO))
            userInfo = databaseHelper.getAllUserData();
        else {
            lytbottom.setVisibility(View.VISIBLE);
            Toast.makeText(OwnerLoginActivity.this, getResources().getString(R.string.deviceinfonotfound), Toast.LENGTH_SHORT).show();
            Toast.makeText(OwnerLoginActivity.this, getResources().getString(R.string.relogin), Toast.LENGTH_SHORT).show();
            return;
        }

        String pin = edtpin.getText().toString().trim();
        if (pin.length() == 0)
            Toast.makeText(OwnerLoginActivity.this, "Enter PIN", Toast.LENGTH_SHORT).show();
        else if (userInfo == null) {
            lytbottom.setVisibility(View.VISIBLE);
            Toast.makeText(OwnerLoginActivity.this, getResources().getString(R.string.relogin), Toast.LENGTH_SHORT).show();
        } else if (userInfo.getPin().equals(pin)) {
            session.setBooleanData(UserSessionManager.IS_USER_LOGIN, true);
            session.setData(UserSessionManager.KEY_LoginType, UserSessionManager.OWNER);
            session.setData(UserSessionManager.KEY_LastLogin, new SimpleDateFormat("dd-MM-yyyy", Locale.US).format(new Date()));
            if (databaseHelper.getAllOrganization().size() != 0 && databaseHelper.getAllCompany(session.getData(UserSessionManager.KEY_ORG_REGKEY)).size() != 0) {
                session.setBooleanData(UserSessionManager.KEY_ORGSET, true);
                session.setBooleanData(session.getData(UserSessionManager.KEY_ORG_REGKEY) + UserSessionManager.KEY_COMSET, true);
                startActivity(new Intent(OwnerLoginActivity.this, OwnerHomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            } else {
                btnsubmit.setEnabled(false);
                progressbar.setVisibility(View.VISIBLE);
                UserInfo finalUserInfo = userInfo;
                if (AppController.isConnected(OwnerLoginActivity.this, findViewById(R.id.activity_main))) {
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.GETORGANIZATIONUrl + userInfo.getMobileno() + "&demo=" + userInfo.getDemo() + "&imei=" + session.getData(UserSessionManager.KEY_IMEI) + "&devicecode=" + session.getData(UserSessionManager.KEY_DEVICECODE), new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressbar.setVisibility(View.GONE);
                            if (response.contains("\n"))
                                response = response.replace("\n", "").trim();
                            //System.out.println("================= == " + response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getInt("records") == 0) {
                                    btnsubmit.setEnabled(true);
                                    Toast.makeText(OwnerLoginActivity.this, getResources().getString(R.string.noorg), Toast.LENGTH_SHORT).show();
                                } else {

                                    JSONArray jsonArray = jsonObject.getJSONArray("rows");
                                    databaseHelper.DeleteData(DatabaseHelper.TABLE_ORGANIZATION, "");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject object = jsonArray.getJSONObject(i);
                                        JSONArray cellarray = object.getJSONArray("cell");

                                        if (cellarray.length() != 0) {
                                            Organization organization = new Organization(cellarray.getString(0), cellarray.getString(1), cellarray.getString(2));
                                            databaseHelper.addOrgdata(organization);
                                            if (i == 0) {
                                                session.setData(UserSessionManager.KEY_ORG_LICENCE, organization.getLicencekey());
                                                session.setData(UserSessionManager.KEY_ORG_NAME, organization.getOrganization_name());
                                                session.setData(UserSessionManager.KEY_ORG_REGKEY, organization.getRegkey());
                                            }
                                        }
                                    }

                                    if (jsonObject.getInt("records") == 1) {
                                        session.setBooleanData(UserSessionManager.KEY_ORGSET, true);
                                        String orgkey = session.getData(UserSessionManager.KEY_ORG_REGKEY);
                                        String orgname = session.getData(UserSessionManager.KEY_ORG_NAME);

                                        //startActivity(new Intent(OwnerLoginActivity.this, CompanyActivity.class).putExtra("from", "ownerlogin").addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                        String url = Constant.GETCOMPANYNUrl + orgkey + "&mobno=" + session.getData(UserSessionManager.KEY_Mobile) + "&imei=" + session.getData(UserSessionManager.KEY_IMEI) + "&devicecode=" + session.getData(UserSessionManager.KEY_DEVICECODE) + "&demo=" + session.getData(UserSessionManager.KEY_DEMO);
                                        //System.out.println("============ com url " + url);

                                        progressbar.setVisibility(View.VISIBLE);
                                        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                                            @Override
                                            public void onResponse(String response) {
                                                if (response.contains("\n"))
                                                    response = response.replace("\n", "").trim();

                                                //System.out.println("============ com data " + response);

                                                try {
                                                    JSONArray jsonArray = new JSONArray(response);
                                                    if (jsonArray.length() > 2) {
                                                        session.setBooleanData(orgkey + UserSessionManager.KEY_COMSET, false);
                                                        databaseHelper.DeleteData(DatabaseHelper.TABLE_COMPANY, orgkey);
                                                        Company company = null;
                                                        for (int i = 1; i < jsonArray.length() - 1; i++) {
                                                            JSONArray comarray = jsonArray.getJSONArray(i);
                                                            company = new Company(comarray.getString(0), comarray.getString(1), comarray.getString(2), comarray.getString(3), orgkey, comarray.getString(4), comarray.getString(5), comarray.getString(6), comarray.getString(7), comarray.getString(8), comarray.getString(9), comarray.getString(10), comarray.getString(11));
                                                            databaseHelper.addCompanydata(company);
                                                            //System.out.println("=========com - " + company.getCompname());
                                                            if (i == 1) {
                                                                session.setBooleanData(orgkey + UserSessionManager.KEY_COMSET, true);
                                                                session.setCompanyData(company, orgkey);
                                                            }

                                                        }

                                                        //System.out.println("=========com length - " + jsonArray.length());
                                                        if (jsonArray.length() == 3) {
                                                            if (databaseHelper.IsMasterExists(company.getCompcode(), orgkey)) {
                                                                progressbar.setVisibility(View.GONE);
                                                                startActivity(new Intent(OwnerLoginActivity.this, OwnerHomeActivity.class));
                                                                lytbottom.setEnabled(true);
                                                                finishAffinity();
                                                            } else if (AppController.isConnected(OwnerLoginActivity.this, findViewById(R.id.snackbarid))) {

                                                                fetchtask = new FetchAsyncRunner(company).execute();

                                                                /*Toast.makeText(OwnerLoginActivity.this, getResources().getString(R.string.fetchingcompanydata), Toast.LENGTH_SHORT).show();

                                                                final Company finalCompany = company;
                                                                Company finalCompany1 = company;
                                                                StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.GETMASTERNUrl + session.getData(UserSessionManager.KEY_ORG_REGKEY) + "&compcode=" + company.getCompcode() + "&mobno=" + session.getData(UserSessionManager.KEY_Mobile) + "&imei=" + session.getData(UserSessionManager.KEY_IMEI) + "&devicecode=" + session.getData(UserSessionManager.KEY_DEVICECODE) + "&demo=" + session.getData(UserSessionManager.KEY_DEMO), new Response.Listener<String>() {
                                                                    @Override
                                                                    public void onResponse(String response) {

                                                                        if (response.contains("\n"))
                                                                            response = response.replace("\n", "").trim();

                                                                        databaseHelper.DeleteMasterData(finalCompany.getCompcode(), orgkey);

                                                                        try {
                                                                            JSONArray jsonArray = new JSONArray(response);
                                                                            for (int i = 1; i < jsonArray.length() - 1; i++) {
                                                                                JSONArray masterarray = jsonArray.getJSONArray(i);
                                                                                //System.out.println("=============data == = " + masterarray.getString(0) + " = " + masterarray.getString(1) + "=" + masterarray.getString(2) + "=" + masterarray.getString(3) + "=" + masterarray.getString(4));
                                                                                Master master = new Master(masterarray.getString(0), masterarray.getString(1), masterarray.getString(2), masterarray.getString(3), masterarray.getString(4), masterarray.getString(5), masterarray.getString(6), masterarray.getString(7), masterarray.getString(8), finalCompany.getCompcode(), orgkey);
                                                                                databaseHelper.addMasterdata(master);
                                                                            }
                                                                            progressbar.setVisibility(View.GONE);
                                                                            startActivity(new Intent(OwnerLoginActivity.this, OwnerHomeActivity.class));
                                                                            finishAffinity();
                                                                        } catch (JSONException e) {
                                                                            progressbar.setVisibility(View.GONE);
                                                                            if (e.toString().contains(getResources().getString(R.string.noresponse)))
                                                                                Toast.makeText(OwnerLoginActivity.this, getResources().getString(R.string.notconnectedserver), Toast.LENGTH_SHORT).show();
                                                                            else {
                                                                                e.printStackTrace();
                                                                                String subject = Constant.ERRORID + " | " + finalUserInfo.getMobileno() + " | " + orgname + " | " + Constant.ERRORNOCOMP;
                                                                                databaseHelper.addJSONERROR(e, subject);
                                                                            }
                                                                        }


                                                                    }

                                                                },
                                                                        new Response.ErrorListener() {
                                                                            @Override
                                                                            public void onErrorResponse(VolleyError error) {
                                                                                progressbar.setVisibility(View.GONE);
                                                                                String message = Constant.VolleyErrorMessage(error);
                                                                                if (!message.equals(""))
                                                                                    Toast.makeText(OwnerLoginActivity.this, message, Toast.LENGTH_SHORT).show();

                                                                            }
                                                                        });
                                                                AppController.getInstance().getRequestQueue().getCache().clear();
                                                                stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                                                AppController.getInstance().addToRequestQueue(stringRequest);*/
                                                            }
                                                            //startActivity(new Intent(OrganizationActivity.this, OwnerHomeActivity.class));
                                                        } else {
                                                            btnsubmit.setEnabled(true);
                                                            progressbar.setVisibility(View.GONE);
                                                            startActivity(new Intent(OwnerLoginActivity.this, CompanyActivity.class).putExtra("from", "ownerlogin").addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                                        }

                                                    } else {
                                                        btnsubmit.setEnabled(true);
                                                        progressbar.setVisibility(View.GONE);
                                                        Toast.makeText(OwnerLoginActivity.this, "Sorry No Company Found", Toast.LENGTH_SHORT).show();
                                                    }
                                                    lytbottom.setEnabled(true);
                                                } catch (JSONException e) {
                                                    btnsubmit.setEnabled(true);
                                                    progressbar.setVisibility(View.GONE);
                                                    lytbottom.setEnabled(true);
                                                    if (e.toString().contains(getResources().getString(R.string.noresponse)))
                                                        Toast.makeText(OwnerLoginActivity.this, getResources().getString(R.string.notconnectedserver), Toast.LENGTH_SHORT).show();
                                                    else {
                                                        e.printStackTrace();
                                                        String subject = Constant.ERRORID + " | " + session.getData(UserSessionManager.KEY_Mobile) + " | " + session.getData(UserSessionManager.KEY_ORG_NAME) + " | " + Constant.ERRORNOCOMP;
                                                        databaseHelper.addJSONERROR(e, subject);
                                                    }
                                                }

                                            }

                                        },
                                                new Response.ErrorListener() {
                                                    @Override
                                                    public void onErrorResponse(VolleyError error) {
                                                        btnsubmit.setEnabled(true);
                                                        String message = Constant.VolleyErrorMessage(error);
                                                        if (!message.equals(""))
                                                            Toast.makeText(OwnerLoginActivity.this, message, Toast.LENGTH_SHORT).show();

                                                    }
                                                });
                                        AppController.getInstance().getRequestQueue().getCache().clear();
                                        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                        AppController.getInstance().addToRequestQueue(stringRequest);

                                    } else {
                                        startActivity(new Intent(OwnerLoginActivity.this, OrganizationActivity.class).putExtra("from", "ownerlogin").addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                    }

                                }

                            } catch (JSONException e) {
                                btnsubmit.setEnabled(true);
                                progressbar.setVisibility(View.GONE);
                                if (e.toString().contains(getResources().getString(R.string.noresponse)))
                                    Toast.makeText(OwnerLoginActivity.this, getResources().getString(R.string.notconnectedserver), Toast.LENGTH_SHORT).show();
                                else {
                                    String subject = Constant.ERRORID + " | " + finalUserInfo.getMobileno() + " | " + Constant.ERRORNOORG + " | " + Constant.ERRORNOCOMP;
                                    databaseHelper.addJSONERROR(e, subject);
                                }
                                e.printStackTrace();
                            }
                            btnsubmit.setEnabled(true);

                        }

                    },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    btnsubmit.setEnabled(true);
                                    progressbar.setVisibility(View.GONE);
                                    String message = Constant.VolleyErrorMessage(error);

                                    if (!message.equals(""))
                                        Toast.makeText(OwnerLoginActivity.this, message, Toast.LENGTH_SHORT).show();

                                }
                            });
                    AppController.getInstance().getRequestQueue().getCache().clear();
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    AppController.getInstance().addToRequestQueue(stringRequest);
                }
            }
        } else
            Toast.makeText(OwnerLoginActivity.this, getResources().getString(R.string.pinnotmatch), Toast.LENGTH_SHORT).show();

    }

    public void OnForgotPinClick(View view) {

        if (session.getData(UserSessionManager.KEY_Mobile) == null || session.getData(UserSessionManager.KEY_Mobile).equals(""))
            startActivity(new Intent(OwnerLoginActivity.this, RegisterMobileActivity.class).putExtra("fromlogin", true));
        else {
            startActivity(new Intent(OwnerLoginActivity.this, OTPVarificationActivity.class)
                    .putExtra("mobileno", session.getData(UserSessionManager.KEY_Mobile))
                    .putExtra("OTP", "")
                    .putExtra("from", "forgot"));
        }

    }

    public void OnDeactivateDeviceClick(View view) {
        final String devicecode = session.getData(UserSessionManager.KEY_DEVICECODE);
        if (devicecode.equals(""))
            Toast.makeText(OwnerLoginActivity.this, getResources().getString(R.string.deviceinfonotfound), Toast.LENGTH_SHORT);
        else if (AppController.isConnected(OwnerLoginActivity.this, findViewById(R.id.activity_main))) {

            final AlertDialog.Builder alertdialog = new AlertDialog.Builder(OwnerLoginActivity.this);
            alertdialog.setTitle(getResources().getString(R.string.deactivatedevice));
            alertdialog.setMessage(getResources().getString(R.string.deactivatemsg));
            alertdialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            alertdialog.setPositiveButton("Deactivate", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    progressbar.setVisibility(View.VISIBLE);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.DEACTIVATEDEVICE + devicecode, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response1) {
                            progressbar.setVisibility(View.GONE);
                            if (response1.contains("Yes")) {
                                Toast.makeText(OwnerLoginActivity.this, "Successfully Deactivated your device", Toast.LENGTH_SHORT).show();
                                databaseHelper.deleteAllTable();
                                session.ClearUserSession();
                                session.logoutUser();
                            } else
                                Toast.makeText(OwnerLoginActivity.this, "Something went wrong, device not Deactivated", Toast.LENGTH_SHORT).show();
                        }

                    },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    progressbar.setVisibility(View.GONE);
                                    String message = Constant.VolleyErrorMessage(error);

                                    if (!message.equals(""))
                                        Toast.makeText(OwnerLoginActivity.this, message, Toast.LENGTH_SHORT).show();

                                }
                            });
                    AppController.getInstance().getRequestQueue().getCache().clear();
                    AppController.getInstance().addToRequestQueue(stringRequest);

                }
            });

            alertdialog.show();

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (session.getData(UserSessionManager.KEY_IMEI).equals(""))
            AppController.getUniqueIMEIId(session, OwnerLoginActivity.this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case ReadPhoneStatemain:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (session.getData(UserSessionManager.KEY_IMEI).equals(""))
                        AppController.getUniqueIMEIId(session, OwnerLoginActivity.this);
                }
                break;
        }
    }


    private class FetchAsyncRunner extends AsyncTask<String, String, String> {

        String companycode ;
        Company company;

        public FetchAsyncRunner(Company company) {
            this.company = company;
            this.companycode = company.getCompcode();
        }

        @Override
        protected String doInBackground(String... params) {

                StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.GETMASTERNUrl + session.getData(UserSessionManager.KEY_ORG_REGKEY) + "&compcode=" + company.getCompcode() + "&mobno=" + session.getData(UserSessionManager.KEY_Mobile) + "&imei=" + session.getData(UserSessionManager.KEY_IMEI) + "&devicecode=" + session.getData(UserSessionManager.KEY_DEVICECODE) + "&demo=" + session.getData(UserSessionManager.KEY_DEMO), new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response.contains("\n"))
                            response = response.replace("\n", "").trim();

                        databaseHelper.DeleteMasterData(companycode, session.getData(UserSessionManager.KEY_ORG_REGKEY));

                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i = 1; i < jsonArray.length() - 1; i++) {
                                JSONArray masterarray = jsonArray.getJSONArray(i);
                                System.out.println("=============data == = " + masterarray.getString(0) + " = " + masterarray.getString(1) + "=" + masterarray.getString(2) + "=" + masterarray.getString(3) + "=" + masterarray.getString(4));
                                Master master = new Master(masterarray.getString(0), masterarray.getString(1), masterarray.getString(2), masterarray.getString(3), masterarray.getString(4), masterarray.getString(5), masterarray.getString(6), masterarray.getString(7), masterarray.getString(8), companycode, session.getData(UserSessionManager.KEY_ORG_REGKEY));
                                databaseHelper.addMasterdata(master);
                            }

                            btnsubmit.setEnabled(true);
                            progressbar.setVisibility(View.GONE);
                            startActivity(new Intent(OwnerLoginActivity.this, OwnerHomeActivity.class));
                            finishAffinity();

                        } catch (JSONException e) {
                            btnsubmit.setEnabled(true);
                            progressbar.setVisibility(View.GONE);
                            if (e.toString().contains(getResources().getString(R.string.noresponse)))
                                Toast.makeText(OwnerLoginActivity.this, getResources().getString(R.string.notconnectedserver), Toast.LENGTH_SHORT).show();
                            else if (e.toString().contains(getResources().getString(R.string.noresponse)))
                                Toast.makeText(OwnerLoginActivity.this, getResources().getString(R.string.notconnectedserver), Toast.LENGTH_SHORT).show();
                            else if (e.toString().contains(getResources().getString(R.string.multiuseword))) {
                                Toast.makeText(OwnerLoginActivity.this, getResources().getString(R.string.multiuse), Toast.LENGTH_SHORT).show();
                            } else if (e.toString().contains(getResources().getString(R.string.qstringerr))) {
                                Toast.makeText(OwnerLoginActivity.this, getResources().getString(R.string.trylater), Toast.LENGTH_SHORT).show();
                            }else {
                                e.printStackTrace();
                                String subject = Constant.ERRORID + " | " + session.getData(UserSessionManager.KEY_Mobile) + " | " + session.getData(UserSessionManager.KEY_ORG_NAME) + " | " + Constant.ERRORNOCOMP;
                                databaseHelper.addJSONERROR(e, subject);
                            }
                        }


                    }

                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                progressbar.setVisibility(View.GONE);
                                btnsubmit.setEnabled(true);
                                String message = Constant.VolleyErrorMessage(error);
                                if (!message.equals(""))
                                    Toast.makeText(OwnerLoginActivity.this, message, Toast.LENGTH_SHORT).show();

                            }
                        });
                AppController.getInstance().getRequestQueue().getCache().clear();
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(stringRequest);


            return null;
        }


        @Override
        protected void onPostExecute(String result) {

        }


        @Override
        protected void onPreExecute() {
            Toast.makeText(OwnerLoginActivity.this, getResources().getString(R.string.fetchingcompanydata), Toast.LENGTH_SHORT).show();
        }
    }
}
