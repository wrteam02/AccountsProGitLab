package com.accountspro.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.accountspro.R;
import com.accountspro.adapter.AutoCompleteAdapter;
import com.accountspro.helper.AppController;
import com.accountspro.helper.Constant;
import com.accountspro.helper.DatabaseHelper;
import com.accountspro.helper.InstantAutoComplete;
import com.accountspro.helper.UserSessionManager;
import com.accountspro.helper.VolleyCallback;
import com.accountspro.model.Master;
import com.accountspro.model.Receivble;
import com.accountspro.model.SalesReport;
import com.accountspro.model.StockStatus;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.apache.poi.hssf.usermodel.HeaderFooter;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Footer;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.PrintSetup;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.ShapeTypes;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFSimpleShape;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Locale;

public class StockStatusActivity extends AppCompatActivity {

    public int ReqReadPermission = 1;
    public int ReqWritePermission = 2;
    Toolbar toolbar;
    LinearLayout lyttotal, lytpdfexcel, lytcontain, lyttop, lytmain, lytbottom, lytprogress;
    Spinner sp_searchby;
    InstantAutoComplete txtsearchvalue;
    ImageView imgclear, imgdown;
    TextView txttotalitem, txttotal, txtasondate, txtsec, txtnodata, datedisplay;
    Button btnsync, btnshow;
    ProgressBar progressbar;
    RecyclerView recycleview;
    ImageButton btnexcel, btnpdf;
    UserSessionManager session;
    DatabaseHelper databaseHelper;
    String from, headertext, asondatedisplay, orgcode, compcode, companyname, address, subject, searchvaluename, searchbyval, path, filename, pdfheadertext, asondate = "", searchvalue = "0", reorder = "No";
    ArrayList<Master> itemgrouplist, itembrandlist, itemcategorylist;
    ArrayList<String> spsearchbyarray;
    int searchbypos = 0;
    Master mastersearchvalue;
    String sessionname = Constant.STOCKSTATUSSESSIONNAME;
    int nCounter = 0;
    private Handler mHandler = new Handler();
    RadioGroup radiogroup;
    RadioButton radioyes, radiono;
    SimpleDateFormat inFormatdate = new SimpleDateFormat("yyyyMMdd");
    LinkedHashMap<String, StockStatus> stockstatuslist;
    ArrayList<StockStatus> stockStatusArrayList;
    AsyncTask<String, String, String> asyntask;
    //NumberFormat indianFormat = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_status);

        from = getIntent().getStringExtra("from");
        databaseHelper = new DatabaseHelper(StockStatusActivity.this);
        session = new UserSessionManager(StockStatusActivity.this);
        orgcode = session.getData(UserSessionManager.KEY_ORG_REGKEY);
        compcode = session.getData(orgcode + UserSessionManager.KEY_COM_CODE);
        companyname = session.getData(orgcode + UserSessionManager.KEY_COM_NAME);
        address = session.getData(orgcode + UserSessionManager.KEY_COM_ADDRESS);
        subject = Constant.ERRORID + " | " + session.getData(UserSessionManager.KEY_Mobile) + " | " + session.getData(UserSessionManager.KEY_ORG_NAME) + " | " + companyname;

        try {
            itemgrouplist = databaseHelper.getMasterRowData(compcode, getResources().getString(R.string.master_item_group), orgcode);
            itembrandlist = databaseHelper.getMasterRowData(compcode, getResources().getString(R.string.master_item_brand), orgcode);
            itemcategorylist = databaseHelper.getMasterRowData(compcode, getResources().getString(R.string.master_item_category), orgcode);
            itemgrouplist.add(0, new Master("0", "All"));
            itembrandlist.add(0, new Master("0", "All"));
            itemcategorylist.add(0, new Master("0", "All"));

            stockstatuslist = new LinkedHashMap<>();
            stockStatusArrayList = new ArrayList<>();

            toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);


            radiogroup = findViewById(R.id.radiogroup);
            radioyes = findViewById(R.id.radioyes);
            radiono = findViewById(R.id.radiono);
            btnpdf = findViewById(R.id.btnpdf);
            btnexcel = findViewById(R.id.btnexcel);
            lyttotal = findViewById(R.id.lyttotal);
            lytpdfexcel = findViewById(R.id.lytpdfexcel);
            lytprogress = findViewById(R.id.lytprogress);
            lytbottom = findViewById(R.id.lytbottom);
            imgdown = findViewById(R.id.imgdown);
            recycleview = findViewById(R.id.recycleview);
            recycleview.setLayoutManager(new LinearLayoutManager(StockStatusActivity.this));
            datedisplay = findViewById(R.id.datedisplay);
            lytmain = findViewById(R.id.lytmain);
            txtnodata = findViewById(R.id.txtnodata);
            txtsec = findViewById(R.id.txtsec);
            progressbar = findViewById(R.id.progressbar);
            btnshow = findViewById(R.id.btnshow);
            btnsync = findViewById(R.id.btnsync);
            txttotal = findViewById(R.id.txttotal);
            txttotalitem = findViewById(R.id.txttotalitem);
            txtasondate = findViewById(R.id.txtasondate);
            imgclear = findViewById(R.id.imgclear);
            lytcontain = findViewById(R.id.lytcontain);
            lyttop = findViewById(R.id.lyttop);
            sp_searchby = findViewById(R.id.sp_searchby);
            txtsearchvalue = findViewById(R.id.txtsearchvalue);

            spsearchbyarray = new ArrayList<>();

            spsearchbyarray.add(getResources().getString(R.string.item_group));
            spsearchbyarray.add(getResources().getString(R.string.item_brand));
            spsearchbyarray.add(getResources().getString(R.string.item_category));

            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spsearchbyarray);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sp_searchby.setAdapter(spinnerArrayAdapter);

            final Calendar calendar = Calendar.getInstance();
            /*String dateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
            String monthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
            if (monthString.length() == 1) {
                monthString = "0" + monthString;
            }
            if (dateString.length() == 1) {
                dateString = "0" + dateString;
            }*/

            String[] datestring = Constant.GetMonthDateString(calendar).split(";");
            String dateString = datestring[0];
            String monthString = datestring[1];

            txtasondate.setText(dateString + "-" + monthString + "-" + calendar.get(Calendar.YEAR));
            asondate = calendar.get(Calendar.YEAR) + monthString + dateString;


            final AutoCompleteAdapter itemgroupadapter = new AutoCompleteAdapter(StockStatusActivity.this, R.layout.lyt_sptext, itemgrouplist);
            final AutoCompleteAdapter itembrandadapter = new AutoCompleteAdapter(StockStatusActivity.this, R.layout.lyt_sptext, itembrandlist);
            final AutoCompleteAdapter itemcategoryadapter = new AutoCompleteAdapter(StockStatusActivity.this, R.layout.lyt_sptext, itemcategorylist);

            itemgroupadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            itembrandadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            itemcategoryadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            sp_searchby.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        Master defaultmaster = null;
                        if (sp_searchby.getSelectedItem().toString().equals(getResources().getString(R.string.item_group))) {
                            txtsearchvalue.setAdapter(itemgroupadapter);
                            defaultmaster = itemgrouplist.get(0);
                        } else if (sp_searchby.getSelectedItem().toString().equals(getResources().getString(R.string.item_brand))) {
                            txtsearchvalue.setAdapter(itembrandadapter);
                            defaultmaster = containNameCode(Constant.DefaultLedgerGroupSearchValue);
                            if (defaultmaster == null)
                                defaultmaster = itembrandlist.get(0);
                        } else if (sp_searchby.getSelectedItem().toString().equals(getResources().getString(R.string.item_category))) {
                            txtsearchvalue.setAdapter(itemcategoryadapter);
                            defaultmaster = itemcategorylist.get(0);
                        }

                        searchbyval = sp_searchby.getSelectedItem().toString().trim();
                        searchbypos = position;

                        if (((TextView) sp_searchby.getSelectedView()) != null)
                            ((TextView) sp_searchby.getSelectedView()).setTextColor(getResources().getColor(R.color.white));

                        if (Integer.parseInt(session.getData(sessionname + "by" + compcode + orgcode, "0")) == position && !session.getData(sessionname + "val" + compcode + orgcode).equals("") && !session.getData(sessionname + "valname" + compcode + orgcode).equals("")) {
                            searchvalue = session.getData(sessionname + "val" + compcode + orgcode);
                            if (!session.getData(sessionname + "valname" + compcode + orgcode).equals(""))
                                txtsearchvalue.setText(session.getData(sessionname + "valname" + compcode + orgcode));
                        } else {
                            mastersearchvalue = defaultmaster;
                            txtsearchvalue.setText(mastersearchvalue.getMastername());
                            searchvalue = mastersearchvalue.getMastercode();
                            searchvaluename = mastersearchvalue.getMastername();
                        }

                        BtnSyncVisibility();
                    } catch (Exception e) {
                        e.printStackTrace();
                        databaseHelper.addExceptionERROR(e, subject);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });


            txtasondate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new DatePickerDialog(StockStatusActivity.this, pickerListenerfrom, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            });

            txtsearchvalue.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    //txtsearchvalue.requestFocus();
                    if (s.length() == 0) {
                        imgclear.setVisibility(View.GONE);
                    } else
                        imgclear.setVisibility(View.VISIBLE);
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });

            txtsearchvalue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    txtsearchvalue.requestFocus();
                    txtsearchvalue.showDropDown();
                }
            });

            txtsearchvalue.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        mastersearchvalue = (Master) parent.getItemAtPosition(position);
                        searchvalue = mastersearchvalue.getMastercode();
                        searchvaluename = mastersearchvalue.getMastername();

                        //System.out.println("==================== ==1 "+searchvalue+" = "+searchvaluename);

                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(txtsearchvalue.getWindowToken(), 0);

                        BtnSyncVisibility();
                    } catch (Exception e) {
                        e.printStackTrace();
                        databaseHelper.addExceptionERROR(e, subject);
                    }
                }
            });


            try {
                if (session.getBooleanData(sessionname + "data" + compcode + orgcode)) {
                    if (!session.getData(sessionname + "by" + compcode + orgcode).equals("")) {
                        searchbypos = Integer.parseInt(session.getData(sessionname + "by" + compcode + orgcode, "0"));
                        sp_searchby.setSelection(searchbypos);
                        searchbyval = sp_searchby.getSelectedItem().toString().trim();
                    }
                    if (!session.getData(sessionname + "date" + compcode + orgcode).equals("")) {
                        asondate = session.getData(sessionname + "date" + compcode + orgcode);
                        txtasondate.setText(asondate.substring(6, 8) + "-" + asondate.substring(4, 6) + "-" + asondate.substring(0, 4));
                    }
                    if (!session.getData(sessionname + "valname" + compcode + orgcode).equals("")) {
                        searchvaluename = session.getData(sessionname + "valname" + compcode + orgcode);
                    }
                    if (!session.getData(sessionname + "val" + compcode + orgcode).equals("0") && !session.getData(sessionname + "val" + compcode + orgcode).equals("") && !session.getData(sessionname + "valname" + compcode + orgcode).equals("")) {
                        searchvalue = session.getData(sessionname + "val" + compcode + orgcode);
                        txtsearchvalue.setText(session.getData(sessionname + "valname" + compcode + orgcode));
                    } else {
                        txtsearchvalue.setText("");
                        searchvalue = "0";
                        searchvaluename = "";
                        txtsearchvalue.setText(searchvaluename);
                    }

                    if (session.getData(sessionname + "reorder" + compcode + orgcode) != null || session.getData(sessionname + "reorder" + compcode + orgcode) != "") {
                        RadioButton radioButton = (RadioButton) findViewById(getResources().getIdentifier("radio" + session.getData(sessionname + "reorder" + compcode + orgcode).toLowerCase(), "id", this.getPackageName()));
                        radioButton.setChecked(true);
                        reorder = radioButton.getTag().toString();
                    }

                    if (from != null && !from.equals("home")) {
                        //OnShowStockClick(lyttop.getRootView());
                        boolean issyncable = SyncVisibility();
                        if (issyncable)
                            OnSyncStockClick(lyttop.getRootView());
                        else
                            Toast.makeText(StockStatusActivity.this, getResources().getString(R.string.datalimit), Toast.LENGTH_SHORT).show();
                        from = "home";
                    }

                } else {
                    imgdown.setVisibility(View.GONE);
                    RadioButton defaultradioButton = (RadioButton) findViewById(radiogroup.getCheckedRadioButtonId());
                    reorder = defaultradioButton.getTag().toString();
                    sp_searchby.setSelection(0);
                    if (itemgrouplist.size() != 0) {
                    } else
                        Toast.makeText(StockStatusActivity.this, getResources().getString(R.string.updatemaster_msg), Toast.LENGTH_SHORT).show();

                }
            } catch (Exception e) {
                e.printStackTrace();
                sp_searchby.setSelection(0);
                if (itemgrouplist.size() != 0) {
                } else
                    Toast.makeText(StockStatusActivity.this, getResources().getString(R.string.updatemaster_msg), Toast.LENGTH_SHORT).show();

            }

            try {
                if (from != null && from.equals("home")) {
                    String localdata = databaseHelper.getStockStatusData(orgcode, compcode, asondate, String.valueOf(searchbypos), searchvalue, reorder);
                    if (!localdata.equals("") && (AppController.isNetConnected(StockStatusActivity.this))) {
                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                OnShowStockClick(lyttop.getRootView());
                            }
                        }, 150);

                    } else if (AppController.isNetConnected(StockStatusActivity.this)) {
                        SyncVisibility();
                        imgdown.setVisibility(View.GONE);
                        btnshow.setVisibility(View.GONE);
                    } else {
                        //lyttop.setVisibility(View.GONE);
                        imgdown.setVisibility(View.GONE);
                        Toast.makeText(StockStatusActivity.this, getResources().getString(R.string.checkinternet), Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                databaseHelper.addExceptionERROR(e, subject);
            }

            radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    reorder = group.findViewById(checkedId).getTag().toString();
                    //System.out.println("===================== =  =" + narration);
                    SyncVisibility();
                }

            });
        } catch (Exception e) {
            e.printStackTrace();

            databaseHelper.addExceptionERROR(e, subject);
        }
    }

    private DatePickerDialog.OnDateSetListener pickerListenerfrom = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            /*String monthString = String.valueOf(selectedMonth + 1);
            String dateString = String.valueOf(selectedDay);
            if (monthString.length() == 1) {
                monthString = "0" + monthString;
            }
            if (dateString.length() == 1) {
                dateString = "0" + dateString;
            }*/

            String[] datestring = Constant.GetMonthDateOfDatePicker(selectedMonth, selectedDay).split(";");
            String dateString = datestring[0];
            String monthString = datestring[1];

            txtasondate.setText(dateString + "-" + monthString + "-" + selectedYear);
            asondate = selectedYear + monthString + dateString;
            BtnSyncVisibility();
        }
    };

    private Master containNameCode(String search) {
        ArrayList<Master> list = null;
        if (sp_searchby.getSelectedItem().toString().equals(getResources().getString(R.string.item_brand))) {
            list = itembrandlist;
        } else if (sp_searchby.getSelectedItem().toString().equals(getResources().getString(R.string.item_group))) {
            list = itemgrouplist;
        } else if (sp_searchby.getSelectedItem().toString().equals(getResources().getString(R.string.item_category))) {
            list = itemcategorylist;
        }

        for (final Master master : list) {
            if (master.getMastername().trim().equalsIgnoreCase(search)) {
                return master;
            }
        }

        return null;
    }

    public void OnStockPdfClick(View view) {
        /*if (ContextCompat.checkSelfPermission(StockStatusActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(StockStatusActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, ReqReadPermission);
        } else if (ContextCompat.checkSelfPermission(StockStatusActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(StockStatusActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, ReqWritePermission);
        } else {*/
        if (Constant.CheckReadWritePermissionGranted(StockStatusActivity.this)) {
            lytprogress.setVisibility(View.VISIBLE);
            btnexcel.setEnabled(false);
            btnpdf.setEnabled(false);
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {

                    /*File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getResources().getString(R.string.app_name) + "/");

                    if (!folder.exists()) {
                        boolean success = folder.mkdir();
                    }

                    path = folder.getAbsolutePath();
                    path = path + "/" + filename + ".pdf";
                    File filepath = new File(path);
                    if (filepath.exists()) {
                        filepath.delete();
                    }*/

                    File filepath = Constant.FilePathPdfExcel(true, ".pdf", StockStatusActivity.this, filename);
                    UserSessionManager.CreateStockStatusPdf(filename, address, companyname, pdfheadertext, stockStatusArrayList, recycleview, txttotal.getText().toString(), txttotalitem.getText().toString().trim());
                    openPdfExcel(filepath, "pdf");

                }
            }, 300);
        }
    }

    public void OnStockExcelClick(View view) {
        /*if (ContextCompat.checkSelfPermission(StockStatusActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(StockStatusActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, ReqReadPermission);
        } else if (ContextCompat.checkSelfPermission(StockStatusActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(StockStatusActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, ReqWritePermission);
        } else {*/
        if (Constant.CheckReadWritePermissionGranted(StockStatusActivity.this)) {
            lytprogress.setVisibility(View.VISIBLE);
            btnexcel.setEnabled(false);
            btnpdf.setEnabled(false);
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    writeStudentsListToExcel();
                }
            }, 300);
        }
    }

    public void writeStudentsListToExcel() {
        try {
            /*File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getResources().getString(R.string.app_name) + "/");

            if (!folder.exists()) {
                boolean success = folder.mkdir();
            }
            String path = folder.getAbsolutePath();
            path = path + "/" + filename + ".xlsx";

            File FILE_PATH = new File(path);
            if (FILE_PATH.exists()) {
                FILE_PATH.delete();
            }*/

            File FILE_PATH = Constant.FilePathPdfExcel(true, ".xlsx", StockStatusActivity.this, filename);
            XSSFWorkbook workbook = new XSSFWorkbook();

            XSSFSheet studentsSheet = workbook.createSheet("StockStatus");
            studentsSheet.getPrintSetup().setPaperSize(PrintSetup.A4_PAPERSIZE);
            studentsSheet.setHorizontallyCenter(true);

            Footer footer = studentsSheet.getFooter();
            footer.setRight("Page " + HeaderFooter.page() + " of " + HeaderFooter.numPages());

            studentsSheet.setColumnWidth(0, (15 * 500));
            studentsSheet.setColumnWidth(1, (15 * 200));
            studentsSheet.setColumnWidth(2, (15 * 200));
            studentsSheet.setColumnWidth(3, (15 * 400));


            int rowIndex = 0, nextrow = 0;
            //Row row = studentsSheet.createRow(rowIndex++);
            int cellIndex = 0;
            CellStyle style = workbook.createCellStyle();

            style.setBorderBottom(CellStyle.BORDER_THIN);
            style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            style.setBorderLeft(CellStyle.BORDER_THIN);
            style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            style.setBorderRight(CellStyle.BORDER_THIN);
            style.setRightBorderColor(IndexedColors.BLACK.getIndex());
            style.setBorderTop(CellStyle.BORDER_THIN);
            style.setTopBorderColor(IndexedColors.BLACK.getIndex());

            CellStyle borderStyle = workbook.createCellStyle();
            borderStyle.setBorderBottom(CellStyle.BORDER_THIN);
            borderStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            borderStyle.setBorderLeft(CellStyle.BORDER_THIN);
            borderStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            borderStyle.setBorderRight(CellStyle.BORDER_THIN);
            borderStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            borderStyle.setBorderTop(CellStyle.BORDER_THIN);
            borderStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
            ((XSSFCellStyle) borderStyle).setAlignment(HorizontalAlignment.CENTER);

            CellStyle rightborderStyle = workbook.createCellStyle();
            rightborderStyle.setBorderBottom(CellStyle.BORDER_THIN);
            rightborderStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            rightborderStyle.setBorderLeft(CellStyle.BORDER_THIN);
            rightborderStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            rightborderStyle.setBorderRight(CellStyle.BORDER_THIN);
            rightborderStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            rightborderStyle.setBorderTop(CellStyle.BORDER_THIN);
            rightborderStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
            ((XSSFCellStyle) rightborderStyle).setAlignment(HorizontalAlignment.RIGHT);
            ((XSSFCellStyle) rightborderStyle).setVerticalAlignment(VerticalAlignment.CENTER);

            CellStyle leftborderStyle = workbook.createCellStyle();
            leftborderStyle.setBorderBottom(CellStyle.BORDER_THIN);
            leftborderStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            leftborderStyle.setBorderLeft(CellStyle.BORDER_THIN);
            leftborderStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            leftborderStyle.setBorderRight(CellStyle.BORDER_THIN);
            leftborderStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            leftborderStyle.setBorderTop(CellStyle.BORDER_THIN);
            leftborderStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
            ((XSSFCellStyle) leftborderStyle).setAlignment(HorizontalAlignment.LEFT);
            ((XSSFCellStyle) leftborderStyle).setVerticalAlignment(VerticalAlignment.CENTER);

            XSSFFont font = workbook.createFont();
            font.setBold(true);
            style.setFont(font);
            ((XSSFCellStyle) style).setAlignment(HorizontalAlignment.CENTER);

            //row.createCell(cellIndex);
            CellStyle cs = workbook.createCellStyle();
            cs.setFont(font);
            ((XSSFCellStyle) cs).setAlignment(HorizontalAlignment.CENTER);
            ((XSSFCellStyle) cs).setVerticalAlignment(VerticalAlignment.CENTER);
            cs.setWrapText(true);


            XSSFFont comFont = workbook.createFont();
            comFont.setFontHeightInPoints((short) 20);
            CellStyle cellstyle = workbook.createCellStyle();
            cellstyle.setFont(comFont);
            ((XSSFCellStyle) cellstyle).setAlignment(HorizontalAlignment.CENTER);


            Row row = studentsSheet.createRow(rowIndex);
            row.createCell(cellIndex).setCellValue(companyname);
            row.getCell(cellIndex).setCellStyle(cellstyle);
            studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 3));

            rowIndex++;
            cellIndex = 0;

            Row addrow = studentsSheet.createRow(rowIndex);
            addrow.createCell(cellIndex).setCellValue(address);
            addrow.getCell(cellIndex).setCellStyle(cs);

            studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex + 3, 0, 3));

            rowIndex = rowIndex + 4;
            //rowIndex++;
            cellIndex = 0;

            Drawing drawing = studentsSheet.createDrawingPatriarch();
            CreationHelper creationHelper = workbook.getCreationHelper();
            ClientAnchor anchor = creationHelper.createClientAnchor();
            anchor.setDx1(0);
            anchor.setCol1(1);
            anchor.setRow1(rowIndex);
            anchor.setRow2(rowIndex);
            anchor.setCol2(3);
            XSSFDrawing xssfdrawing = (XSSFDrawing) drawing;
            XSSFClientAnchor xssfanchor = (XSSFClientAnchor) anchor;
            XSSFSimpleShape xssfshape = xssfdrawing.createSimpleShape(xssfanchor);
            xssfshape.setShapeType(ShapeTypes.LINE);
            xssfshape.setLineWidth(1);
            xssfshape.setLineStyle(0);
            xssfshape.setLineStyleColor(0, 0, 0);

            Row daterow = studentsSheet.createRow(rowIndex);
            daterow.createCell(cellIndex).setCellValue("StockStatus on " + asondatedisplay);
            daterow.getCell(cellIndex).setCellStyle(cs);
            studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 3));

            rowIndex++;
            Row blankrow = studentsSheet.createRow(rowIndex);

            rowIndex++;
            nextrow = rowIndex;

            cellIndex = 0;
            Row srow = studentsSheet.createRow(rowIndex);
            srow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.particulars));
            srow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.qty));
            srow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.rate));
            srow.createCell(cellIndex).setCellValue(getResources().getString(R.string.value));

            studentsSheet.setRepeatingRows(CellRangeAddress.valueOf("1:" + (rowIndex + 1)));
            for (int j = 0; j < 4; j++) {
                srow.getCell(j).setCellStyle(style);
            }

            int bottomrow = rowIndex;

            for (int i = 0; i < stockStatusArrayList.size(); i++) {
                rowIndex++;
                bottomrow = rowIndex;
                StockStatus stockStatus = stockStatusArrayList.get(i);


                cellIndex = 0;
                Row nrow = studentsSheet.createRow(rowIndex);
                nrow.createCell(cellIndex++).setCellValue(stockStatus.getGroupname());
                nrow.createCell(cellIndex++).setCellValue(String.valueOf(stockStatus.getDoubleqty()));
                nrow.createCell(cellIndex++).setCellValue(Constant.ConvertToIndianRupeeFormat(stockStatus.getDoubleamount(), false, true, false));
                nrow.createCell(cellIndex);
                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, cellIndex - 1, cellIndex));

                for (int j = 0; j < 4; j++) {
                    nrow.getCell(j).setCellStyle(style);
                }

                View rowlist = recycleview.getLayoutManager().findViewByPosition(i);
                RecyclerView recycleviewdata = rowlist.findViewById(R.id.recycleview);
                //rowIndex++;
                if (recycleviewdata.getVisibility() == View.VISIBLE) {
                    ArrayList<StockStatus> datalist = stockStatus.getStockStatusArrayList();

                    for (int j = 0; j < datalist.size(); j++) {
                        rowIndex++;
                        bottomrow = rowIndex;
                        StockStatus stockstatussub = datalist.get(j);

                        Row drow = studentsSheet.createRow(rowIndex);
                        cellIndex = 0;
                        drow.createCell(cellIndex++).setCellValue(stockstatussub.getItemname());
                        drow.createCell(cellIndex++).setCellValue(stockstatussub.getQty());
                        drow.createCell(cellIndex++).setCellValue(stockstatussub.getRate());
                        drow.createCell(cellIndex).setCellValue(stockstatussub.getAmount());
                        for (int k = 0; k < 4; k++) {
                            if (k == 0)
                                drow.getCell(k).setCellStyle(leftborderStyle);
                            else if (k == 1)
                                drow.getCell(k).setCellStyle(borderStyle);
                            else
                                drow.getCell(k).setCellStyle(rightborderStyle);
                        }

                    }
                }
            }

            bottomrow++;
            cellIndex = 0;
            Row drow = studentsSheet.createRow(bottomrow);
            drow.createCell(cellIndex++).setCellValue(txttotalitem.getText().toString().trim());
            drow.createCell(cellIndex++);
            drow.createCell(cellIndex++).setCellValue(txttotal.getText().toString().trim());
            drow.createCell(cellIndex);
            studentsSheet.addMergedRegion(new CellRangeAddress(bottomrow, bottomrow, 0, 1));
            studentsSheet.addMergedRegion(new CellRangeAddress(bottomrow, bottomrow, 2, 3));

            for (int j = 0; j < 4; j++) {
                drow.getCell(j).setCellStyle(style);
            }

            try {
                FileOutputStream fos = new FileOutputStream(FILE_PATH);

                workbook.write(fos);
                fos.close();


                //System.out.println(FILE_PATH + " is successfully written");
                openPdfExcel(FILE_PATH, "excel");
            } catch (FileNotFoundException e) {
                e.printStackTrace();

                databaseHelper.addFileNotFoundERROR(e, subject);
            } catch (IOException e) {
                e.printStackTrace();

                databaseHelper.addIOERROR(e, subject);
            }
        } catch (Exception e) {
            e.printStackTrace();
            databaseHelper.addExceptionERROR(e, subject);
        }
    }

    private void openPdfExcel(File file, String type) {
        lytprogress.setVisibility(View.GONE);

        /*Uri uri = FileProvider.getUriForFile(StockStatusActivity.this, getResources().getString(R.string.authority), file);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        if (type.equals("pdf"))
            intent.setDataAndType(uri, "application/pdf");
        else
            intent.setDataAndType(uri, "application/vnd.ms-excel");

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(intent);*/
        Constant.openPdfExcel(file, type, StockStatusActivity.this);
        btnexcel.setEnabled(true);
        btnpdf.setEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    private void StopHandlerProcess() {
        Constant.BtnEnableDisable(true, btnsync, btnshow);
        progressbar.setVisibility(View.GONE);
        txtsec.setVisibility(View.GONE);
        mHandler.removeCallbacks(hMyTimeTask);
    }

    private void StartHandlerProcess() {
        Constant.BtnEnableDisable(false, btnsync, btnshow);
        progressbar.setVisibility(View.VISIBLE);
        txtsec.setVisibility(View.VISIBLE);
        txtsec.setText("");
        nCounter = 0;
        mHandler.postDelayed(hMyTimeTask, 1000);
    }

    private Runnable hMyTimeTask = new Runnable() {
        public void run() {
            nCounter++;
            txtsec.setText("" + nCounter);
            mHandler.postDelayed(hMyTimeTask, 1000);
        }
    };

    public void OnClearClick(View view) {
        txtsearchvalue.setText("");
        searchvalue = "0";
        searchvaluename = "";
        txtsearchvalue.performClick();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    /*public void BtnEnableDisable(boolean isenable) {
        btnsync.setEnabled(isenable);
        btnshow.setEnabled(isenable);
    }*/

    public void OnSyncStockClick(View view) {
        try {
            /*Date asondt = Constant.FormateDate(inFormatdate, asondate, subject, databaseHelper);
            String searchdata = txtsearchvalue.getText().toString().trim();

            if (asondt.after(new Date())) {
                StopHandlerProcess();
                Toast.makeText(StockStatusActivity.this, getResources().getString(R.string.asondateerr), Toast.LENGTH_SHORT).show();
                return;
            } *//*else if (searchvalue.equals("")) {
                StopHandlerProcess();
                Toast.makeText(StockStatusActivity.this, "Search Value not found", Toast.LENGTH_SHORT).show();
                return;
            } else if (txtsearchvalue.getText().toString().trim().length() == 0) {
                StopHandlerProcess();
                Toast.makeText(StockStatusActivity.this, "Please Add Search Value", Toast.LENGTH_SHORT).show();
                return;
            } *//* else if (searchdata.length() != 0 && containNameCode(searchdata) == null) {
                StopHandlerProcess();
                Toast.makeText(StockStatusActivity.this, "Please Add Correct Search Value", Toast.LENGTH_SHORT).show();
                return;
            }*/

            try {
                if (!CheckValidation()) {
                    if (txtsearchvalue.getText().toString().trim().length() == 0) {
                        searchvalue = "0";
                    }

                    if (AppController.isConnected(StockStatusActivity.this, findViewById(R.id.activity_main))) {

                        lytbottom.setVisibility(View.GONE);
                        lytmain.setVisibility(View.GONE);

                        StartHandlerProcess();

                        String url = Constant.StockStatusUrl + orgcode + "&compcode=" + compcode + "&demo=" + session.getData(UserSessionManager.KEY_DEMO) + "&date2=" + asondate + "&searchby=" + searchbyval + "&searchValue=" + searchvalue + "&reOrder=" + reorder + "&mobno=" + session.getData(UserSessionManager.KEY_Mobile) + "&imei=" + session.getData(UserSessionManager.KEY_IMEI) + "&devicecode=" + session.getData(UserSessionManager.KEY_DEVICECODE);
                        url = url.replaceAll(" ", "%20");

                        //System.out.println("=============url -- " + url);
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                                if (session.getData(UserSessionManager.KEY_DEMO).equalsIgnoreCase("Yes"))
                                    OwnerHomeActivity.DemoReqCout();

                                if (response.contains("\n"))
                                    response = response.replace("\n", "").trim();


                                /*if (response.contains(getResources().getString(R.string.noresponse))) {
                                    StopHandlerProcess();
                                    Toast.makeText(StockStatusActivity.this, getResources().getString(R.string.notconnectedserver), Toast.LENGTH_SHORT).show();
                                } else if (response.contains(getResources().getString(R.string.multiuseword))) {
                                    StopHandlerProcess();
                                    Toast.makeText(StockStatusActivity.this, getResources().getString(R.string.multiuse), Toast.LENGTH_SHORT).show();
                                }*/

                                if (Constant.ResponseErrorCheck(StockStatusActivity.this, response, true)) {
                                    StopHandlerProcess();
                                } else {
                                    String localdata = databaseHelper.getReceivableData(orgcode, compcode, asondate, String.valueOf(searchbypos), searchvalue);
                                    if (localdata.equals("")) {
                                        databaseHelper.addStockStatusdata(compcode, asondate, String.valueOf(searchbypos), searchvalue, response, orgcode, reorder);
                                    } else {
                                        databaseHelper.UpdateStockStatusData(orgcode, compcode, asondate, String.valueOf(searchbypos), searchvalue, response, reorder);
                                    }
                                    DisplayStockStatus(response);

                                }
                                StopHandlerProcess();
                            }

                        },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        StopHandlerProcess();
                                        String message = Constant.VolleyErrorMessage(error);

                                        if (!message.equals(""))
                                            Toast.makeText(StockStatusActivity.this, message, Toast.LENGTH_SHORT).show();

                                    }
                                });

                        AppController.getInstance().getRequestQueue().getCache().clear();
                        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        AppController.getInstance().addToRequestQueue(stringRequest);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();

                databaseHelper.addExceptionERROR(e, subject);
            }
        } catch (Exception e) {
            e.printStackTrace();
            databaseHelper.addExceptionERROR(e, subject);
        }
    }

    public boolean CheckValidation() {
        boolean iserr = false;

        Date asondt = Constant.FormateDate(inFormatdate, asondate, subject, databaseHelper);
        //System.out.println("==================== ==....-- " + searchvaluename + " == " + searchvalue);

        String searchdata = txtsearchvalue.getText().toString().trim();

        if (asondt.after(new Date())) {
            //StopHandlerProcess();
            Toast.makeText(StockStatusActivity.this, getResources().getString(R.string.asondateerr), Toast.LENGTH_SHORT).show();
            iserr = true;
        } /*else if (searchvalue.equals("")) {
                StopHandlerProcess();
                Toast.makeText(StockStatusActivity.this, "Search Value not found", Toast.LENGTH_SHORT).show();
                return;
            } else if (txtsearchvalue.getText().toString().trim().length() == 0) {
                StopHandlerProcess();
                Toast.makeText(StockStatusActivity.this, "Please Add Search Value", Toast.LENGTH_SHORT).show();
                return;
            } */ else if (searchdata.length() != 0 && containNameCode(searchdata) == null) {
            //StopHandlerProcess();
            Toast.makeText(StockStatusActivity.this, "Please Add Correct Search Value", Toast.LENGTH_SHORT).show();
            iserr = true;
        }

        return iserr;
    }

    public void OnShowStockClick(View view) {
        try {
            /*Date asondt = Constant.FormateDate(inFormatdate,asondate,subject,databaseHelper);
            System.out.println("==================== ==....-- " + searchvaluename + " == " + searchvalue);

            String searchdata = txtsearchvalue.getText().toString().trim();

            if (asondt.after(new Date())) {
                //StopHandlerProcess();
                Toast.makeText(StockStatusActivity.this, getResources().getString(R.string.asondateerr), Toast.LENGTH_SHORT).show();
                return;
            } *//*else if (searchvalue.equals("")) {
                StopHandlerProcess();
                Toast.makeText(StockStatusActivity.this, "Search Value not found", Toast.LENGTH_SHORT).show();
                return;
            } else if (txtsearchvalue.getText().toString().trim().length() == 0) {
                StopHandlerProcess();
                Toast.makeText(StockStatusActivity.this, "Please Add Search Value", Toast.LENGTH_SHORT).show();
                return;
            } *//* else if (searchdata.length() != 0 && containNameCode(searchdata) == null) {
                //StopHandlerProcess();
                Toast.makeText(StockStatusActivity.this, "Please Add Correct Search Value", Toast.LENGTH_SHORT).show();
                return;
            }*/

            try {
                if (!CheckValidation()) {
                    if (txtsearchvalue.getText().toString().trim().length() == 0) {
                        searchvalue = "0";
                    }

                    boolean issyncable = SyncVisibility();
                    if (from != null && !from.equals("home")) {
                        if (issyncable)
                            OnSyncStockClick(lyttop.getRootView());
                        else
                            Toast.makeText(StockStatusActivity.this, getResources().getString(R.string.datalimit), Toast.LENGTH_SHORT).show();
                        from = "home";
                    } else {
                        String localdata = databaseHelper.getStockStatusData(orgcode, compcode, asondate, String.valueOf(searchbypos), searchvalue, reorder);
                        if (localdata.equals("")) {
                            Toast.makeText(StockStatusActivity.this, getResources().getString(R.string.trytosync), Toast.LENGTH_SHORT).show();
                        } else {
                            StartHandlerProcess();
                            DisplayStockStatus(localdata);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                databaseHelper.addExceptionERROR(e, subject);
            }
        } catch (Exception e) {
            e.printStackTrace();
            databaseHelper.addExceptionERROR(e, subject);
        }
    }

    public void OnHideStockClick(View view) {
        Constant.SetViewVisiblility(imgdown, lyttop);
    }

    public void BtnSyncVisibility() {

        String localdata = "";
        try {
            localdata = databaseHelper.getStockStatusData(orgcode, compcode, asondate, String.valueOf(searchbypos), searchvalue, reorder);
        } catch (Exception e) {
            e.printStackTrace();
            databaseHelper.addExceptionERROR(e, subject);
        }


        if (localdata.equals("")) {
            //btnsync.setVisibility(View.VISIBLE);
            btnshow.setVisibility(View.GONE);
        } else {
            btnshow.setVisibility(View.VISIBLE);
            //btnsync.setVisibility(View.VISIBLE);
        }
        SyncVisibility();
    }

    public boolean SyncVisibility() {
        boolean issyncenable = false;
        if (session.getData(UserSessionManager.KEY_DEMO).equalsIgnoreCase("Yes") && session.getIntData(UserSessionManager.KEY_DEMOREQCOUNT) > (session.getIntData(UserSessionManager.KEY_DEMOREQTOTAL))) {
            btnsync.setVisibility(View.GONE);
            issyncenable = false;
        } else {
            btnsync.setVisibility(View.VISIBLE);
            issyncenable = true;
        }
        return issyncenable;
    }

   /* public void SetViewVisiblility() {
        imgdown.setVisibility(View.VISIBLE);
        if (lyttop.getVisibility() == View.GONE) {
            imgdown.setImageResource(R.drawable.ic_bup);
            lyttop.setVisibility(View.VISIBLE);
        } else {
            imgdown.setImageResource(R.drawable.ic_bdown);
            lyttop.setVisibility(View.GONE);
        }

    }*/

    public void DisplayStockStatus(String response) {

        if (databaseHelper.getAllMasterData(compcode, orgcode).size() == 0) {
            StopHandlerProcess();
            Toast.makeText(StockStatusActivity.this, getResources().getString(R.string.master_updatemsg), Toast.LENGTH_SHORT).show();
            return;
        }

        session.editor.putString(sessionname + "by" + compcode + orgcode, "" + searchbypos);
        session.editor.putString(sessionname + "date" + compcode + orgcode, asondate);
        session.editor.putString(sessionname + "valname" + compcode + orgcode, searchvaluename);
        session.editor.putString(sessionname + "val" + compcode + orgcode, searchvalue);
        session.editor.putBoolean(sessionname + "data" + compcode + orgcode, true);
        session.setData(sessionname + "reorder" + compcode + orgcode, reorder);
        session.editor.commit();

        try {
            JSONArray jsonArray = new JSONArray(response);

            if (jsonArray.length() == 2) {
                lytbottom.setVisibility(View.GONE);
                lytmain.setVisibility(View.GONE);
                lytpdfexcel.setVisibility(View.GONE);
                lyttotal.setVisibility(View.GONE);
                txtnodata.setVisibility(View.VISIBLE);
                imgdown.setVisibility(View.GONE);
                StopHandlerProcess();
            } else {
                asyntask = new AsyncTaskRunner(response).execute();
            }
        } catch (JSONException e) {
            StopHandlerProcess();
            e.printStackTrace();
            databaseHelper.addJSONERROR(e, subject);
        }/* catch (ParseException e) {
            e.printStackTrace();
            databaseHelper.addParseERROR(e, subject);
        }*/
    }

    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        String response;
        SalesReport totalreport;
        double total = 0;
        JSONArray jsonArray;

        public AsyncTaskRunner(String response) {
            this.response = response;
            try {
                this.jsonArray = new JSONArray(response);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //System.out.println("================== " + response);
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                //JSONArray jsonArray = new JSONArray(response);

                for (int i = 1; i < jsonArray.length() - 1; i++) {
                    //System.out.println("====================== == "+i);

                    JSONArray stockarray = jsonArray.getJSONArray(i);
                    String groupcode = stockarray.getString(0);
                    String groupname = databaseHelper.getMasterValue(compcode, DatabaseHelper.MASTER_MASTERCODE, groupcode, getResources().getString(R.string.master_item_group), DatabaseHelper.MASTER_MASTERNAME, orgcode);
                    String unitname = databaseHelper.getMasterValue(compcode, DatabaseHelper.MASTER_MASTERCODE, stockarray.getString(4), getResources().getString(R.string.master_item_unit), DatabaseHelper.MASTER_MASTERNAME, orgcode);

                    StockStatus stockStatus = null;
                    double qty = 0;
                    double rate = 0;
                    double amount = 0;

                    if (stockarray.getString(3).equals("0") || stockarray.getString(3).equals("")) {
                        amount = 0;
                        qty = 0;
                    } else {
                        qty = Constant.ConvertToDouble(stockarray.getString(3));
                    }
                    if (stockarray.getString(5).equals("0") || stockarray.getString(5).equals("")) {
                        rate = 0;
                        amount = 0;
                    } else {
                        rate = Constant.ConvertToDouble(stockarray.getString(5));
                    }

                    if (rate != 0 & qty != 0)
                        amount = qty * rate;

                    total = total + amount;

                    StockStatus stockStatuslist = new StockStatus(groupcode, stockarray.getString(1), stockarray.getString(2), Constant.ConvertToIndianRupeeFormat(qty, false, true, true) + "/" + unitname, stockarray.getString(4), Constant.ConvertToIndianRupeeFormat(rate, false, true, false), stockarray.getString(6), Constant.ConvertToIndianRupeeFormat(amount, false, true, false));

                    if (stockstatuslist.containsKey(groupcode)) {
                        stockStatus = stockstatuslist.get(groupcode);
                        stockStatus.setDoubleqty(stockStatus.getDoubleqty() + qty);
                        stockStatus.setDoubleamount(stockStatus.getDoubleamount() + amount);
                        ArrayList<StockStatus> stockStatusArrayList = stockStatus.getStockStatusArrayList();
                        stockStatusArrayList.add(stockStatuslist);
                        stockStatus.setStockStatusArrayList(stockStatusArrayList);

                    } else {
                        ArrayList<StockStatus> stockStatusArrayList = new ArrayList<>();
                        stockStatusArrayList.add(stockStatuslist);
                        stockStatus = new StockStatus(groupcode, groupname, qty, amount, stockStatusArrayList);
                    }

                    stockstatuslist.put(groupcode, stockStatus);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                databaseHelper.addJSONERROR(e, subject);
            }
            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            StopHandlerProcess();

            try {

                asondatedisplay = txtasondate.getText().toString().trim();

                String header = companyname + "\n Stock Status on " + asondatedisplay;
                SpannableString ssdate = new SpannableString(header);
                ssdate.setSpan(new RelativeSizeSpan(1.3f), 0, companyname.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ssdate.setSpan(new StyleSpan(Typeface.BOLD), 0, companyname.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ssdate.setSpan(new UnderlineSpan(), header.indexOf("Stock"), header.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                datedisplay.setText(ssdate);
                filename = companyname + "-Stockstatus-" + searchvalue + " on " + asondatedisplay;

                pdfheadertext = "Stock Status on " + asondatedisplay;
                headertext = "\n" + companyname + "\n" + address + "\n\nStock Status on " + asondatedisplay + "\n";


                stockStatusArrayList = new ArrayList<StockStatus>(stockstatuslist.values());

                if (stockStatusArrayList.size() != 0) {
                    Constant.SetViewVisiblility(imgdown, lyttop);
                    BtnSyncVisibility();
                    txtnodata.setVisibility(View.GONE);
                    lytbottom.setVisibility(View.VISIBLE);
                    lytmain.setVisibility(View.VISIBLE);
                    lytpdfexcel.setVisibility(View.VISIBLE);
                    lyttotal.setVisibility(View.VISIBLE);
                    txttotalitem.setText("Total   ( " + (jsonArray.length() - 2) + " Items )");
                    txttotal.setText(Constant.ConvertToIndianRupeeFormat(total, false, true, false));

                }

                recycleview.setAdapter(new StockStatusAdapter(stockStatusArrayList));

            } catch (Exception e) {
                e.printStackTrace();
                databaseHelper.addExceptionERROR(e, subject);
            }
        }


        @Override
        protected void onPreExecute() {
            stockstatuslist.clear();
            stockStatusArrayList.clear();
            Constant.BtnEnableDisable(false, btnsync, btnshow);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (asyntask != null)
            asyntask.cancel(true);
    }

    public class StockStatusAdapter extends RecyclerView.Adapter<StockStatusAdapter.ViewHolder> {

        public ArrayList<StockStatus> stockStatus;


        public StockStatusAdapter(ArrayList<StockStatus> stockStatus) {
            this.stockStatus = stockStatus;
        }

        @NonNull
        @Override
        public StockStatusAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lyt_stockstatus, parent, false);

            return new StockStatusAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

            if (position % 2 == 0)
                holder.lytheader.setBackgroundColor(getResources().getColor(R.color.stock2));
            else
                holder.lytheader.setBackgroundColor(getResources().getColor(R.color.stock3));

            final StockStatus model = stockStatus.get(position);

            //holder.txtqty.setText(indianFormat.format(new BigDecimal(model.getDoubleqty())).replaceAll("₹", "").trim());
            holder.txtqty.setText(Constant.ConvertToIndianRupeeFormat(model.getDoubleqty(), false, true, true));

            String data = Constant.ConvertToIndianRupeeFormat(model.getDoubleamount(), false, true, false);
            holder.txtamount.setText(data);

            //holder.txtname.setText(model.getGroupname());
            String name = model.getGroupname();
            if (model.getGroupname() == null || model.getGroupname().equals(""))
                name = getResources().getString(R.string.nodata_updatemaster);

            holder.txtname.setText(Html.fromHtml(name));


            holder.txtname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (model.getGroupname() == null || model.getGroupname().equals(""))
                        session.OnUpdateMasterClick(new VolleyCallback() {
                            @Override
                            public void onSuccess(boolean result) {
                                if (result)
                                    OnShowStockClick(lyttop.getRootView());
                            }

                            public void onSuccessWithMsg(boolean result, String message) {
                            }
                        }, StockStatusActivity.this, session, orgcode, compcode, databaseHelper, subject, findViewById(R.id.activity_main));

                }

            });

            holder.imgarrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.recycleview.getVisibility() == View.VISIBLE) {
                        holder.recycleview.setVisibility(View.GONE);
                        holder.imgarrow.setImageResource(R.drawable.ic_downarrow);
                    } else {
                        holder.recycleview.setVisibility(View.VISIBLE);
                        holder.imgarrow.setImageResource(R.drawable.ic_up);
                    }
                }
            });

            if (stockStatus.size() == 1) {
                holder.imgarrow.performClick();
            }

            holder.recycleview.setAdapter(new StockStatusChildAdapter(model.getStockStatusArrayList()));
        }


        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return stockStatus.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            public TextView txtqty, txtname, txtamount;
            ImageView imgarrow;
            RecyclerView recycleview;
            LinearLayout lytheader;

            public ViewHolder(View itemView) {
                super(itemView);

                txtqty = itemView.findViewById(R.id.txtqty);
                txtname = itemView.findViewById(R.id.txtname);
                imgarrow = itemView.findViewById(R.id.imgarrow);
                txtamount = itemView.findViewById(R.id.txtamount);
                recycleview = itemView.findViewById(R.id.recycleview);
                lytheader = itemView.findViewById(R.id.lytheader);
                recycleview.setLayoutManager(new LinearLayoutManager(StockStatusActivity.this));

            }

        }
    }

    public class StockStatusChildAdapter extends RecyclerView.Adapter<StockStatusChildAdapter.ViewHolder> {

        public ArrayList<StockStatus> salesReports;


        public StockStatusChildAdapter(ArrayList<StockStatus> salesReports) {
            this.salesReports = salesReports;
        }

        @NonNull
        @Override
        public StockStatusChildAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lyt_stockstatus_child, parent, false);

            return new StockStatusChildAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

            final StockStatus model = salesReports.get(position);
            holder.txtqty.setText(model.getQty());
            holder.txtname.setText(model.getItemname());
            holder.txtrate.setText(model.getRate());

            String data = model.getAmount();
            holder.txtamount.setText(data);
            int size;
            if (data.length() <= 12) {
                size = 12;
            } else if (data.length() <= 14) {
                size = 10;
            } else if (data.length() <= 16) {
                size = 9;
            } else {
                size = 8;
            }
            holder.txtamount.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
        }


        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return salesReports.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            public TextView txtname, txtrate, txtamount, txtqty;

            public ViewHolder(View itemView) {
                super(itemView);

                txtname = itemView.findViewById(R.id.txtname);
                txtrate = itemView.findViewById(R.id.txtrate);
                txtamount = itemView.findViewById(R.id.txtamount);
                txtqty = itemView.findViewById(R.id.txtqty);

            }

        }
    }


}
