package com.accountspro.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.accountspro.R;
import com.accountspro.helper.AppController;
import com.accountspro.helper.Constant;
import com.accountspro.helper.DatabaseHelper;
import com.accountspro.helper.UserSessionManager;
import com.accountspro.helper.VolleyCallback;
import com.accountspro.model.Company;
import com.accountspro.model.Master;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class CompanyActivity extends AppCompatActivity {

    RecyclerView recycleview;
    TextView txtsecbtncom, txtnodata, txtsec, txtsecbtn;//, txtmobile;
    ProgressBar progressbar, progressbarbtn, progressbarcom;
    LinearLayout lytlist;
    DatabaseHelper databaseHelper;
    ArrayList<Company> companyArrayList, localcomlist;
    UserSessionManager session;
    String from, subject;
    ImageView imgsync;
    String ccode = "";
    Button lytbottom;
    int lastSelectedPosition = 0, nCounter = 0;
    private Handler mHandler = new Handler();
    Company companydata;
    String orgreg;
    AsyncTask<String, String, String> asyntask, fetchtask;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company);

        session = new UserSessionManager(CompanyActivity.this);
        databaseHelper = new DatabaseHelper(CompanyActivity.this);
        orgreg = session.getData(UserSessionManager.KEY_ORG_REGKEY);
        subject = Constant.ERRORID + " | " + session.getData(UserSessionManager.KEY_Mobile) + " | " + session.getData(UserSessionManager.KEY_ORG_NAME) + " | " + Constant.ERRORNOCOMP;
        try {
            from = getIntent().getStringExtra("from");
            companyArrayList = new ArrayList<>();
            localcomlist = new ArrayList<>();

            lytbottom = (Button) findViewById(R.id.lytbottom);
            imgsync = (ImageView) findViewById(R.id.imgsync);
            recycleview = (RecyclerView) findViewById(R.id.recycleview);
            recycleview.setLayoutManager(new LinearLayoutManager(CompanyActivity.this));
            txtnodata = (TextView) findViewById(R.id.txtnodata);
            txtsecbtncom = (TextView) findViewById(R.id.txtsecbtncom);
            txtsec = (TextView) findViewById(R.id.txtsec);
            txtsecbtn = (TextView) findViewById(R.id.txtsecbtn);
            progressbarbtn = (ProgressBar) findViewById(R.id.progressbarbtn);
            //txtmobile = (TextView) findViewById(R.id.txtmobile);
            //txtmobile.setText(session.getData(UserSessionManager.KEY_Mobile));
            progressbar = (ProgressBar) findViewById(R.id.progressbar);
            progressbarcom = (ProgressBar) findViewById(R.id.progressbarcom);
            lytlist = (LinearLayout) findViewById(R.id.lytlist);


        /*if (from != null && from.equals("home")) {
            imgsync.setVisibility(View.VISIBLE);
        }*/

            imgsync.setVisibility(View.VISIBLE);
            ccode = session.getData(orgreg + UserSessionManager.KEY_COM_CODE);

        /*localcomlist = databaseHelper.getAllCompany(orgreg);
        if(localcomlist.size() != 0)
            GetCompany(localcomlist);
        else
            OnRefreshClick(imgsync.getRootView());*/

            GetCompany();

            if (AppController.isNetConnected(CompanyActivity.this))
                RefreshCompanyData(false);

        } catch (Exception e) {
            e.printStackTrace();

            databaseHelper.addExceptionERROR(e, subject);
        }
    }

    private void StopHandlerProcess() {
        progressbar.setVisibility(View.GONE);
        txtsec.setVisibility(View.GONE);
        mHandler.removeCallbacks(hMyTimeTask);
    }

    private void StartHandlerProcess() {
        progressbar.setVisibility(View.VISIBLE);
        txtsec.setVisibility(View.VISIBLE);
        txtsec.setText("");
        nCounter = 0;
        mHandler.postDelayed(hMyTimeTask, 1000);
    }

    private Runnable hMyTimeTask = new Runnable() {
        public void run() {
            nCounter++;
            txtsec.setText("" + nCounter);
            mHandler.postDelayed(hMyTimeTask, 1000);
        }
    };

    private void GetCompany() {
        localcomlist = databaseHelper.getAllCompany(orgreg);
        //System.out.println("============== = =com " + orgreg + " = " + localcomlist.size());

        if (localcomlist.size() != 0) {

            progressbar.setVisibility(View.VISIBLE);
            int i = 0;
            for (Company company : localcomlist) {
                companyArrayList.add(company);
                //System.out.println("============== = =com " + company.getCompname() + " = " + company.getCompcode());
                if (!ccode.equals("") && !orgreg.equals("") && company.getOrgreg().equals(orgreg) && company.getCompcode().equals(ccode))
                    lastSelectedPosition = i;

                i++;
            }

            progressbar.setVisibility(View.GONE);
            recycleview.setAdapter(new CompanyAdapter(companyArrayList, CompanyActivity.this));
        } else {
            //System.out.println("=============== = =  = = = refresh click");
            OnRefreshClick(imgsync.getRootView());
        }
    }

    public void OnRefreshClick(View view) {
        if (AppController.isConnected(CompanyActivity.this, null))
            RefreshCompanyData(true);
    }

    private void RefreshCompanyData(boolean isprogressvisible) {
        //if (AppController.isConnected(CompanyActivity.this, findViewById(R.id.snackbarid))) {

        if (isprogressvisible) {
            imgsync.setEnabled(false);
            StartHandlerProcess();
        }
        String url = Constant.GETCOMPANYNUrl + session.getData(UserSessionManager.KEY_ORG_REGKEY) + "&mobno=" + session.getData(UserSessionManager.KEY_Mobile) + "&imei=" + session.getData(UserSessionManager.KEY_IMEI) + "&devicecode=" + session.getData(UserSessionManager.KEY_DEVICECODE) + "&demo=" + session.getData(UserSessionManager.KEY_DEMO);
        //System.out.println("=============== = =  = = = "+isprogressvisible+" = " + url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.contains("\n"))
                    response = response.replace("\n", "").trim();

                //System.out.println("=============== = =  = = ="+isprogressvisible+" = " + response);

                if (Constant.ResponseErrorCheck(CompanyActivity.this, response, isprogressvisible)) {
                    if (isprogressvisible) {
                        StopHandlerProcess();
                    }
                } else {

                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        if (jsonArray.length() > 2) {

                            asyntask = new AsyncTaskRunner(response, isprogressvisible).execute();

                            /*session.setBooleanData(UserSessionManager.KEY_ORGSET, true);
                            session.setBooleanData(orgreg + UserSessionManager.KEY_COMSET, true);
                            databaseHelper.DeleteData(DatabaseHelper.TABLE_COMPANY, orgreg);

                            if (isprogressvisible) companyArrayList.clear();

                            for (int i = 1; i < jsonArray.length() - 1; i++) {
                                JSONArray comarray = jsonArray.getJSONArray(i);
                                Company company = new Company(comarray.getString(0), comarray.getString(1), comarray.getString(2), comarray.getString(3), orgreg, comarray.getString(4), comarray.getString(5), comarray.getString(6), comarray.getString(7), comarray.getString(8), comarray.getString(9), comarray.getString(10), comarray.getString(11));
                                databaseHelper.addCompanydata(company);
                                //System.out.println("=========com - " + company.getCompname());
                                if (isprogressvisible && i == 1) {
                                    session.setCompanyData(company, orgreg);
                                }

                            }

                            if(isprogressvisible)
                                GetCompany();*/

                        } else if (isprogressvisible) {
                            StopHandlerProcess();
                            Toast.makeText(CompanyActivity.this, "Sorry No Company Found", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        StopHandlerProcess();
                        if (e.toString().contains(getResources().getString(R.string.noresponse)))
                            Toast.makeText(CompanyActivity.this, getResources().getString(R.string.notconnectedserver), Toast.LENGTH_SHORT).show();
                        else {
                            e.printStackTrace();
                            databaseHelper.addJSONERROR(e, subject);
                        }
                    }
                }


            }

        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (isprogressvisible) {
                            imgsync.setEnabled(true);
                            StopHandlerProcess();
                        }
                        String message = Constant.VolleyErrorMessage(error);

                        if (isprogressvisible && !message.equals(""))
                            Toast.makeText(CompanyActivity.this, message, Toast.LENGTH_SHORT).show();

                    }
                });
        AppController.getInstance().getRequestQueue().getCache().clear();
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);

        /*} else {
            lytbottom.setEnabled(true);
        }*/
    }

    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        String response;
        JSONArray jsonArray;
        boolean isprogressvisible;

        public AsyncTaskRunner(String response, boolean isprogressvisible) {
            this.response = response;
            this.isprogressvisible = isprogressvisible;
            try {
                jsonArray = new JSONArray(response);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                for (int i = 1; i < jsonArray.length() - 1; i++) {
                    JSONArray comarray = jsonArray.getJSONArray(i);
                    Company company = new Company(comarray.getString(0), comarray.getString(1), comarray.getString(2), comarray.getString(3), orgreg, comarray.getString(4), comarray.getString(5), comarray.getString(6), comarray.getString(7), comarray.getString(8), comarray.getString(9), comarray.getString(10), comarray.getString(11));
                    databaseHelper.addCompanydata(company);
                    //System.out.println("=========com - " + company.getCompname());
                    if (isprogressvisible && i == 1) {
                        session.setCompanyData(company, orgreg);
                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();

                databaseHelper.addJSONERROR(e, subject);
            }
            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            //System.out.println("================= =  =post - "+result);
            try {

                if (isprogressvisible) {
                    imgsync.setEnabled(true);
                    StopHandlerProcess();
                }

                if (isprogressvisible)
                    GetCompany();

            } catch (Exception e) {
                e.printStackTrace();
                StopHandlerProcess();
                databaseHelper.addExceptionERROR(e, subject);
            }

        }


        @Override
        protected void onPreExecute() {
            session.setBooleanData(UserSessionManager.KEY_ORGSET, true);
            session.setBooleanData(orgreg + UserSessionManager.KEY_COMSET, true);
            databaseHelper.DeleteData(DatabaseHelper.TABLE_COMPANY, orgreg);

            if (isprogressvisible) companyArrayList.clear();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (asyntask != null)
            asyntask.cancel(true);

        if (fetchtask != null)
            fetchtask.cancel(true);
    }

    private void StopBtnHandlerProcess() {
        progressbarbtn.setVisibility(View.GONE);
        txtsecbtn.setVisibility(View.GONE);
        mHandler.removeCallbacks(hMyTimeTaskBtn);
    }

    private void StartBtnHandlerProcess() {
        progressbarbtn.setVisibility(View.VISIBLE);
        txtsecbtn.setVisibility(View.VISIBLE);
        txtsecbtn.setText("");
        nCounter = 0;
        mHandler.postDelayed(hMyTimeTaskBtn, 1000);
    }

    private Runnable hMyTimeTaskBtn = new Runnable() {
        public void run() {
            nCounter++;
            txtsecbtn.setText("" + nCounter);
            mHandler.postDelayed(hMyTimeTaskBtn, 1000);
        }
    };

    public class CompanyAdapter extends RecyclerView.Adapter<CompanyAdapter.ItemRowHolder> {
        private ArrayList<Company> chapterlist;
        private Context mContext;
        int selectedItem;

        public CompanyAdapter(ArrayList<Company> chapterlist, Context context) {
            this.chapterlist = chapterlist;
            this.mContext = context;
        }

        @Override
        public ItemRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lyt_organization, parent, false);
            return new ItemRowHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull final ItemRowHolder holder, final int position) {

            final Company company = chapterlist.get(position);
            holder.txtname.setText(company.getCompname());
            //holder.txtkey.setText(company.getCompcode());

            if (lastSelectedPosition == position) {

                companydata = company;
                /*comcode = company.getCompcode();
                comname = company.getCompname();
                comyr = company.getAcyear();
                comyrcode = company.getYearcode();*/

                holder.btnradio.setChecked(true);
                holder.txtname.setTextColor(getResources().getColor(R.color.colorPrimary));
            } else {
                holder.btnradio.setChecked(false);
                holder.txtname.setTextColor(getResources().getColor(R.color.black));
            }
            holder.btnradio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = position;
                    holder.btnradio.setChecked(true);

                   /* comcode = company.getCompcode();
                    comname = company.getCompname();
                    comyr = company.getAcyear();
                    comyrcode = company.getYearcode();*/

                   /* session.setData(orgreg+UserSessionManager.KEY_COM_CODE, company.getCompcode());
                    session.setData(orgreg+UserSessionManager.KEY_COM_NAME, company.getCompname());
                    session.setData(orgreg+UserSessionManager.KEY_COM_ACYEAR, company.getAcyear());
                    session.setData(orgreg+UserSessionManager.KEY_COM_YEARCODE, company.getYearcode());*/
                    notifyDataSetChanged();
                }
            });

            holder.lytmain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.btnradio.performClick();
                }
            });


        }

        public void setSelecteditem(int selecteditem) {
            this.selectedItem = selecteditem;
            notifyDataSetChanged();
        }

        @Override
        public int getItemCount() {
            return (null != chapterlist ? chapterlist.size() : 0);
        }

        public class ItemRowHolder extends RecyclerView.ViewHolder {
            TextView txtname;
            RadioButton btnradio;
            LinearLayout lytmain;

            public ItemRowHolder(View itemView) {
                super(itemView);
                //txtkey = itemView.findViewById(R.id.txtkey);
                txtname = itemView.findViewById(R.id.txtname);
                btnradio = itemView.findViewById(R.id.btnradio);
                lytmain = itemView.findViewById(R.id.lytmain);

            }
        }

    }

    public void OnCompanySelectClick(View view) {

        //session.setCompanyData(companydata,orgreg);

        lytbottom.setEnabled(false);

        //System.out.println("============== = == = =data -  "+session.getData(orgreg+UserSessionManager.KEY_COM_CODE)+" = "+session.getData(orgreg+UserSessionManager.KEY_COM_NAME));

        if (companydata == null) {
            Toast.makeText(CompanyActivity.this, getResources().getString(R.string.datanotfound), Toast.LENGTH_SHORT).show();
            return;
        }

        session.setBooleanData(orgreg + UserSessionManager.KEY_COMSET, true);
        //final String companycode = session.getData(orgreg+UserSessionManager.KEY_COM_CODE);
        final String companycode = companydata.getCompcode();
        if (databaseHelper.IsMasterExists(companycode, orgreg)) {
            session.setCompanyData(companydata, orgreg);
            startActivity(new Intent(CompanyActivity.this, OwnerHomeActivity.class));
            lytbottom.setEnabled(true);
            finishAffinity();
        } else if (AppController.isConnected(CompanyActivity.this, findViewById(R.id.snackbarid))) {

            //fetchtask = new FetchAsyncRunner(companycode).execute();

            //Toast.makeText(CompanyActivity.this, getResources().getString(R.string.fetchingcompanydata), Toast.LENGTH_SHORT).show();
            //StartBtnHandlerProcess();

            progressbarcom.setVisibility(View.VISIBLE);
            txtsecbtncom.setVisibility(View.VISIBLE);

            GetComMaster(companycode, new VolleyCallback() {
                @Override
                public void onSuccess(boolean result) {
                    if (result) {
                        //StopBtnHandlerProcess();
                        progressbarcom.setVisibility(View.GONE);
                        txtsecbtncom.setVisibility(View.GONE);
                        startActivity(new Intent(CompanyActivity.this, OwnerHomeActivity.class));
                        finishAffinity();
                    }
                }

                @Override
                public void onSuccessWithMsg(boolean result, String message) {
                }
            });

            /*Toast.makeText(CompanyActivity.this, getResources().getString(R.string.fetchingcompanydata), Toast.LENGTH_SHORT).show();
            StartBtnHandlerProcess();

            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.GETMASTERNUrl + session.getData(UserSessionManager.KEY_ORG_REGKEY) + "&compcode=" + companycode + "&mobno=" + session.getData(UserSessionManager.KEY_Mobile) + "&imei=" + session.getData(UserSessionManager.KEY_IMEI) + "&devicecode=" + session.getData(UserSessionManager.KEY_DEVICECODE) + "&demo=" + session.getData(UserSessionManager.KEY_DEMO), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    if (response.contains("\n"))
                        response = response.replace("\n", "").trim();

                    if (response.contains(getResources().getString(R.string.noresponse))) {
                        StopBtnHandlerProcess();
                        Toast.makeText(CompanyActivity.this, getResources().getString(R.string.notconnectedserver), Toast.LENGTH_SHORT).show();
                    } else if (response.contains(getResources().getString(R.string.multiuseword))) {
                        StopBtnHandlerProcess();
                        Toast.makeText(CompanyActivity.this, getResources().getString(R.string.multiuse), Toast.LENGTH_SHORT).show();
                    }else if (response.contains(getResources().getString(R.string.qstringerr))) {
                        StopBtnHandlerProcess();
                        Toast.makeText(CompanyActivity.this, getResources().getString(R.string.trylater), Toast.LENGTH_SHORT).show();
                    } else {

                        databaseHelper.DeleteMasterData(companycode, orgreg);

                        try {
                            session.setCompanyData(companydata, orgreg);
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i = 1; i < jsonArray.length() - 1; i++) {
                                JSONArray masterarray = jsonArray.getJSONArray(i);
                                //System.out.println("=============data == = " + masterarray.getString(0) + " = " + masterarray.getString(1) + "=" + masterarray.getString(2) + "=" + masterarray.getString(3) + "=" + masterarray.getString(4));
                                Master master = new Master(masterarray.getString(0), masterarray.getString(1), masterarray.getString(2), masterarray.getString(3), masterarray.getString(4), masterarray.getString(5), masterarray.getString(6), masterarray.getString(7), masterarray.getString(8), companycode, orgreg);
                                databaseHelper.addMasterdata(master);

                            }


                            StopBtnHandlerProcess();
                            startActivity(new Intent(CompanyActivity.this, OwnerHomeActivity.class));
                            finishAffinity();
                        } catch (JSONException e) {
                            StopBtnHandlerProcess();
                            if (e.toString().contains(getResources().getString(R.string.noresponse)))
                                Toast.makeText(CompanyActivity.this, getResources().getString(R.string.notconnectedserver), Toast.LENGTH_SHORT).show();
                            else {
                                e.printStackTrace();
                                databaseHelper.addJSONERROR(e, subject);
                            }
                        }
                    }

                    lytbottom.setEnabled(true);

                }

            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            lytbottom.setEnabled(true);
                            StopBtnHandlerProcess();
                            String message = Constant.VolleyErrorMessage(error);
                            if (!message.equals(""))
                                Toast.makeText(CompanyActivity.this, message, Toast.LENGTH_SHORT).show();

                        }
                    });
            AppController.getInstance().getRequestQueue().getCache().clear();
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(stringRequest);*/
        } else {
            lytbottom.setEnabled(true);
        }
    }

    private void GetComMaster(String companycode, VolleyCallback volleyCallback) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.GETMASTERNUrl + session.getData(UserSessionManager.KEY_ORG_REGKEY) + "&compcode=" + companycode + "&mobno=" + session.getData(UserSessionManager.KEY_Mobile) + "&imei=" + session.getData(UserSessionManager.KEY_IMEI) + "&devicecode=" + session.getData(UserSessionManager.KEY_DEVICECODE) + "&demo=" + session.getData(UserSessionManager.KEY_DEMO), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response.contains("\n"))
                    response = response.replace("\n", "").trim();

                /*if (response.contains(getResources().getString(R.string.noresponse))) {
                    progressbarcom.setVisibility(View.GONE);
                    txtsecbtncom.setVisibility(View.GONE);
                    Toast.makeText(CompanyActivity.this, getResources().getString(R.string.notconnectedserver), Toast.LENGTH_SHORT).show();
                } else if (response.contains(getResources().getString(R.string.multiuseword))) {
                    progressbarcom.setVisibility(View.GONE);
                    txtsecbtncom.setVisibility(View.GONE);
                    Toast.makeText(CompanyActivity.this, getResources().getString(R.string.multiuse), Toast.LENGTH_SHORT).show();
                } else if (response.contains(getResources().getString(R.string.qstringerr))) {
                    progressbarcom.setVisibility(View.GONE);
                    txtsecbtncom.setVisibility(View.GONE);
                    Toast.makeText(CompanyActivity.this, getResources().getString(R.string.trylater), Toast.LENGTH_SHORT).show();
                } */
                if (Constant.ResponseErrorCheck(CompanyActivity.this, response, true)) {
                    progressbarcom.setVisibility(View.GONE);
                    txtsecbtncom.setVisibility(View.GONE);
                } else {
                    databaseHelper.DeleteMasterData(companycode, orgreg);

                    try {
                        session.setCompanyData(companydata, orgreg);
                        JSONArray jsonArray = new JSONArray(response);
                        for (int i = 1; i < jsonArray.length() - 1; i++) {
                            JSONArray masterarray = jsonArray.getJSONArray(i);
                            //System.out.println("=============data == = " + masterarray.getString(0) + " = " + masterarray.getString(1) + "=" + masterarray.getString(2) + "=" + masterarray.getString(3) + "=" + masterarray.getString(4));
                            Master master = new Master(masterarray.getString(0), masterarray.getString(1), masterarray.getString(2), masterarray.getString(3), masterarray.getString(4), masterarray.getString(5), masterarray.getString(6), masterarray.getString(7), masterarray.getString(8), companycode, orgreg);
                            databaseHelper.addMasterdata(master);

                        }

                        volleyCallback.onSuccess(true);
                        /*StopBtnHandlerProcess();
                        startActivity(new Intent(CompanyActivity.this, OwnerHomeActivity.class));
                        finishAffinity();*/
                    } catch (JSONException e) {
                        progressbarcom.setVisibility(View.GONE);
                        txtsecbtncom.setVisibility(View.GONE);
                        if (e.toString().contains(getResources().getString(R.string.noresponse)))
                            Toast.makeText(CompanyActivity.this, getResources().getString(R.string.notconnectedserver), Toast.LENGTH_SHORT).show();
                        else {
                            e.printStackTrace();
                            databaseHelper.addJSONERROR(e, subject);
                        }
                    }
                }

                lytbottom.setEnabled(true);

            }

        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        lytbottom.setEnabled(true);
                        progressbarcom.setVisibility(View.GONE);
                        txtsecbtncom.setVisibility(View.GONE);
                        String message = Constant.VolleyErrorMessage(error);
                        if (!message.equals(""))
                            Toast.makeText(CompanyActivity.this, message, Toast.LENGTH_SHORT).show();

                    }
                });
        AppController.getInstance().getRequestQueue().getCache().clear();
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }


}
