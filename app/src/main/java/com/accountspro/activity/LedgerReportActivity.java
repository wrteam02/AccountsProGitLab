package com.accountspro.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.accountspro.R;
import com.accountspro.adapter.AutoCompleteAdapter;
import com.accountspro.helper.AppController;
import com.accountspro.helper.Constant;
import com.accountspro.helper.DatabaseHelper;
import com.accountspro.helper.InstantAutoComplete;
import com.accountspro.helper.UserSessionManager;
import com.accountspro.helper.VolleyCallback;
import com.accountspro.model.LedgerReport;
import com.accountspro.model.Master;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.apache.poi.hssf.usermodel.HeaderFooter;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Footer;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.PrintSetup;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class LedgerReportActivity extends AppCompatActivity {

    LedgerReportAdapter adapter;
    RecyclerView recycleview;
    ArrayList<LedgerReport> ledgerReportArrayList;//, allReportlist, newlist;
    InstantAutoComplete txtledgername;
    UserSessionManager session;
    DatabaseHelper databaseHelper;
    String subject, fromhome, from, localdata, sdt, edt, startdate, enddate, companyname, compcode, orgcode, address, ledgername, headertext, path, filename, pdfheadertext, ledgernamecode = "0", narration = "No";
    ArrayList<Master> ledgernamelist;
    public int ReqReadPermission = 1;
    public int ReqWritePermission = 2;
    Toolbar toolbar;
    Master masterledgernamedata;
    Button btnsync, btnshow;
    ImageView imgdown, imgclear;
    ImageButton btnpdf, btnexcel, btnemail;
    LinearLayout lytinput, lyttotaldrcr, lyttotal, lytaddress, lytledgermain, lytdisplay, lytmain, lytpdfexcel, lytprogress, lytbottom, lytfrom, lytto, lytcontain, lyttop;
    TextView totalname, datedisplay, txtnodata, txttotalcr, txttotaldr, txtsec, txtfrom, txtfromdt, txttodt, txtto, txtledger, txtaddress, txtmobno, txtemail, txtopeningbalance, txtclosingbalance;
    RadioGroup radiogroup;
    RadioButton radioyes, radiono;
    int nCounter = 0;
    private Handler mHandler = new Handler();
    private Handler loadhandler = new Handler();
    ProgressBar progressbar, recycleprogress;
    Spinner spquick;

    SimpleDateFormat month_date = new SimpleDateFormat("MMM");
    SimpleDateFormat date_name = new SimpleDateFormat("EEEE");
    SimpleDateFormat output = new SimpleDateFormat("dd/MM/yy");

    SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat inFormatdate = new SimpleDateFormat("yyyyMMdd");
    //NumberFormat indianFormat = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
    //SimpleDateFormat input = new SimpleDateFormat("yyyyMMdd");

    Calendar calendar;
    //String sessionname = "ledgereport";
    String sessionname = Constant.LedgerReportSessionName;
    boolean isfirst = false;
    AsyncTask<String, String, String> asyntask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ledger_report);

        Intent intent = getIntent();
        from = intent.getStringExtra("from");
        fromhome = from;

        databaseHelper = new DatabaseHelper(LedgerReportActivity.this);
        session = new UserSessionManager(LedgerReportActivity.this);
        orgcode = session.getData(UserSessionManager.KEY_ORG_REGKEY);
        compcode = session.getData(orgcode + UserSessionManager.KEY_COM_CODE);
        companyname = session.getData(orgcode + UserSessionManager.KEY_COM_NAME);
        address = session.getData(orgcode + UserSessionManager.KEY_COM_ADDRESS);
        subject = Constant.ERRORID + " | " + session.getData(UserSessionManager.KEY_Mobile) + " | " + session.getData(UserSessionManager.KEY_ORG_NAME) + " | " + companyname;


        try {

            ledgernamelist = databaseHelper.getMasterRowData(compcode, getResources().getString(R.string.master_ledger_name), orgcode);
            ledgerReportArrayList = new ArrayList<>();

            totalname = findViewById(R.id.totalname);
            lyttotal = findViewById(R.id.lyttotal);
            lyttotaldrcr = findViewById(R.id.lyttotaldrcr);


            lytaddress = findViewById(R.id.lytaddress);
            lytledgermain = findViewById(R.id.lytledgermain);

            //scrollView = findViewById(R.id.scrollView);
            datedisplay = findViewById(R.id.datedisplay);
            lytmain = findViewById(R.id.lytmain);
            txtnodata = findViewById(R.id.txtnodata);
            btnexcel = findViewById(R.id.btnexcel);
            btnemail = findViewById(R.id.btnemail);
            btnpdf = findViewById(R.id.btnpdf);
            lytpdfexcel = findViewById(R.id.lytpdfexcel);
            lytprogress = findViewById(R.id.lytprogress);
            lytbottom = findViewById(R.id.lytbottom);
            txttotalcr = findViewById(R.id.txttotalcr);
            txtledgername = findViewById(R.id.txtledgername);
            recycleview = findViewById(R.id.recycleview);

            btnsync = findViewById(R.id.btnsync);
            btnshow = findViewById(R.id.btnshow);
            imgclear = findViewById(R.id.imgclear);
            imgdown = findViewById(R.id.imgdown);
            lytto = findViewById(R.id.lytto);
            lytfrom = findViewById(R.id.lytfrom);
            lyttop = findViewById(R.id.lyttop);
            lytcontain = findViewById(R.id.lytcontain);
            lytdisplay = findViewById(R.id.lytdisplay);
            txtfrom = findViewById(R.id.txtfrom);
            txtfromdt = findViewById(R.id.txtfromdt);
            txttodt = findViewById(R.id.txttodt);
            txtto = findViewById(R.id.txtto);
            txtledger = findViewById(R.id.txtledger);
            txtaddress = findViewById(R.id.txtaddress);
            txtmobno = findViewById(R.id.txtmobno);
            txtemail = findViewById(R.id.txtemail);
            txtopeningbalance = findViewById(R.id.txtopeningbalance);
            txtclosingbalance = findViewById(R.id.txtclosingbalance);
            radiogroup = findViewById(R.id.radiogroup);
            radioyes = findViewById(R.id.radioyes);
            radiono = findViewById(R.id.radiono);
            recycleprogress = findViewById(R.id.recycleprogress);
            progressbar = findViewById(R.id.progressbar);
            txtsec = findViewById(R.id.txtsec);
            txttotaldr = findViewById(R.id.txttotaldr);
            spquick = findViewById(R.id.spquick);
            lytinput = findViewById(R.id.lytinput);

        /*if (from.equals("home")) {
            lytinput.setVisibility(View.VISIBLE);
        }*/


            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);

            final AutoCompleteAdapter nameadapter = new AutoCompleteAdapter(LedgerReportActivity.this, R.layout.lyt_sptext, ledgernamelist);
            nameadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            txtledgername.setAdapter(nameadapter);


            radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    narration = group.findViewById(checkedId).getTag().toString();
                    //System.out.println("===================== =  =" + narration);
                    SyncVisibility();
                }

            });


            txtledgername.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    try {
                        masterledgernamedata = (Master) parent.getItemAtPosition(position);
                        ledgernamecode = masterledgernamedata.getMastercode();
                        ledgername = masterledgernamedata.getMastername();

                        //System.out.println("================ = = = ledger - " + ledgername + " == " + ledgernamecode);
                        //System.out.println("===================== =  =" + ledgername + " = " + ledgernamecode);

                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(txtledgername.getWindowToken(), 0);

                        SyncVisibility();
                    } catch (Exception e) {
                        e.printStackTrace();
                        databaseHelper.addExceptionERROR(e, subject);
                    }
                }
            });


            txtledgername.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    //txtsearchvalue.requestFocus();
                    if (s.length() == 0) {
                        imgclear.setVisibility(View.GONE);
                    } else
                        imgclear.setVisibility(View.VISIBLE);
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });

            txtledgername.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    txtledgername.requestFocus();
                    txtledgername.showDropDown();
                }
            });


            SpannableString ss;

            try {
                if (ledgernamelist.size() == 0) {
                    Toast.makeText(LedgerReportActivity.this, getResources().getString(R.string.novoucherfound), Toast.LENGTH_SHORT).show();
                } else if (session.getData(sessionname + "sdate" + orgcode + compcode) != null && !session.getData(sessionname + "sdate" + orgcode + compcode).equals("") && !session.getData(sessionname + "edate" + orgcode + compcode).equals("")) {
                    startdate = session.getData(sessionname + "sdate" + orgcode + compcode);
                    enddate = session.getData(sessionname + "edate" + orgcode + compcode);

                    String fdate = startdate.substring(Math.max(startdate.length() - 2, 0));
                    String tdate = enddate.substring(Math.max(enddate.length() - 2, 0));
                    txtfromdt.setText(fdate);
                    txttodt.setText(tdate);

                    try {
                        Date date_end = Constant.FormateDate(inFormatdate, enddate, subject, databaseHelper);
                        Date date_start = Constant.FormateDate(inFormatdate, startdate, subject, databaseHelper);

                        Calendar scal = Calendar.getInstance();
                        scal.setTime(date_start);
                        Calendar ecal = Calendar.getInstance();
                        ecal.setTime(date_end);

                        String fmonth_name = Constant.FormateDate(month_date, scal.getTime());
                        String fday_name = Constant.FormateDate(date_name, scal.getTime());
                        String fcurrentdate = "  " + fmonth_name + " " + scal.get(Calendar.YEAR) + "\n  " + fday_name;

                        String emonth_name = Constant.FormateDate(month_date, ecal.getTime());
                        String eday_name = Constant.FormateDate(date_name, ecal.getTime());
                        String ecurrentdate = "  " + emonth_name + " " + ecal.get(Calendar.YEAR) + "\n  " + eday_name;

                        ss = new SpannableString(fcurrentdate);
                        ss.setSpan(new ForegroundColorSpan(Color.BLACK), 0, fcurrentdate.indexOf(fday_name), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        txtfrom.setText(ss);

                        SpannableString sst = new SpannableString(ecurrentdate);
                        sst.setSpan(new ForegroundColorSpan(Color.BLACK), 0, ecurrentdate.indexOf(eday_name), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        txtto.setText(sst);
                    } catch (Exception e) {
                        e.printStackTrace();
                        databaseHelper.addExceptionERROR(e, subject);
                    }


                    if (session.getData(sessionname + "narration" + compcode + orgcode) != null || session.getData(sessionname + "narration" + compcode + orgcode) != "") {
                        ledgernamecode = session.getData(sessionname + "namecode" + compcode + orgcode);
                        ledgername = session.getData(sessionname + "name" + compcode + orgcode);


                        if (ledgername == null || ledgername.equals(""))
                            GetMasterName(ledgernamecode);

                        txtledgername.setText(ledgername);
                        RadioButton radioButton = (RadioButton) findViewById(getResources().getIdentifier("radio" + session.getData(sessionname + "narration" + compcode + orgcode).toLowerCase(), "id", this.getPackageName()));
                        radioButton.setChecked(true);
                        narration = radioButton.getTag().toString();
                        txtledger.setText(ledgername);
                        totalname.setText(ledgername);
                    } else {
                        ledgername = "";
                        txtledgername.setText(ledgername);
                        ledgernamecode = "";
                    }


                    if (from != null && !from.equals("home")) {
                        //OnShowLedgerReportClick(lyttop.getRootView());
                        boolean issyncable = SyncVisibility();
                        if (issyncable)
                            OnSyncLedgerReportClick(lyttop.getRootView());
                        else
                            Toast.makeText(LedgerReportActivity.this, getResources().getString(R.string.datalimit), Toast.LENGTH_SHORT).show();
                        from = "home";
                    }
                } else {

                    RadioButton defaultradioButton = (RadioButton) findViewById(radiogroup.getCheckedRadioButtonId());
                    narration = defaultradioButton.getTag().toString();
                    ledgername = "";
                    txtledgername.setText(ledgername);
                    ledgernamecode = "";
                    imgdown.setVisibility(View.GONE);
                    calendar = Calendar.getInstance();
                    //SetDateData(calendar, "cyear");
                    //if (!type.equals("")) {
                    String[] dates = Constant.SetDateData(calendar, "cyear", txtfromdt, txttodt, txtfrom, txtto).split(";");
                    startdate = dates[0];
                    enddate = dates[1];
                    //}

                }
            } catch (Exception e) {
                e.printStackTrace();
                databaseHelper.addExceptionERROR(e, subject);
            }

            lytfrom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Date date_start = Constant.FormateDate(inFormatdate, startdate, subject, databaseHelper);
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(date_start);


                        new DatePickerDialog(LedgerReportActivity.this, pickerListenerfrom, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
                    } catch (Exception e) {
                        e.printStackTrace();

                        databaseHelper.addExceptionERROR(e, subject);
                    }
                }
            });

            lytto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Date date_end = Constant.FormateDate(inFormatdate, enddate, subject, databaseHelper);
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(date_end);
                        new DatePickerDialog(LedgerReportActivity.this, pickerListenerto, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
                    } catch (Exception e) {
                        e.printStackTrace();

                        databaseHelper.addExceptionERROR(e, subject);
                    }
                }
            });

            spquick.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    //btnsync.setVisibility(View.GONE);
                    String item = spquick.getSelectedItem().toString();
                    calendar = Calendar.getInstance();
                /*if (item.equals("Today")) {
                    SetDateData(calendar, "today");
                } else if (item.equals("Yesterday")) {
                    SetDateData(calendar, "yesterday");
                } else if (item.equals("Current Week")) {
                    SetDateData(calendar, "week");
                } else if (item.equals("Current Month")) {
                    SetDateData(calendar, "cmonth");
                } else if (item.equals("Last Month")) {
                    SetDateData(calendar, "lmonth");
                } else if (item.equals("Current Quarter")) {
                    SetDateData(calendar, "cqua");
                } else if (item.equals("Last Quarter")) {
                    SetDateData(calendar, "lqua");
                } else if (item.equals("Current A/c Year")) {
                    SetDateData(calendar, "cyear");
                } else if (item.equals("Last A/c Year")) {
                    SetDateData(calendar, "lyear");
                }*/

                    String type = Constant.QuickSpinnerType(item);
                /*if (item.equals("Today")) {
                    type = "today";
                } else if (item.equals("Yesterday")) {
                    type = "yesterday";
                } else if (item.equals("Current Week")) {
                    type = "week";
                } else if (item.equals("Current Month")) {
                    type = "cmonth";
                } else if (item.equals("Last Month")) {
                    type = "lmonth";
                } else if (item.equals("Current Quarter")) {
                    type = "cqua";
                } else if (item.equals("Last Quarter")) {
                    type = "lqua";
                } else if (item.equals("Current A/c Year")) {
                    type = "cyear";
                } else if (item.equals("Last A/c Year")) {
                    type = "lyear";
                }*/

                    if (!type.equals("")) {
                        String[] dates = Constant.SetDateData(calendar, type, txtfromdt, txttodt, txtfrom, txtto).split(";");
                        startdate = dates[0];
                        enddate = dates[1];
                    }

                    SyncVisibility();

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }

            });

            //System.out.println("===================== =  =" + ledgernamecode + " = " + startdate + " = " + enddate + " = " + narration);
            try {
                localdata = databaseHelper.getLedgerReportData(compcode, orgcode, ledgernamecode, startdate, enddate, narration);

                if (!localdata.equals("")) {
                    //if (AppController.isNetConnected(LedgerReportActivity.this))
                    //StartHandlerProcess();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            OnShowLedgerReportClick(lyttop.getRootView());
                        }
                    }, 150);

                } else if (AppController.isNetConnected(LedgerReportActivity.this)) {
                    SyncVisibility();
                    if (fromhome != null && fromhome.equals("home")) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    OnNameClearClick(txtledgername.getRootView());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }, 500);
                    }

                    btnshow.setVisibility(View.GONE);
                    imgdown.setVisibility(View.GONE);
                } else {
                    //lyttop.setVisibility(View.GONE);
                    imgdown.setVisibility(View.GONE);
                    Toast.makeText(LedgerReportActivity.this, getResources().getString(R.string.checkinternet), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                databaseHelper.addExceptionERROR(e, subject);
            }

            recycleview.setLayoutManager(new LinearLayoutManager(LedgerReportActivity.this));
        } catch (Exception e) {
            e.printStackTrace();

            databaseHelper.addExceptionERROR(e, subject);
        }
    }



    /*public void BtnEnableDisable(boolean isenable) {
        btnsync.setEnabled(isenable);
        btnshow.setEnabled(isenable);
    }*/

    private void SpinnerVoucherTypeSelection(String vouchertypecode) {
        int i = 0;
        for (Master master : ledgernamelist) {
            if (master.getMastercode().equals(vouchertypecode)) {
                txtledgername.setSelection(i);
                break;
            }
            i++;
        }


    }

    private void GetMasterName(String vouchertypecode) {
        for (Master master : ledgernamelist) {
            if (master.getMastercode().equals(vouchertypecode)) {
                ledgername = master.getMastername();
                break;
            }
        }
    }


    private DatePickerDialog.OnDateSetListener pickerListenerfrom = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            /*String monthString = String.valueOf(selectedMonth + 1);
            String dateString = String.valueOf(selectedDay);
            if (monthString.length() == 1) {
                monthString = "0" + monthString;
            }
            if (dateString.length() == 1) {
                dateString = "0" + dateString;
            }*/
            String[] datestring = Constant.GetMonthDateOfDatePicker(selectedMonth, selectedDay).split(";");
            String dateString = datestring[0];
            String monthString = datestring[1];

            try {
                Date myDate = Constant.FormateDate(inFormat, selectedYear + "-" + monthString + "-" + dateString, subject, databaseHelper);

                String month_name = Constant.FormateDate(month_date, myDate);
                String day_name = Constant.FormateDate(date_name, myDate);

                String currentdate = "  " + month_name + " " + selectedYear + "\n  " + day_name;

                txtfromdt.setText(dateString);
                //txttodt.setText(dateString);

                final SpannableString ss = new SpannableString(currentdate);
                ss.setSpan(new ForegroundColorSpan(Color.BLACK), 0, currentdate.indexOf(day_name), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                txtfrom.setText(ss);
                startdate = selectedYear + monthString + dateString;
                //btnsync.setVisibility(View.GONE);
                SyncVisibility();
            } catch (Exception e) {
                e.printStackTrace();
                databaseHelper.addExceptionERROR(e, subject);
            }

            /*if (AppController.isConnected(LedgerReportActivity.this)) {
                //GetData();
            }*/
        }
    };

    private DatePickerDialog.OnDateSetListener pickerListenerto = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            /*String monthString = String.valueOf(selectedMonth + 1);
            String dateString = String.valueOf(selectedDay);
            if (monthString.length() == 1) {
                monthString = "0" + monthString;
            }
            if (dateString.length() == 1) {
                dateString = "0" + dateString;
            }*/
            String[] datestring = Constant.GetMonthDateOfDatePicker(selectedMonth, selectedDay).split(";");
            String dateString = datestring[0];
            String monthString = datestring[1];


            try {
                Date myDate = Constant.FormateDate(inFormat, selectedYear + "-" + monthString + "-" + dateString, subject, databaseHelper);

                String month_name = Constant.FormateDate(month_date, myDate);
                String day_name = Constant.FormateDate(date_name, myDate);

                String currentdate = "  " + month_name + " " + selectedYear + "\n  " + day_name;

                //txtfromdt.setText(dateString);
                txttodt.setText(dateString);

                final SpannableString ss = new SpannableString(currentdate);
                ss.setSpan(new ForegroundColorSpan(Color.BLACK), 0, currentdate.indexOf(day_name), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                txtto.setText(ss);
                enddate = selectedYear + monthString + dateString;
                //btnsync.setVisibility(View.GONE);
                SyncVisibility();
            } catch (Exception e) {
                e.printStackTrace();

                databaseHelper.addExceptionERROR(e, subject);
            }

            /*if (AppController.isConnected(LedgerReportActivity.this)) {
                //GetData();
            }*/
        }
    };

    public boolean SyncVisibility() {

        String localdata = "";
        try {
            localdata = databaseHelper.getLedgerReportData(compcode, orgcode, ledgernamecode, startdate, enddate, narration);
        } catch (Exception e) {
            e.printStackTrace();
            databaseHelper.addExceptionERROR(e, subject);
        }

        if (localdata.equals("")) {
            btnshow.setVisibility(View.GONE);
        } else {
            btnshow.setVisibility(View.VISIBLE);
        }


        boolean issyncenable = false;
        if (session.getData(UserSessionManager.KEY_DEMO).equalsIgnoreCase("Yes") && session.getIntData(UserSessionManager.KEY_DEMOREQCOUNT) > (session.getIntData(UserSessionManager.KEY_DEMOREQTOTAL))) {
            btnsync.setVisibility(View.GONE);
            issyncenable = false;
        } else {
            btnsync.setVisibility(View.VISIBLE);
            issyncenable = true;
        }
        return issyncenable;
    }

    private void StopHandlerProcess() {
        Constant.BtnEnableDisable(true, btnsync, btnshow);
        progressbar.setVisibility(View.GONE);
        txtsec.setVisibility(View.GONE);
        mHandler.removeCallbacks(hMyTimeTask);
    }

    private void StartHandlerProcess() {
        Constant.BtnEnableDisable(false, btnsync, btnshow);
        progressbar.setVisibility(View.VISIBLE);
        txtsec.setVisibility(View.VISIBLE);
        txtsec.setText("");
        nCounter = 0;
        mHandler.postDelayed(hMyTimeTask, 1000);
    }

    private Runnable hMyTimeTask = new Runnable() {
        public void run() {
            nCounter++;
            txtsec.setText("" + nCounter);
            mHandler.postDelayed(hMyTimeTask, 1000);
        }
    };

    public void OnHideClick(View view) {
        Constant.SetViewVisiblility(imgdown, lyttop);
    }

    /*public void SetViewVisiblility() {
        imgdown.setVisibility(View.VISIBLE);
        if (lyttop.getVisibility() == View.GONE) {
            imgdown.setImageResource(R.drawable.ic_bup);
            lyttop.setVisibility(View.VISIBLE);
        } else {
            imgdown.setImageResource(R.drawable.ic_bdown);
            lyttop.setVisibility(View.GONE);
        }
    }*/

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    public void OnSyncLedgerReportClick(View view) {

        //if (from.equals("home")) {
        //CheckValidation();
            /*if (txtledgername.getText().toString().trim().length() == 0) {
                //StopHandlerProcess();
                Toast.makeText(LedgerReportActivity.this, "Please Select Ledger Name", Toast.LENGTH_SHORT).show();
                return;
            }

            if (containLedgerCode(txtledgername.getText().toString().trim()) == null) {
                //StopHandlerProcess();
                Toast.makeText(LedgerReportActivity.this, "Please Select Correct Ledger Name", Toast.LENGTH_SHORT).show();
                return;
            }*/
        //}


        if (from.equals("home")) {
            if (!CheckValidation())
                CallSyncApi();
        } else {
            CallSyncApi();
        }

    }

    public void CallSyncApi() {
        try {
            if (AppController.isConnected(LedgerReportActivity.this, findViewById(R.id.activity_main))) {

                lytbottom.setVisibility(View.GONE);
                lytmain.setVisibility(View.GONE);
                StartHandlerProcess();

                String url = Constant.LedgerReportUrl + orgcode + "&mobno=" + session.getData(UserSessionManager.KEY_Mobile) + "&imei=" + session.getData(UserSessionManager.KEY_IMEI) + "&devicecode=" + session.getData(UserSessionManager.KEY_DEVICECODE) + "&compcode=" + compcode + "&demo=" + session.getData(UserSessionManager.KEY_DEMO) + "&date1=" + startdate + "&date2=" + enddate + "&ledgercode=" + ledgernamecode + "&narr=" + narration;

                url = url.replaceAll(" ", "%20");

                //System.out.println("=============url -- " + url);
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (session.getData(UserSessionManager.KEY_DEMO).equalsIgnoreCase("Yes"))
                            OwnerHomeActivity.DemoReqCout();

                        if (response.contains("\n"))
                            response = response.replace("\n", "").trim();

                        //System.out.println("=============url --res " + response);

                        /*if (response.contains(getResources().getString(R.string.noresponse))) {
                            StopHandlerProcess();
                            Toast.makeText(LedgerReportActivity.this, getResources().getString(R.string.notconnectedserver), Toast.LENGTH_SHORT).show();
                        } else if (response.contains(getResources().getString(R.string.NOVCH))) {
                            StopHandlerProcess();
                            Toast.makeText(LedgerReportActivity.this, getResources().getString(R.string.nodatafound), Toast.LENGTH_SHORT).show();
                        } else if (response.contains(getResources().getString(R.string.multiuseword))) {
                            StopHandlerProcess();
                            Toast.makeText(LedgerReportActivity.this, getResources().getString(R.string.multiuse), Toast.LENGTH_SHORT).show();
                        }*/
                        if (Constant.ResponseErrorCheck(LedgerReportActivity.this, response, true)) {
                            StopHandlerProcess();
                        } else {
                            localdata = databaseHelper.getLedgerReportData(compcode, orgcode, ledgernamecode, startdate, enddate, narration);
                            if (localdata.equals("")) {
                                databaseHelper.addLedgerReportdata(compcode, orgcode, response, ledgernamecode, startdate, enddate, narration);
                            } else {
                                databaseHelper.UpdateLedgerReportData(compcode, orgcode, response, ledgernamecode, startdate, enddate, narration);
                            }

                            DisplayLedgerReport(response);

                        }
                        //StopHandlerProcess();
                    }

                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                StopHandlerProcess();
                                String message = Constant.VolleyErrorMessage(error);

                                if (!message.equals(""))
                                    Toast.makeText(LedgerReportActivity.this, message, Toast.LENGTH_SHORT).show();

                            }
                        });

                AppController.getInstance().getRequestQueue().getCache().clear();
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(stringRequest);
            }
        } catch (Exception e) {
            e.printStackTrace();
            databaseHelper.addExceptionERROR(e, subject);
        }
    }

    private boolean CheckValidation() {
        boolean iserror = false;
        String ledgername = txtledgername.getText().toString().trim();

        try {
           /* Date date_end = Constant.FormateDate(inFormatdate, enddate, subject, databaseHelper);
            Date date_start = Constant.FormateDate(inFormatdate, startdate, subject, databaseHelper);

            if (date_start.after(date_end)) {
                //StopHandlerProcess();
                Toast.makeText(LedgerReportActivity.this, getResources().getString(R.string.fromtodatevalidation), Toast.LENGTH_SHORT).show();
                iserror = true;
            } */
            if (Constant.CheckValidation(LedgerReportActivity.this, inFormatdate, startdate, enddate, subject, databaseHelper)) {
                iserror = true;
            } else if (ledgername.length() == 0) {
                //StopHandlerProcess();
                Toast.makeText(LedgerReportActivity.this, getResources().getString(R.string.selectledgername), Toast.LENGTH_SHORT).show();
                iserror = true;
            } else if (containLedgerCode(ledgername) == null) {
                //StopHandlerProcess();
                Toast.makeText(LedgerReportActivity.this, getResources().getString(R.string.selectcorrectledgername), Toast.LENGTH_SHORT).show();
                iserror = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            databaseHelper.addExceptionERROR(e, subject);
        }
        return iserror;
    }

    private Master containLedgerCode(final String search) {
        for (final Master master : ledgernamelist) {
            if (master.getMastername().trim().equals(search)) {
                return master;
            }
        }

        return null;
    }

    public void OnShowLedgerReportClick(View view) {
        try {
            if (!CheckValidation()) {
                boolean issyncable = SyncVisibility();
                if (from != null && !from.equals("home")) {
                    if (issyncable)
                        OnSyncLedgerReportClick(lyttop.getRootView());
                    else
                        Toast.makeText(LedgerReportActivity.this, getResources().getString(R.string.datalimit), Toast.LENGTH_SHORT).show();
                    from = "home";
                } else {
                    localdata = databaseHelper.getLedgerReportData(compcode, orgcode, ledgernamecode, startdate, enddate, narration);

                    if (localdata.equals("")) {
                        //Toast.makeText(LedgerReportActivity.this, "Try to Sync data Online, Locally Data not available", Toast.LENGTH_SHORT).show();
                        if (from != null && from.equals("home")) {
                            Toast.makeText(LedgerReportActivity.this, getResources().getString(R.string.trytosync), Toast.LENGTH_SHORT).show();
                        } else if (issyncable)
                            OnSyncLedgerReportClick(lyttop.getRootView());
                        else
                            Toast.makeText(LedgerReportActivity.this, getResources().getString(R.string.datalimit), Toast.LENGTH_SHORT).show();
                    } else {
                        //SyncVisibility();
                        StartHandlerProcess();
                        DisplayLedgerReport(localdata);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            databaseHelper.addExceptionERROR(e, subject);
        }
    }

    public void DisplayLedgerReport(String response) {

        if (databaseHelper.getAllMasterData(compcode, orgcode).size() == 0) {
            StopHandlerProcess();
            Toast.makeText(LedgerReportActivity.this, getResources().getString(R.string.master_updatemsg), Toast.LENGTH_SHORT).show();
            return;
        }

        //System.out.println("================= = = = " + startdate);

        session.setData(sessionname + "sdate" + orgcode + compcode, startdate);
        session.setData(sessionname + "edate" + orgcode + compcode, enddate);
        session.setData(sessionname + "namecode" + compcode + orgcode, ledgernamecode);
        session.setData(sessionname + "name" + compcode + orgcode, ledgername);
        session.setData(sessionname + "narration" + compcode + orgcode, narration);

        try {

            if (response.contains(getResources().getString(R.string.NOVCH))) {
                lytbottom.setVisibility(View.GONE);
                lytledgermain.setVisibility(View.GONE);
                lytpdfexcel.setVisibility(View.GONE);
                lytmain.setVisibility(View.GONE);
                txtnodata.setVisibility(View.VISIBLE);
                imgdown.setVisibility(View.GONE);
                StopHandlerProcess();
            } else {
                JSONArray jsonArray = new JSONArray(response);
                ledgerReportArrayList = new ArrayList<>();

                if (jsonArray.length() == 2) {
                    NoDataVisibility();
                } else {
                    JSONArray ledgeropenbal = jsonArray.getJSONArray(1);
                    JSONArray ledgerclosebal = jsonArray.getJSONArray(jsonArray.length() - 2);

                    if (jsonArray.length() == 4 && (ledgeropenbal.getString(3).equalsIgnoreCase("0") || ledgeropenbal.getString(3).equalsIgnoreCase("")) && (ledgeropenbal.getString(4).equalsIgnoreCase("0") || ledgeropenbal.getString(4).equalsIgnoreCase("")) && (ledgerclosebal.getString(3).equalsIgnoreCase("0") || ledgerclosebal.getString(3).equalsIgnoreCase("")) && (ledgerclosebal.getString(4).equalsIgnoreCase("0") || ledgerclosebal.getString(4).equalsIgnoreCase(""))) {
                        NoDataVisibility();
                    } else {


                        asyntask = new AsyncTaskRunner(response).execute();

                        /*Date date_end = inFormatdate.parse(enddate);
                        Date date_start = inFormatdate.parse(startdate);
                        sdt = new SimpleDateFormat("dd-MM-yyyy", Locale.US).format(date_start);
                        edt = new SimpleDateFormat("dd-MM-yyyy", Locale.US).format(date_end);

                        String header = companyname + "\n" + sdt + " to " + edt;

                        SpannableString ssdate = new SpannableString(header);
                        ssdate.setSpan(new RelativeSizeSpan(1.3f), 0, companyname.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        ssdate.setSpan(new StyleSpan(Typeface.BOLD), 0, companyname.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        ssdate.setSpan(new UnderlineSpan(), header.indexOf(sdt), header.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        datedisplay.setText(ssdate);


                        //pdfheadertext = "\n\n\n\n\nBusiness View from " + sdt + " to " + edt;
                        pdfheadertext = "Ledger Report from " + sdt + " to " + edt;
                        headertext = companyname + "\n" + address + "\n\nLedger Report from " + sdt + " to " + edt + "\n";
                        filename = companyname + "-LedgerReportActivity-" + sdt + " to " + edt;

                        ledgerReportArrayList.clear();

                        txtledger.setText(ledgername);
                        totalname.setText(ledgername);

                        String ledgermobileno = databaseHelper.getMasterValue(compcode, DatabaseHelper.MASTER_MASTERCODE, ledgernamecode, getResources().getString(R.string.master_ledger_name), DatabaseHelper.MASTER_DTL2, orgcode);
                        String ledgeraddress = databaseHelper.getMasterValue(compcode, DatabaseHelper.MASTER_MASTERCODE, ledgernamecode, getResources().getString(R.string.master_ledger_name), DatabaseHelper.MASTER_DTL1, orgcode);
                        String ledgeremail = databaseHelper.getMasterValue(compcode, DatabaseHelper.MASTER_MASTERCODE, ledgernamecode, getResources().getString(R.string.master_ledger_name), DatabaseHelper.MASTER_DTL3, orgcode);
                        txtmobno.setText(ledgermobileno);
                        txtaddress.setText(ledgeraddress);
                        txtemail.setText(ledgeremail);

                        if (ledgeraddress.length() == 0)
                            lytaddress.setVisibility(View.GONE);
                        else
                            lytaddress.setVisibility(View.VISIBLE);

                        txtemail.setVisibility(View.GONE);
                        txtmobno.setVisibility(View.GONE);


                        if (ledgeremail.length() != 0) {
                            txtemail.setVisibility(View.VISIBLE);
                        }
                        if (ledgermobileno.length() != 0) {
                            txtmobno.setVisibility(View.VISIBLE);
                        }

                        System.out.println("=============== =  =data" + jsonArray.toString());
                        double totaldb = 0, totalcr = 0;
                        ledgerReportArrayList.clear();
                        for (int i = 1; i < jsonArray.length() - 1; i++) {
                            JSONArray ledgerarray = jsonArray.getJSONArray(i);

                            String particular = ledgerarray.getString(1);
                            double debit = Double.parseDouble(ledgerarray.getString(3));
                            double credit = Double.parseDouble(ledgerarray.getString(4));
                            String displayamount;

                            if (ledgerarray.getString(3).equalsIgnoreCase("0") || ledgerarray.getString(3).equalsIgnoreCase("")) {
                                displayamount = indianFormat.format(new BigDecimal(credit)).replaceAll("₹", "").trim() + " <font color='#57A303'>Cr</font>";
                            } else {
                                displayamount = indianFormat.format(new BigDecimal(debit)).replaceAll("₹", "").trim() + " <font color='#0557E2'>Dr</font>";
                            }

                            System.out.println("=============== =  =data" + i);
                            if (particular.equalsIgnoreCase(getResources().getString(R.string.opening_balance))) {
                                System.out.println("=============== =  =" + i);
                                txtopeningbalance.setText(Html.fromHtml(displayamount), TextView.BufferType.SPANNABLE);
                            } else if (particular.equalsIgnoreCase(getResources().getString(R.string.closing_balance))) {
                                System.out.println("=============== =  =" + i + " == " + (jsonArray.length() - 1));
                                txtclosingbalance.setText(Html.fromHtml(displayamount), TextView.BufferType.SPANNABLE);
                            } else {
                                totaldb = totaldb + debit;
                                totalcr = totalcr + credit;

                                ledgerReportArrayList.add(new LedgerReport(output.format(input.parse(ledgerarray.getString(0))), ledgerarray.getString(1), ledgerarray.getString(2), ledgerarray.getString(3), ledgerarray.getString(4), ledgerarray.getString(5), ledgerarray.getString(6), ledgerarray.getString(7), displayamount));
                                //allReportlist.add(new LedgerReport(output.format(input.parse(ledgerarray.getString(0))), ledgerarray.getString(1), ledgerarray.getString(2), ledgerarray.getString(3), ledgerarray.getString(4), ledgerarray.getString(5), ledgerarray.getString(6), ledgerarray.getString(7), displayamount));
                            }

                        }*/




                        /*if (ledgerReportArrayList.size() == 0) {
                            lyttotaldrcr.setVisibility(View.GONE);
                        } else {

                            lyttotaldrcr.setVisibility(View.VISIBLE);
                            txttotaldr.setText(indianFormat.format(new BigDecimal(totaldb)).replaceAll("₹", "").trim());
                            txttotalcr.setText(indianFormat.format(new BigDecimal(totalcr)).replaceAll("₹", "").trim());
                        }*/
                    }
                }
            }

            //StopHandlerProcess();

        } catch (Exception e) {
            StopHandlerProcess();
            e.printStackTrace();
            StopHandlerProcess();
            databaseHelper.addExceptionERROR(e, subject);
        }
    }

    private void NoDataVisibility() {
        lytbottom.setVisibility(View.GONE);
        lytpdfexcel.setVisibility(View.GONE);
        lytmain.setVisibility(View.GONE);
        lytledgermain.setVisibility(View.GONE);
        txtnodata.setVisibility(View.VISIBLE);
        StopHandlerProcess();
    }

    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        double totaldb = 0, totalcr = 0;
        JSONArray jsonArray;
        String opendisplayamount, closedisplayamount;

        public AsyncTaskRunner(String response) {
            try {
                jsonArray = new JSONArray(response);
            } catch (JSONException e) {
                e.printStackTrace();

                databaseHelper.addJSONERROR(e, subject);
            }
        }

        @Override
        protected String doInBackground(String... params) {

            try {

                //System.out.println("=============== =  =data" + jsonArray.toString());

                for (int i = 1; i < jsonArray.length() - 1; i++) {
                    JSONArray ledgerarray = jsonArray.getJSONArray(i);

                    String particular = ledgerarray.getString(1);
                    double debit = Constant.ConvertToDouble(ledgerarray.getString(3));
                    double credit = Constant.ConvertToDouble(ledgerarray.getString(4));

                    String displayamount;
                    if (ledgerarray.getString(3).equalsIgnoreCase("0") || ledgerarray.getString(3).equalsIgnoreCase("")) {
                        displayamount = Constant.ConvertToIndianRupeeFormat(credit, false, true, false) + " <font color='#57A303'>Cr</font>";
                    } else {
                        displayamount = Constant.ConvertToIndianRupeeFormat(debit, false, true, false) + " <font color='#0557E2'>Dr</font>";
                    }

                    //System.out.println("=============== =  =data" + displayamount);
                    if (particular.equalsIgnoreCase(getResources().getString(R.string.opening_balance))) {
                        //System.out.println("=============== =  =" + i);
                        opendisplayamount = displayamount;

                    } else if (particular.equalsIgnoreCase(getResources().getString(R.string.closing_balance))) {
                        //System.out.println("=============== =  =" + i + " == " + (jsonArray.length() - 1));
                        closedisplayamount = displayamount;

                    } else {
                        totaldb = totaldb + debit;
                        totalcr = totalcr + credit;

                        ledgerReportArrayList.add(new LedgerReport(Constant.FormateDate(output, Constant.FormateDate(inFormatdate, ledgerarray.getString(0), subject, databaseHelper)), ledgerarray.getString(1), ledgerarray.getString(2), ledgerarray.getString(3), ledgerarray.getString(4), ledgerarray.getString(5), ledgerarray.getString(6), ledgerarray.getString(7), displayamount));
                        //allReportlist.add(new LedgerReport(output.format(input.parse(ledgerarray.getString(0))), ledgerarray.getString(1), ledgerarray.getString(2), ledgerarray.getString(3), ledgerarray.getString(4), ledgerarray.getString(5), ledgerarray.getString(6), ledgerarray.getString(7), displayamount));
                    }

                    if (isCancelled())
                        break;
                }

            } catch (JSONException e) {
                e.printStackTrace();
                databaseHelper.addJSONERROR(e, subject);
            } catch (Exception e) {
                e.printStackTrace();
                databaseHelper.addExceptionERROR(e, subject);
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            try {
                Date date_end = Constant.FormateDate(inFormatdate, enddate, subject, databaseHelper);
                Date date_start = Constant.FormateDate(inFormatdate, startdate, subject, databaseHelper);
                sdt = new SimpleDateFormat("dd-MM-yyyy", Locale.US).format(date_start);
                edt = new SimpleDateFormat("dd-MM-yyyy", Locale.US).format(date_end);

                String header = companyname + "\n" + sdt + " to " + edt;

                SpannableString ssdate = new SpannableString(header);
                ssdate.setSpan(new RelativeSizeSpan(1.3f), 0, companyname.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ssdate.setSpan(new StyleSpan(Typeface.BOLD), 0, companyname.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ssdate.setSpan(new UnderlineSpan(), header.indexOf(sdt), header.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                datedisplay.setText(ssdate);


                //pdfheadertext = "\n\n\n\n\nBusiness View from " + sdt + " to " + edt;
                pdfheadertext = "Ledger Report from " + sdt + " to " + edt;
                headertext = companyname + "\n" + address + "\n\nLedger Report from " + sdt + " to " + edt + "\n";
                filename = companyname + "-LedgerReportActivity-" + sdt + " to " + edt;

                //System.out.println("=============== =  =dataname"+ledgername+" = "+ledgernamecode);

                if (ledgername != null && !ledgername.equals("")) {
                    txtledger.setText(ledgername);
                    totalname.setText(ledgername);
                }

                String ledgermobileno = databaseHelper.getMasterValue(compcode, DatabaseHelper.MASTER_MASTERCODE, ledgernamecode, getResources().getString(R.string.master_ledger_name), DatabaseHelper.MASTER_DTL2, orgcode);
                String ledgeraddress = databaseHelper.getMasterValue(compcode, DatabaseHelper.MASTER_MASTERCODE, ledgernamecode, getResources().getString(R.string.master_ledger_name), DatabaseHelper.MASTER_DTL1, orgcode);
                String ledgeremail = databaseHelper.getMasterValue(compcode, DatabaseHelper.MASTER_MASTERCODE, ledgernamecode, getResources().getString(R.string.master_ledger_name), DatabaseHelper.MASTER_DTL3, orgcode);
                txtmobno.setText(ledgermobileno);
                txtaddress.setText(ledgeraddress);
                txtemail.setText(ledgeremail);

                if (ledgeraddress == null || ledgeraddress.length() == 0)
                    lytaddress.setVisibility(View.GONE);
                else
                    lytaddress.setVisibility(View.VISIBLE);

                txtemail.setVisibility(View.GONE);
                txtmobno.setVisibility(View.GONE);


                if (ledgeremail != null && ledgeremail.length() != 0) {
                    txtemail.setVisibility(View.VISIBLE);
                }
                if (ledgeremail != null && ledgermobileno.length() != 0) {
                    txtmobno.setVisibility(View.VISIBLE);
                }

                txtopeningbalance.setText(Html.fromHtml(opendisplayamount), TextView.BufferType.SPANNABLE);
                txtclosingbalance.setText(Html.fromHtml(closedisplayamount), TextView.BufferType.SPANNABLE);
            } catch (Exception e) {
                e.printStackTrace();
                databaseHelper.addExceptionERROR(e, subject);
            }

            SyncVisibility();
            Constant.SetViewVisiblility(imgdown, lyttop);
            StopHandlerProcess();

            txtnodata.setVisibility(View.GONE);
            lytbottom.setVisibility(View.VISIBLE);
            lytmain.setVisibility(View.VISIBLE);
            lytledgermain.setVisibility(View.VISIBLE);
            lytpdfexcel.setVisibility(View.VISIBLE);


            /*if (ledgerReportArrayList.size() == 0) {
                lyttotaldrcr.setVisibility(View.GONE);
            } else {
                lyttotaldrcr.setVisibility(View.VISIBLE);
                txttotaldr.setText(Constant.ConvertToIndianRupeeFormat(totaldb, false, true, false));
                txttotalcr.setText(Constant.ConvertToIndianRupeeFormat(totalcr, false, true, false));
            }*/
            lyttotaldrcr.setVisibility(View.VISIBLE);
            txttotaldr.setText(Constant.ConvertToIndianRupeeFormat(totaldb, false, true, false));
            txttotalcr.setText(Constant.ConvertToIndianRupeeFormat(totalcr, false, true, false));


            //System.out.println("=============== =  =data -- "+ledgerReportArrayList.size());
            recycleview.setAdapter(new LedgerReportAdapter(ledgerReportArrayList));
            //recycleprogress.setVisibility(View.GONE);


        }


        @Override
        protected void onPreExecute() {
            ledgerReportArrayList = new ArrayList<>();
            //recycleprogress.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (asyntask != null)
            asyntask.cancel(true);

    }

    public void OnNameClearClick(View view) {
        txtledgername.setText("");
        ledgername = "";
        ledgernamecode = "";
        txtledgername.performClick();

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

    }

    public void OnLeExcelClick(View view) {
        /*if (ContextCompat.checkSelfPermission(LedgerReportActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(LedgerReportActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, ReqReadPermission);
        } else if (ContextCompat.checkSelfPermission(LedgerReportActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(LedgerReportActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, ReqWritePermission);
        } else {*/
        if (Constant.CheckReadWritePermissionGranted(LedgerReportActivity.this)) {
            lytprogress.setVisibility(View.VISIBLE);
            btnexcel.setEnabled(false);
            btnemail.setEnabled(false);
            btnpdf.setEnabled(false);
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    writeStudentsListToExcel(false);
                }
            }, 300);
        }
    }

    public void writeStudentsListToExcel(boolean isshare) {
        try {
            /*File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getResources().getString(R.string.app_name) + "/");

            if (!folder.exists()) {
                boolean success = folder.mkdir();
            }
            String path = folder.getAbsolutePath();
            path = path + "/" + filename + ".xlsx";

            File FILE_PATH = new File(path);

            if (FILE_PATH.exists()) {
                FILE_PATH.delete();
            }*/

            File FILE_PATH = Constant.FilePathPdfExcel(true, ".xlsx", LedgerReportActivity.this, filename);

            XSSFWorkbook workbook = new XSSFWorkbook();

            XSSFSheet studentsSheet = workbook.createSheet("VoucherInfo");
            studentsSheet.getPrintSetup().setPaperSize(PrintSetup.A4_PAPERSIZE);
            studentsSheet.setHorizontallyCenter(true);

            Footer footer = studentsSheet.getFooter();
            footer.setRight("Page " + HeaderFooter.page() + " of " + HeaderFooter.numPages());

            studentsSheet.setColumnWidth(0, (15 * 200));
            studentsSheet.setColumnWidth(1, (15 * 500));
            studentsSheet.setColumnWidth(2, (15 * 200));
            studentsSheet.setColumnWidth(3, (15 * 400));

            int rowIndex = 0, nextrow = 0;
            //Row row = studentsSheet.createRow(rowIndex++);
            int cellIndex = 0;
            CellStyle style = workbook.createCellStyle();

            style.setBorderBottom(CellStyle.BORDER_THIN);
            style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            style.setBorderLeft(CellStyle.BORDER_THIN);
            style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            style.setBorderRight(CellStyle.BORDER_THIN);
            style.setRightBorderColor(IndexedColors.BLACK.getIndex());
            style.setBorderTop(CellStyle.BORDER_THIN);
            style.setTopBorderColor(IndexedColors.BLACK.getIndex());
            ((XSSFCellStyle) style).setVerticalAlignment(VerticalAlignment.CENTER);


            CellStyle borderStyle = workbook.createCellStyle();
            borderStyle.setBorderBottom(CellStyle.BORDER_THIN);
            borderStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            borderStyle.setBorderLeft(CellStyle.BORDER_THIN);
            borderStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            borderStyle.setBorderRight(CellStyle.BORDER_THIN);
            borderStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            borderStyle.setBorderTop(CellStyle.BORDER_THIN);
            borderStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
            ((XSSFCellStyle) borderStyle).setAlignment(HorizontalAlignment.CENTER);
            ((XSSFCellStyle) borderStyle).setVerticalAlignment(VerticalAlignment.CENTER);

            CellStyle rightborderStyle = workbook.createCellStyle();
            rightborderStyle.setBorderBottom(CellStyle.BORDER_THIN);
            rightborderStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            rightborderStyle.setBorderLeft(CellStyle.BORDER_THIN);
            rightborderStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            rightborderStyle.setBorderRight(CellStyle.BORDER_THIN);
            rightborderStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            rightborderStyle.setBorderTop(CellStyle.BORDER_THIN);
            rightborderStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
            ((XSSFCellStyle) rightborderStyle).setAlignment(HorizontalAlignment.RIGHT);
            ((XSSFCellStyle) rightborderStyle).setVerticalAlignment(VerticalAlignment.CENTER);

            CellStyle startborderStyle = workbook.createCellStyle();
            startborderStyle.setBorderBottom(CellStyle.BORDER_THIN);
            startborderStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            startborderStyle.setBorderLeft(CellStyle.BORDER_THIN);
            startborderStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            startborderStyle.setBorderRight(CellStyle.BORDER_THIN);
            startborderStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            startborderStyle.setBorderTop(CellStyle.BORDER_THIN);
            startborderStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
            ((XSSFCellStyle) rightborderStyle).setAlignment(HorizontalAlignment.LEFT);
            ((XSSFCellStyle) rightborderStyle).setVerticalAlignment(VerticalAlignment.CENTER);

            XSSFFont font = workbook.createFont();
            font.setBold(true);
            style.setFont(font);
            ((XSSFCellStyle) style).setAlignment(HorizontalAlignment.CENTER);

            //row.createCell(cellIndex);
            CellStyle cs = workbook.createCellStyle();
            cs.setFont(font);
            ((XSSFCellStyle) cs).setAlignment(HorizontalAlignment.CENTER);
            ((XSSFCellStyle) cs).setVerticalAlignment(VerticalAlignment.CENTER);
            cs.setWrapText(true);

            XSSFFont comFont = workbook.createFont();
            comFont.setFontHeightInPoints((short) 20);
            CellStyle cellstyle = workbook.createCellStyle();
            cellstyle.setFont(comFont);
            ((XSSFCellStyle) cellstyle).setAlignment(HorizontalAlignment.CENTER);

            Row row = studentsSheet.createRow(rowIndex);
            row.createCell(cellIndex).setCellValue(companyname);
            row.getCell(cellIndex).setCellStyle(cellstyle);
            studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 3));

            rowIndex++;
            cellIndex = 0;

            Row addrow = studentsSheet.createRow(rowIndex);
            addrow.createCell(cellIndex).setCellValue(address);
            addrow.getCell(cellIndex).setCellStyle(cs);

            studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex + 3, 0, 3));

            rowIndex = rowIndex + 4;

            Row blankrow = studentsSheet.createRow(rowIndex);


            rowIndex++;
            nextrow = rowIndex;
            cellIndex = 0;
            Row shrow = studentsSheet.createRow(rowIndex);
            shrow.createCell(cellIndex).setCellValue(getResources().getString(R.string.ledger));
            shrow.getCell(cellIndex++).setCellStyle(style);
            shrow.createCell(cellIndex++).setCellValue(txtledger.getText().toString().trim());
            shrow.createCell(cellIndex++);
            shrow.createCell(cellIndex);
            studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 1, 3));

            for (int i = 1; i < 4; i++) {
                shrow.getCell(i).setCellStyle(startborderStyle);
            }

            String address = txtaddress.getText().toString().trim();
            String email = txtemail.getText().toString().trim();
            String mobile = txtmobno.getText().toString().trim();

            if (address.length() != 0) {
                rowIndex++;
                nextrow = rowIndex;
                cellIndex = 0;
                Row ahrow = studentsSheet.createRow(rowIndex);
                ahrow.createCell(cellIndex).setCellValue(getResources().getString(R.string.address));
                ahrow.getCell(cellIndex++).setCellStyle(style);
                ahrow.createCell(cellIndex++).setCellValue(address);
                ahrow.createCell(cellIndex++);
                ahrow.createCell(cellIndex);
                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 1, 3));

                for (int i = 1; i < 4; i++) {
                    ahrow.getCell(i).setCellStyle(startborderStyle);
                }
            }

            if (email.length() != 0 || mobile.length() != 0) {
                rowIndex++;
                nextrow = rowIndex;
                cellIndex = 0;
                Row ahrow = studentsSheet.createRow(rowIndex);
                if (email.length() != 0 && mobile.length() != 0) {
                    ahrow.createCell(cellIndex++).setCellValue(mobile);
                    ahrow.createCell(cellIndex++);
                    ahrow.createCell(cellIndex++).setCellValue(email);
                    ahrow.createCell(cellIndex);
                    studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 1));
                    studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 2, 3));
                } else if (mobile.length() != 0) {
                    ahrow.createCell(cellIndex).setCellValue(mobile);
                    ahrow.createCell(cellIndex++);
                    ahrow.createCell(cellIndex++);
                    ahrow.createCell(cellIndex);
                    studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 3));
                } else if (email.length() != 0) {
                    ahrow.createCell(cellIndex).setCellValue(email);
                    ahrow.createCell(cellIndex++);
                    ahrow.createCell(cellIndex++);
                    ahrow.createCell(cellIndex);
                    studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 3));
                }

                for (int i = 0; i < 4; i++) {
                    if (ahrow.getCell(i) != null)
                        ahrow.getCell(i).setCellStyle(startborderStyle);
                }
            }

            rowIndex++;
            nextrow = rowIndex;

            cellIndex = 0;
            Row srow = studentsSheet.createRow(rowIndex);
            srow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.date));
            srow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.particulars));
            srow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.vch_no));
            srow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.amount));

            for (int i = 0; i < 4; i++) {
                srow.getCell(i).setCellStyle(style);
            }

            studentsSheet.setRepeatingRows(CellRangeAddress.valueOf("1:" + (rowIndex + 1)));

            rowIndex++;
            cellIndex = 0;
            Row srow1 = studentsSheet.createRow(rowIndex);
            srow1.createCell(cellIndex++).setCellValue(getResources().getString(R.string.opening_balance));
            srow1.createCell(cellIndex++);
            srow1.createCell(cellIndex++);
            srow1.createCell(cellIndex).setCellValue(txtopeningbalance.getText().toString());
            studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 2));
            for (int i = 0; i < 4; i++) {
                srow1.getCell(i).setCellStyle(style);
            }

            rowIndex++;
            if (ledgerReportArrayList.size() != 0) {
                for (LedgerReport ledgerReport : ledgerReportArrayList) {
                    cellIndex = 0;
                    Row recrow = studentsSheet.createRow(rowIndex);
                    recrow.createCell(cellIndex++).setCellValue(ledgerReport.getTrdate2());
                    recrow.createCell(cellIndex++).setCellValue(ledgerReport.getParticulars());
                    recrow.createCell(cellIndex++).setCellValue(ledgerReport.getVchno());
                    recrow.createCell(cellIndex).setCellValue(String.valueOf(Html.fromHtml(ledgerReport.getAmount())));
                    recrow.getCell(0).setCellStyle(borderStyle);
                    recrow.getCell(1).setCellStyle(borderStyle);
                    recrow.getCell(2).setCellStyle(borderStyle);
                    recrow.getCell(3).setCellStyle(rightborderStyle);
                    rowIndex++;

                    if (ledgerReport.getNarration().length() != 0) {
                        cellIndex = 0;
                        Row recrownarration = studentsSheet.createRow(rowIndex);
                        recrownarration.createCell(cellIndex++).setCellValue(ledgerReport.getNarration());
                        recrownarration.createCell(cellIndex++);
                        recrownarration.createCell(cellIndex++);
                        recrownarration.createCell(cellIndex);
                        for (int i = 0; i < 4; i++) {
                            recrownarration.getCell(i).setCellStyle(borderStyle);
                        }
                        studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 3));
                        rowIndex++;
                    }
                }
            }

            cellIndex = 0;
            Row recbottom = studentsSheet.createRow(rowIndex++);
            recbottom.createCell(cellIndex++).setCellValue(txtledger.getText().toString());
            recbottom.createCell(cellIndex++);
            recbottom.createCell(cellIndex++).setCellValue(getResources().getString(R.string.total_dr));
            recbottom.createCell(cellIndex).setCellValue(txttotaldr.getText().toString());
            recbottom.getCell(0).setCellStyle(style);
            recbottom.getCell(1).setCellStyle(style);
            recbottom.getCell(2).setCellStyle(style);
            recbottom.getCell(3).setCellStyle(style);

            cellIndex = 0;
            Row recbottom1 = studentsSheet.createRow(rowIndex);
            recbottom1.createCell(cellIndex++);
            recbottom1.createCell(cellIndex++);
            recbottom1.createCell(cellIndex++).setCellValue(getResources().getString(R.string.total_cr));
            recbottom1.createCell(cellIndex).setCellValue(txttotalcr.getText().toString());
            recbottom1.getCell(0).setCellStyle(style);
            recbottom1.getCell(1).setCellStyle(style);
            recbottom1.getCell(2).setCellStyle(style);
            recbottom1.getCell(3).setCellStyle(style);

            studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex - 1, rowIndex, 0, 1));

            rowIndex++;
            cellIndex = 0;
            Row recbottom2 = studentsSheet.createRow(rowIndex);
            recbottom2.createCell(cellIndex++).setCellValue(getResources().getString(R.string.closing_balance));
            recbottom2.createCell(cellIndex++);
            recbottom2.createCell(cellIndex++);
            recbottom2.createCell(cellIndex).setCellValue(txtclosingbalance.getText().toString());
            recbottom2.getCell(0).setCellStyle(style);
            recbottom2.getCell(1).setCellStyle(style);
            recbottom2.getCell(2).setCellStyle(style);
            recbottom2.getCell(3).setCellStyle(style);
            studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 2));


            try {
                FileOutputStream fos = new FileOutputStream(FILE_PATH);
                workbook.write(fos);
                fos.close();

                openPdfExcel(FILE_PATH, "excel", isshare);
            } catch (FileNotFoundException e) {
                e.printStackTrace();

                databaseHelper.addFileNotFoundERROR(e, subject);
            } catch (IOException e) {
                e.printStackTrace();

                databaseHelper.addIOERROR(e, subject);
            }
        } catch (Exception e) {
            e.printStackTrace();
            databaseHelper.addExceptionERROR(e, subject);
        }
    }

    public void OnLeEmailClick(View emailview) {
        final AlertDialog.Builder alertdialog = new AlertDialog.Builder(LedgerReportActivity.this);
        alertdialog.setTitle("Share Ledger Report");
        final View view = getLayoutInflater().inflate(R.layout.lyt_email_share, null);
        final RadioGroup rglang = (RadioGroup) view.findViewById(R.id.emailradiogroup);
        alertdialog.setCancelable(false);
        alertdialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alertdialog.setView(view);
        alertdialog.setPositiveButton("Share", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                RadioButton radioButton = (RadioButton) view.findViewById(rglang.getCheckedRadioButtonId());
                String type, extention;
                if (radioButton.getTag().equals("pdf")) {
                    type = "pdf";
                    extention = ".pdf";
                } else {
                    type = "excel";
                    extention = "xlsx";
                }


                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        try {

                            File filepath = Constant.FilePathPdfExcel(false, extention, LedgerReportActivity.this, filename);

                            if (filepath.exists()) {
                                openPdfExcel(filepath, type, true);
                            } else {
                                lytprogress.setVisibility(View.VISIBLE);
                                btnexcel.setEnabled(false);
                                btnemail.setEnabled(false);
                                btnpdf.setEnabled(false);
                                new Handler().postDelayed(new Runnable() {

                                    @Override
                                    public void run() {
                                        //takeScreenShot();
                                        if (type.equals("pdf"))
                                            PDFCreate(true);
                                        else
                                            writeStudentsListToExcel(true);
                                    }
                                }, 300);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                            databaseHelper.addExceptionERROR(e, subject);
                        }
                    }
                }, 300);

            }
                   /* new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            try {

                                File filepath = Constant.FilePathPdfExcel(false,".pdf",LedgerReportActivity.this,filename);

                                if (filepath.exists()) {
                                    openPdfExcel(filepath, type, true);
                                } else {
                                    lytprogress.setVisibility(View.VISIBLE);
                                    btnexcel.setEnabled(false);
                                    btnemail.setEnabled(false);
                                    btnpdf.setEnabled(false);
                                    new Handler().postDelayed(new Runnable() {

                                        @Override
                                        public void run() {
                                            //takeScreenShot();
                                            PDFCreate(true);
                                        }
                                    }, 300);
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                                databaseHelper.addExceptionERROR(e, subject);
                            }
                        }
                    }, 300);
                } else {
                    type = "excel";
                    File FILE_PATH = Constant.FilePathPdfExcel(false,".xlsx",LedgerReportActivity.this,filename);

                    if (FILE_PATH.exists()) {
                        openPdfExcel(FILE_PATH, type, true);
                    } else {
                        lytprogress.setVisibility(View.VISIBLE);
                        btnemail.setEnabled(false);
                        btnexcel.setEnabled(false);
                        btnpdf.setEnabled(false);
                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                writeStudentsListToExcel(true);
                            }
                        }, 300);
                    }
                }
                dialog.dismiss();
            }*/
        });
        final AlertDialog dialog = alertdialog.create();
        dialog.show();

    }

    public void OnLePdfClick(View view) {
        /*if (ContextCompat.checkSelfPermission(LedgerReportActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(LedgerReportActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, ReqReadPermission);
        } else if (ContextCompat.checkSelfPermission(LedgerReportActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(LedgerReportActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, ReqWritePermission);
        } else {*/
        if (Constant.CheckReadWritePermissionGranted(LedgerReportActivity.this)) {
            lytprogress.setVisibility(View.VISIBLE);
            btnexcel.setEnabled(false);
            btnemail.setEnabled(false);
            btnpdf.setEnabled(false);
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    //takeScreenShot();
                    PDFCreate(false);
                }
            }, 300);
        }

    }

    public void PDFCreate(boolean isshare) {

        /*File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getResources().getString(R.string.app_name) + "/");

        if (!folder.exists()) {
            boolean success = folder.mkdir();
        }

        path = folder.getAbsolutePath();
        path = path + "/" + filename + ".pdf";
        File filepath = new File(path);
        if (filepath.exists()) {
            filepath.delete();
        }*/
        File filepath = Constant.FilePathPdfExcel(true, ".pdf", LedgerReportActivity.this, filename);

        UserSessionManager.CreateLedgerReportPdf(filename, address, companyname, pdfheadertext, ledgerReportArrayList, txtledgername.getText().toString().trim(), txtaddress.getText().toString().trim(), txtmobno.getText().toString().trim(), txtemail.getText().toString(), txttotaldr.getText().toString(), txttotalcr.getText().toString(), txtopeningbalance.getText().toString(), txtclosingbalance.getText().toString());

        openPdfExcel(filepath, "pdf", isshare);
    }

    private void openPdfExcel(File file, String type, boolean isshare) {
        /*if (ContextCompat.checkSelfPermission(LedgerReportActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(LedgerReportActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, ReqReadPermission);
        } else if (ContextCompat.checkSelfPermission(LedgerReportActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(LedgerReportActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, ReqWritePermission);
        } else if (isshare) {*/
        if (Constant.CheckReadWritePermissionGranted(LedgerReportActivity.this) && isshare) {
            lytprogress.setVisibility(View.GONE);
            Uri uri = FileProvider.getUriForFile(LedgerReportActivity.this, getResources().getString(R.string.authority), file);
            Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
            //emailIntent.setType("*/*");
            try {
                emailIntent.setType("vnd.android.cursor.dir/email");
            } catch (Exception e) {
                e.printStackTrace();
                emailIntent.setType("*/*");
            }
            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Ledger Report  from " + companyname);
            emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{txtemail.getText().toString().trim()});
            emailIntent.putExtra(android.content.Intent.EXTRA_TEXT,
                    "Dear Sir... \n" + "\t\tAs per your requirement, Please find attached report.\n" + "Regards,\n\t\t" + companyname + "\n");
            emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));

            btnexcel.setEnabled(true);
            btnemail.setEnabled(true);
            btnpdf.setEnabled(true);
        } else {
            lytprogress.setVisibility(View.GONE);

            Constant.openPdfExcel(file, type, LedgerReportActivity.this);

            btnexcel.setEnabled(true);
            btnemail.setEnabled(true);
            btnpdf.setEnabled(true);
        }
    }

    public class LedgerReportAdapter extends RecyclerView.Adapter<LedgerReportAdapter.ViewHolder> {

        public ArrayList<LedgerReport> reportArrayList;

        public LedgerReportAdapter(ArrayList<LedgerReport> reportArrayList) {
            this.reportArrayList = reportArrayList;
        }

        @NonNull
        @Override
        public LedgerReportAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lyt_ledgerreport, parent, false);
            return new LedgerReportAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final LedgerReportAdapter.ViewHolder holder, int position) {
            // System.out.println("=============== =  =data adapter" + position);
            final LedgerReport model = reportArrayList.get(position);
            holder.txtamount.setText(Html.fromHtml(model.getAmount()));
            holder.txtvchno.setText(Html.fromHtml("<u><i>" + model.getVchno() + "</i></u>"));
            holder.txtdate.setText(model.getTrdate2());
            //holder.txtparticulars.setText(model.getParticulars());
            String name = model.getParticulars();
            if (model.getParticulars() == null || model.getParticulars().equals(""))
                name = getResources().getString(R.string.nodata_updatemaster);

            holder.txtparticulars.setText(Html.fromHtml(name));


            String narration = model.getNarration().trim();
            if (narration.length() != 0) {
                holder.lytnarration.setVisibility(View.VISIBLE);
                holder.txtnarration.setText(narration);
            } else {
                holder.lytnarration.setVisibility(View.GONE);
            }

            holder.txtvchno.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //System.out.println("=================== = = " + model.getVchcode() + " = " + model.getVouchertypecode());
                    if (!model.getVchcode().equals("") && !model.getVouchertypecode().equals(""))
                        startActivity(new Intent(LedgerReportActivity.this, VoucherInfoActivity.class).putExtra("from", "ledgerreport").putExtra("code", model.getVchcode()).putExtra("typecode", model.getVouchertypecode()));
                }
            });

            holder.txtparticulars.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (model.getParticulars() == null || model.getParticulars().equals("")) {
                        session.OnUpdateMasterClick(new VolleyCallback() {
                            @Override
                            public void onSuccess(boolean result) {
                                if (result)
                                    OnShowLedgerReportClick(lyttop.getRootView());
                            }

                            public void onSuccessWithMsg(boolean result, String message) {
                            }
                        }, LedgerReportActivity.this, session, orgcode, compcode, databaseHelper, subject, findViewById(R.id.activity_main));

                    }
                }
            });

           /* if (position == reportArrayList.size() - 1) {
                if (progressbar.getVisibility() == View.VISIBLE)
                    progressbar.setVisibility(View.GONE);
            }*/
        }

        public void addAll(List<LedgerReport> mcList) {
            for (LedgerReport mc : mcList) {
                add(mc);
            }
        }

        public void add(LedgerReport mc) {
            reportArrayList.add(mc);
            notifyItemInserted(reportArrayList.size() - 1);
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return reportArrayList.size();
        }


        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView txtamount, txtvchno, txtparticulars, txtdate, txtnarration;
            LinearLayout lytnarration;

            public ViewHolder(View itemView) {
                super(itemView);
                txtamount = itemView.findViewById(R.id.txtamount);
                txtvchno = itemView.findViewById(R.id.txtvchno);
                txtparticulars = itemView.findViewById(R.id.txtparticulars);
                txtdate = itemView.findViewById(R.id.txtdate);
                txtnarration = itemView.findViewById(R.id.txtnarration);
                lytnarration = itemView.findViewById(R.id.lytnarration);
            }

        }
    }
}
