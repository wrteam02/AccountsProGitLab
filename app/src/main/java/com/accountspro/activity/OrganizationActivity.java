package com.accountspro.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.accountspro.R;
import com.accountspro.helper.AppController;
import com.accountspro.helper.Constant;
import com.accountspro.helper.DatabaseHelper;
import com.accountspro.helper.UserSessionManager;
import com.accountspro.model.Company;
import com.accountspro.model.Master;
import com.accountspro.model.Organization;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class OrganizationActivity extends AppCompatActivity {

    RecyclerView recycleview;
    TextView txtnodata, txtsec, txtsecbtn;//,txtmobileno;
    ProgressBar progressbar, progressbarbtn;
    LinearLayout lytlist;
    DatabaseHelper databaseHelper;
    ArrayList<Organization> organizationArrayList, localorglist;
    UserSessionManager session;
    ImageView imgsync;
    String from, orgid = "";
    Button lytbottom;
    int lastSelectedPosition = 0, nCounter = 0;
    private Handler mHandler = new Handler();
    String orglic, orgname, orgkey, subject;
    boolean isclick = false;
    AsyncTask<String, String, String> fetchtask;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (fetchtask != null)
            fetchtask.cancel(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_organization);

        session = new UserSessionManager(OrganizationActivity.this);
        databaseHelper = new DatabaseHelper(OrganizationActivity.this);
        from = getIntent().getStringExtra("from");
        orgid = session.getData(UserSessionManager.KEY_ORG_REGKEY);
        try {

            organizationArrayList = new ArrayList<>();
            localorglist = new ArrayList<>();

            lytbottom = (Button) findViewById(R.id.lytbottom);
            recycleview = (RecyclerView) findViewById(R.id.recycleview);
            recycleview.setLayoutManager(new LinearLayoutManager(OrganizationActivity.this));
            txtnodata = (TextView) findViewById(R.id.txtnodata);
            txtsec = (TextView) findViewById(R.id.txtsec);
            txtsecbtn = (TextView) findViewById(R.id.txtsecbtn);
            progressbarbtn = (ProgressBar) findViewById(R.id.progressbarbtn);
            //txtmobileno = (TextView) findViewById(R.id.txtmobileno);
            progressbar = (ProgressBar) findViewById(R.id.progressbar);
            lytlist = (LinearLayout) findViewById(R.id.lytlist);
            imgsync = (ImageView) findViewById(R.id.imgsync);

            //txtmobileno.setText(session.getData(UserSessionManager.KEY_Mobile));

        /*if (from != null && from.equals("home")) {
            imgsync.setVisibility(View.VISIBLE);
        }*/

            imgsync.setVisibility(View.VISIBLE);

            //System.out.println("============== = = "+orgid+" == "+session.getData(UserSessionManager.KEY_ORG_NAME));

        /*localorglist = databaseHelper.getAllOrganization();
        if (localorglist.size() != 0)
            GetOrganization(localorglist);
        else
            OnOrgRefreshClick(imgsync.getRootView());*/

        /*if (AppController.isNetConnected(OrganizationActivity.this))
            OnOrgRefreshClick(imgsync.getRootView());
        else
            GetOrganization();*/

            GetOrganization();
            RefreshOrgData(false);
        } catch (Exception e) {
            e.printStackTrace();
            subject = Constant.ERRORID + " | " + session.getData(UserSessionManager.KEY_Mobile) + " | " + session.getData(UserSessionManager.KEY_ORG_NAME) + " | " + Constant.ERRORNOCOMP;
            databaseHelper.addExceptionERROR(e, subject);
        }
    }

    public void OnOrganizationSelectClick(View view) {

        lytbottom.setEnabled(false);

        if (orglic == null || orgname == null || orgkey == null) {
            Toast.makeText(OrganizationActivity.this, getResources().getString(R.string.datanotfound), Toast.LENGTH_SHORT).show();
            return;
        }

        //final String orgreg = session.getData(UserSessionManager.KEY_ORG_REGKEY);
        ArrayList<Company> companies = databaseHelper.getAllCompany(orgkey);
        if (companies.size() != 0) {
            session.setData(UserSessionManager.KEY_ORG_LICENCE, orglic);
            session.setData(UserSessionManager.KEY_ORG_NAME, orgname);
            session.setData(UserSessionManager.KEY_ORG_REGKEY, orgkey);
            //System.out.println("============ com data " + orgkey + " == " + orgname);
            if (companies.size() == 1) {
                for (Company company : companies) {
                    session.setData(orgkey + UserSessionManager.KEY_COM_CODE, company.getCompcode());
                    session.setData(orgkey + UserSessionManager.KEY_COM_NAME, company.getCompname());
                    session.setData(orgkey + UserSessionManager.KEY_COM_ACYEAR, company.getAcyear());
                    session.setData(orgkey + UserSessionManager.KEY_COM_YEARCODE, company.getYearcode());

                }
                startActivity(new Intent(OrganizationActivity.this, OwnerHomeActivity.class));
                GetCompany();
            } else
                startActivity(new Intent(OrganizationActivity.this, CompanyActivity.class).putExtra("from", "organization"));

            lytbottom.setEnabled(true);
        } else if (AppController.isConnected(OrganizationActivity.this, findViewById(R.id.snackbarid))) {

            StartBtnHandlerProcess();
            //System.out.println("============  " + orgreg + " = "+session.getData(UserSessionManager.KEY_ORG_REGKEY));

            String url = Constant.GETCOMPANYNUrl + orgkey + "&mobno=" + session.getData(UserSessionManager.KEY_Mobile) + "&imei=" + session.getData(UserSessionManager.KEY_IMEI) + "&devicecode=" + session.getData(UserSessionManager.KEY_DEVICECODE) + "&demo=" + session.getData(UserSessionManager.KEY_DEMO);
            //System.out.println("============ com url " + url);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (response.contains("\n"))
                        response = response.replace("\n", "").trim();

                    //System.out.println("============ com data " + response);

                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        if (jsonArray.length() > 2) {

                            session.setData(UserSessionManager.KEY_ORG_LICENCE, orglic);
                            session.setData(UserSessionManager.KEY_ORG_NAME, orgname);
                            session.setData(UserSessionManager.KEY_ORG_REGKEY, orgkey);

                            session.setBooleanData(UserSessionManager.KEY_ORGSET, true);
                            session.setBooleanData(orgkey + UserSessionManager.KEY_COMSET, false);
                            databaseHelper.DeleteData(DatabaseHelper.TABLE_COMPANY, orgkey);
                            Company company = null;
                            for (int i = 1; i < jsonArray.length() - 1; i++) {
                                JSONArray comarray = jsonArray.getJSONArray(i);
                                company = new Company(comarray.getString(0), comarray.getString(1), comarray.getString(2), comarray.getString(3), orgkey, comarray.getString(4), comarray.getString(5), comarray.getString(6), comarray.getString(7), comarray.getString(8), comarray.getString(9), comarray.getString(10), comarray.getString(11));
                                databaseHelper.addCompanydata(company);
                                //System.out.println("=========com - " + company.getCompname());
                                if (i == 1) {
                                    session.setBooleanData(orgkey + UserSessionManager.KEY_COMSET, true);
                                    session.setCompanyData(company, orgkey);
                                }

                            }


                            if (jsonArray.length() == 3) {
                                StopBtnHandlerProcess();
                                if (databaseHelper.IsMasterExists(company.getCompcode(), orgkey)) {
                                    startActivity(new Intent(OrganizationActivity.this, OwnerHomeActivity.class));
                                    lytbottom.setEnabled(true);
                                    finishAffinity();
                                } else if (AppController.isConnected(OrganizationActivity.this, findViewById(R.id.snackbarid))) {
                                    fetchtask = new FetchAsyncRunner(company).execute();

                                    /*Toast.makeText(OrganizationActivity.this, getResources().getString(R.string.fetchingcompanydata), Toast.LENGTH_SHORT).show();
                                    StartBtnHandlerProcess();

                                    final Company finalCompany = company;
                                    Company finalCompany1 = company;
                                    StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.GETMASTERNUrl + session.getData(UserSessionManager.KEY_ORG_REGKEY) + "&compcode=" + company.getCompcode() + "&mobno=" + session.getData(UserSessionManager.KEY_Mobile) + "&imei=" + session.getData(UserSessionManager.KEY_IMEI) + "&devicecode=" + session.getData(UserSessionManager.KEY_DEVICECODE) + "&demo=" + session.getData(UserSessionManager.KEY_DEMO), new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {

                                            if (response.contains("\n"))
                                                response = response.replace("\n", "").trim();

                                            databaseHelper.DeleteMasterData(finalCompany.getCompcode(), orgkey);

                                            try {
                                                JSONArray jsonArray = new JSONArray(response);
                                                for (int i = 1; i < jsonArray.length() - 1; i++) {
                                                    JSONArray masterarray = jsonArray.getJSONArray(i);
                                                    //System.out.println("=============data == = " + masterarray.getString(0) + " = " + masterarray.getString(1) + "=" + masterarray.getString(2) + "=" + masterarray.getString(3) + "=" + masterarray.getString(4));
                                                    Master master = new Master(masterarray.getString(0), masterarray.getString(1), masterarray.getString(2), masterarray.getString(3), masterarray.getString(4), masterarray.getString(5), masterarray.getString(6), masterarray.getString(7), masterarray.getString(8), finalCompany.getCompcode(), orgkey);
                                                    databaseHelper.addMasterdata(master);
                                                }

                                                startActivity(new Intent(OrganizationActivity.this, OwnerHomeActivity.class));
                                                finishAffinity();
                                            } catch (JSONException e) {
                                                if (e.toString().contains(getResources().getString(R.string.noresponse)))
                                                    Toast.makeText(OrganizationActivity.this, getResources().getString(R.string.notconnectedserver), Toast.LENGTH_SHORT).show();
                                                else {
                                                    e.printStackTrace();

                                                    subject = Constant.ERRORID + " | " + session.getData(UserSessionManager.KEY_Mobile) + " | " + session.getData(UserSessionManager.KEY_ORG_NAME) + " | " + finalCompany1.getCompname();
                                                    databaseHelper.addJSONERROR(e, subject);
                                                }
                                            }
                                            StopBtnHandlerProcess();
                                            lytbottom.setEnabled(true);

                                        }

                                    },
                                            new Response.ErrorListener() {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {
                                                    lytbottom.setEnabled(true);
                                                    StopBtnHandlerProcess();
                                                    String message = Constant.VolleyErrorMessage(error);
                                                    if (!message.equals(""))
                                                        Toast.makeText(OrganizationActivity.this, message, Toast.LENGTH_SHORT).show();

                                                }
                                            });
                                    AppController.getInstance().getRequestQueue().getCache().clear();
                                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                    AppController.getInstance().addToRequestQueue(stringRequest);*/
                                }
                                //startActivity(new Intent(OrganizationActivity.this, OwnerHomeActivity.class));
                            } else
                                startActivity(new Intent(OrganizationActivity.this, CompanyActivity.class).putExtra("from", "organization"));

                        } else {
                            Toast.makeText(OrganizationActivity.this, "Sorry No Company Found", Toast.LENGTH_SHORT).show();
                        }
                        lytbottom.setEnabled(true);
                    } catch (JSONException e) {
                        lytbottom.setEnabled(true);
                        if (e.toString().contains(getResources().getString(R.string.noresponse)))
                            Toast.makeText(OrganizationActivity.this, getResources().getString(R.string.notconnectedserver), Toast.LENGTH_SHORT).show();
                        else {
                            e.printStackTrace();
                            subject = Constant.ERRORID + " | " + session.getData(UserSessionManager.KEY_Mobile) + " | " + session.getData(UserSessionManager.KEY_ORG_NAME) + " | " + Constant.ERRORNOCOMP;
                            databaseHelper.addJSONERROR(e, subject);
                        }
                    }
                    StopBtnHandlerProcess();

                }

            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            lytbottom.setEnabled(true);
                            StopBtnHandlerProcess();

                            String message = Constant.VolleyErrorMessage(error);
                            if (!message.equals(""))
                                Toast.makeText(OrganizationActivity.this, message, Toast.LENGTH_SHORT).show();

                        }
                    });
            AppController.getInstance().getRequestQueue().getCache().clear();
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(stringRequest);

        } else {
            lytbottom.setEnabled(true);
        }
    }

    private void GetCompany() {
        if (AppController.isNetConnected(OrganizationActivity.this)) {
            String url = Constant.GETCOMPANYNUrl + session.getData(UserSessionManager.KEY_ORG_REGKEY) + "&mobno=" + session.getData(UserSessionManager.KEY_Mobile) + "&imei=" + session.getData(UserSessionManager.KEY_IMEI) + "&devicecode=" + session.getData(UserSessionManager.KEY_DEVICECODE) + "&demo=" + session.getData(UserSessionManager.KEY_DEMO);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (response.contains("\n"))
                        response = response.replace("\n", "").trim();

                    //System.out.println("=============== = =  = = ="+isprogressvisible+" = " + response);
                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        if (jsonArray.length() > 2) {

                            databaseHelper.DeleteData(DatabaseHelper.TABLE_COMPANY, session.getData(UserSessionManager.KEY_ORG_REGKEY));

                            for (int i = 1; i < jsonArray.length() - 1; i++) {
                                JSONArray comarray = jsonArray.getJSONArray(i);
                                Company company = new Company(comarray.getString(0), comarray.getString(1), comarray.getString(2), comarray.getString(3), session.getData(UserSessionManager.KEY_ORG_REGKEY), comarray.getString(4), comarray.getString(5), comarray.getString(6), comarray.getString(7), comarray.getString(8), comarray.getString(9), comarray.getString(10), comarray.getString(11));
                                databaseHelper.addCompanydata(company);
                            }


                        }
                    } catch (JSONException e) {

                        if (!e.toString().contains(getResources().getString(R.string.noresponse))) {
                            e.printStackTrace();
                            databaseHelper.addJSONERROR(e, subject);
                        }

                    }


                }

            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {


                        }
                    });
            AppController.getInstance().getRequestQueue().getCache().clear();
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(stringRequest);
        }
    }

    private void GetOrganization() {

        localorglist = databaseHelper.getAllOrganization();
        if (localorglist.size() != 0) {

            progressbar.setVisibility(View.VISIBLE);
            int i = 0;
            for (Organization organization : localorglist) {
                organizationArrayList.add(organization);
                //System.out.println("============== = =--- "+organization.getRegkey()+" == "+organization.getOrganization_name());
                if (!orgid.equals("") && organization.getRegkey().equals(orgid))
                    lastSelectedPosition = i;

                i++;
            }

            progressbar.setVisibility(View.GONE);
            recycleview.setAdapter(new OrganizationAdapter(organizationArrayList, OrganizationActivity.this));
        } else {
            OnOrgRefreshClick(imgsync.getRootView());
        }
    }

    private void StopHandlerProcess() {
        progressbar.setVisibility(View.GONE);
        txtsec.setVisibility(View.GONE);
        mHandler.removeCallbacks(hMyTimeTask);
    }

    private void StartHandlerProcess() {
        progressbar.setVisibility(View.VISIBLE);
        txtsec.setVisibility(View.VISIBLE);
        txtsec.setText("");
        nCounter = 0;
        mHandler.postDelayed(hMyTimeTask, 1000);
    }

    private Runnable hMyTimeTask = new Runnable() {
        public void run() {
            nCounter++;
            txtsec.setText("" + nCounter);
            mHandler.postDelayed(hMyTimeTask, 1000);
        }
    };

    private void StopBtnHandlerProcess() {
        progressbarbtn.setVisibility(View.GONE);
        txtsecbtn.setVisibility(View.GONE);
        mHandler.removeCallbacks(hMyTimeTaskBtn);
    }

    private void StartBtnHandlerProcess() {
        progressbarbtn.setVisibility(View.VISIBLE);
        txtsecbtn.setVisibility(View.VISIBLE);
        txtsecbtn.setText("");
        nCounter = 0;
        mHandler.postDelayed(hMyTimeTaskBtn, 1000);
    }

    private Runnable hMyTimeTaskBtn = new Runnable() {
        public void run() {
            nCounter++;
            txtsecbtn.setText("" + nCounter);
            mHandler.postDelayed(hMyTimeTaskBtn, 1000);
        }
    };

    public void OnOrgRefreshClick(View view) {
        RefreshOrgData(true);
    }

    private void RefreshOrgData(boolean isprogress) {
        if (AppController.isConnected(OrganizationActivity.this, findViewById(R.id.snackbarid))) {

            if (isprogress) {
                imgsync.setEnabled(false);
                StartHandlerProcess();
            }

            String url = Constant.GETORGANIZATIONUrl + session.getData(UserSessionManager.KEY_Mobile) + "&demo=" + session.getData(UserSessionManager.KEY_DEMO) + "&imei=" + session.getData(UserSessionManager.KEY_IMEI) + "&devicecode=" + session.getData(UserSessionManager.KEY_DEVICECODE);
            //System.out.println("============  orgurl " + url);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (response.contains("\n"))
                        response = response.replace("\n", "").trim();


                    if (!Constant.ResponseErrorCheck(OrganizationActivity.this, response, isprogress)) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            //System.out.println("============  org= =" + response);
                            if (jsonObject.getInt("records") == 0) {
                                Toast.makeText(OrganizationActivity.this, getResources().getString(R.string.noorg), Toast.LENGTH_SHORT).show();
                            } else {

                                JSONArray jsonArray = jsonObject.getJSONArray("rows");
                                databaseHelper.DeleteData(DatabaseHelper.TABLE_ORGANIZATION, "");

                                if (isprogress) organizationArrayList.clear();

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    JSONArray cellarray = object.getJSONArray("cell");

                                    if (cellarray.length() != 0) {
                                        Organization organization = new Organization(cellarray.getString(0), cellarray.getString(1), cellarray.getString(2));
                                        databaseHelper.addOrgdata(organization);
                                        if (isprogress && i == 0) {
                                            session.setData(UserSessionManager.KEY_ORG_LICENCE, organization.getLicencekey());
                                            session.setData(UserSessionManager.KEY_ORG_NAME, organization.getOrganization_name());
                                            session.setData(UserSessionManager.KEY_ORG_REGKEY, organization.getRegkey());
                                        }
                                    }
                                }

                                if (isprogress)
                                    GetOrganization();

                            }

                        } catch (JSONException e) {
                            if (isprogress)
                                StopHandlerProcess();
                            if (e.toString().contains(getResources().getString(R.string.noresponse)))
                                Toast.makeText(OrganizationActivity.this, getResources().getString(R.string.notconnectedserver), Toast.LENGTH_SHORT).show();
                            else {
                                e.printStackTrace();

                                subject = Constant.ERRORID + " | " + session.getData(UserSessionManager.KEY_Mobile) + " | " + session.getData(UserSessionManager.KEY_ORG_NAME) + " | " + Constant.ERRORNOCOMP;
                                databaseHelper.addJSONERROR(e, subject);
                            }
                        }
                    }

                    if (isprogress) {
                        imgsync.setEnabled(true);
                        StopHandlerProcess();
                    }
                }

            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (isprogress) {
                                imgsync.setEnabled(true);
                                StopHandlerProcess();
                            }
                            String message = Constant.VolleyErrorMessage(error);

                            if (!message.equals(""))
                                Toast.makeText(OrganizationActivity.this, message, Toast.LENGTH_SHORT).show();

                        }
                    });
            AppController.getInstance().getRequestQueue().getCache().clear();
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(stringRequest);
        } else {
            lytbottom.setEnabled(true);
        }
    }

    private class FetchAsyncRunner extends AsyncTask<String, String, String> {

        String companycode;
        Company company;

        public FetchAsyncRunner(Company company) {
            this.company = company;
            this.companycode = company.getCompcode();
        }

        @Override
        protected String doInBackground(String... params) {
            try {


                StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.GETMASTERNUrl + session.getData(UserSessionManager.KEY_ORG_REGKEY) + "&compcode=" + company.getCompcode() + "&mobno=" + session.getData(UserSessionManager.KEY_Mobile) + "&imei=" + session.getData(UserSessionManager.KEY_IMEI) + "&devicecode=" + session.getData(UserSessionManager.KEY_DEVICECODE) + "&demo=" + session.getData(UserSessionManager.KEY_DEMO), new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (response.contains("\n"))
                            response = response.replace("\n", "").trim();

                        if (Constant.ResponseErrorCheck(OrganizationActivity.this, response, true)) {
                            StopBtnHandlerProcess();
                            lytbottom.setEnabled(true);
                        } else {
                            databaseHelper.DeleteMasterData(companycode, orgkey);

                            try {
                                JSONArray jsonArray = new JSONArray(response);
                                for (int i = 1; i < jsonArray.length() - 1; i++) {
                                    JSONArray masterarray = jsonArray.getJSONArray(i);
                                    //System.out.println("=============data == = " + masterarray.getString(0) + " = " + masterarray.getString(1) + "=" + masterarray.getString(2) + "=" + masterarray.getString(3) + "=" + masterarray.getString(4));
                                    Master master = new Master(masterarray.getString(0), masterarray.getString(1), masterarray.getString(2), masterarray.getString(3), masterarray.getString(4), masterarray.getString(5), masterarray.getString(6), masterarray.getString(7), masterarray.getString(8), companycode, orgkey);
                                    databaseHelper.addMasterdata(master);
                                }

                                StopBtnHandlerProcess();
                                lytbottom.setEnabled(true);
                                startActivity(new Intent(OrganizationActivity.this, OwnerHomeActivity.class));
                                finishAffinity();
                            } catch (JSONException e) {
                                StopBtnHandlerProcess();
                                lytbottom.setEnabled(true);
                                if (e.toString().contains(getResources().getString(R.string.noresponse)))
                                    Toast.makeText(OrganizationActivity.this, getResources().getString(R.string.notconnectedserver), Toast.LENGTH_SHORT).show();
                                else if (e.toString().contains(getResources().getString(R.string.multiuseword))) {
                                    Toast.makeText(OrganizationActivity.this, getResources().getString(R.string.multiuse), Toast.LENGTH_SHORT).show();
                                } else if (e.toString().contains(getResources().getString(R.string.qstringerr))) {
                                    Toast.makeText(OrganizationActivity.this, getResources().getString(R.string.trylater), Toast.LENGTH_SHORT).show();
                                } else {
                                    e.printStackTrace();
                                    subject = Constant.ERRORID + " | " + session.getData(UserSessionManager.KEY_Mobile) + " | " + session.getData(UserSessionManager.KEY_ORG_NAME) + " | " + company.getCompname();
                                    databaseHelper.addJSONERROR(e, subject);
                                }
                            }

                        }
                    }

                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                lytbottom.setEnabled(true);
                                StopBtnHandlerProcess();
                                String message = Constant.VolleyErrorMessage(error);
                                if (!message.equals(""))
                                    Toast.makeText(OrganizationActivity.this, message, Toast.LENGTH_SHORT).show();

                            }
                        });
                AppController.getInstance().getRequestQueue().getCache().clear();
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(stringRequest);

            } catch (Exception e) {
                e.printStackTrace();
                databaseHelper.addExceptionERROR(e, subject);
            }
            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            //StopBtnHandlerProcess();
            //lytbottom.setEnabled(true);
        }


        @Override
        protected void onPreExecute() {
            Toast.makeText(OrganizationActivity.this, getResources().getString(R.string.fetchingcompanydata), Toast.LENGTH_SHORT).show();
            StartBtnHandlerProcess();
        }
    }

    public class OrganizationAdapter extends RecyclerView.Adapter<OrganizationAdapter.ItemRowHolder> {
        private ArrayList<Organization> chapterlist;
        private Context mContext;

        public OrganizationAdapter(ArrayList<Organization> chapterlist, Context context) {
            this.chapterlist = chapterlist;
            this.mContext = context;
        }

        @Override
        public ItemRowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.lyt_organization, parent, false);
            return new ItemRowHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull final ItemRowHolder holder, final int position) {

            final Organization organization = chapterlist.get(position);

            holder.txtname.setText(organization.getOrganization_name());
            //holder.txtkey.setText(organization.getLicencekey());

            if (lastSelectedPosition == position) {

                orglic = organization.getLicencekey();
                orgname = organization.getOrganization_name();
                orgkey = organization.getRegkey();

                holder.btnradio.setChecked(true);
                holder.txtname.setTextColor(getResources().getColor(R.color.colorPrimary));
            } else {
                holder.btnradio.setChecked(false);
                holder.txtname.setTextColor(getResources().getColor(R.color.black));
            }
            holder.btnradio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = position;
                    holder.btnradio.setChecked(true);

                    /*orglic = organization.getLicencekey();
                    orgname = organization.getOrganization_name();
                    orgkey = organization.getRegkey();*/

                    /*session.setData(UserSessionManager.KEY_ORG_LICENCE, organization.getLicencekey());
                    session.setData(UserSessionManager.KEY_ORG_NAME, organization.getOrganization_name());
                    session.setData(UserSessionManager.KEY_ORG_REGKEY, organization.getRegkey());*/
                    notifyDataSetChanged();
                }
            });

            holder.lytmain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.btnradio.performClick();
                }
            });
        }

        @Override
        public int getItemCount() {
            return (null != chapterlist ? chapterlist.size() : 0);
        }

        public class ItemRowHolder extends RecyclerView.ViewHolder {
            TextView txtname;
            RadioButton btnradio;
            LinearLayout lytmain;

            public ItemRowHolder(View itemView) {
                super(itemView);
                //txtkey = itemView.findViewById(R.id.txtkey);
                txtname = itemView.findViewById(R.id.txtname);
                btnradio = itemView.findViewById(R.id.btnradio);
                lytmain = itemView.findViewById(R.id.lytmain);

            }
        }
    }
}
