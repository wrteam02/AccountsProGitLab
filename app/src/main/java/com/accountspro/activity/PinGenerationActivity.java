package com.accountspro.activity;

import android.content.Intent;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.accountspro.R;
import com.accountspro.helper.AppController;
import com.accountspro.helper.Constant;
import com.accountspro.helper.DatabaseHelper;
import com.accountspro.helper.UserSessionManager;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.util.ArrayList;

public class PinGenerationActivity extends AppCompatActivity {

    ProgressBar progressbar;
    ArrayList<String> pinlist;
    UserSessionManager session;
    DatabaseHelper databaseHelper;
    Button btnsave;
    String from;
    TextView txtpin;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_generation);

        databaseHelper = new DatabaseHelper(PinGenerationActivity.this);

        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        pinlist = new ArrayList<>();
        session = new UserSessionManager(PinGenerationActivity.this);

        txtpin = (TextView) findViewById(R.id.txtpin);


        btnsave = (Button) findViewById(R.id.btnsave);
        from = getIntent().getStringExtra("from");

        if (from != null && from.equals("splash")) {
            btnsave.setText("Submit");
            txtpin.setText("Enter Lock PIN");
        } else if (from != null && from.equals("confirm")) {
            btnsave.setText("Confirm");
            txtpin.setText("Confirm Your Lock PIN");
        } else {
            txtpin.setText("Generate PIN");
        }
    }

    public void OnSavePinClick(View view) {
        if (from != null && from.equals("splash")) {
            String mainpin = session.getData(UserSessionManager.KEY_PIN);
            String pin = "";
            for (int i = 0; i < pinlist.size(); i++) {
                pin = pin + pinlist.get(i);
            }

            if (mainpin.equals(pin))
                startActivity(new Intent(PinGenerationActivity.this, OwnerHomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            else {
                Toast.makeText(PinGenerationActivity.this, "Wrong PIN", Toast.LENGTH_SHORT).show();
            }

        } else if (from != null && from.equals("confirm")) {
            String pin = getIntent().getStringExtra("pin");

            String cpin = "";
            for (int i = 0; i < pinlist.size(); i++) {
                cpin = cpin + pinlist.get(i);
            }

            //System.out.println("========== = =  =pin " + pin);

            if (cpin.length() == 0)
                Toast.makeText(PinGenerationActivity.this, "Enter PIN for Confirmation", Toast.LENGTH_SHORT).show();
            else if (!pin.equals(cpin)) {
                Toast.makeText(PinGenerationActivity.this, getResources().getString(R.string.pinnotmatch), Toast.LENGTH_SHORT).show();
            } else if (AppController.isConnected(PinGenerationActivity.this,findViewById(R.id.activity_main))) {
                progressbar.setVisibility(View.VISIBLE);
                final String finalCpin = cpin;
                StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.PINSETUrl + session.getData(UserSessionManager.KEY_Mobile) + "&pin=" + cpin, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response1) {
                        progressbar.setVisibility(View.GONE);
                        if (response1.contains("\n"))
                            response1 = response1.replace("\n", "").trim();

                        if (response1.equalsIgnoreCase("Yes")) {
                            session.setBooleanData(UserSessionManager.KEY_PINSET, true);
                            session.setData(UserSessionManager.KEY_PIN, finalCpin);
                            session.setData(UserSessionManager.KEY_LoginType, UserSessionManager.OWNER);
                            databaseHelper.addUserdata(session.getData(UserSessionManager.KEY_Mobile), session.getData(UserSessionManager.KEY_DEVICEINFO), finalCpin, session.getData(UserSessionManager.KEY_OTP), session.getData(UserSessionManager.KEY_REGI), session.getData(UserSessionManager.KEY_DEMO));
                            Toast.makeText(PinGenerationActivity.this, "PIN Successfully set", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(PinGenerationActivity.this, OwnerLoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        } else {
                            Toast.makeText(PinGenerationActivity.this, "Sorry Your PIN not set,Try Again", Toast.LENGTH_SHORT).show();
                        }

                    }

                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                progressbar.setVisibility(View.GONE);
                                String message = Constant.VolleyErrorMessage(error);
                                if (!message.equals(""))
                                    Toast.makeText(PinGenerationActivity.this, message, Toast.LENGTH_SHORT).show();

                            }
                        });
                AppController.getInstance().getRequestQueue().getCache().clear();
                AppController.getInstance().addToRequestQueue(stringRequest);
            }

        } else {
            if (pinlist.size() != 4)
                Toast.makeText(PinGenerationActivity.this, getResources().getString(R.string.pinlengtherr), Toast.LENGTH_SHORT).show();
            else {
                String pin = "";
                for (int i = 0; i < pinlist.size(); i++) {
                    pin = pin + pinlist.get(i);
                }

                Intent intent = new Intent(PinGenerationActivity.this, PinGenerationActivity.class);
                intent.putExtra("from", "confirm");
                intent.putExtra("pin", pin);
                startActivity(intent);

                /*final AlertDialog.Builder alertdialog = new AlertDialog.Builder(PinGenerationActivity.this);
                alertdialog.setTitle("Confirm Your PIN");
                final EditText editText = new EditText(PinGenerationActivity.this);
                editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                editText.setTypeface(ResourcesCompat.getFont(PinGenerationActivity.this, R.font.robotoregular));
                alertdialog.setView(editText);
                alertdialog.setNegativeButton("Reset PIN", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        pinlist.clear();
                        for(int i=1;i<=4;i++) {
                            ImageView imageView = (ImageView) findViewById(getResources().getIdentifier("img" + i, "id", getPackageName()));
                            imageView.setImageResource(R.drawable.blankpin_circle);
                        }
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                        dialog.dismiss();
                    }
                });



                alertdialog.setPositiveButton("Confirm PIN", null);
                final AlertDialog dialog = alertdialog.create();
                editText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if(s.length() == 4)
                            dialog.getButton(DialogInterface.BUTTON_POSITIVE).performClick();
                    }

                    @Override
                    public void afterTextChanged(Editable s) {                    }
                });

                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog1) {
                        Button btnsave = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                        btnsave.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String pin = "";
                                for (int i = 0; i < pinlist.size(); i++) {
                                    pin = pin + pinlist.get(i);
                                }
                                System.out.println("========== = =  =pin " + pin);
                                final String cpin = editText.getText().toString().trim();
                                if (cpin.length() == 0)
                                    Toast.makeText(PinGenerationActivity.this, "Enter PIN for Confirmation", Toast.LENGTH_SHORT).show();
                                else if (!pin.equals(cpin)) {
                                    Toast.makeText(PinGenerationActivity.this, "PIN not Match", Toast.LENGTH_SHORT).show();
                                } else if (AppController.isConnected(PinGenerationActivity.this)) {
                                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                    imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                                    progressbar.setVisibility(View.VISIBLE);
                                    StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.PINSETUrl + session.getData(UserSessionManager.KEY_Mobile) + "&pin=" + cpin, new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response1) {
                                            progressbar.setVisibility(View.GONE);
                                            if (response1.contains("\n"))
                                                response1 = response1.replace("\n", "").trim();

                                            if (response1.equalsIgnoreCase("Yes")) {
                                                session.setBooleanData(UserSessionManager.KEY_PINSET, true);
                                                session.setData(UserSessionManager.KEY_PIN, cpin);
                                                session.setData(UserSessionManager.KEY_LoginType, UserSessionManager.OWNER);
                                                databaseHelper.addUserdata(session.getData(UserSessionManager.KEY_Mobile), session.getData(UserSessionManager.KEY_DEVICEINFO), cpin, session.getData(UserSessionManager.KEY_OTP), session.getData(UserSessionManager.KEY_REGI), session.getData(UserSessionManager.KEY_DEMO));
                                                Toast.makeText(PinGenerationActivity.this, "PIN Successfully set", Toast.LENGTH_SHORT).show();
                                                startActivity(new Intent(PinGenerationActivity.this, OwnerLoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                            } else {
                                                Toast.makeText(PinGenerationActivity.this, "Sorry Your PIN not set,Try Again", Toast.LENGTH_SHORT).show();
                                            }

                                        }

                                    },
                                            new Response.ErrorListener() {
                                                @Override
                                                public void onErrorResponse(VolleyError error) {
                                                    progressbar.setVisibility(View.GONE);
                                                    String message = "";
                                                    if (error instanceof NetworkError) {
                                                        message = "Cannot connect to Internet...Please check your connection!";
                                                    } else if (error instanceof ServerError) {
                                                        message = "The server could not be found. Please try again after some time!!";
                                                    } else if (error instanceof AuthFailureError) {
                                                        message = "Cannot connect to Internet...Please check your connection!";
                                                    } else if (error instanceof ParseError) {
                                                        message = "Parsing error! Please try again after some time!!";
                                                    } else if (error instanceof NoConnectionError) {
                                                        message = "Cannot connect to Internet...Please check your connection!";
                                                    } else if (error instanceof TimeoutError) {
                                                        message = "Connection TimeOut! Please check your internet connection.";
                                                    }

                                                    if (!message.equals(""))
                                                        Toast.makeText(PinGenerationActivity.this, message, Toast.LENGTH_SHORT).show();

                                                }
                                            });
                                    AppController.getInstance().getRequestQueue().getCache().clear();
                                    AppController.getInstance().addToRequestQueue(stringRequest);
                                }

                            }
                        });
                    }
                });
                dialog.show();
                editText.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);*/
            }

        }
    }

    public void OnNumberClick(View view) {
        Button btn = (Button) view;
        String buttonText = btn.getText().toString();
        if (pinlist.size() == 4)
            Toast.makeText(PinGenerationActivity.this, getResources().getString(R.string.maxpinlimit), Toast.LENGTH_SHORT).show();
        else {
            pinlist.add(buttonText);
            //System.out.println("============= = + =" + pinlist.size());
            ImageView imageView = (ImageView) findViewById(getResources().getIdentifier("img" + pinlist.size(), "id", this.getPackageName()));
            imageView.setImageResource(R.drawable.fillpin_circle);

            if (pinlist.size() == 4)
                OnSavePinClick(view);
        }
    }

    public void OnClearClick(View view) {

        if (pinlist.size() != 0) {
            ImageView imageView = (ImageView) findViewById(getResources().getIdentifier("img" + pinlist.size(), "id", this.getPackageName()));
            imageView.setImageResource(R.drawable.blankpin_circle);
            pinlist.remove(pinlist.size() - 1);
            //System.out.println("============= = - =" + pinlist.size());
        }
    }
}
