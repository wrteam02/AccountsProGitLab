package com.accountspro.activity;

import android.Manifest;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.accountspro.R;
import com.accountspro.helper.AppController;
import com.accountspro.helper.Constant;
import com.accountspro.helper.DatabaseHelper;
import com.accountspro.helper.PinView;
import com.accountspro.helper.SMSBroadcastReceiver;
import com.accountspro.helper.UserSessionManager;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import org.jetbrains.annotations.NotNull;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.SecureRandom;

public class OTPVarificationActivity extends AppCompatActivity {

    String mobileno, OTP, from;
    TextView txtno, txtresend;
    PinView edtotpverification;
    SmsRetrieverClient client;
    Task<Void> retriever;
    SMSBroadcastReceiver smsBroadcastReceiver;
    ProgressBar progressbar;
    Button btnverify;
    UserSessionManager session;
    String deviceinfo;
    DatabaseHelper databaseHelper;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otpvarification);

        session = new UserSessionManager(OTPVarificationActivity.this);

        databaseHelper = new DatabaseHelper(OTPVarificationActivity.this);

        mobileno = getIntent().getStringExtra("mobileno");
        OTP = getIntent().getStringExtra("OTP");
        from = getIntent().getStringExtra("from");

        if (ContextCompat.checkSelfPermission(OTPVarificationActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(OTPVarificationActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, 0);
        }

        btnverify = (Button) findViewById(R.id.btnverify);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        txtno = (TextView) findViewById(R.id.txtno);
        txtresend = (TextView) findViewById(R.id.txtresend);
        edtotpverification = (PinView) findViewById(R.id.edtotpverification);


        btnverify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnVerifyClick(v);
            }
        });

        String text = "If you not receive code! <font color=" + getResources().getColor(R.color.colorPrimary) + ">Resend</font>";
        txtresend.setText(Html.fromHtml(text));
        txtresend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppController.isConnected(OTPVarificationActivity.this,findViewById(R.id.activity_main)))
                    ResendOTP();
            }
        });

        if (from != null && from.equals("forgot")) {
            if (AppController.isConnected(OTPVarificationActivity.this,findViewById(R.id.activity_main)))
                ResendOTP();
        }

        txtno.setText("We have send the OTP on " + mobileno + " will apply auto to the fields");

        client = SmsRetriever.getClient(this);
        retriever = client.startSmsRetriever();
        smsBroadcastReceiver = new SMSBroadcastReceiver();

        retriever.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                //Toast.makeText(OTPVarifivationActivity.this, "Listener Started", Toast.LENGTH_SHORT).show();

                smsBroadcastReceiver.injectOTPListener(new SMSBroadcastReceiver.OTPListener() {
                    @Override
                    public void onOTPReceived(@NotNull String otp) {
                        edtotpverification.setText(otp);
                    }

                    @Override
                    public void onOTPTimeOut() {
                        Toast.makeText(OTPVarificationActivity.this, "Timeout", Toast.LENGTH_SHORT).show();
                    }
                });

                registerReceiver(smsBroadcastReceiver, new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION));

            }
        });

        retriever.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                //Toast.makeText(OTPVarifivationActivity.this, "Problem to start listener", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void ResendOTP() {
        txtresend.setEnabled(false);
        txtresend.setClickable(false);
        progressbar.setVisibility(View.VISIBLE);
        final String OTPText = String.format("%04d", new SecureRandom().nextInt(10000));
        String smstext = null;
        System.out.println("=============== otp "+OTPText);
        try {
            smstext = URLEncoder.encode("<#> Your Verification OTP for " + getResources().getString(R.string.app_name) + " is : " + OTPText + "\n" + Constant.SMSHashCode, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();

            String subject = Constant.ERRORID+" | "+mobileno+" | "+Constant.ERRORNOORG +" | "+Constant.ERRORNOCOMP;
            databaseHelper.addUnSuEncodingERROR(e,subject);

        }
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.OTPUrl + "mobile=" + mobileno + "&message=" + smstext, new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {
                progressbar.setVisibility(View.GONE);
                if (response1.contains("\n"))
                    response1 = response1.replace("\n", "").trim();
                if (response1.contains("success")) {
                    OTP = OTPText;
                    edtotpverification.setText("");
                    Toast.makeText(OTPVarificationActivity.this, "OTP Send Successfully", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(OTPVarificationActivity.this, "Failed...Try Again", Toast.LENGTH_SHORT).show();
                }
                txtresend.setEnabled(true);
                txtresend.setClickable(true);
            }

        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        txtresend.setEnabled(true);
                        txtresend.setClickable(true);
                        progressbar.setVisibility(View.GONE);
                        String message = Constant.VolleyErrorMessage(error);
                        if (!message.equals(""))
                            Toast.makeText(OTPVarificationActivity.this, message, Toast.LENGTH_SHORT).show();

                    }
                });
        AppController.getInstance().getRequestQueue().getCache().clear();
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(smsBroadcastReceiver);
    }

    public void OnVerifyClick(View view) {
        String editotp = edtotpverification.getText().toString().trim();
        if (editotp.length() == 0)
            Toast.makeText(OTPVarificationActivity.this, getResources().getString(R.string.resendotp), Toast.LENGTH_SHORT).show();
        else if (!editotp.equals(OTP)) {
            Toast.makeText(OTPVarificationActivity.this, getResources().getString(R.string.wrongotp), Toast.LENGTH_SHORT).show();
        } else if (ContextCompat.checkSelfPermission(OTPVarificationActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            session.setData(UserSessionManager.KEY_Mobile, mobileno);
            session.setData(UserSessionManager.KEY_OTP, OTP);
            session.setBooleanData(UserSessionManager.KEY_OTPSEND, true);
            ActivityCompat.requestPermissions(OTPVarificationActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
        } else if (AppController.isConnected(OTPVarificationActivity.this,findViewById(R.id.activity_main))) {
            session.setData(UserSessionManager.KEY_Mobile, mobileno);
            session.setData(UserSessionManager.KEY_OTP, OTP);
            session.setBooleanData(UserSessionManager.KEY_OTPSEND, true);
            try {
                deviceinfo = URLEncoder.encode(AppController.getUniqueIMEIId(session, OTPVarificationActivity.this), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();

                String subject = Constant.ERRORID+" | "+mobileno+" | "+Constant.ERRORNOORG +" | "+Constant.ERRORNOCOMP;
                databaseHelper.addUnSuEncodingERROR(e,subject);
            }
            progressbar.setVisibility(View.VISIBLE);
            btnverify.setEnabled(false);
            txtresend.setEnabled(false);
            String url = Constant.SETDEVICEINFO + mobileno + "&imei=" + session.getData(UserSessionManager.KEY_IMEI) + "&deviceinfo=" + deviceinfo + "&devicecode=" + session.getData(UserSessionManager.KEY_DEVICECODE);
            //System.out.println("======================= = =" + url);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response1) {
                    progressbar.setVisibility(View.GONE);
                    if (response1.contains("\n"))
                        response1 = response1.replace("\n", "").trim();

                    //System.out.println("======================= = =" + response1);
                    if (response1.contains(getResources().getString(R.string.qstringerr))) {
                        btnverify.setEnabled(true);
                        txtresend.setEnabled(true);
                        Toast.makeText(OTPVarificationActivity.this, getResources().getString(R.string.trylater), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (response1.equalsIgnoreCase("Yes")) {
                        session.setBooleanData(UserSessionManager.KEY_VERIFIED, true);
                        session.setData(UserSessionManager.KEY_DEVICEINFO, deviceinfo);
                        startActivity(new Intent(OTPVarificationActivity.this, PinGenerationActivity.class).putExtra("from", "otp"));
                    } else {
                        Toast.makeText(OTPVarificationActivity.this, getResources().getString(R.string.deviceinfonotset), Toast.LENGTH_SHORT).show();
                    }

                    btnverify.setEnabled(true);
                    txtresend.setEnabled(true);

                }

            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            btnverify.setEnabled(true);
                            txtresend.setEnabled(true);
                            progressbar.setVisibility(View.GONE);
                            String message = Constant.VolleyErrorMessage(error);

                            if (!message.equals(""))
                                Toast.makeText(OTPVarificationActivity.this, message, Toast.LENGTH_SHORT).show();

                        }
                    });
            AppController.getInstance().getRequestQueue().getCache().clear();
            AppController.getInstance().addToRequestQueue(stringRequest);

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    btnverify.performClick();
                } else {
                    Toast.makeText(OTPVarificationActivity.this, getResources().getString(R.string.deviceinfopermissionmsg), Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }
}
