package com.accountspro.activity;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.text.Html;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.accountspro.R;
import com.accountspro.helper.AppController;
import com.accountspro.helper.Constant;
import com.accountspro.helper.DatabaseHelper;
import com.accountspro.helper.UserSessionManager;
import com.accountspro.helper.VolleyCallback;
import com.accountspro.model.Business;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.apache.poi.hssf.usermodel.HeaderFooter;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Footer;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.PrintSetup;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.ShapeTypes;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFSimpleShape;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class BusinessViewActivity extends AppCompatActivity {

    public int ReqReadPermission = 1;
    public int ReqWritePermission = 2;
    Toolbar toolbar;
    RecyclerView receipt0_recycleview, receipt1_recycleview, payment0_recycleview, payment1_recycleview, cash_recycleview, bank_recycleview;
    LinearLayout lytpdfexcel, lytbankmain, lytcashmain, lytpaymain, lytreceiptmain, lytprogress, lytbottom, lytstock, lytbank, lytcash, lytpayment, lytreceipt, lytpurchase, lytsales;
    TextView txtsec, datedisplay, txtnodata, txtto, txtfrom, txttodt, txtfromdt, sale4, sale2, sale1, purchase4, purchase2, purchase1, receipt4, receipt2, receipt1, payment4, payment2, payment1, cash2, cash1, bank2, bank1, stock2, stock1;
    ProgressBar progressbar;
    LinearLayout lyttop, lytfrom, lytto, lytmain, lytdatedisplay;
    Spinner spquick;

    SimpleDateFormat month_date = new SimpleDateFormat("MMM");
    SimpleDateFormat date_name = new SimpleDateFormat("EEEE");

    SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat inFormatdate = new SimpleDateFormat("yyyyMMdd");

    //DecimalFormat decimalFormat = new DecimalFormat("#.##");
    //NumberFormat indianFormat = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));


    Calendar calendar;
    String startdate, enddate, orgname, orgcode, companycode, path, sdt, edt, sessionsdate = "", sessionedate = "";
    File myPath;
    String subject, filename, imagesUri, pdfheadertext, headertext, companyname, address;
    UserSessionManager session;
    ArrayList<Business> receiptlist0, receiptlist1, paymentlist0, paymentlist1, cashlist, banklist;

    DatabaseHelper databaseHelper;
    Button btnsync, btnshow;
    Bitmap bitmap;
    ImageView imgdown;
    int nCounter = 0;
    private Handler mHandler = new Handler();
    ImageButton btnexcel, btnpdf;


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_view);


        session = new UserSessionManager(BusinessViewActivity.this);
        databaseHelper = new DatabaseHelper(BusinessViewActivity.this);
        orgcode = session.getData(UserSessionManager.KEY_ORG_REGKEY);
        orgname = session.getData(UserSessionManager.KEY_ORG_NAME);
        companycode = session.getData(orgcode + UserSessionManager.KEY_COM_CODE);
        companyname = session.getData(orgcode + UserSessionManager.KEY_COM_NAME);
        address = session.getData(orgcode + UserSessionManager.KEY_COM_ADDRESS);
        subject = Constant.ERRORID + " | " + session.getData(UserSessionManager.KEY_Mobile) + " | " + orgname + " | " + companyname;

        Context context;
        try {
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);


            btnpdf = (ImageButton) findViewById(R.id.btnpdf);
            btnexcel = (ImageButton) findViewById(R.id.btnexcel);
            imgdown = (ImageView) findViewById(R.id.imgdown);
            btnsync = (Button) findViewById(R.id.btnsync);
            btnshow = (Button) findViewById(R.id.btnshow);
            spquick = (Spinner) findViewById(R.id.spquick);

            txtnodata = (TextView) findViewById(R.id.txtnodata);
            txtto = (TextView) findViewById(R.id.txtto);
            txttodt = (TextView) findViewById(R.id.txttodt);
            txtfrom = (TextView) findViewById(R.id.txtfrom);
            txtsec = (TextView) findViewById(R.id.txtsec);
            datedisplay = (TextView) findViewById(R.id.datedisplay);
            txtfromdt = (TextView) findViewById(R.id.txtfromdt);
            progressbar = (ProgressBar) findViewById(R.id.progressbar);
            lytprogress = (LinearLayout) findViewById(R.id.lytprogress);
            lytfrom = (LinearLayout) findViewById(R.id.lytfrom);
            lytto = (LinearLayout) findViewById(R.id.lytto);
            lytmain = (LinearLayout) findViewById(R.id.lytmain);
            lytdatedisplay = (LinearLayout) findViewById(R.id.lytdatedisplay);
            lyttop = (LinearLayout) findViewById(R.id.lyttop);
            lytbottom = (LinearLayout) findViewById(R.id.lytbottom);
            lytstock = (LinearLayout) findViewById(R.id.lytstock);
            lytbank = (LinearLayout) findViewById(R.id.lytbank);
            lytcash = (LinearLayout) findViewById(R.id.lytcash);
            lytpayment = (LinearLayout) findViewById(R.id.lytpayment);
            lytreceipt = (LinearLayout) findViewById(R.id.lytreceipt);
            lytpurchase = (LinearLayout) findViewById(R.id.lytpurchase);
            lytsales = (LinearLayout) findViewById(R.id.lytsales);
            lytreceiptmain = (LinearLayout) findViewById(R.id.lytreceiptmain);
            lytpaymain = (LinearLayout) findViewById(R.id.lytpaymain);
            lytcashmain = (LinearLayout) findViewById(R.id.lytcashmain);
            lytbankmain = (LinearLayout) findViewById(R.id.lytbankmain);
            lytpdfexcel = (LinearLayout) findViewById(R.id.lytpdfexcel);

            receipt0_recycleview = (RecyclerView) findViewById(R.id.receipt0_recycleview);
            receipt0_recycleview.setLayoutManager(new LinearLayoutManager(BusinessViewActivity.this));
            receipt1_recycleview = (RecyclerView) findViewById(R.id.receipt1_recycleview);
            receipt1_recycleview.setLayoutManager(new LinearLayoutManager(BusinessViewActivity.this));
            payment0_recycleview = (RecyclerView) findViewById(R.id.payment0_recycleview);
            payment0_recycleview.setLayoutManager(new LinearLayoutManager(BusinessViewActivity.this));
            payment1_recycleview = (RecyclerView) findViewById(R.id.payment1_recycleview);
            payment1_recycleview.setLayoutManager(new LinearLayoutManager(BusinessViewActivity.this));
            cash_recycleview = (RecyclerView) findViewById(R.id.cash_recycleview);
            cash_recycleview.setLayoutManager(new LinearLayoutManager(BusinessViewActivity.this));
            bank_recycleview = (RecyclerView) findViewById(R.id.bank_recycleview);
            bank_recycleview.setLayoutManager(new LinearLayoutManager(BusinessViewActivity.this));

            sale4 = (TextView) findViewById(R.id.sale4);
            //sale3 = (TextView) findViewById(R.id.sale3);
            sale2 = (TextView) findViewById(R.id.sale2);
            sale1 = (TextView) findViewById(R.id.sale1);
            purchase4 = (TextView) findViewById(R.id.purchase4);
            //purchase3 = (TextView) findViewById(R.id.purchase3);
            purchase2 = (TextView) findViewById(R.id.purchase2);
            purchase1 = (TextView) findViewById(R.id.purchase1);
            receipt4 = (TextView) findViewById(R.id.receipt4);
            //receipt3 = (TextView) findViewById(R.id.receipt3);
            receipt2 = (TextView) findViewById(R.id.receipt2);
            receipt1 = (TextView) findViewById(R.id.receipt1);
            payment4 = (TextView) findViewById(R.id.payment4);
            //payment3 = (TextView) findViewById(R.id.payment3);
            payment2 = (TextView) findViewById(R.id.payment2);
            payment1 = (TextView) findViewById(R.id.payment1);
            cash2 = (TextView) findViewById(R.id.cash2);
            cash1 = (TextView) findViewById(R.id.cash1);
            bank2 = (TextView) findViewById(R.id.bank2);
            bank1 = (TextView) findViewById(R.id.bank1);
            stock2 = (TextView) findViewById(R.id.stock2);
            stock1 = (TextView) findViewById(R.id.stock1);

            String dateString, monthString, month_name, day_name, currentdate;
            SpannableString ss;


            try {
                //get session
                if (!session.getData("bs" + orgcode + companycode).equals("") && !session.getData("be" + orgcode + companycode).equals("")) {
                    startdate = session.getData("bs" + orgcode + companycode);
                    enddate = session.getData("be" + orgcode + companycode);

                    String fdate = startdate.substring(Math.max(startdate.length() - 2, 0));
                    String tdate = enddate.substring(Math.max(enddate.length() - 2, 0));
                    txtfromdt.setText(fdate);
                    txttodt.setText(tdate);

                    try {
                    /*Date date_end = inFormatdate.parse(enddate);
                    Date date_start = inFormatdate.parse(startdate);*/
                        Date date_end = Constant.FormateDate(inFormatdate, enddate, subject, databaseHelper);
                        Date date_start = Constant.FormateDate(inFormatdate, startdate, subject, databaseHelper);

                        Calendar scal = Calendar.getInstance();
                        scal.setTime(date_start);
                        Calendar ecal = Calendar.getInstance();
                        ecal.setTime(date_end);

                        //System.out.println("=========== = =cal " + scal.get(Calendar.MONTH) + " " + ecal.get(Calendar.MONTH));

                        //String fmonth_name = month_date.format(scal.getTime());
                        String fmonth_name = Constant.FormateDate(month_date, scal.getTime());
                        String fday_name = Constant.FormateDate(date_name, scal.getTime());
                        String fcurrentdate = "  " + fmonth_name + " " + scal.get(Calendar.YEAR) + "\n  " + fday_name;

                        String emonth_name = Constant.FormateDate(month_date, ecal.getTime());
                        String eday_name = Constant.FormateDate(date_name, ecal.getTime());
                        String ecurrentdate = "  " + emonth_name + " " + ecal.get(Calendar.YEAR) + "\n  " + eday_name;

                        ss = new SpannableString(fcurrentdate);
                        ss.setSpan(new ForegroundColorSpan(Color.BLACK), 0, fcurrentdate.indexOf(fday_name), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        txtfrom.setText(ss);

                        SpannableString sst = new SpannableString(ecurrentdate);
                        sst.setSpan(new ForegroundColorSpan(Color.BLACK), 0, ecurrentdate.indexOf(eday_name), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        txtto.setText(sst);
                    } catch (Exception e) {
                        e.printStackTrace();
                        databaseHelper.addExceptionERROR(e, subject);
                    }
                } else {

                    calendar = Calendar.getInstance();

                    /*dateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                    monthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                    if (monthString.length() == 1) {
                        monthString = "0" + monthString;
                    }
                    if (dateString.length() == 1) {
                        dateString = "0" + dateString;
                    }*/

                    String[] datestring = Constant.GetMonthDateString(calendar).split(";");
                    dateString = datestring[0];
                    monthString = datestring[1];

                    month_name = Constant.FormateDate(month_date, calendar.getTime());
                    day_name = Constant.FormateDate(date_name, calendar.getTime());

                    currentdate = "  " + month_name + " " + calendar.get(Calendar.YEAR) + "\n  " + day_name;

                    txtfromdt.setText(dateString);
                    txttodt.setText(dateString);

                    startdate = calendar.get(Calendar.YEAR) + monthString + dateString;
                    enddate = calendar.get(Calendar.YEAR) + monthString + dateString;

                    ss = new SpannableString(currentdate);
                    ss.setSpan(new ForegroundColorSpan(Color.BLACK), 0, currentdate.indexOf(day_name), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    txtfrom.setText(ss);
                    txtto.setText(ss);
                }
            } catch (Exception e) {
                e.printStackTrace();

                databaseHelper.addExceptionERROR(e, subject);
            }

            //click of from date
            lytfrom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Date date_start = Constant.FormateDate(inFormatdate, startdate, subject, databaseHelper);
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(date_start);
                        new DatePickerDialog(BusinessViewActivity.this, pickerListenerfrom, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        databaseHelper.addExceptionERROR(e, subject);
                    }
                }
            });

            //click of to date
            lytto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Date date_end = Constant.FormateDate(inFormatdate, enddate, subject, databaseHelper);
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(date_end);
                        new DatePickerDialog(BusinessViewActivity.this, pickerListenerto, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        databaseHelper.addExceptionERROR(e, subject);
                    }
                }
            });


            try {
                //get local data
                final String localdata = databaseHelper.getBusinessData(startdate, enddate, companycode, orgcode);
                if (!localdata.equals("")) {

                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            OnShowDataClick(lyttop.getRootView());
                        }
                    }, 150);

                } else if (AppController.isNetConnected(BusinessViewActivity.this)) {
                    SyncVisibility();
                    btnshow.setVisibility(View.GONE);
                    imgdown.setVisibility(View.GONE);
                } else {
                    //lyttop.setVisibility(View.GONE);
                    imgdown.setVisibility(View.GONE);
                    Toast.makeText(BusinessViewActivity.this, getResources().getString(R.string.checkinternet), Toast.LENGTH_SHORT).show();
                }

                //quick date selection option
                spquick.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        //btnsync.setVisibility(View.GONE);
                        String item = spquick.getSelectedItem().toString();
                        calendar = Calendar.getInstance();
                        String type = Constant.QuickSpinnerType(item);

                        if (!type.equals("")) {
                            String[] dates = Constant.SetDateData(calendar, type, txtfromdt, txttodt, txtfrom, txtto).split(";");
                            startdate = dates[0];
                            enddate = dates[1];
                        }

                        BtnSyncVisibility();
                        //System.out.println("====== = =" + startdate + " = " + enddate);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }

                });

                SetArrowToText();

                //redirect to salesreport page
                sale1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        session.setData(Constant.SalesReportSessionName + "typecode" + companycode + orgcode, "0");
                        session.setData(Constant.SalesReportSessionName + "sdate" + orgcode + companycode, startdate);
                        session.setData(Constant.SalesReportSessionName + "edate" + orgcode + companycode, enddate);
                        startActivity(new Intent(BusinessViewActivity.this, SalesReportActivity.class).putExtra("from", "business"));
                    }
                });

                //redirect to stockstatus page
                stock1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        session.editor.putString(Constant.STOCKSTATUSSESSIONNAME + "by" + companycode + orgcode, "0");
                        session.editor.putString(Constant.STOCKSTATUSSESSIONNAME + "date" + companycode + orgcode, enddate);
                        session.editor.putString(Constant.STOCKSTATUSSESSIONNAME + "valname" + companycode + orgcode, "");
                        session.editor.putString(Constant.STOCKSTATUSSESSIONNAME + "val" + companycode + orgcode, "0");
                        session.editor.putBoolean(Constant.STOCKSTATUSSESSIONNAME + "data" + companycode + orgcode, true);
                        session.setData(Constant.STOCKSTATUSSESSIONNAME + "reorder" + companycode + orgcode, "No");
                        session.editor.commit();

                        startActivity(new Intent(BusinessViewActivity.this, StockStatusActivity.class).putExtra("from", "business"));
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
                databaseHelper.addExceptionERROR(e, subject);
            }
        } catch (Exception e) {
            e.printStackTrace();

            databaseHelper.addExceptionERROR(e, subject);
        }
    }


    private void SetArrowToText() {
        for (int i = 1; i <= 12; i++) {
            ImageView imageView = (ImageView) findViewById(getResources().getIdentifier("img" + i, "id", this.getPackageName()));
            imageView.setImageResource(R.drawable.ic_arrow_down);
        }
    }


    public void BtnSyncVisibility() {
        String localdata = databaseHelper.getBusinessData(startdate, enddate, companycode, orgcode);
        if (localdata.equals("")) {
            //btnsync.setVisibility(View.VISIBLE);
            btnshow.setVisibility(View.GONE);
        } else {
            btnshow.setVisibility(View.VISIBLE);
            //btnsync.setVisibility(View.VISIBLE);
        }

        SyncVisibility();
    }

    public void SyncVisibility() {
        if (session.getData(UserSessionManager.KEY_DEMO).equalsIgnoreCase("Yes") && session.getIntData(UserSessionManager.KEY_DEMOREQCOUNT) > (session.getIntData(UserSessionManager.KEY_DEMOREQTOTAL)))
            btnsync.setVisibility(View.GONE);
        else
            btnsync.setVisibility(View.VISIBLE);
    }

    /*public void BtnEnableDisable(boolean isenable) {
        btnsync.setEnabled(isenable);
        btnshow.setEnabled(isenable);
    }*/

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }


    //stop progressbar
    private void StopHandlerProcess() {
        Constant.BtnEnableDisable(true, btnsync, btnshow);
        progressbar.setVisibility(View.GONE);
        txtsec.setVisibility(View.GONE);
        mHandler.removeCallbacks(hMyTimeTask);
    }

    //start progressbar
    private void StartHandlerProcess() {
        Constant.BtnEnableDisable(false, btnsync, btnshow);
        progressbar.setVisibility(View.VISIBLE);
        txtsec.setVisibility(View.VISIBLE);
        txtsec.setText("");
        nCounter = 0;
        mHandler.postDelayed(hMyTimeTask, 1000);
    }

    //handler to display second in progressbar
    private Runnable hMyTimeTask = new Runnable() {
        public void run() {
            nCounter++;
            txtsec.setText("" + nCounter);
            mHandler.postDelayed(hMyTimeTask, 1000);
        }
    };

    //set fetched data
    private void GetBusinessData(String response) {

        if (databaseHelper.getAllMasterData(companycode, orgcode).size() == 0) {
            StopHandlerProcess();
            Toast.makeText(BusinessViewActivity.this, getResources().getString(R.string.master_updatemsg), Toast.LENGTH_SHORT).show();
            return;
        }


        //System.out.println("============= = =  = =re " + response);
        try {
            JSONArray jsonArray = new JSONArray(response);

            receiptlist0 = new ArrayList<>();
            receiptlist1 = new ArrayList<>();
            paymentlist0 = new ArrayList<>();
            paymentlist1 = new ArrayList<>();
            cashlist = new ArrayList<>();
            banklist = new ArrayList<>();

            long days = 0;
            if (jsonArray.length() == 2) {
                txtnodata.setVisibility(View.VISIBLE);
                imgdown.setVisibility(View.GONE);
            } else {


                Date date_end = Constant.FormateDate(inFormatdate, enddate, subject, databaseHelper);
                Date date_start = Constant.FormateDate(inFormatdate, startdate, subject, databaseHelper);
                sdt = new SimpleDateFormat("dd-MM-yyyy", Locale.US).format(date_start);
                edt = new SimpleDateFormat("dd-MM-yyyy", Locale.US).format(date_end);

                String header = companyname + "\n" + sdt + " to " + edt;

                SpannableString ssdate = new SpannableString(header);
                ssdate.setSpan(new RelativeSizeSpan(1.3f), 0, companyname.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ssdate.setSpan(new StyleSpan(Typeface.BOLD), 0, companyname.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ssdate.setSpan(new UnderlineSpan(), header.indexOf(sdt), header.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                datedisplay.setText(ssdate);

                pdfheadertext = "Business View from " + sdt + " to " + edt;
                headertext = companyname + "\n" + address + "\n\nBusiness View from " + sdt + " to " + edt + "\n";
                filename = companyname + "-BusinessView-" + sdt + " to " + edt;
                days = TimeUnit.DAYS.convert((date_end.getTime() - date_start.getTime()), TimeUnit.MILLISECONDS);


                for (int i = 1; i < jsonArray.length() - 1; i++) {
                    JSONArray businessarray = jsonArray.getJSONArray(i);
                    String namedate = businessarray.getString(0);
                    String avgtext = null, masternameval = null;

                    if (namedate.equals("Sales") || namedate.equals("Purchase") || namedate.equals("Receipt") || namedate.equals("Payment")) {
                        if (days != 0) {
                            avgtext = "Day Avg : " + withSuffix(Math.round(Constant.ConvertToDouble(businessarray.getString(2)) / days));
                        } else {
                            avgtext = "Day Avg : " + withSuffix(Math.round(Constant.ConvertToDouble(businessarray.getString(2))));
                        }
                    } else {
                        masternameval = databaseHelper.getMasterValue(companycode, DatabaseHelper.MASTER_MASTERCODE, businessarray.getString(1), businessarray.getString(3), DatabaseHelper.MASTER_MASTERNAME, orgcode);

                    }


                    if (businessarray.getString(0).equals("Sales") && !businessarray.getString(2).equals("0")) {
                        lytsales.setVisibility(View.VISIBLE);
                        sale1.setText(Html.fromHtml("<u>" + businessarray.getString(1) + " bill</u>"));
                        //sale2.setText(indianFormat.format(new BigDecimal(Constant.ConvertToDouble(businessarray.getString(2)))));
                        sale2.setText(Constant.ConvertToIndianRupeeFormat(Constant.ConvertToDouble(businessarray.getString(2)), false, false, false));

                        //sale3.setText("    ");
                        //sale4.setText("Day Avg : " + indianFormat.format(new BigDecimal(decimalFormat.format(Double.parseDouble(businessarray.getString(2)) / days))));
                        //System.out.println("============== = = = = == " + businessarray.getString(2) + " = " + days + " == " + Math.round(Double.parseDouble(businessarray.getString(2)) / days));
                        sale4.setText(avgtext);
                        //sale4.setText("Day Avg : " + withSuffix(Math.round(Double.parseDouble(businessarray.getString(2)) / days)));
                    } else if (businessarray.getString(0).equals("Purchase") && !businessarray.getString(2).equals("0")) {
                        lytpurchase.setVisibility(View.VISIBLE);
                        purchase1.setText(businessarray.getString(1) + " bill");
                        purchase2.setText(Constant.ConvertToIndianRupeeFormat(Constant.ConvertToDouble(businessarray.getString(2)), false, false, false));
                        //purchase3.setText("    ");
                        purchase4.setText(avgtext);

                    } else if (businessarray.getString(0).equals("Receipt") && !businessarray.getString(2).equals("0")) {
                        lytreceipt.setVisibility(View.VISIBLE);
                        receipt1.setText(businessarray.getString(1) + " vch");
                        receipt2.setText(Constant.ConvertToIndianRupeeFormat(Constant.ConvertToDouble(businessarray.getString(2)), false, false, false));
                        //receipt3.setText("    ");
                        receipt4.setText(avgtext);
                    } else if (businessarray.getString(0).equals("Payment") && !businessarray.getString(2).equals("0")) {
                        lytpayment.setVisibility(View.VISIBLE);
                        payment1.setText(businessarray.getString(1) + " vch");
                        payment2.setText(Constant.ConvertToIndianRupeeFormat(Constant.ConvertToDouble(businessarray.getString(2)), false, false, false));
                        //payment3.setText("    ");
                        payment4.setText(avgtext);
                    } else if (businessarray.getString(0).equals("cashbal") && !businessarray.getString(1).equals("0")) {
                        lytcash.setVisibility(View.VISIBLE);
                        cash1.setText(businessarray.getString(2) + " Ledgers");

                        String data = businessarray.getString(1);
                        if (data != null && !data.equals("") && data.matches("[0-9.]+")) {
                            cash2.setText(Constant.ConvertToIndianRupeeFormat(Constant.ConvertToDouble(data), true, false, false));
                        } else {
                            String lasttwo = businessarray.getString(1).substring(businessarray.getString(1).length() - 2).trim();
                            double main = Constant.ConvertToDouble(businessarray.getString(1).replaceAll(lasttwo, "").trim());
                            cash2.setText(Constant.ConvertToIndianRupeeFormat(main, true, false, false) + " " + lasttwo);
                        }
                    } else if (businessarray.getString(0).equals("bankbal") && !businessarray.getString(1).equals("0")) {
                        lytbank.setVisibility(View.VISIBLE);
                        bank1.setText(businessarray.getString(2) + " Ledgers");

                        String data = businessarray.getString(1);
                        if (data != null && !data.equals("") && data.matches("[0-9.]+")) {
                            bank2.setText(Constant.ConvertToIndianRupeeFormat(Constant.ConvertToDouble(data), true, false, false));
                        } else {
                            String lasttwo = businessarray.getString(1).substring(businessarray.getString(1).length() - 2).trim();
                            double main = Constant.ConvertToDouble(businessarray.getString(1).replaceAll(lasttwo, "").trim());
                            bank2.setText(Constant.ConvertToIndianRupeeFormat(main, true, false, false) + " " + lasttwo);
                        }
                    } else if (businessarray.getString(0).equals("stockvalue") && !businessarray.getString(1).equals("0")) {
                        lytstock.setVisibility(View.VISIBLE);
                        stock1.setText(Html.fromHtml("<u>" + businessarray.getString(2) + " item" + "</u>"));

                        String data = businessarray.getString(1);
                        if (data != null && !data.equals("") && data.matches("[0-9.]+")) {
                            stock2.setText(Constant.ConvertToIndianRupeeFormat(Constant.ConvertToDouble(data), true, false, false));
                        } else {
                            String lasttwo = businessarray.getString(1).substring(businessarray.getString(1).length() - 2).trim();
                            double main = Constant.ConvertToDouble(businessarray.getString(1).replaceAll(lasttwo, "").trim());
                            stock2.setText(Constant.ConvertToIndianRupeeFormat(main, true, false, false) + " " + lasttwo);
                        }
                    } else {
                        String data = businessarray.getString(2).trim();
                        String amount;
                        if (data != null && !data.equals("") && data.matches("[0-9.]+")) {
                            amount = Constant.ConvertToIndianRupeeFormat(Constant.ConvertToDouble(businessarray.getString(2)), false, true, false);
                        } else {
                            //holder.txtamt.setText(data);
                            String lasttwo = data.substring(data.length() - 2).trim();
                            double main = Constant.ConvertToDouble(data.replaceAll(lasttwo, "").trim());
                            amount = (Constant.ConvertToIndianRupeeFormat(main, true, false, false) + " " + lasttwo).replaceAll("₹", "").trim();
                        }
                        if (businessarray.getString(0).equalsIgnoreCase("Payment01")) {
                            //paymentlist0.add(new Business(businessarray.getString(0), businessarray.getString(1), businessarray.getString(2), businessarray.getString(3), databaseHelper.getMasterValue(companycode, DatabaseHelper.MASTER_MASTERCODE, businessarray.getString(1), businessarray.getString(3), DatabaseHelper.MASTER_MASTERNAME)));
                            paymentlist0.add(new Business(businessarray.getString(0), businessarray.getString(1), amount, businessarray.getString(3), masternameval, data));
                        } else if (businessarray.getString(0).equalsIgnoreCase("Payment02")) {
                            paymentlist1.add(new Business(businessarray.getString(0), businessarray.getString(1), amount, businessarray.getString(3), masternameval, data));
                        } else if (businessarray.getString(0).equalsIgnoreCase("Receipt01")) {
                            receiptlist0.add(new Business(businessarray.getString(0), businessarray.getString(1), amount, businessarray.getString(3), masternameval, data));
                        } else if (businessarray.getString(0).equalsIgnoreCase("Receipt02")) {
                            receiptlist1.add(new Business(businessarray.getString(0), businessarray.getString(1), amount, businessarray.getString(3), masternameval, data));
                        } else if (businessarray.getString(0).equalsIgnoreCase("cashbal_dtl")) {
                            cashlist.add(new Business(businessarray.getString(0), businessarray.getString(1), amount, businessarray.getString(3), masternameval, data));
                        } else if (businessarray.getString(0).equalsIgnoreCase("bankbal_dtl")) {
                            banklist.add(new Business(businessarray.getString(0), businessarray.getString(1), amount, businessarray.getString(3), masternameval, data));
                        }
                    }
                }

                SetArrowToText();
                Constant.SetViewVisiblility(imgdown, lyttop);
                BtnSyncVisibility();
                StopHandlerProcess();
                txtnodata.setVisibility(View.GONE);
                lytbottom.setVisibility(View.VISIBLE);
                lytdatedisplay.setVisibility(View.VISIBLE);
                lytmain.setVisibility(View.VISIBLE);
                lytpdfexcel.setVisibility(View.VISIBLE);


                receipt0_recycleview.setAdapter(new BusinessAdapter(receiptlist0, BusinessViewActivity.this, "receipt"));
                receipt1_recycleview.setAdapter(new BusinessAdapter(receiptlist1, BusinessViewActivity.this, "receipt"));
                payment0_recycleview.setAdapter(new BusinessAdapter(paymentlist0, BusinessViewActivity.this, "payment"));
                payment1_recycleview.setAdapter(new BusinessAdapter(paymentlist1, BusinessViewActivity.this, "payment"));
                cash_recycleview.setAdapter(new BusinessAdapter(cashlist, BusinessViewActivity.this, "cash"));
                bank_recycleview.setAdapter(new BusinessAdapter(banklist, BusinessViewActivity.this, "bank"));

                for (int j = 1; j <= 12; j++) {
                    ImageView imageView = (ImageView) findViewById(getResources().getIdentifier("img" + j, "id", this.getPackageName()));
                    imageView.setTag("1");
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
            StopHandlerProcess();
            databaseHelper.addJSONERROR(e, subject);

        } catch (ArithmeticException e) {
            e.printStackTrace();
            StopHandlerProcess();
            databaseHelper.addArithmeticERROR(e, subject);

        } catch (NumberFormatException e) {
            e.printStackTrace();
            StopHandlerProcess();
            databaseHelper.addNumberERROR(e, subject);

        } catch (Exception e) {
            e.printStackTrace();
            StopHandlerProcess();
            databaseHelper.addExceptionERROR(e, subject);
        }


        if (lytprogress.getVisibility() == View.VISIBLE)
            lytprogress.setVisibility(View.GONE);
    }


    private void DataViewGone() {
        lytbottom.setVisibility(View.GONE);
        lytdatedisplay.setVisibility(View.GONE);
        lytpdfexcel.setVisibility(View.GONE);
        lytmain.setVisibility(View.GONE);
    }

    public static String withSuffix(long count) {

        int exp;

        if (count >= 1000000000) {
            exp = (int) (Math.log(count) / Math.log(1000000000));
            return String.format("%.2f %s", count / Math.pow(1000000000, exp), "A");
        } else if (count >= 10000000) {
            exp = (int) (Math.log(count) / Math.log(10000000));
            return String.format("%.2f %s", count / Math.pow(10000000, exp), "C");
        } else if (count >= 100000) {
            exp = (int) (Math.log(count) / Math.log(100000));
            return String.format("%.2f %s", count / Math.pow(100000, exp), "L");
        } else if (count >= 1000) {
            exp = (int) (Math.log(count) / Math.log(1000));
            return String.format("%.2f %s", count / Math.pow(1000, exp), "K");
        } else {
            return "" + count;
        }

    }


    //pdf button click
    public void OnPdfClick(View view) {

        if (Constant.CheckReadWritePermissionGranted(BusinessViewActivity.this)) {
            lytprogress.setVisibility(View.VISIBLE);
            btnexcel.setEnabled(false);
            btnpdf.setEnabled(false);
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {


                    File filepath = Constant.FilePathPdfExcel(true, ".pdf", BusinessViewActivity.this, filename);

                    UserSessionManager.CreateBusinessViewPdf(filename, address, companyname, pdfheadertext, lytsales.getVisibility() == View.VISIBLE, sale1.getText().toString().trim(), sale2.getText().toString().trim(), sale4.getText().toString().trim(), lytpurchase.getVisibility() == View.VISIBLE, purchase1.getText().toString().trim(), purchase2.getText().toString().trim(), purchase4.getText().toString().trim(), lytreceipt.getVisibility() == View.VISIBLE, lytreceiptmain.getVisibility() == View.VISIBLE, receipt1.getText().toString().trim(), receipt2.getText().toString().trim(), receipt4.getText().toString().trim(), receiptlist0, receiptlist1, lytpayment.getVisibility() == View.VISIBLE, lytpaymain.getVisibility() == View.VISIBLE, payment1.getText().toString().trim(), payment2.getText().toString().trim(), payment4.getText().toString().trim(), paymentlist0, paymentlist1, lytcash.getVisibility() == View.VISIBLE, lytcashmain.getVisibility() == View.VISIBLE, cash1.getText().toString().trim(), cash2.getText().toString().trim(), cashlist, lytbank.getVisibility() == View.VISIBLE, lytbankmain.getVisibility() == View.VISIBLE, bank1.getText().toString().trim(), bank2.getText().toString().trim(), banklist, lytstock.getVisibility() == View.VISIBLE, stock1.getText().toString().trim(), stock2.getText().toString().trim());

                    openPdfExcel(filepath, "pdf");
                }
            }, 300);
        }
    }

    //excel button click
    public void OnExcelClick(View view) {

        if (Constant.CheckReadWritePermissionGranted(BusinessViewActivity.this)) {
            lytprogress.setVisibility(View.VISIBLE);
            btnexcel.setEnabled(false);
            btnpdf.setEnabled(false);
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    writeStudentsListToExcel();
                }
            }, 300);
        }

    }

    //selection of from date
    private DatePickerDialog.OnDateSetListener pickerListenerfrom = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            /*String monthString = String.valueOf(selectedMonth + 1);
            String dateString = String.valueOf(selectedDay);
            if (monthString.length() == 1) {
                monthString = "0" + monthString;
            }
            if (dateString.length() == 1) {
                dateString = "0" + dateString;
            }*/
            String[] datestring = Constant.GetMonthDateOfDatePicker(selectedMonth,selectedDay).split(";");
            String dateString = datestring[0];
            String monthString = datestring[1];

            try {
                Date myDate = Constant.FormateDate(inFormat, selectedYear + "-" + monthString + "-" + dateString, subject, databaseHelper);

                String month_name = Constant.FormateDate(month_date, myDate);
                String day_name = Constant.FormateDate(date_name, myDate);

                String currentdate = "  " + month_name + " " + selectedYear + "\n  " + day_name;

                txtfromdt.setText(dateString);
                //txttodt.setText(dateString);

                final SpannableString ss = new SpannableString(currentdate);
                ss.setSpan(new ForegroundColorSpan(Color.BLACK), 0, currentdate.indexOf(day_name), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                txtfrom.setText(ss);
                startdate = selectedYear + monthString + dateString;
                //btnsync.setVisibility(View.GONE);
                BtnSyncVisibility();
            } catch (Exception e) {
                e.printStackTrace();
                databaseHelper.addExceptionERROR(e, subject);
            }
            /*if (AppController.isConnected(BusinessViewActivity.this)) {
                //GetData();
            }*/
        }
    };

    //selection of to date
    private DatePickerDialog.OnDateSetListener pickerListenerto = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            /*String monthString = String.valueOf(selectedMonth + 1);
            String dateString = String.valueOf(selectedDay);
            if (monthString.length() == 1) {
                monthString = "0" + monthString;
            }
            if (dateString.length() == 1) {
                dateString = "0" + dateString;
            }*/
            String[] datestring = Constant.GetMonthDateOfDatePicker(selectedMonth,selectedDay).split(";");
            String dateString = datestring[0];
            String monthString = datestring[1];


            try {
                Date myDate = Constant.FormateDate(inFormat, selectedYear + "-" + monthString + "-" + dateString, subject, databaseHelper);

                String month_name = Constant.FormateDate(month_date, myDate);
                String day_name = Constant.FormateDate(date_name, myDate);

                String currentdate = "  " + month_name + " " + selectedYear + "\n  " + day_name;

                //txtfromdt.setText(dateString);
                txttodt.setText(dateString);

                final SpannableString ss = new SpannableString(currentdate);
                ss.setSpan(new ForegroundColorSpan(Color.BLACK), 0, currentdate.indexOf(day_name), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                txtto.setText(ss);
                enddate = selectedYear + monthString + dateString;
                //btnsync.setVisibility(View.GONE);
                BtnSyncVisibility();
            } catch (Exception e) {
                e.printStackTrace();

                databaseHelper.addExceptionERROR(e, subject);
            }

           /* if (AppController.isConnected(BusinessViewActivity.this)) {
                //GetData();
            }*/
        }
    };


    //Generate Excel file
    public void writeStudentsListToExcel() {

        try {

            File FILE_PATH = Constant.FilePathPdfExcel(true, ".xlsx", BusinessViewActivity.this, filename);

            XSSFWorkbook workbook = new XSSFWorkbook();

            XSSFSheet studentsSheet = workbook.createSheet("Business");
            studentsSheet.getPrintSetup().setPaperSize(PrintSetup.A4_PAPERSIZE);
            studentsSheet.setHorizontallyCenter(true);

            Footer footer = studentsSheet.getFooter();
            footer.setRight("Page " + HeaderFooter.page() + " of " + HeaderFooter.numPages());

            studentsSheet.setColumnWidth(0, (15 * 300));
            studentsSheet.setColumnWidth(1, (15 * 300));
            studentsSheet.setColumnWidth(2, (15 * 300));
            studentsSheet.setColumnWidth(3, (15 * 300));

            int rowIndex = 0, nextrow = 0;
            //Row row = studentsSheet.createRow(rowIndex++);
            int cellIndex = 0;
            CellStyle style = workbook.createCellStyle();

            style.setBorderBottom(CellStyle.BORDER_THIN);
            style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            style.setBorderLeft(CellStyle.BORDER_THIN);
            style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            style.setBorderRight(CellStyle.BORDER_THIN);
            style.setRightBorderColor(IndexedColors.BLACK.getIndex());
            style.setBorderTop(CellStyle.BORDER_THIN);
            style.setTopBorderColor(IndexedColors.BLACK.getIndex());

            CellStyle borderStyle = workbook.createCellStyle();
            borderStyle.setBorderBottom(CellStyle.BORDER_THIN);
            borderStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            borderStyle.setBorderLeft(CellStyle.BORDER_THIN);
            borderStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            borderStyle.setBorderRight(CellStyle.BORDER_THIN);
            borderStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            borderStyle.setBorderTop(CellStyle.BORDER_THIN);
            borderStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
            ((XSSFCellStyle) borderStyle).setAlignment(HorizontalAlignment.CENTER);

            XSSFFont font = workbook.createFont();
            font.setBold(true);
            style.setFont(font);
            ((XSSFCellStyle) style).setAlignment(HorizontalAlignment.CENTER);

            //row.createCell(cellIndex);
            CellStyle cs = workbook.createCellStyle();
            cs.setFont(font);
            ((XSSFCellStyle) cs).setAlignment(HorizontalAlignment.CENTER);
            ((XSSFCellStyle) cs).setVerticalAlignment(VerticalAlignment.CENTER);
            cs.setWrapText(true);
            //row.getCell(cellIndex).setCellStyle(cs);


            XSSFFont comFont = workbook.createFont();
            comFont.setFontHeightInPoints((short) 20);
            CellStyle cellstyle = workbook.createCellStyle();
            cellstyle.setFont(comFont);
            ((XSSFCellStyle) cellstyle).setAlignment(HorizontalAlignment.CENTER);
            //richString.applyFont(headertext.indexOf(companyname), companyname.length() + 1, comFont);

            Row row = studentsSheet.createRow(rowIndex);
            row.createCell(cellIndex).setCellValue(companyname);
            row.getCell(cellIndex).setCellStyle(cellstyle);
            studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 3));

            rowIndex++;
            cellIndex = 0;

            Row addrow = studentsSheet.createRow(rowIndex);
            addrow.createCell(cellIndex).setCellValue(address);
            addrow.getCell(cellIndex).setCellStyle(cs);
        /*int sindext = rowIndex;

        Row addrow1 = studentsSheet.createRow(rowIndex++);
        Row addrow2 = studentsSheet.createRow(rowIndex++);
        Row addrow3 = studentsSheet.createRow(rowIndex);*/
            studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex + 3, 0, 3));

            rowIndex = rowIndex + 4;
            //rowIndex++;
            cellIndex = 0;

            Drawing drawing = studentsSheet.createDrawingPatriarch();
            CreationHelper creationHelper = workbook.getCreationHelper();
            ClientAnchor anchor = creationHelper.createClientAnchor();
            anchor.setDx1(0);
            anchor.setCol1(1);
            anchor.setRow1(rowIndex);
            anchor.setRow2(rowIndex);
            anchor.setCol2(3);
            XSSFDrawing xssfdrawing = (XSSFDrawing) drawing;
            XSSFClientAnchor xssfanchor = (XSSFClientAnchor) anchor;
            XSSFSimpleShape xssfshape = xssfdrawing.createSimpleShape(xssfanchor);
            xssfshape.setShapeType(ShapeTypes.LINE);
            xssfshape.setLineWidth(1);
            xssfshape.setLineStyle(0);
            xssfshape.setLineStyleColor(0, 0, 0);

            Row daterow = studentsSheet.createRow(rowIndex);
            daterow.createCell(cellIndex).setCellValue("Business View from " + sdt + " to " + edt);
            daterow.getCell(cellIndex).setCellStyle(cs);

            studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 3));

            rowIndex++;
            Row blankrow = studentsSheet.createRow(rowIndex);
            rowIndex++;
            Row blankrow1 = studentsSheet.createRow(rowIndex);

            studentsSheet.setRepeatingRows(CellRangeAddress.valueOf("1:" + rowIndex));

            rowIndex++;

            //rowIndex = rowIndex + 5;
            nextrow = rowIndex;
            if (lytsales.getVisibility() == View.VISIBLE) {

                cellIndex = 0;
                Row srow = studentsSheet.createRow(rowIndex);
                srow.createCell(cellIndex).setCellValue(getResources().getString(R.string.sales_figure));
                srow.getCell(cellIndex).setCellStyle(style);
                srow.createCell(1).setCellStyle(borderStyle);
                srow.createCell(2).setCellStyle(borderStyle);
                srow.createCell(3).setCellStyle(borderStyle);
                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 3));
                rowIndex++;


                cellIndex = 0;
                Row sdatarow = studentsSheet.createRow(rowIndex);
                sdatarow.createCell(cellIndex++).setCellValue(sale1.getText().toString());
                sdatarow.createCell(cellIndex++).setCellValue(sale2.getText().toString());
                //sdatarow.createCell(cellIndex++).setCellValue(sale3.getText().toString());
                sdatarow.createCell(cellIndex).setCellValue(sale4.getText().toString());
                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 2, 3));
                sdatarow.getCell(0).setCellStyle(borderStyle);
                sdatarow.getCell(1).setCellStyle(borderStyle);
                sdatarow.getCell(2).setCellStyle(borderStyle);
                sdatarow.createCell(3).setCellStyle(borderStyle);
                rowIndex++;
                nextrow = rowIndex;
            }

            if (lytpurchase.getVisibility() == View.VISIBLE) {
                cellIndex = 0;
                Row prow = studentsSheet.createRow(rowIndex);
                prow.createCell(cellIndex).setCellValue(getResources().getString(R.string.purchase_figure));
                prow.getCell(cellIndex).setCellStyle(style);
                prow.createCell(1).setCellStyle(borderStyle);
                prow.createCell(2).setCellStyle(borderStyle);
                prow.createCell(3).setCellStyle(borderStyle);
                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 3));
                rowIndex++;

                cellIndex = 0;
                Row pdatarow = studentsSheet.createRow(rowIndex);
                pdatarow.createCell(cellIndex++).setCellValue(purchase1.getText().toString());
                pdatarow.createCell(cellIndex++).setCellValue(purchase2.getText().toString());
                //pdatarow.createCell(cellIndex++).setCellValue(purchase3.getText().toString());
                pdatarow.createCell(cellIndex).setCellValue(purchase4.getText().toString());
                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 2, 3));
                pdatarow.getCell(0).setCellStyle(borderStyle);
                pdatarow.getCell(1).setCellStyle(borderStyle);
                pdatarow.getCell(2).setCellStyle(borderStyle);
                pdatarow.createCell(3).setCellStyle(borderStyle);
                rowIndex++;
                nextrow = rowIndex;
            }

            int receiptlistindex = 0, receiptlistindex1 = 0, recerowheader = rowIndex;
            if (lytreceipt.getVisibility() == View.VISIBLE) {
                cellIndex = 0;
                Row rrow = studentsSheet.createRow(rowIndex);
                rrow.createCell(cellIndex).setCellValue(getResources().getString(R.string.receipt_figure));
                rrow.getCell(cellIndex).setCellStyle(style);
                rrow.createCell(1).setCellStyle(borderStyle);
                rrow.createCell(2).setCellStyle(borderStyle);
                rrow.createCell(3).setCellStyle(borderStyle);
                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 3));
                rowIndex++;

                cellIndex = 0;
                Row rdatarow = studentsSheet.createRow(rowIndex);
                rdatarow.createCell(cellIndex++).setCellValue(receipt1.getText().toString());
                rdatarow.createCell(cellIndex++).setCellValue(receipt2.getText().toString());
                //rdatarow.createCell(cellIndex++).setCellValue(receipt3.getText().toString());
                rdatarow.createCell(cellIndex).setCellValue(receipt4.getText().toString());
                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 2, 3));
                rdatarow.getCell(0).setCellStyle(borderStyle);
                rdatarow.getCell(1).setCellStyle(borderStyle);
                rdatarow.getCell(2).setCellStyle(borderStyle);
                rdatarow.createCell(3).setCellStyle(borderStyle);
                rowIndex++;
                recerowheader = rowIndex;
                nextrow = recerowheader;
                cellIndex = 0;

                if (lytreceiptmain.getVisibility() == View.VISIBLE) {
                    Row rerow = studentsSheet.createRow(recerowheader);
                    rerow.createCell(cellIndex).setCellValue(getResources().getString(R.string.customer_party));
                    rerow.getCell(cellIndex).setCellStyle(style);
                    rerow.createCell(1).setCellStyle(borderStyle);
                    studentsSheet.addMergedRegion(new CellRangeAddress(recerowheader, recerowheader, 0, 1));

                    cellIndex = cellIndex + 2;
                    rerow.createCell(cellIndex).setCellValue(getResources().getString(R.string.cash_bank));
                    rerow.getCell(cellIndex).setCellStyle(style);
                    rerow.createCell(3).setCellStyle(borderStyle);
                    studentsSheet.addMergedRegion(new CellRangeAddress(recerowheader, recerowheader, 2, 3));


                    receiptlistindex = recerowheader + 1;
                    for (Business model : receiptlist0) {
                        cellIndex = 0;
                        Row recrow = studentsSheet.createRow(receiptlistindex++);
                /*String data = model.getDtl2().trim();
                if (data != null && !data.equals("") && data.matches("[0-9.]+")) {
                    data = indianFormat.format(new BigDecimal(Double.parseDouble(model.getDtl2()))).replaceAll("₹", "").trim();
                }*/
                        recrow.createCell(cellIndex++).setCellValue(model.getDtl2());
                        recrow.createCell(cellIndex).setCellValue(model.getNamedisplay());
                        recrow.getCell(0).setCellStyle(borderStyle);
                        recrow.getCell(1).setCellStyle(borderStyle);
                    }


                    receiptlistindex1 = recerowheader + 1;
                    for (Business model : receiptlist1) {
                        cellIndex = 2;

                        Row recrow = studentsSheet.getRow(receiptlistindex1);
                        if (recrow == null)
                            recrow = studentsSheet.createRow(receiptlistindex1);

                        receiptlistindex1++;

                /*String data = model.getDtl2().trim();
                if (data != null && !data.equals("") && data.matches("[0-9.]+")) {
                    data = indianFormat.format(new BigDecimal(Double.parseDouble(model.getDtl2()))).replaceAll("₹", "").trim();
                }*/
                        recrow.createCell(cellIndex++).setCellValue(model.getDtl2());
                        recrow.createCell(cellIndex).setCellValue(model.getNamedisplay());

                        recrow.getCell(2).setCellStyle(borderStyle);
                        recrow.getCell(3).setCellStyle(borderStyle);
                    }

                    if (receiptlistindex > receiptlistindex1) {
                        nextrow = receiptlistindex;

                        //System.out.println("=  = = = = = " + receiptlistindex + " = " + receiptlistindex1);
                        for (int i = receiptlistindex1; i <= receiptlistindex; i++) {
                            Row recrow = studentsSheet.getRow(i);
                            if (recrow == null)
                                recrow = studentsSheet.createRow(i);

                            cellIndex = 2;
                            recrow.createCell(cellIndex++).setCellStyle(borderStyle);
                            recrow.createCell(cellIndex).setCellStyle(borderStyle);

                        }
                    } else {
                        nextrow = receiptlistindex1;

                        for (int i = receiptlistindex; i <= receiptlistindex1; i++) {
                            Row recrow = studentsSheet.getRow(i);
                            if (recrow == null)
                                recrow = studentsSheet.createRow(i);

                            cellIndex = 0;
                            recrow.createCell(cellIndex++).setCellStyle(borderStyle);
                            recrow.createCell(cellIndex).setCellStyle(borderStyle);
                        }
                    }
                }
            }

            //
            int paylistindex1 = 0, paylistindex = 0;
            if (lytpayment.getVisibility() == View.VISIBLE) {
                cellIndex = 0;
                int paind = nextrow;
            /*if (receiptlistindex > receiptlistindex1)
                paind = receiptlistindex;
            else
                paind = receiptlistindex1;*/

                Row parow = studentsSheet.createRow(paind);
                parow.createCell(cellIndex).setCellValue(getResources().getString(R.string.payment_figure));
                parow.getCell(cellIndex).setCellStyle(style);
                parow.createCell(1).setCellStyle(borderStyle);
                parow.createCell(2).setCellStyle(borderStyle);
                parow.createCell(3).setCellStyle(borderStyle);
                studentsSheet.addMergedRegion(new CellRangeAddress(paind, paind, 0, 3));

                paind++;
                cellIndex = 0;
                Row padatarow = studentsSheet.createRow(paind);
                padatarow.createCell(cellIndex++).setCellValue(payment1.getText().toString());
                padatarow.createCell(cellIndex++).setCellValue(payment2.getText().toString());
                //padatarow.createCell(cellIndex++).setCellValue(payment3.getText().toString());
                padatarow.createCell(cellIndex).setCellValue(payment4.getText().toString());
                studentsSheet.addMergedRegion(new CellRangeAddress(paind, paind, 2, 3));
                padatarow.getCell(0).setCellStyle(borderStyle);
                padatarow.getCell(1).setCellStyle(borderStyle);
                padatarow.getCell(2).setCellStyle(borderStyle);
                padatarow.createCell(3).setCellStyle(borderStyle);
                paind++;

                int payrowheader = paind;
                nextrow = payrowheader;
                cellIndex = 0;

                if (lytpaymain.getVisibility() == View.VISIBLE) {
                    Row pasubrow = studentsSheet.createRow(payrowheader);
                    pasubrow.createCell(cellIndex).setCellValue(getResources().getString(R.string.payment_a_c));
                    pasubrow.getCell(cellIndex).setCellStyle(style);
                    studentsSheet.addMergedRegion(new CellRangeAddress(payrowheader, payrowheader, 0, 1));

                    cellIndex = cellIndex + 2;
                    pasubrow.createCell(cellIndex).setCellValue(getResources().getString(R.string.cash_bank));
                    pasubrow.getCell(cellIndex).setCellStyle(style);
                    studentsSheet.addMergedRegion(new CellRangeAddress(payrowheader, payrowheader, 2, 3));

                    paylistindex = payrowheader + 1;
                    for (Business model : paymentlist0) {
                        cellIndex = 0;
                        Row recrow = studentsSheet.createRow(paylistindex++);
               /* String data = model.getDtl2().trim();
                if (data != null && !data.equals("") && data.matches("[0-9.]+")) {
                    data = indianFormat.format(new BigDecimal(Double.parseDouble(model.getDtl2()))).replaceAll("₹", "").trim();
                }*/
                        recrow.createCell(cellIndex++).setCellValue(model.getDtl2());
                        recrow.createCell(cellIndex).setCellValue(model.getNamedisplay());

                        recrow.getCell(0).setCellStyle(borderStyle);
                        recrow.getCell(1).setCellStyle(borderStyle);
                    }


                    paylistindex1 = payrowheader + 1;
                    for (Business model : paymentlist1) {
                        cellIndex = 2;

                        Row recrow = studentsSheet.getRow(paylistindex1);
                        if (recrow == null)
                            recrow = studentsSheet.createRow(paylistindex1);

                        paylistindex1++;

                /*String data = model.getDtl2().trim();
                if (data != null && !data.equals("") && data.matches("[0-9.]+")) {
                    data = indianFormat.format(new BigDecimal(Double.parseDouble(model.getDtl2()))).replaceAll("₹", "").trim();
                }*/
                        recrow.createCell(cellIndex++).setCellValue(model.getDtl2());
                        recrow.createCell(cellIndex).setCellValue(model.getNamedisplay());

                        recrow.getCell(2).setCellStyle(borderStyle);
                        recrow.getCell(3).setCellStyle(borderStyle);
                    }

                    if (paylistindex > paylistindex1) {
                        nextrow = paylistindex;

                        for (int i = paylistindex1; i <= paylistindex; i++) {
                            Row recrow = studentsSheet.getRow(i);
                            if (recrow == null)
                                recrow = studentsSheet.createRow(i);

                            cellIndex = 2;
                            recrow.createCell(cellIndex++).setCellStyle(borderStyle);
                            recrow.createCell(cellIndex).setCellStyle(borderStyle);

                        }

                    } else {
                        nextrow = paylistindex1;

                        for (int i = paylistindex; i <= paylistindex1; i++) {
                            Row recrow = studentsSheet.getRow(i);
                            if (recrow == null)
                                recrow = studentsSheet.createRow(i);

                            cellIndex = 0;
                            recrow.createCell(cellIndex++).setCellStyle(borderStyle);
                            recrow.createCell(cellIndex).setCellStyle(borderStyle);

                        }
                    }
                }
            }

            //
            int cashlistindex = 0;
            if (lytcash.getVisibility() == View.VISIBLE) {
                cellIndex = 0;
                int caind = nextrow;
            /*if (paylistindex > paylistindex1)
                caind = paylistindex;
            else
                caind = paylistindex1;*/

                Row carow = studentsSheet.createRow(caind);
                carow.createCell(cellIndex).setCellValue(getResources().getString(R.string.cash_balance));
                carow.getCell(cellIndex).setCellStyle(style);
                carow.createCell(1).setCellStyle(borderStyle);
                carow.createCell(2).setCellStyle(borderStyle);
                carow.createCell(3).setCellStyle(borderStyle);
                studentsSheet.addMergedRegion(new CellRangeAddress(caind, caind, 0, 3));

                caind++;
                cellIndex = 0;
                Row cadatarow = studentsSheet.createRow(caind);
                cadatarow.createCell(cellIndex).setCellValue(cash1.getText().toString());
                studentsSheet.addMergedRegion(new CellRangeAddress(caind, caind, 0, 1));
                cellIndex = cellIndex + 2;
                cadatarow.createCell(cellIndex++).setCellValue(cash2.getText().toString());
                studentsSheet.addMergedRegion(new CellRangeAddress(caind, caind, 2, 3));

                cadatarow.getCell(0).setCellStyle(borderStyle);
                cadatarow.createCell(1).setCellStyle(borderStyle);
                cadatarow.getCell(2).setCellStyle(borderStyle);
                cadatarow.createCell(3).setCellStyle(borderStyle);

                caind++;

                int cashrowheader = caind;
                nextrow = cashrowheader;
                cellIndex = 0;

                if (lytcashmain.getVisibility() == View.VISIBLE) {
                    Row casubrow = studentsSheet.createRow(cashrowheader);
                    casubrow.createCell(cellIndex).setCellValue(getResources().getString(R.string.ledger_name));
                    casubrow.getCell(cellIndex).setCellStyle(style);
                    casubrow.createCell(1).setCellStyle(style);
                    studentsSheet.addMergedRegion(new CellRangeAddress(cashrowheader, cashrowheader, 0, 1));

                    cellIndex = cellIndex + 2;
                    casubrow.createCell(cellIndex).setCellValue(getResources().getString(R.string.balance));
                    casubrow.getCell(cellIndex).setCellStyle(style);
                    casubrow.createCell(3).setCellStyle(style);
                    studentsSheet.addMergedRegion(new CellRangeAddress(cashrowheader, cashrowheader, 2, 3));

                    cashlistindex = cashrowheader + 1;
                    for (Business model : cashlist) {
                        cellIndex = 0;
                        Row recrow = studentsSheet.createRow(cashlistindex);

                /*String data = model.getDtl2().trim();
                if (data != null && !data.equals("") && data.matches("[0-9.]+")) {
                    data = indianFormat.format(new BigDecimal(Double.parseDouble(model.getDtl2()))).replaceAll("₹", "").trim();
                }*/
                        recrow.createCell(cellIndex).setCellValue(model.getNamedisplay());
                        studentsSheet.addMergedRegion(new CellRangeAddress(cashlistindex, cashlistindex, 0, 1));
                        cellIndex = cellIndex + 2;
                        recrow.createCell(cellIndex).setCellValue(model.getDtl2());
                        studentsSheet.addMergedRegion(new CellRangeAddress(cashlistindex, cashlistindex, 2, 3));

                        recrow.getCell(0).setCellStyle(borderStyle);
                        recrow.createCell(1).setCellStyle(borderStyle);
                        recrow.getCell(2).setCellStyle(borderStyle);
                        recrow.createCell(3).setCellStyle(borderStyle);

                        cashlistindex++;
                    }
                    nextrow = cashlistindex;
                }
            }

            //
            int banklistindex = 0;
            if (lytbank.getVisibility() == View.VISIBLE) {
                cellIndex = 0;
                int baind = nextrow;

                Row barow = studentsSheet.createRow(baind);
                barow.createCell(cellIndex).setCellValue(getResources().getString(R.string.bank_balance));
                barow.getCell(cellIndex).setCellStyle(style);
                barow.createCell(1).setCellStyle(borderStyle);
                barow.createCell(2).setCellStyle(borderStyle);
                barow.createCell(3).setCellStyle(borderStyle);
                studentsSheet.addMergedRegion(new CellRangeAddress(baind, baind, 0, 3));

                baind++;
                cellIndex = 0;
                Row badatarow = studentsSheet.createRow(baind);
                badatarow.createCell(cellIndex).setCellValue(bank1.getText().toString());
                studentsSheet.addMergedRegion(new CellRangeAddress(baind, baind, 0, 1));
                cellIndex = cellIndex + 2;
                badatarow.createCell(cellIndex++).setCellValue(bank2.getText().toString());
                studentsSheet.addMergedRegion(new CellRangeAddress(baind, baind, 2, 3));

                badatarow.getCell(0).setCellStyle(borderStyle);
                badatarow.createCell(1).setCellStyle(borderStyle);
                badatarow.getCell(2).setCellStyle(borderStyle);
                badatarow.createCell(3).setCellStyle(borderStyle);

                baind++;

                int bankrowheader = baind;
                nextrow = bankrowheader;
                cellIndex = 0;

                if (lytbankmain.getVisibility() == View.VISIBLE) {
                    Row basubrow = studentsSheet.createRow(bankrowheader);
                    basubrow.createCell(cellIndex).setCellValue(getResources().getString(R.string.ledger_name));
                    basubrow.getCell(cellIndex).setCellStyle(style);
                    studentsSheet.addMergedRegion(new CellRangeAddress(bankrowheader, bankrowheader, 0, 1));

                    cellIndex = cellIndex + 2;
                    basubrow.createCell(cellIndex).setCellValue(getResources().getString(R.string.balance));
                    basubrow.getCell(cellIndex).setCellStyle(style);
                    studentsSheet.addMergedRegion(new CellRangeAddress(bankrowheader, bankrowheader, 2, 3));

                    banklistindex = bankrowheader + 1;
                    for (Business model : banklist) {
                        cellIndex = 0;
                        Row recrow = studentsSheet.createRow(banklistindex);

               /* String data = model.getDtl2().trim();
                if (data != null && !data.equals("") && data.matches("[0-9.]+")) {
                    data = indianFormat.format(new BigDecimal(Double.parseDouble(model.getDtl2()))).replaceAll("₹", "").trim();
                }*/
                        recrow.createCell(cellIndex).setCellValue(model.getNamedisplay());
                        studentsSheet.addMergedRegion(new CellRangeAddress(banklistindex, banklistindex, 0, 1));
                        cellIndex = cellIndex + 2;
                        recrow.createCell(cellIndex).setCellValue(model.getDtl2());
                        studentsSheet.addMergedRegion(new CellRangeAddress(banklistindex, banklistindex, 2, 3));

                        recrow.getCell(0).setCellStyle(borderStyle);
                        recrow.createCell(1).setCellStyle(borderStyle);
                        recrow.getCell(2).setCellStyle(borderStyle);
                        recrow.createCell(3).setCellStyle(borderStyle);

                        banklistindex++;
                    }

                    nextrow = banklistindex;
                }
            }

            //
            if (lytstock.getVisibility() == View.VISIBLE) {
                cellIndex = 0;
                int stind = nextrow;

                Row sarow = studentsSheet.createRow(stind);
                sarow.createCell(cellIndex).setCellValue(getResources().getString(R.string.stock_valuation));
                sarow.getCell(cellIndex).setCellStyle(style);
                sarow.createCell(1).setCellStyle(borderStyle);
                sarow.createCell(2).setCellStyle(borderStyle);
                sarow.createCell(3).setCellStyle(borderStyle);
                studentsSheet.addMergedRegion(new CellRangeAddress(stind, stind, 0, 3));

                stind++;
                cellIndex = 0;
                Row stdatarow = studentsSheet.createRow(stind);
                stdatarow.createCell(cellIndex).setCellValue(stock1.getText().toString());
                studentsSheet.addMergedRegion(new CellRangeAddress(stind, stind, 0, 1));
                cellIndex = cellIndex + 2;
                stdatarow.createCell(cellIndex++).setCellValue(stock2.getText().toString());
                studentsSheet.addMergedRegion(new CellRangeAddress(stind, stind, 2, 3));

                stdatarow.getCell(0).setCellStyle(borderStyle);
                stdatarow.createCell(1).setCellStyle(borderStyle);
                stdatarow.getCell(2).setCellStyle(borderStyle);
                stdatarow.createCell(3).setCellStyle(borderStyle);
            }

            try {
                FileOutputStream fos = new FileOutputStream(FILE_PATH);
                workbook.write(fos);
                fos.close();

                //System.out.println(FILE_PATH + " is successfully written");
                openPdfExcel(FILE_PATH, "excel");
            } catch (FileNotFoundException e) {
                e.printStackTrace();

                databaseHelper.addFileNotFoundERROR(e, subject);

            } catch (IOException e) {
                e.printStackTrace();

                databaseHelper.addIOERROR(e, subject);
            }
        } catch (Exception e) {
            e.printStackTrace();
            databaseHelper.addExceptionERROR(e, subject);
        }
    }

    //open created pdf/excel file
    private void openPdfExcel(File file, String type) {
        lytprogress.setVisibility(View.GONE);

        Constant.openPdfExcel(file, type, BusinessViewActivity.this);
        btnexcel.setEnabled(true);
        btnpdf.setEnabled(true);
    }


    //to hide/show input layout
    public void OnHideClick(View view) {
        Constant.SetViewVisiblility(imgdown, lyttop);
    }

    //change top arrow
    /*public void SetViewVisiblility() {
        imgdown.setVisibility(View.VISIBLE);
        //System.out.println("=========top - " + lyttop.getVisibility());
        if (lyttop.getVisibility() == View.GONE) {
            imgdown.setImageResource(R.drawable.ic_bup);
            lyttop.setVisibility(View.VISIBLE);
        } else {
            imgdown.setImageResource(R.drawable.ic_bdown);
            lyttop.setVisibility(View.GONE);
        }
    }*/


    //Layout visibility Arrow
    public void OnReceiptlytClick(View view) {
        ChangeArrowView(view, lytreceiptmain);
    }

    public void OnPaymentlytClick(View view) {
        ChangeArrowView(view, lytpaymain);
    }

    public void OnCashlytClick(View view) {
        ChangeArrowView(view, lytcashmain);
    }

    public void OnBanklytClick(View view) {
        ChangeArrowView(view, lytbankmain);
    }

    public void ChangeArrowView(View view, LinearLayout lyt) {
        StartHandlerProcess();
        ImageView imageView = (ImageView) view;
        if (lyt.getVisibility() == View.VISIBLE) {
            lyt.setVisibility(View.GONE);
            imageView.setImageResource(R.drawable.ic_downarrow);
        } else {
            lyt.setVisibility(View.VISIBLE);
            imageView.setImageResource(R.drawable.ic_up);
        }
        StopHandlerProcess();
    }

    //Alphabatic Order
    public void OnBankAlphaSortClick(View view) {
        /*StartHandlerProcess();
        ImageView imageView = (ImageView) findViewById(getResources().getIdentifier("img" + 11, "id", this.getPackageName()));
        if (imageView.getTag() != null && imageView.getTag().equals("2")) {
            imageView.setImageResource(R.drawable.ic_arrow_up);
            imageView.setTag("1");
            SortAlphaData(banklist, 2);
        } else {
            imageView.setImageResource(R.drawable.ic_arrow_down);
            imageView.setTag("2");
            SortAlphaData(banklist, 1);
        }

        bank_recycleview.setAdapter(new BusinessAdapter(banklist, BusinessViewActivity.this, "bank"));*/

        SortAlphaData(banklist, 11, "bank", bank_recycleview);

    }

    public void OnCashAlphaSortClick(View view) {
        SortAlphaData(cashlist, 9, "cash", cash_recycleview);
    }

    public void OnPaymentAcAlphaSortClick(View view) {
        SortAlphaData(paymentlist0, 6, "payment", payment0_recycleview);
    }

    public void OnPaymentCashAlphaSortClick(View view) {
        SortAlphaData(paymentlist1, 8, "payment", payment1_recycleview);
    }

    public void OnReceiptPartyAlphaSortClick(View view) {
        SortAlphaData(receiptlist0, 2, "receipt", receipt0_recycleview);
    }

    public void OnReceiptCashAlphaSortClick(View view) {
        SortAlphaData(receiptlist1, 4, "receipt", receipt1_recycleview);
    }

    private void SortAlphaData(ArrayList<Business> listdata, int imgid, String type, RecyclerView recyclerView) {
        try {
            StartHandlerProcess();
            ImageView imageView = (ImageView) findViewById(getResources().getIdentifier("img" + imgid, "id", this.getPackageName()));

            int isrev;

            if (imageView.getTag() != null && imageView.getTag().equals("2")) {
                imageView.setImageResource(R.drawable.ic_arrow_up);
                imageView.setTag("1");
                isrev = 2;
            } else {
                imageView.setImageResource(R.drawable.ic_arrow_down);
                imageView.setTag("2");
                isrev = 1;
            }

            //
            Collections.sort(listdata, new Comparator<Business>() {
                @Override
                public int compare(Business lhs, Business rhs) {
                    if (isrev == 1)
                        return lhs.getNamedisplay().trim().compareToIgnoreCase(rhs.getNamedisplay().trim());
                    else
                        return rhs.getNamedisplay().trim().compareToIgnoreCase(lhs.getNamedisplay().trim());
                }
            });
            StopHandlerProcess();

            recyclerView.setAdapter(new BusinessAdapter(listdata, BusinessViewActivity.this, type));

        } catch (Exception e) {
            e.printStackTrace();
            databaseHelper.addExceptionERROR(e, subject);
        }
    }

    //Numeric Order
    private void SortNumData(ArrayList<Business> listdata, int imgid, String type, RecyclerView recyclerView) {
        try {
            int isrev;
            StartHandlerProcess();
            ImageView imageView = (ImageView) findViewById(getResources().getIdentifier("img" + imgid, "id", this.getPackageName()));
            if (imageView.getTag() != null && imageView.getTag().equals("2")) {
                imageView.setImageResource(R.drawable.ic_arrow_up);
                imageView.setTag("1");
                isrev = 2;
            } else {
                imageView.setImageResource(R.drawable.ic_arrow_down);
                imageView.setTag("2");
                isrev = 1;
            }


            Collections.sort(listdata, new Comparator<Business>() {
                @Override
                public int compare(Business lhs, Business rhs) {
                    String ldata = lhs.getMaindtl2().trim();
                    String rdata = rhs.getMaindtl2().trim();

                    if (ldata != null && !ldata.equals("") && !ldata.matches("[0-9.]+")) {
                        String lasttwo = ldata.substring(ldata.length() - 2).trim();
                        ldata = ldata.replaceAll(lasttwo, "").trim();
                    }
                    if (rdata != null && !rdata.equals("") && !rdata.matches("[0-9.]+")) {
                        String lasttwo = rdata.substring(rdata.length() - 2).trim();
                        rdata = rdata.replaceAll(lasttwo, "").trim();
                    }

                    if (isrev == 1)
                        return Double.compare(Constant.ConvertToDouble(ldata), Constant.ConvertToDouble(rdata));
                    else
                        return Double.compare(Constant.ConvertToDouble(rdata), Constant.ConvertToDouble(ldata));
                }
            });
            StopHandlerProcess();
            recyclerView.setAdapter(new BusinessAdapter(listdata, BusinessViewActivity.this, type));
        } catch (Exception e) {
            e.printStackTrace();
            databaseHelper.addExceptionERROR(e, subject);
        }
    }

    public void OnReceiptCashNumSortClick(View view) {
        /*StartHandlerProcess();
        ImageView imageView = (ImageView) findViewById(getResources().getIdentifier("img" + 3, "id", this.getPackageName()));
        if (imageView.getTag() != null && imageView.getTag().equals("2")) {
            imageView.setImageResource(R.drawable.ic_arrow_up);
            imageView.setTag("1");
            SortNumData(receiptlist1, 2);
        } else {
            imageView.setImageResource(R.drawable.ic_arrow_down);
            imageView.setTag("2");
            SortNumData(receiptlist1, 1);
        }

        receipt1_recycleview.setAdapter(new BusinessAdapter(receiptlist1, BusinessViewActivity.this, "receipt"));*/

        SortNumData(receiptlist1, 3, "receipt", receipt1_recycleview);

    }

    public void OnReceiptPartyNumSortClick(View view) {
        SortNumData(receiptlist0, 1, "receipt", receipt0_recycleview);
    }

    public void OnPaymentCashNumSortClick(View view) {
        SortNumData(paymentlist1, 7, "payment", payment1_recycleview);
    }

    public void OnPaymentAcNumSortClick(View view) {
        SortNumData(paymentlist0, 5, "payment", payment0_recycleview);

    }

    public void OnCashNumSortClick(View view) {
        SortNumData(cashlist, 10, "cash", cash_recycleview);
    }

    public void OnBankNumSortClick(View view) {
        SortNumData(banklist, 12, "bank", bank_recycleview);
    }

    //show button click
    public void OnShowDataClick(View view) {
        /*try {
            Date date_end = Constant.FormateDate(inFormatdate,enddate, subject, databaseHelper);
            Date date_start = Constant.FormateDate(inFormatdate,startdate, subject, databaseHelper);

            if (date_start.after(date_end)) {
                Toast.makeText(BusinessViewActivity.this, getResources().getString(R.string.fromtodatevalidation), Toast.LENGTH_SHORT).show();
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
            databaseHelper.addExceptionERROR(e, subject);
        }*/

        //if (!CheckValidation()) {

        if (!Constant.CheckValidation(BusinessViewActivity.this, inFormatdate, startdate, enddate, subject, databaseHelper)) {
            SetSession();
            String localdata = "";
            try {
                localdata = databaseHelper.getBusinessData(startdate, enddate, companycode, orgcode);
            } catch (Exception e) {
                e.printStackTrace();
                databaseHelper.addExceptionERROR(e, subject);
            }


            if (localdata.equals("")) {
                Toast.makeText(BusinessViewActivity.this, getResources().getString(R.string.trytosync), Toast.LENGTH_SHORT).show();
            } else {
                DataViewGone();
                StartHandlerProcess();
                GetBusinessData(localdata);
            }
        }
    }

    private void SetSession() {
        session.setData("bs" + orgcode + companycode, startdate);
        session.setData("be" + orgcode + companycode, enddate);
    }

    /*public boolean CheckValidation(Activity activity,SimpleDateFormat inFormatdate, String startdate, String enddate, String subject, DatabaseHelper databaseHelper) {
        boolean iserr = false;
        try {
            Date date_end = Constant.FormateDate(inFormatdate, enddate, subject, databaseHelper);
            Date date_start = Constant.FormateDate(inFormatdate, startdate, subject, databaseHelper);

            if (date_start.after(date_end)) {
                Toast.makeText(activity, activity.getResources().getString(R.string.fromtodatevalidation), Toast.LENGTH_SHORT).show();
                iserr = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
            iserr = true;
            databaseHelper.addExceptionERROR(e, subject);
        }
        return iserr;
    }*/

    //sync button click
    public void OnSyncDataClick(View view) {
        /*try {
            Date date_end = Constant.FormateDate(inFormatdate,enddate, subject, databaseHelper);
            Date date_start = Constant.FormateDate(inFormatdate,startdate, subject, databaseHelper);

            if (date_start.after(date_end)) {
                Toast.makeText(BusinessViewActivity.this, getResources().getString(R.string.fromtodatevalidation), Toast.LENGTH_SHORT).show();
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
            databaseHelper.addExceptionERROR(e, subject);
        }*/

        try {
            if (!Constant.CheckValidation(BusinessViewActivity.this, inFormatdate, startdate, enddate, subject, databaseHelper) && AppController.isConnected(BusinessViewActivity.this, findViewById(R.id.activity_main))) {
                SetSession();

                DataViewGone();
                StartHandlerProcess();

                String url = Constant.BusinessUrl + session.getData(UserSessionManager.KEY_ORG_REGKEY) + "&compcode=" + companycode + "&demo=" + session.getData(UserSessionManager.KEY_DEMO) + "&date1=" + startdate + "&date2=" + enddate + "&mobno=" + session.getData(UserSessionManager.KEY_Mobile) + "&imei=" + session.getData(UserSessionManager.KEY_IMEI) + "&devicecode=" + session.getData(UserSessionManager.KEY_DEVICECODE);
                //System.out.println("=================== = businessurl " + url);

                StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        StopHandlerProcess();
                        if (session.getData(UserSessionManager.KEY_DEMO).equalsIgnoreCase("Yes"))
                            OwnerHomeActivity.DemoReqCout();

                        if (response.contains("\n"))
                            response = response.replace("\n", "").trim();

                        /*if (response.contains(getResources().getString(R.string.noresponse))) {
                            StopHandlerProcess();
                            Toast.makeText(BusinessViewActivity.this, getResources().getString(R.string.notconnectedserver), Toast.LENGTH_SHORT).show();
                        } else if (response.contains(getResources().getString(R.string.multiuseword))) {
                            StopHandlerProcess();
                            Toast.makeText(BusinessViewActivity.this, getResources().getString(R.string.multiuse), Toast.LENGTH_SHORT).show();
                        } else {
                            String localdata = databaseHelper.getBusinessData(startdate, enddate, companycode, orgcode);
                            if (localdata.equals("")) {
                                databaseHelper.addBusinessdata(startdate, enddate, companycode, response, orgcode);
                            } else {
                                databaseHelper.UpdateBusinessData(startdate, enddate, companycode, response, orgcode);
                            }


                            GetBusinessData(response);
                        }*/

                        if (Constant.ResponseErrorCheck(BusinessViewActivity.this, response, true)) {
                            StopHandlerProcess();
                        } else {
                            String localdata = databaseHelper.getBusinessData(startdate, enddate, companycode, orgcode);
                            if (localdata.equals("")) {
                                databaseHelper.addBusinessdata(startdate, enddate, companycode, response, orgcode);
                            } else {
                                databaseHelper.UpdateBusinessData(startdate, enddate, companycode, response, orgcode);
                            }
                            GetBusinessData(response);
                        }
                    }

                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                StopHandlerProcess();
                                String message = Constant.VolleyErrorMessage(error);
                                if (!message.equals(""))
                                    Toast.makeText(BusinessViewActivity.this, message, Toast.LENGTH_SHORT).show();

                            }
                        });

                AppController.getInstance().getRequestQueue().getCache().clear();
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(stringRequest);
            }
        } catch (Exception e) {
            e.printStackTrace();
            databaseHelper.addExceptionERROR(e, subject);
        }
    }

    //adapter for all list data
    public class BusinessAdapter extends RecyclerView.Adapter<BusinessAdapter.ViewHolder> {

        public ArrayList<Business> businessArrayList;
        Context context;
        String type;
        String sessionname = Constant.LedgerReportSessionName;

        public BusinessAdapter(ArrayList<Business> businessArrayList, Context context, String type) {
            this.businessArrayList = businessArrayList;
            this.context = context;
            this.type = type;
        }

        @NonNull
        @Override
        public BusinessAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lyt_balance, parent, false);
            return new BusinessAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final BusinessAdapter.ViewHolder holder, int position) {

            final Business model = businessArrayList.get(position);

            /*String amount, name;
            name = model.getNamedisplay();
            String data = model.getDtl2().trim();
            if (data != null && !data.equals("") && data.matches("[0-9.]+")) {
                amount = (indianFormat.format(new BigDecimal(Double.parseDouble(model.getDtl2())))).replaceAll("₹", "").trim();
            } else {
                //holder.txtamt.setText(data);
                String lasttwo = data.substring(data.length() - 2).trim();
                double main = Double.parseDouble(data.replaceAll(lasttwo, "").trim());
                amount = (indianFormat.format(new BigDecimal(decimalFormat.format(main))) + " " + lasttwo).replaceAll("₹", "").trim();
            }*/

            String name, amount;
            amount = model.getDtl2();

            if (model.getNamedisplay() == null)
                name = getResources().getString(R.string.nodata_updatemaster);
            else
                name = "<u><i>" + model.getNamedisplay() + "</i></u>";

            if (type.equals("cash") || type.equals("bank")) {
                holder.txtname.setText(Html.fromHtml(name));
                holder.txtamt.setText(amount);
                holder.txtname.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (model.getNamedisplay() == null) {
                            session.OnUpdateMasterClick(new VolleyCallback() {
                                @Override
                                public void onSuccess(boolean result) {
                                    if (result)
                                        OnShowDataClick(lyttop.getRootView());
                                }

                                public void onSuccessWithMsg(boolean result, String message) {
                                }
                            }, BusinessViewActivity.this, session, orgcode, companycode, databaseHelper, subject, findViewById(R.id.activity_main));

                        } else {
                            session.setData(sessionname + "sdate" + orgcode + companycode, startdate);
                            session.setData(sessionname + "edate" + orgcode + companycode, enddate);
                            session.setData(sessionname + "namecode" + companycode + orgcode, model.getDtl1());
                            session.setData(sessionname + "name" + companycode + orgcode, model.getNamedisplay());
                            session.setData(sessionname + "narration" + companycode + orgcode, "No");
                            startActivity(new Intent(BusinessViewActivity.this, LedgerReportActivity.class).putExtra("from", "business"));
                        }
                    }
                });

            } else {

                holder.txtamt.setText(Html.fromHtml(name));
                holder.txtname.setText(amount);


                int size;
                //System.out.println("==============** " + amount.length());
                if (amount.length() <= 15) {
                    size = 12;
                } else if (amount.length() <= 18) {
                    size = 10;
                } else if (amount.length() <= 20) {
                    size = 9;
                } else {
                    size = 8;
                }
                holder.txtname.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);


                holder.txtamt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (model.getNamedisplay() == null) {

                            session.OnUpdateMasterClick(new VolleyCallback() {
                                @Override
                                public void onSuccess(boolean result) {
                                    if (result)
                                        OnShowDataClick(lyttop.getRootView());
                                }

                                public void onSuccessWithMsg(boolean result, String message) {
                                }
                            }, BusinessViewActivity.this, session, orgcode, companycode, databaseHelper, subject, findViewById(R.id.activity_main));


                        } else {
                            session.setData(sessionname + "sdate" + orgcode + companycode, startdate);
                            session.setData(sessionname + "edate" + orgcode + companycode, enddate);
                            session.setData(sessionname + "namecode" + companycode + orgcode, model.getDtl1());
                            session.setData(sessionname + "name" + companycode + orgcode, model.getNamedisplay());
                            session.setData(sessionname + "narration" + companycode + orgcode, "No");
                            startActivity(new Intent(BusinessViewActivity.this, LedgerReportActivity.class).putExtra("from", "business"));
                        }
                    }
                });
            }
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return businessArrayList.size();
        }


        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView txtname, txtamt;

            public ViewHolder(View itemView) {
                super(itemView);
                txtname = itemView.findViewById(R.id.txtname);
                txtamt = itemView.findViewById(R.id.txtamt);
            }

        }
    }
}


/*
private void SetDateData(String day) {
        calendar = Calendar.getInstance();
        String dateString = "", monthString = "", currentdate = "", day_name = "", month_name = "", yearString = "";
        String tdateString = "", tmonthString = "", tcurrentdate = "", tday_name = "", tmonth_name = "", tyearString = "";

        if (day.equals("today")) {
            dateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
            tdateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
            monthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
            tmonthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
            yearString = String.valueOf(calendar.get(Calendar.YEAR));
            tyearString = String.valueOf(calendar.get(Calendar.YEAR));
            if (monthString.length() == 1) {
                monthString = "0" + monthString;
                tmonthString = "0" + tmonthString;
            }
            if (dateString.length() == 1) {
                dateString = "0" + dateString;
                tdateString = "0" + tdateString;
            }

            month_name = month_date.format(calendar.getTime());
            tmonth_name = month_date.format(calendar.getTime());
            day_name = date_name.format(calendar.getTime());
            tday_name = date_name.format(calendar.getTime());

            currentdate = "  " + month_name + " " + calendar.get(Calendar.YEAR) + "\n  " + day_name;
            tcurrentdate = "  " + tmonth_name + " " + calendar.get(Calendar.YEAR) + "\n  " + tday_name;

        } else if (day.equals("yesterday")) {
            calendar.add(Calendar.DATE, -1);

            dateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
            tdateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
            monthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
            tmonthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
            yearString = String.valueOf(calendar.get(Calendar.YEAR));
            tyearString = String.valueOf(calendar.get(Calendar.YEAR));
            if (monthString.length() == 1) {
                monthString = "0" + monthString;
                tmonthString = "0" + tmonthString;
            }
            if (dateString.length() == 1) {
                dateString = "0" + dateString;
                tdateString = "0" + tdateString;
            }

            month_name = month_date.format(calendar.getTime());
            tmonth_name = month_date.format(calendar.getTime());
            day_name = date_name.format(calendar.getTime());
            tday_name = date_name.format(calendar.getTime());

            currentdate = "  " + month_name + " " + calendar.get(Calendar.YEAR) + "\n  " + day_name;
            tcurrentdate = "  " + tmonth_name + " " + calendar.get(Calendar.YEAR) + "\n  " + tday_name;

        } else if (day.equals("week")) {

            tdateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
            tmonthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
            tyearString = String.valueOf(calendar.get(Calendar.YEAR));
            if (tmonthString.length() == 1) {
                tmonthString = "0" + tmonthString;
            }
            if (tdateString.length() == 1) {
                tdateString = "0" + tdateString;
            }


            tmonth_name = month_date.format(calendar.getTime());
            tday_name = date_name.format(calendar.getTime());

            tcurrentdate = "  " + tmonth_name + " " + calendar.get(Calendar.YEAR) + "\n  " + tday_name;


            calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

            dateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
            monthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
            yearString = String.valueOf(calendar.get(Calendar.YEAR));
            if (monthString.length() == 1) {
                monthString = "0" + monthString;
            }
            if (dateString.length() == 1) {
                dateString = "0" + dateString;
            }
            startdate = calendar.get(Calendar.YEAR) + monthString + dateString;
            month_name = month_date.format(calendar.getTime());
            day_name = date_name.format(calendar.getTime());

            currentdate = "  " + month_name + " " + calendar.get(Calendar.YEAR) + "\n  " + day_name;

            txtfromdt.setText(dateString);


        } else if (day.equals("cmonth")) {

                tdateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                tmonthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                tyearString = String.valueOf(calendar.get(Calendar.YEAR));
                if (tmonthString.length() == 1) {
                tmonthString = "0" + tmonthString;
                }
                if (tdateString.length() == 1) {
                tdateString = "0" + tdateString;
                }

                tmonth_name = month_date.format(calendar.getTime());
                tday_name = date_name.format(calendar.getTime());

                tcurrentdate = "  " + tmonth_name + " " + calendar.get(Calendar.YEAR) + "\n  " + tday_name;


                calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
                dateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                monthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                yearString = String.valueOf(calendar.get(Calendar.YEAR));
                if (monthString.length() == 1) {
                monthString = "0" + monthString;
                }
                if (dateString.length() == 1) {
                dateString = "0" + dateString;
                }

                month_name = month_date.format(calendar.getTime());
                day_name = date_name.format(calendar.getTime());

                currentdate = "  " + month_name + " " + calendar.get(Calendar.YEAR) + "\n  " + day_name;


                } else if (day.equals("lmonth")) {
                calendar.add(Calendar.MONTH, -1);
                calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
                dateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                monthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                yearString = String.valueOf(calendar.get(Calendar.YEAR));
                if (monthString.length() == 1) {
                monthString = "0" + monthString;
                }
                if (dateString.length() == 1) {
                dateString = "0" + dateString;
                }

                month_name = month_date.format(calendar.getTime());
                day_name = date_name.format(calendar.getTime());

                currentdate = "  " + month_name + " " + calendar.get(Calendar.YEAR) + "\n  " + day_name;


                calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                tdateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                tmonthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                tyearString = String.valueOf(calendar.get(Calendar.YEAR));
                if (tmonthString.length() == 1) {
                tmonthString = "0" + tmonthString;
                }
                if (tdateString.length() == 1) {
                tdateString = "0" + tdateString;
                }

                tmonth_name = month_date.format(calendar.getTime());
                tday_name = date_name.format(calendar.getTime());

                tcurrentdate = "  " + tmonth_name + " " + calendar.get(Calendar.YEAR) + "\n  " + tday_name;

                } else if (day.equals("cyear")) {
                tdateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                tmonthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                tyearString = String.valueOf(calendar.get(Calendar.YEAR));
                if (tmonthString.length() == 1) {
                tmonthString = "0" + tmonthString;
                }
                if (tdateString.length() == 1) {
                tdateString = "0" + tdateString;
                }

                tmonth_name = month_date.format(calendar.getTime());
                tday_name = date_name.format(calendar.getTime());

                tcurrentdate = "  " + tmonth_name + " " + calendar.get(Calendar.YEAR) + "\n  " + tday_name;


                calendar.set(Calendar.MONTH, Calendar.APRIL);
                calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
                //calendar.set(Calendar.DAY_OF_YEAR, 1);
                dateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                monthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                yearString = String.valueOf(calendar.get(Calendar.YEAR));
                if (monthString.length() == 1) {
                monthString = "0" + monthString;
                }
                if (dateString.length() == 1) {
                dateString = "0" + dateString;
                }

                month_name = month_date.format(calendar.getTime());
                day_name = date_name.format(calendar.getTime());

                currentdate = "  " + month_name + " " + calendar.get(Calendar.YEAR) + "\n  " + day_name;



                } else if (day.equals("lyear")) {
                calendar.add(Calendar.YEAR, -1);
                calendar.set(Calendar.MONTH, Calendar.APRIL);
                calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));

                dateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                monthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                yearString = String.valueOf(calendar.get(Calendar.YEAR));
                if (monthString.length() == 1) {
                monthString = "0" + monthString;
                }
                if (dateString.length() == 1) {
                dateString = "0" + dateString;
                }

                month_name = month_date.format(calendar.getTime());
                day_name = date_name.format(calendar.getTime());

                currentdate = "  " + month_name + " " + calendar.get(Calendar.YEAR) + "\n  " + day_name;


                calendar.add(Calendar.YEAR, 1);
                calendar.set(Calendar.MONTH, Calendar.MARCH);
                calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));

                tdateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                tmonthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                tyearString = String.valueOf(calendar.get(Calendar.YEAR));
                if (tmonthString.length() == 1) {
                tmonthString = "0" + tmonthString;
                }
                if (tdateString.length() == 1) {
                tdateString = "0" + tdateString;
                }

                tmonth_name = month_date.format(calendar.getTime());
                tday_name = date_name.format(calendar.getTime());

                tcurrentdate = "  " + tmonth_name + " " + calendar.get(Calendar.YEAR) + "\n  " + tday_name;

                } else if (day.equals("cqua")) {

                tdateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                tmonthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                tyearString = String.valueOf(calendar.get(Calendar.YEAR));
                if (tmonthString.length() == 1) {
                tmonthString = "0" + tmonthString;
                }
                if (tdateString.length() == 1) {
                tdateString = "0" + tdateString;
                }

                tmonth_name = month_date.format(calendar.getTime());
                tday_name = date_name.format(calendar.getTime());

                tcurrentdate = "  " + tmonth_name + " " + calendar.get(Calendar.YEAR) + "\n  " + tday_name;


                int month = calendar.get(Calendar.MONTH);
                calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));


                calendar.set(Calendar.MONTH, QuarterInRange(month));

                dateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                monthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                yearString = String.valueOf(calendar.get(Calendar.YEAR));
                if (monthString.length() == 1) {
                monthString = "0" + monthString;
                }
                if (dateString.length() == 1) {
                dateString = "0" + dateString;
                }

                month_name = month_date.format(calendar.getTime());
                day_name = date_name.format(calendar.getTime());

                currentdate = "  " + month_name + " " + calendar.get(Calendar.YEAR) + "\n  " + day_name;



                } else if (day.equals("lqua")) {
                calendar.add(Calendar.MONTH, -3);
                int month = calendar.get(Calendar.MONTH);
                calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));



                calendar.set(Calendar.MONTH, QuarterInRange(month));

                dateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                monthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                yearString = String.valueOf(calendar.get(Calendar.YEAR));
                if (monthString.length() == 1) {
                monthString = "0" + monthString;
                }
                if (dateString.length() == 1) {
                dateString = "0" + dateString;
                }

                month_name = month_date.format(calendar.getTime());
                day_name = date_name.format(calendar.getTime());

                currentdate = "  " + month_name + " " + calendar.get(Calendar.YEAR) + "\n  " + day_name;


                calendar.add(Calendar.MONTH, 2);
                calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                tdateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
                tmonthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
                tyearString = String.valueOf(calendar.get(Calendar.YEAR));
                if (tmonthString.length() == 1) {
                tmonthString = "0" + tmonthString;
                }
                if (tdateString.length() == 1) {
                tdateString = "0" + tdateString;
                }

                tmonth_name = month_date.format(calendar.getTime());
                tday_name = date_name.format(calendar.getTime());

                tcurrentdate = "  " + tmonth_name + " " + calendar.get(Calendar.YEAR) + "\n  " + tday_name;
                }

                txtfromdt.setText(dateString);
                txttodt.setText(tdateString);

final SpannableString ss = new SpannableString(currentdate);
        ss.setSpan(new ForegroundColorSpan(Color.BLACK), 0, currentdate.indexOf(day_name), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
final SpannableString tss = new SpannableString(tcurrentdate);
        tss.setSpan(new ForegroundColorSpan(Color.BLACK), 0, tcurrentdate.indexOf(tday_name), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        txtfrom.setText(ss);
        txtto.setText(tss);
        startdate = yearString + monthString + dateString;
        enddate = tyearString + tmonthString + tdateString;

        BtnSyncVisibility();

        //System.out.println("======== = == " + startdate + " = " + enddate);

        }

private int QuarterInRange(int number) {
        //0-2,3-5,6-8,9-11
        return number <= 2 ? 0 : number <= 5 ? 3 : number <= 8 ? 6 : 9;
        }
 */
