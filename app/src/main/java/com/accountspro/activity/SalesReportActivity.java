package com.accountspro.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.accountspro.R;
import com.accountspro.helper.AppController;
import com.accountspro.helper.Constant;
import com.accountspro.helper.DatabaseHelper;
import com.accountspro.helper.UserSessionManager;
import com.accountspro.helper.VolleyCallback;
import com.accountspro.model.Master;
import com.accountspro.model.SalesReport;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.apache.poi.hssf.usermodel.HeaderFooter;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Footer;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.PrintSetup;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.ShapeTypes;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFSimpleShape;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SalesReportActivity extends AppCompatActivity {

    public int ReqReadPermission = 1;
    public int ReqWritePermission = 2;
    LinkedHashMap<String, SalesReport> paymentlist, voucherlist;
    ImageButton btnexcel, btnpdf;
    ArrayList<Master> vouchertypelist;
    ArrayList<SalesReport> salesReportArrayList, vList, pList;
    LinearLayout lytvouchersub, lytpaymentsub, lytvoucherheader, lytpdfexcel, lytprogress, lytbottom, lytvoucher, lytpayment, lytcontain, lyttop, lytinput, lytfrom, lytto;//lytvoucherinfo,lytdisplay, lytmain;
    Spinner spvouchertype, spquick;
    TextView txtfromdt, txtfrom, txttodt, txtto, txtsec, txtnodata, datedisplay;
    Button btnsync, btnshow;
    ProgressBar progressbar, recycleprogress;
    RecyclerView recycleview, recycleviewpayment, recycleviewvoucher;
    ImageView imgdown, imgarrowpayment, imgarrowvoucher;
    UserSessionManager session;
    DatabaseHelper databaseHelper;
    Toolbar toolbar;
    String sessionname = Constant.SalesReportSessionName;
    String from, subject, localdata, sdt, edt, startdate, enddate, companyname, compcode, orgcode, address, headertext, path, filename, pdfheadertext, vouchertypename, vouchertypecode = "0";
    SimpleDateFormat month_date = new SimpleDateFormat("MMM");
    SimpleDateFormat date_name = new SimpleDateFormat("EEEE");

    SimpleDateFormat output = new SimpleDateFormat("dd/MM/yy");
    SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat inFormatdate = new SimpleDateFormat("yyyyMMdd");
    //NumberFormat indianFormat = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
    //SimpleDateFormat input = new SimpleDateFormat("yyyyMMdd");

    Calendar calendar;
    int nCounter = 0, start = 1;
    private Handler mHandler = new Handler();
    AsyncTask<String, String, String> asyntask;
    SalesReportAdapter salesadapter;
    Boolean isScrolling = false;
    int currentItems, totalItems, scrollOutItems;
    LinearLayoutManager manager;
    String token = "";
    RecyclerViewAdapter mAdapter;
    int indexnew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_report);

        from = getIntent().getStringExtra("from");

        databaseHelper = new DatabaseHelper(SalesReportActivity.this);
        session = new UserSessionManager(SalesReportActivity.this);
        orgcode = session.getData(UserSessionManager.KEY_ORG_REGKEY);
        compcode = session.getData(orgcode + UserSessionManager.KEY_COM_CODE);
        companyname = session.getData(orgcode + UserSessionManager.KEY_COM_NAME);
        address = session.getData(orgcode + UserSessionManager.KEY_COM_ADDRESS);
        subject = Constant.ERRORID + " | " + session.getData(UserSessionManager.KEY_Mobile) + " | " + session.getData(UserSessionManager.KEY_ORG_NAME) + " | " + companyname;

        try {
            vouchertypelist = databaseHelper.getMasterRowData(compcode, getResources().getString(R.string.master_voucher_type), orgcode, DatabaseHelper.MASTER_DTL1, getResources().getString(R.string.master_col_val_sale));

            salesReportArrayList = new ArrayList<>();
            vList = new ArrayList<>();
            pList = new ArrayList<>();
            voucherlist = new LinkedHashMap<>();
            paymentlist = new LinkedHashMap<>();

            btnexcel = findViewById(R.id.btnexcel);
            btnpdf = findViewById(R.id.btnpdf);
            lytpdfexcel = findViewById(R.id.lytpdfexcel);
            lytvoucherheader = findViewById(R.id.lytvoucherheader);
            lytpaymentsub = findViewById(R.id.lytpaymentsub);
            lytvouchersub = findViewById(R.id.lytvouchersub);
            lytprogress = findViewById(R.id.lytprogress);
            lytbottom = findViewById(R.id.lytbottom);
            imgdown = findViewById(R.id.imgdown);
            imgarrowvoucher = findViewById(R.id.imgarrowvoucher);
            imgarrowpayment = findViewById(R.id.imgarrowpayment);
            recycleviewvoucher = findViewById(R.id.recycleviewvoucher);
            lytvoucher = findViewById(R.id.lytvoucher);
            recycleviewpayment = findViewById(R.id.recycleviewpayment);
            lytpayment = findViewById(R.id.lytpayment);
            recycleview = findViewById(R.id.recycleview);
            datedisplay = findViewById(R.id.datedisplay);
            //lytvoucherinfo = findViewById(R.id.lytvoucherinfo);
            //lytdisplay = findViewById(R.id.lytdisplay);
            //lytmain = findViewById(R.id.lytmain);
            txtnodata = findViewById(R.id.txtnodata);
            txtsec = findViewById(R.id.txtsec);
            progressbar = findViewById(R.id.progressbar);
            recycleprogress = findViewById(R.id.recycleprogress);
            btnshow = findViewById(R.id.btnshow);
            btnsync = findViewById(R.id.btnsync);
            txtto = findViewById(R.id.txtto);
            lytto = findViewById(R.id.lytto);
            txttodt = findViewById(R.id.txttodt);
            txtfrom = findViewById(R.id.txtfrom);
            txtfromdt = findViewById(R.id.txtfromdt);
            lytcontain = findViewById(R.id.lytcontain);
            lytfrom = findViewById(R.id.lytfrom);
            lyttop = findViewById(R.id.lyttop);
            lytinput = findViewById(R.id.lytinput);
            spvouchertype = findViewById(R.id.spvouchertype);
            spquick = findViewById(R.id.spquick);

            manager = new LinearLayoutManager(SalesReportActivity.this);
            recycleview.setLayoutManager(manager);
            recycleviewpayment.setLayoutManager(new LinearLayoutManager(SalesReportActivity.this));
            recycleviewvoucher.setLayoutManager(new LinearLayoutManager(SalesReportActivity.this));

            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);

            final ArrayAdapter<Master> voucheradapter = new ArrayAdapter<Master>(SalesReportActivity.this, R.layout.lyt_sptext, vouchertypelist);
            voucheradapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spvouchertype.setAdapter(voucheradapter);

            SpannableString ss;

            //check session data
            try {
                if (vouchertypelist.size() == 0) {
                    Toast.makeText(SalesReportActivity.this, getResources().getString(R.string.novoucherfound), Toast.LENGTH_SHORT).show();
                } else if (session.getData(sessionname + "sdate" + orgcode + compcode) != null && !session.getData(sessionname + "sdate" + orgcode + compcode).equals("") && !session.getData(sessionname + "edate" + orgcode + compcode).equals("")) {
                    startdate = session.getData(sessionname + "sdate" + orgcode + compcode);
                    enddate = session.getData(sessionname + "edate" + orgcode + compcode);

                    String fdate = startdate.substring(Math.max(startdate.length() - 2, 0));
                    String tdate = enddate.substring(Math.max(enddate.length() - 2, 0));
                    txtfromdt.setText(fdate);
                    txttodt.setText(tdate);

                    try {
                        Date date_end = Constant.FormateDate(inFormatdate, enddate, subject, databaseHelper);
                        Date date_start = Constant.FormateDate(inFormatdate, startdate, subject, databaseHelper);

                        Calendar scal = Calendar.getInstance();
                        scal.setTime(date_start);
                        Calendar ecal = Calendar.getInstance();
                        ecal.setTime(date_end);

                        String fmonth_name = Constant.FormateDate(month_date, scal.getTime());
                        String fday_name = Constant.FormateDate(date_name, scal.getTime());
                        String fcurrentdate = "  " + fmonth_name + " " + scal.get(Calendar.YEAR) + "\n  " + fday_name;

                        String emonth_name = Constant.FormateDate(month_date, ecal.getTime());
                        String eday_name = Constant.FormateDate(date_name, ecal.getTime());
                        String ecurrentdate = "  " + emonth_name + " " + ecal.get(Calendar.YEAR) + "\n  " + eday_name;

                        ss = new SpannableString(fcurrentdate);
                        ss.setSpan(new ForegroundColorSpan(Color.BLACK), 0, fcurrentdate.indexOf(fday_name), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        txtfrom.setText(ss);

                        SpannableString sst = new SpannableString(ecurrentdate);
                        sst.setSpan(new ForegroundColorSpan(Color.BLACK), 0, ecurrentdate.indexOf(eday_name), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        txtto.setText(sst);

                        vouchertypecode = session.getData(sessionname + "typecode" + compcode + orgcode);
                        SpinnerVoucherTypeSelection(vouchertypecode);

                        if (from != null && !from.equals("home")) {
                            //OnShowSalesReportClick(lyttop.getRootView());
                            boolean issyncable = SyncVisibility();
                            if (issyncable)
                                OnSyncSalesReportClick(lyttop.getRootView());
                            else
                                Toast.makeText(SalesReportActivity.this, getResources().getString(R.string.datalimit), Toast.LENGTH_SHORT).show();
                            from = "home";

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        databaseHelper.addExceptionERROR(e, subject);
                    }

                } else {
                    calendar = Calendar.getInstance();
                    //SetDateData(calendar, "cyear");
                    String[] dates = Constant.SetDateData(calendar, "cyear", txtfromdt, txttodt, txtfrom, txtto).split(";");
                    startdate = dates[0];
                    enddate = dates[1];
                    imgdown.setVisibility(View.GONE);
                    spvouchertype.setSelection(vouchertypelist.size() - 1);
                }
            } catch (Exception e) {
                e.printStackTrace();
                databaseHelper.addExceptionERROR(e, subject);
            }

            //from date selection
            lytfrom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Date date_start = Constant.FormateDate(inFormatdate, startdate, subject, databaseHelper);
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(date_start);

                        new DatePickerDialog(SalesReportActivity.this, pickerListenerfrom, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        databaseHelper.addExceptionERROR(e, subject);
                    }
                }
            });

            //voucher type selected item
            spvouchertype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        ((TextView) spvouchertype.getSelectedView()).setTextColor(getResources().getColor(R.color.white));
                    } catch (Exception e) {
                        e.printStackTrace();
                        databaseHelper.addExceptionERROR(e, subject);
                    }
                    Master master = (Master) parent.getItemAtPosition(position);
                    vouchertypecode = master.getMastercode();
                    vouchertypename = master.getMastername();

                    SyncVisibility();
                    //System.out.println("=====================typecode" + vouchertypecode+" = "+vouchertypename);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

            //to date selection
            lytto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Date date_end = Constant.FormateDate(inFormatdate, enddate, subject, databaseHelper);
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(date_end);
                        new DatePickerDialog(SalesReportActivity.this, pickerListenerto, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        databaseHelper.addExceptionERROR(e, subject);
                    }
                }
            });

            //quick date selection
            spquick.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    //btnsync.setVisibility(View.GONE);
                    String item = spquick.getSelectedItem().toString();
                    calendar = Calendar.getInstance();

                    String type = Constant.QuickSpinnerType(item);
                    if (!type.equals("")) {
                        String[] dates = Constant.SetDateData(calendar, type, txtfromdt, txttodt, txtfrom, txtto).split(";");
                        startdate = dates[0];
                        enddate = dates[1];
                    }

                    SyncVisibility();

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }

            });

            //check local local data availability
            try {
                localdata = databaseHelper.getSalesReportData(compcode, orgcode, vouchertypecode, startdate, enddate);
                if (!localdata.equals("") && (AppController.isNetConnected(SalesReportActivity.this))) {
                    SyncVisibility();
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            OnShowSalesReportClick(lyttop.getRootView());
                        }
                    }, 150);

                } else if (AppController.isNetConnected(SalesReportActivity.this)) {
                    SyncVisibility();
                    imgdown.setVisibility(View.GONE);
                    btnshow.setVisibility(View.GONE);
                } else {
                    //lyttop.setVisibility(View.GONE);
                    imgdown.setVisibility(View.GONE);
                    Toast.makeText(SalesReportActivity.this, getResources().getString(R.string.checkinternet), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                databaseHelper.addExceptionERROR(e, subject);
            }
        } catch (Exception e) {
            e.printStackTrace();

            databaseHelper.addExceptionERROR(e, subject);
        }
    }


    //selection of voucher type from code
    private void SpinnerVoucherTypeSelection(String vouchertypecode) {
        int i = 0;
        for (Master master : vouchertypelist) {
            if (master.getMastercode().equals(vouchertypecode)) {
                spvouchertype.setSelection(i);
                break;
            }
            i++;
        }
    }

    /*@Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }*/

    private void StopHandlerProcess() {
        Constant.BtnEnableDisable(true, btnsync, btnshow);
        progressbar.setVisibility(View.GONE);
        txtsec.setVisibility(View.GONE);
        mHandler.removeCallbacks(hMyTimeTask);
    }

    private void StartHandlerProcess() {
        Constant.BtnEnableDisable(false, btnsync, btnshow);
        progressbar.setVisibility(View.VISIBLE);
        txtsec.setVisibility(View.VISIBLE);
        txtsec.setText("");
        nCounter = 0;
        mHandler.postDelayed(hMyTimeTask, 1000);
    }

    public void OnHideClick(View view) {
        Constant.SetViewVisiblility(imgdown, lyttop);
    }


    private Runnable hMyTimeTask = new Runnable() {
        public void run() {
            nCounter++;
            txtsec.setText("" + nCounter);
            mHandler.postDelayed(hMyTimeTask, 1000);
        }
    };


    private DatePickerDialog.OnDateSetListener pickerListenerfrom = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {

            String[] datestring = Constant.GetMonthDateOfDatePicker(selectedMonth, selectedDay).split(";");
            String dateString = datestring[0];
            String monthString = datestring[1];

            try {
                Date myDate = Constant.FormateDate(inFormat, selectedYear + "-" + monthString + "-" + dateString, subject, databaseHelper);

                String month_name = Constant.FormateDate(month_date, myDate);
                String day_name = Constant.FormateDate(date_name, myDate);

                String currentdate = "  " + month_name + " " + selectedYear + "\n  " + day_name;

                txtfromdt.setText(dateString);
                //txttodt.setText(dateString);

                final SpannableString ss = new SpannableString(currentdate);
                ss.setSpan(new ForegroundColorSpan(Color.BLACK), 0, currentdate.indexOf(day_name), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                txtfrom.setText(ss);
                startdate = selectedYear + monthString + dateString;

                SyncVisibility();
            } catch (Exception e) {
                e.printStackTrace();

                databaseHelper.addExceptionERROR(e, subject);
            }

        }
    };

    private DatePickerDialog.OnDateSetListener pickerListenerto = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {

            String[] datestring = Constant.GetMonthDateOfDatePicker(selectedMonth, selectedDay).split(";");
            String dateString = datestring[0];
            String monthString = datestring[1];


            try {
                Date myDate = Constant.FormateDate(inFormat, selectedYear + "-" + monthString + "-" + dateString, subject, databaseHelper);

                String month_name = Constant.FormateDate(month_date, myDate);
                String day_name = Constant.FormateDate(date_name, myDate);

                String currentdate = "  " + month_name + " " + selectedYear + "\n  " + day_name;

                //txtfromdt.setText(dateString);
                txttodt.setText(dateString);

                final SpannableString ss = new SpannableString(currentdate);
                ss.setSpan(new ForegroundColorSpan(Color.BLACK), 0, currentdate.indexOf(day_name), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                txtto.setText(ss);
                enddate = selectedYear + monthString + dateString;

                SyncVisibility();
            } catch (Exception e) {
                e.printStackTrace();

                databaseHelper.addExceptionERROR(e, subject);
            }

        }
    };

    public boolean SyncVisibility() {
        String localdata = "";
        try {
            localdata = databaseHelper.getSalesReportData(compcode, orgcode, vouchertypecode, startdate, enddate);
        } catch (Exception e) {
            e.printStackTrace();
            databaseHelper.addExceptionERROR(e, subject);
        }
        if (localdata.equals("")) {
            btnshow.setVisibility(View.GONE);
        } else {
            btnshow.setVisibility(View.VISIBLE);
        }

        boolean issyncenable = false;
        if (session.getData(UserSessionManager.KEY_DEMO).equalsIgnoreCase("Yes") && session.getIntData(UserSessionManager.KEY_DEMOREQCOUNT) > (session.getIntData(UserSessionManager.KEY_DEMOREQTOTAL))) {
            btnsync.setVisibility(View.GONE);
            issyncenable = false;
        } else {
            btnsync.setVisibility(View.VISIBLE);
            issyncenable = true;
        }
        return issyncenable;
    }


    //show button click
    public void OnShowSalesReportClick(View view) {


        try {
            if (!Constant.CheckValidation(SalesReportActivity.this, inFormatdate, startdate, enddate, subject, databaseHelper)) {
                localdata = databaseHelper.getSalesReportData(compcode, orgcode, vouchertypecode, startdate, enddate);
                boolean issyncable = SyncVisibility();
                if (from != null && !from.equals("home")) {
                    if (issyncable)
                        OnSyncSalesReportClick(lyttop.getRootView());
                    else
                        Toast.makeText(SalesReportActivity.this, getResources().getString(R.string.datalimit), Toast.LENGTH_SHORT).show();
                    from = "home";
                } else {
                    if (localdata.equals("")) {
                        Toast.makeText(SalesReportActivity.this, getResources().getString(R.string.trytosync), Toast.LENGTH_SHORT).show();
                    } else {
                        //SyncVisibility();
                        StartHandlerProcess();
                        GoneData();
                        GetSalesReportData(localdata);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            databaseHelper.addExceptionERROR(e, subject);
        }

    }


    //sync button click
    public void OnSyncSalesReportClick(View view) {


        try {
            if (!Constant.CheckValidation(SalesReportActivity.this, inFormatdate, startdate, enddate, subject, databaseHelper) && AppController.isConnected(SalesReportActivity.this, findViewById(R.id.activity_main))) {
                StartHandlerProcess();
                GoneData();
                String url = Constant.SalesReportUrl + orgcode + "&mobno=" + session.getData(UserSessionManager.KEY_Mobile) + "&imei=" + session.getData(UserSessionManager.KEY_IMEI) + "&devicecode=" + session.getData(UserSessionManager.KEY_DEVICECODE) + "&compcode=" + compcode + "&demo=" + session.getData(UserSessionManager.KEY_DEMO) + "&vouchertypecode=" + vouchertypecode + "&date1=" + startdate + "&date2=" + enddate;

                url = url.replaceAll(" ", "%20");

                System.out.println("=============url -- " + url);
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (session.getData(UserSessionManager.KEY_DEMO).equalsIgnoreCase("Yes"))
                            OwnerHomeActivity.DemoReqCout();

                        if (response.contains("\n"))
                            response = response.replace("\n", "").trim();

                        if (Constant.ResponseErrorCheck(SalesReportActivity.this, response, true)) {
                            StopHandlerProcess();
                        } else {
                            String localdata = databaseHelper.getSalesReportData(compcode, orgcode, vouchertypecode, startdate, enddate);
                            if (localdata.equals("")) {
                                databaseHelper.addSalesReportdata(compcode, orgcode, response, vouchertypecode, startdate, enddate);
                            } else {
                                databaseHelper.UpdateSalesReportData(compcode, orgcode, response, vouchertypecode, startdate, enddate);
                            }
                            GetSalesReportData(response);

                        }
                        //StopHandlerProcess();
                    }

                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                StopHandlerProcess();
                                String message = Constant.VolleyErrorMessage(error);
                                if (!message.equals(""))
                                    Toast.makeText(SalesReportActivity.this, message, Toast.LENGTH_SHORT).show();

                            }
                        });

                AppController.getInstance().getRequestQueue().getCache().clear();
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(stringRequest);
            }
        } catch (Exception e) {
            e.printStackTrace();
            databaseHelper.addExceptionERROR(e, subject);
        }
    }

    //display data
    private void GetSalesReportData(String response) {

        if (databaseHelper.getAllMasterData(compcode, orgcode).size() == 0) {
            StopHandlerProcess();
            Toast.makeText(SalesReportActivity.this, getResources().getString(R.string.master_updatemsg), Toast.LENGTH_SHORT).show();
            return;
        }

        session.setData(sessionname + "typecode" + compcode + orgcode, vouchertypecode);
        session.setData(sessionname + "sdate" + orgcode + compcode, startdate);
        session.setData(sessionname + "edate" + orgcode + compcode, enddate);

        try {

            //GoneData();

            if (response.contains(getResources().getString(R.string.NOVCH))) {
                StopHandlerProcess();
                txtnodata.setVisibility(View.VISIBLE);
                imgdown.setVisibility(View.GONE);
            } else {
                JSONArray jsonArray = new JSONArray(response);
                salesReportArrayList = new ArrayList<>();

                if (jsonArray.length() == 2) {
                    StopHandlerProcess();
                    txtnodata.setVisibility(View.VISIBLE);
                } else {

                    try {
                        asyntask = new AsyncTaskRunner(response).execute();

                    } catch (Exception e) {
                        e.printStackTrace();
                        databaseHelper.addExceptionERROR(e, subject);
                    }

                    mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                        @Override
                        public void onLoadMore() {

                            // System.out.println("=============  " + salesReportArrayList.size() + " == " + jsonArray.length() + " = " + start);

                            int index = indexnew + 1;
                            int end = index + Constant.SalesReport_PAGELIMIT;
                            int jsonlength = jsonArray.length() - 1;
                            if (jsonlength < end)
                                end = index + (jsonlength - index);

                            //end = end + 1;
                            //System.out.println("============length**--  " + index + " == " + end);
                            if (index < end) {
                                //if (salesReportArrayList.size() < (jsonArray.length() - start - 1)) {
                                salesReportArrayList.add(null);
                                mAdapter.notifyItemInserted(salesReportArrayList.size() - 1);
                                int finalEnd = end;
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        salesReportArrayList.remove(salesReportArrayList.size() - 1);
                                        mAdapter.notifyItemRemoved(salesReportArrayList.size());

                                        if (salesReportArrayList.contains(null)) {
                                            //System.out.println("=============  null " + salesReportArrayList.size());
                                            for (int i = 0; i < salesReportArrayList.size(); i++) {
                                                if (salesReportArrayList.get(i) == null) {
                                                    salesReportArrayList.remove(i);
                                                    break;
                                                }
                                            }
                                        }


                                        /*int index = indexnew+1;
                                        int end = index + Constant.SalesReport_PAGELIMIT;
                                        int jsonlength = jsonArray.length() - 1;
                                        if (jsonlength < end)
                                            end = index + (jsonlength - index);

                                        //end = end + 1;*/

                                        System.out.println("============length** " + index + " == " + finalEnd);
                                        SetJsonData(index, finalEnd, jsonArray, false);

                                        indexnew = indexnew + Constant.SalesReport_PAGELIMIT;
                                        mAdapter.notifyDataSetChanged();
                                        mAdapter.setLoaded();
                                    }
                                }, 1000);
                            } /*else {
                                Toast.makeText(SalesReportActivity.this, "Loading data completed", Toast.LENGTH_SHORT).show();
                            }*/
                        }
                    });

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            StopHandlerProcess();
            databaseHelper.addExceptionERROR(e, subject);
        }
    }

    private void SetJsonData(int start, int length, JSONArray jsonArray, boolean isfirst) {
        try {
            double total = 0.0;

            for (int i = start; i < length; i++) {
                //System.out.println("============="+i);
                //String trdate2, String ledgercode, String cashdebit, String billtype, String vchno, String vchcode, String amount, String vouchertypecode
                JSONArray salesarray = jsonArray.getJSONArray(i);

                if (salesarray.length() == 8) {

                    String trdate = Constant.FormateDate(output, Constant.FormateDate(inFormatdate, salesarray.getString(0), subject, databaseHelper));
                    String partyname;

                    System.out.println("================ = = = sales" + salesarray.getString(1));

                    if (salesarray.getString(1).equals("0") || salesarray.getString(1).equals("-1"))
                        partyname = "Retail Customer";
                    else
                        partyname = databaseHelper.getMasterValue(compcode, DatabaseHelper.MASTER_MASTERCODE, salesarray.getString(1), getResources().getString(R.string.master_ledger_name), DatabaseHelper.MASTER_MASTERNAME, orgcode);


                    String paymenttypecode = salesarray.getString(3);
                    String type = databaseHelper.getMasterValue(compcode, DatabaseHelper.MASTER_MASTERCODE, paymenttypecode, getResources().getString(R.string.master_paym_type), DatabaseHelper.MASTER_MASTERNAME, orgcode);


                    double mainamount = Constant.ConvertToDouble(salesarray.getString(6));
                    String amount = Constant.ConvertToIndianRupeeFormat(mainamount, false, true, false);

                    //System.out.println("================ = = = sales"+i+" = "+partyname+" = "+salesarray.getString(1)+" == "+amount+" == "+salesarray.getString(4));

                    String typecode = salesarray.getString(7);

                    //System.out.println("=========== = =" + i);
                    salesReportArrayList.add(new SalesReport(trdate, salesarray.getString(1), salesarray.getString(2), paymenttypecode, salesarray.getString(4), salesarray.getString(5), amount, typecode, partyname, type));

                    total = total + mainamount;


                }
            }


            if (isfirst) {
                indexnew = length;
            }


        } catch (JSONException e) {
            e.printStackTrace();
            databaseHelper.addJSONERROR(e, subject);
        }
    }

    private void GoneData() {
        lytbottom.setVisibility(View.GONE);
        lytpdfexcel.setVisibility(View.GONE);
        recycleview.setVisibility(View.GONE);
        txtnodata.setVisibility(View.GONE);
        lytvoucherheader.setVisibility(View.GONE);
        datedisplay.setVisibility(View.GONE);
        lytpayment.setVisibility(View.GONE);
        lytvoucher.setVisibility(View.GONE);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void OnPaymentClick(View view) {
        if (lytpaymentsub.getVisibility() == View.VISIBLE) {
            lytpaymentsub.setVisibility(View.GONE);
            imgarrowpayment.setImageResource(R.drawable.ic_arrow_up);
        } else {
            lytpaymentsub.setVisibility(View.VISIBLE);
            imgarrowpayment.setImageResource(R.drawable.ic_arrow_down);
        }
    }

    public void OnVoucherClick(View view) {
        if (lytvouchersub.getVisibility() == View.VISIBLE) {
            lytvouchersub.setVisibility(View.GONE);
            imgarrowvoucher.setImageResource(R.drawable.ic_arrow_up);
        } else {
            lytvouchersub.setVisibility(View.VISIBLE);
            imgarrowvoucher.setImageResource(R.drawable.ic_arrow_down);
        }
    }

    //helper class for display data
    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        String response;
        SalesReport totalreportpaysummary;//, totalreportvouchersummary;
        JSONArray jsonArray;

        public AsyncTaskRunner(String response) {
            this.response = response;

            try {
                jsonArray = new JSONArray(response);
            } catch (JSONException e) {
                e.printStackTrace();
                databaseHelper.addJSONERROR(e, subject);
            }
        }

        @Override
        protected String doInBackground(String... params) {
            start = 1;
            try {
                for (int i = 1; i < jsonArray.length() - 1; i++) {
                    //System.out.println("==================");
                    try {
                        JSONArray salesarray = jsonArray.getJSONArray(i);

                        String typecode = salesarray.getString(7);

                        start = i;
                        if (!(typecode.equals("-1") || typecode.equals("-2"))) {
                            break;
                        } else {

                            int totalitem = Integer.parseInt(salesarray.getString(5));
                            double mainamount = Constant.ConvertToDouble(salesarray.getString(6));
                            String paymenttypecode = salesarray.getString(3);

                            if (typecode.equals("-1")) {
                                String type = databaseHelper.getMasterValue(compcode, DatabaseHelper.MASTER_MASTERCODE, paymenttypecode, getResources().getString(R.string.master_paym_type), DatabaseHelper.MASTER_MASTERNAME, orgcode);
                                pList.add(new SalesReport(type, totalitem, mainamount));

                                if (i == 1) {
                                    totalreportpaysummary.setDoubleamount(mainamount);
                                    totalreportpaysummary.setNo(totalitem);
                                } else {
                                    totalreportpaysummary.setDoubleamount(totalreportpaysummary.getDoubleamount() + mainamount);
                                    totalreportpaysummary.setNo(totalreportpaysummary.getNo() + totalitem);
                                }
                            } else {
                                String vouchername = databaseHelper.getMasterValue(compcode, DatabaseHelper.MASTER_MASTERCODE, paymenttypecode, getResources().getString(R.string.master_voucher_type), DatabaseHelper.MASTER_MASTERNAME, orgcode);
                                vList.add(new SalesReport(vouchername, totalitem, mainamount));

                                /*if (i == 1) {
                                    totalreportvouchersummary.setDoubleamount(mainamount);
                                    totalreportvouchersummary.setNo(totalitem);
                                } else {
                                    totalreportvouchersummary.setDoubleamount(totalreportvouchersummary.getDoubleamount() + mainamount);
                                    totalreportvouchersummary.setNo(totalreportvouchersummary.getNo() + totalitem);
                                }*/
                            }


                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                        databaseHelper.addExceptionERROR(e, subject);
                    }
                }
            } catch (IndexOutOfBoundsException e) {
                e.printStackTrace();
                databaseHelper.addExceptionERROR(e, subject);
            }


            int length = start + Constant.SalesReport_PAGELIMIT;
            int jsonlength = jsonArray.length() - 2;
            if (jsonlength < length)
                length = jsonlength;

            int end = length + 1;

            /*int length = Constant.SalesReport_PAGELIMIT;
            if (jsonArray.length() < Constant.SalesReport_PAGELIMIT)
                length = jsonArray.length();*/

            //System.out.println("============length " + start + " == " + end);
            SetJsonData(start, end, jsonArray, true);

            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            //System.out.println("================= =  =post - "+result);
            try {

                Date date_end = Constant.FormateDate(inFormatdate, enddate, subject, databaseHelper);
                Date date_start = Constant.FormateDate(inFormatdate, startdate, subject, databaseHelper);
                sdt = new SimpleDateFormat("dd-MM-yyyy", Locale.US).format(date_start);
                edt = new SimpleDateFormat("dd-MM-yyyy", Locale.US).format(date_end);

                String header = companyname + "\n" + sdt + " to " + edt;

                SpannableString ssdate = new SpannableString(header);
                ssdate.setSpan(new RelativeSizeSpan(1.3f), 0, companyname.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ssdate.setSpan(new StyleSpan(Typeface.BOLD), 0, companyname.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ssdate.setSpan(new UnderlineSpan(), header.indexOf(sdt), header.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                datedisplay.setText(ssdate);

                filename = companyname + "-salesreport-" + vouchertypename;

                pdfheadertext = "Sales Report of type " + vouchertypename + " from " + sdt + " to " + edt;
                headertext = "\n" + companyname + "\n" + address + "\n\nSales Report of type " + vouchertypename + "\nfrom " + sdt + " to " + edt + "\n";

                Constant.BtnEnableDisable(true, btnsync, btnshow);


                Constant.SetViewVisiblility(imgdown, lyttop);
                SyncVisibility();
                StopHandlerProcess();
                txtnodata.setVisibility(View.GONE);
                lytbottom.setVisibility(View.VISIBLE);
                //lytmain.setVisibility(View.VISIBLE);
                recycleview.setVisibility(View.VISIBLE);
                lytpdfexcel.setVisibility(View.VISIBLE);
                lytvoucherheader.setVisibility(View.VISIBLE);
                datedisplay.setVisibility(View.VISIBLE);

                if (pList.size() != 0) {
                    lytpayment.setVisibility(View.VISIBLE);
                    pList.add(totalreportpaysummary);
                }

                if (vouchertypecode.equals("0") && vList.size() != 0) {
                    lytvoucher.setVisibility(View.VISIBLE);
                    lytvouchersub.setVisibility(View.VISIBLE);
                    imgarrowvoucher.setImageResource(R.drawable.ic_arrow_down);
                    vList.add(totalreportpaysummary);
                } else if (pList.size() != 0) {
                    lytpaymentsub.setVisibility(View.VISIBLE);
                    imgarrowpayment.setImageResource(R.drawable.ic_arrow_down);
                }

                recycleviewpayment.setAdapter(new OtherDataAdapter(pList));
                recycleviewvoucher.setAdapter(new OtherDataAdapter(vList));


            } catch (Exception e) {
                e.printStackTrace();
                StopHandlerProcess();
                databaseHelper.addExceptionERROR(e, subject);
            }

        }


        @Override
        protected void onPreExecute() {

            salesReportArrayList = new ArrayList<>();
            voucherlist.clear();
            paymentlist.clear();
            pList.clear();
            vList.clear();
            Constant.BtnEnableDisable(false, btnsync, btnshow);

            lytbottom.setVisibility(View.GONE);
            lytpdfexcel.setVisibility(View.GONE);
            //lytmain.setVisibility(View.GONE);
            recycleview.setVisibility(View.GONE);
            totalreportpaysummary = new SalesReport("Total");
            //totalreportvouchersummary = new SalesReport("Total");

            indexnew = 1;
            //salesReportArrayList = new ArrayList<>();
            mAdapter = new RecyclerViewAdapter(salesReportArrayList, recycleview);
            recycleview.setAdapter(mAdapter);
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (asyntask != null)
            asyntask.cancel(true);
    }

    //adapter to display data
    public class SalesReportAdapter extends RecyclerView.Adapter<SalesReportAdapter.ViewHolder> {

        public ArrayList<SalesReport> salesReports;


        public SalesReportAdapter(ArrayList<SalesReport> salesReports) {
            this.salesReports = salesReports;
        }


        public void addAll(List<SalesReport> mcList) {
            for (SalesReport mc : mcList) {
                add(mc);
            }
        }

        public void add(SalesReport mc) {
            salesReports.add(mc);
            notifyItemInserted(salesReports.size() - 1);
        }

        @NonNull
        @Override
        public SalesReportAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lyt_salesreport, parent, false);

            return new SalesReportAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
            //System.out.println("================= =  =postadapter - " + position);
            final SalesReport model = salesReports.get(position);


            holder.txtdate.setText(model.getTrdate2());
            holder.txttype.setText(model.getTypename());
            holder.txtvchno.setText(Html.fromHtml("<u><i>" + model.getVchno() + "</i></u>"));

            String data = model.getAmount();
            holder.txtamount.setText(data);
            int size;
            if (data.length() <= 10) {
                size = 12;
            } else if (data.length() <= 14) {
                size = 10;
            } else if (data.length() <= 16) {
                size = 9;
            } else {
                size = 8;
            }
            holder.txtamount.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);

            holder.txtvchno.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //System.out.println("=================== = = " + model.getVchcode() + " = " + model.getVouchertypecode());
                    if (!model.getVchcode().equals("") && !model.getVouchertypecode().equals("")) {
                        String vchno = model.getVchno().replaceAll("[^0-9]", "").trim();
                        startActivity(new Intent(SalesReportActivity.this, VoucherInfoActivity.class).putExtra("from", "salesreport").putExtra("code", model.getVchcode()).putExtra("vno", vchno).putExtra("typecode", model.getVouchertypecode()));
                    }
                }
            });


            String name = model.getPartyname();
            if (model.getPartyname() == null || name.equals(""))
                name = getResources().getString(R.string.nodata_updatemaster);

            holder.txtname.setText(Html.fromHtml(name));

            holder.txtname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (model.getPartyname() == null || model.getPartyname().equals("")) {
                        session.OnUpdateMasterClick(new VolleyCallback() {
                            @Override
                            public void onSuccess(boolean result) {
                                if (result)
                                    OnShowSalesReportClick(lyttop.getRootView());
                            }

                            @Override
                            public void onSuccessWithMsg(boolean result, String message) {
                            }
                        }, SalesReportActivity.this, session, orgcode, compcode, databaseHelper, subject, findViewById(R.id.activity_main));

                    }
                }
            });
        }


        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return salesReports.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            public TextView txtdate, txtname, txttype, txtvchno, txtamount;

            public ViewHolder(View itemView) {
                super(itemView);

                txtdate = itemView.findViewById(R.id.txtdate);
                txtname = itemView.findViewById(R.id.txtname);
                txttype = itemView.findViewById(R.id.txttype);
                txtvchno = itemView.findViewById(R.id.txtvchno);
                txtamount = itemView.findViewById(R.id.txtamount);

            }

        }
    }

    //adapter for sub-list
    public class OtherDataAdapter extends RecyclerView.Adapter<OtherDataAdapter.ViewHolder> {

        public ArrayList<SalesReport> salesReports;

        public OtherDataAdapter(ArrayList<SalesReport> salesReports) {
            this.salesReports = salesReports;
        }

        public void addAll(List<SalesReport> mcList) {
            for (SalesReport mc : mcList) {
                add(mc);
            }
        }

        public void add(SalesReport mc) {
            salesReports.add(mc);
            notifyItemInserted(salesReports.size() - 1);
        }

        @NonNull
        @Override
        public OtherDataAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lyt_salesreport_otherdata, parent, false);

            return new OtherDataAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
            //System.out.println("================= =  =postother - " + position);
            if (position == salesReports.size() - 1) {
                holder.txtamount.setTypeface(null, Typeface.BOLD);
                holder.txtno.setTypeface(null, Typeface.BOLD);
                holder.txtname.setTypeface(null, Typeface.BOLD);
            }

            final SalesReport model = salesReports.get(position);
            holder.txtno.setText(model.getNo() + "");
            String name = model.getName();
            if (name == null || name.equals(""))
                name = getResources().getString(R.string.nodata_updatemaster);

            holder.txtname.setText(Html.fromHtml(name));

            String data = Constant.ConvertToIndianRupeeFormat(model.getDoubleamount(), false, true, false);
            holder.txtamount.setText(data);


            holder.txtname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (model.getName() == null || model.getName().equals("")) {
                        session.OnUpdateMasterClick(new VolleyCallback() {
                            @Override
                            public void onSuccess(boolean result) {
                                if (result)
                                    OnShowSalesReportClick(lyttop.getRootView());
                            }

                            @Override
                            public void onSuccessWithMsg(boolean result, String message) {
                            }
                        }, SalesReportActivity.this, session, orgcode, compcode, databaseHelper, subject, findViewById(R.id.activity_main));

                    }
                }
            });

        }


        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return salesReports.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            public TextView txtname, txtno, txtamount;

            public ViewHolder(View itemView) {
                super(itemView);

                txtname = itemView.findViewById(R.id.txtname);
                txtno = itemView.findViewById(R.id.txtno);
                txtamount = itemView.findViewById(R.id.txtamount);

            }

        }
    }

    //pdf button click
    public void OnSalesReportPdfClick(View view) {
        /*if (ContextCompat.checkSelfPermission(SalesReportActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SalesReportActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, ReqReadPermission);
        } else if (ContextCompat.checkSelfPermission(SalesReportActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SalesReportActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, ReqWritePermission);
        } else {*/
        if (Constant.CheckReadWritePermissionGranted(SalesReportActivity.this)) {
            lytprogress.setVisibility(View.VISIBLE);
            btnexcel.setEnabled(false);
            btnpdf.setEnabled(false);
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {

                    File filepath = Constant.FilePathPdfExcel(true, ".pdf", SalesReportActivity.this, filename);
                    UserSessionManager.CreateSalesReportPdf(filename, address, companyname, pdfheadertext, salesReportArrayList, vList, pList);
                    openPdfExcel(filepath, "pdf");

                }
            }, 300);
        }

    }

    //excel button click
    public void OnSalesReportExcelClick(View view) {
        /*if (ContextCompat.checkSelfPermission(SalesReportActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SalesReportActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, ReqReadPermission);
        } else if (ContextCompat.checkSelfPermission(SalesReportActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SalesReportActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, ReqWritePermission);
        } else {*/
        if (Constant.CheckReadWritePermissionGranted(SalesReportActivity.this)) {
            lytprogress.setVisibility(View.VISIBLE);
            btnexcel.setEnabled(false);
            btnpdf.setEnabled(false);
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    writeStudentsListToExcel();
                }
            }, 300);
        }
    }

    //process for excel file creation
    public void writeStudentsListToExcel() {
        try {

            File FILE_PATH = Constant.FilePathPdfExcel(true, ".xlsx", SalesReportActivity.this, filename);
            XSSFWorkbook workbook = new XSSFWorkbook();

            XSSFSheet studentsSheet = workbook.createSheet("SalesReport");
            studentsSheet.getPrintSetup().setPaperSize(PrintSetup.A4_PAPERSIZE);
            studentsSheet.setHorizontallyCenter(true);

            Footer footer = studentsSheet.getFooter();
            footer.setRight("Page " + HeaderFooter.page() + " of " + HeaderFooter.numPages());

            studentsSheet.setColumnWidth(0, (15 * 200));
            studentsSheet.setColumnWidth(1, (15 * 500));
            studentsSheet.setColumnWidth(2, (15 * 200));
            studentsSheet.setColumnWidth(3, (15 * 200));
            studentsSheet.setColumnWidth(4, (15 * 300));

            int rowIndex = 0, nextrow = 0;
            //Row row = studentsSheet.createRow(rowIndex++);
            int cellIndex = 0;
            CellStyle style = workbook.createCellStyle();

            style.setBorderBottom(CellStyle.BORDER_THIN);
            style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            style.setBorderLeft(CellStyle.BORDER_THIN);
            style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            style.setBorderRight(CellStyle.BORDER_THIN);
            style.setRightBorderColor(IndexedColors.BLACK.getIndex());
            style.setBorderTop(CellStyle.BORDER_THIN);
            style.setTopBorderColor(IndexedColors.BLACK.getIndex());

            CellStyle borderStyle = workbook.createCellStyle();
            borderStyle.setBorderBottom(CellStyle.BORDER_THIN);
            borderStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            borderStyle.setBorderLeft(CellStyle.BORDER_THIN);
            borderStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            borderStyle.setBorderRight(CellStyle.BORDER_THIN);
            borderStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            borderStyle.setBorderTop(CellStyle.BORDER_THIN);
            borderStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
            ((XSSFCellStyle) borderStyle).setAlignment(HorizontalAlignment.CENTER);
            ((XSSFCellStyle) borderStyle).setVerticalAlignment(VerticalAlignment.CENTER);

            CellStyle rightborderStyle = workbook.createCellStyle();
            rightborderStyle.setBorderBottom(CellStyle.BORDER_THIN);
            rightborderStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            rightborderStyle.setBorderLeft(CellStyle.BORDER_THIN);
            rightborderStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            rightborderStyle.setBorderRight(CellStyle.BORDER_THIN);
            rightborderStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            rightborderStyle.setBorderTop(CellStyle.BORDER_THIN);
            rightborderStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
            ((XSSFCellStyle) rightborderStyle).setAlignment(HorizontalAlignment.RIGHT);
            ((XSSFCellStyle) rightborderStyle).setVerticalAlignment(VerticalAlignment.CENTER);

            CellStyle leftborderStyle = workbook.createCellStyle();
            leftborderStyle.setBorderBottom(CellStyle.BORDER_THIN);
            leftborderStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            leftborderStyle.setBorderLeft(CellStyle.BORDER_THIN);
            leftborderStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            leftborderStyle.setBorderRight(CellStyle.BORDER_THIN);
            leftborderStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            leftborderStyle.setBorderTop(CellStyle.BORDER_THIN);
            leftborderStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
            ((XSSFCellStyle) leftborderStyle).setAlignment(HorizontalAlignment.LEFT);
            ((XSSFCellStyle) leftborderStyle).setVerticalAlignment(VerticalAlignment.CENTER);

            XSSFFont font = workbook.createFont();
            font.setBold(true);
            style.setFont(font);
            ((XSSFCellStyle) style).setAlignment(HorizontalAlignment.CENTER);

            //row.createCell(cellIndex);
            CellStyle cs = workbook.createCellStyle();
            cs.setFont(font);
            ((XSSFCellStyle) cs).setAlignment(HorizontalAlignment.CENTER);
            ((XSSFCellStyle) cs).setVerticalAlignment(VerticalAlignment.CENTER);
            cs.setWrapText(true);

            XSSFFont comFont = workbook.createFont();
            comFont.setFontHeightInPoints((short) 20);
            CellStyle cellstyle = workbook.createCellStyle();
            cellstyle.setFont(comFont);
            ((XSSFCellStyle) cellstyle).setAlignment(HorizontalAlignment.CENTER);

            Row row = studentsSheet.createRow(rowIndex);
            row.createCell(cellIndex).setCellValue(companyname);
            row.getCell(cellIndex).setCellStyle(cellstyle);
            studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 4));

            rowIndex++;
            cellIndex = 0;

            Row addrow = studentsSheet.createRow(rowIndex);
            addrow.createCell(cellIndex).setCellValue(address);
            addrow.getCell(cellIndex).setCellStyle(cs);

            studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex + 3, 0, 4));

            rowIndex = rowIndex + 4;

            cellIndex = 0;

            Drawing drawing = studentsSheet.createDrawingPatriarch();
            CreationHelper creationHelper = workbook.getCreationHelper();
            ClientAnchor anchor = creationHelper.createClientAnchor();
            anchor.setDx1(0);
            anchor.setCol1(1);
            anchor.setRow1(rowIndex);
            anchor.setRow2(rowIndex);
            anchor.setCol2(3);
            XSSFDrawing xssfdrawing = (XSSFDrawing) drawing;
            XSSFClientAnchor xssfanchor = (XSSFClientAnchor) anchor;
            XSSFSimpleShape xssfshape = xssfdrawing.createSimpleShape(xssfanchor);
            xssfshape.setShapeType(ShapeTypes.LINE);
            xssfshape.setLineWidth(1);
            xssfshape.setLineStyle(0);
            xssfshape.setLineStyleColor(0, 0, 0);

            Row daterow = studentsSheet.createRow(rowIndex);
            daterow.createCell(cellIndex).setCellValue("SalesReport of " + vouchertypename + " from " + sdt + " to " + edt);
            daterow.getCell(cellIndex).setCellStyle(cs);

            studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 4));

            rowIndex++;
            Row blankrow = studentsSheet.createRow(rowIndex);
            rowIndex++;
            Row blankrow1 = studentsSheet.createRow(rowIndex);

            studentsSheet.setRepeatingRows(CellRangeAddress.valueOf("1:" + rowIndex));

            rowIndex++;

            nextrow = rowIndex;
            if (recycleview.getVisibility() == View.VISIBLE) {

                cellIndex = 0;
                Row sdatarow = studentsSheet.createRow(rowIndex++);
                sdatarow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.date));
                sdatarow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.party_name));
                sdatarow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.type));
                sdatarow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.vch_no));
                sdatarow.createCell(cellIndex).setCellValue(getResources().getString(R.string.amount));

                sdatarow.getCell(0).setCellStyle(style);
                sdatarow.getCell(1).setCellStyle(style);
                sdatarow.getCell(2).setCellStyle(style);
                sdatarow.getCell(3).setCellStyle(style);
                sdatarow.getCell(4).setCellStyle(style);

                for (SalesReport salesReport : salesReportArrayList) {
                    cellIndex = 0;
                    Row recrow = studentsSheet.createRow(rowIndex++);
                    recrow.createCell(cellIndex++).setCellValue(salesReport.getTrdate2());
                    recrow.createCell(cellIndex++).setCellValue(salesReport.getPartyname());
                    recrow.createCell(cellIndex++).setCellValue(salesReport.getTypename());
                    recrow.createCell(cellIndex++).setCellValue(salesReport.getVchno());
                    recrow.createCell(cellIndex).setCellValue(salesReport.getAmount());

                    recrow.getCell(0).setCellStyle(borderStyle);
                    recrow.getCell(1).setCellStyle(leftborderStyle);
                    recrow.getCell(2).setCellStyle(borderStyle);
                    recrow.getCell(3).setCellStyle(borderStyle);
                    recrow.getCell(4).setCellStyle(rightborderStyle);
                }

            }

            if (lytpayment.getVisibility() == View.VISIBLE) {
                cellIndex = 0;
                Row prow = studentsSheet.createRow(rowIndex);
                prow.createCell(cellIndex).setCellValue(getResources().getString(R.string.paymentwise_summary));
                prow.getCell(cellIndex).setCellStyle(style);
                prow.createCell(1).setCellStyle(borderStyle);
                prow.createCell(2).setCellStyle(borderStyle);
                prow.createCell(3).setCellStyle(borderStyle);
                prow.createCell(4).setCellStyle(borderStyle);
                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 4));
                rowIndex++;

                cellIndex = 0;
                Row pdatarow = studentsSheet.createRow(rowIndex);
                pdatarow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.payment));
                pdatarow.createCell(cellIndex++);
                pdatarow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.no_s));
                pdatarow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.amount));
                pdatarow.createCell(cellIndex);

                pdatarow.getCell(0).setCellStyle(style);
                pdatarow.getCell(1).setCellStyle(style);
                pdatarow.getCell(2).setCellStyle(style);
                pdatarow.getCell(3).setCellStyle(style);
                pdatarow.getCell(4).setCellStyle(style);

                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 1));
                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 3, 4));


                rowIndex++;

                for (int i = 0; i < pList.size(); i++) {
                    SalesReport salesReport = pList.get(i);
                    cellIndex = 0;
                    Row recrow = studentsSheet.createRow(rowIndex);
                    recrow.createCell(cellIndex++).setCellValue(salesReport.getName());
                    recrow.createCell(cellIndex++);
                    recrow.createCell(cellIndex++).setCellValue(salesReport.getNo() + "");
                    recrow.createCell(cellIndex++).setCellValue(Constant.ConvertToIndianRupeeFormat(salesReport.getDoubleamount(), false, true, false));
                    recrow.createCell(cellIndex);

                    if (i == pList.size() - 1) {
                        for (int j = 0; j < 5; j++)
                            recrow.getCell(j).setCellStyle(style);
                    } else {
                        recrow.getCell(0).setCellStyle(leftborderStyle);
                        recrow.getCell(1).setCellStyle(leftborderStyle);
                        recrow.getCell(2).setCellStyle(borderStyle);
                        recrow.getCell(3).setCellStyle(rightborderStyle);
                        recrow.getCell(4).setCellStyle(rightborderStyle);
                    }

                    studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 1));
                    studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 3, 4));
                    rowIndex++;
                }

            }

            if (lytvoucher.getVisibility() == View.VISIBLE) {
                cellIndex = 0;
                Row prow = studentsSheet.createRow(rowIndex);
                prow.createCell(cellIndex).setCellValue(getResources().getString(R.string.voucherwise_summary));
                prow.getCell(cellIndex).setCellStyle(style);
                prow.createCell(1).setCellStyle(borderStyle);
                prow.createCell(2).setCellStyle(borderStyle);
                prow.createCell(3).setCellStyle(borderStyle);
                prow.createCell(4).setCellStyle(borderStyle);
                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 4));
                rowIndex++;

                cellIndex = 0;
                Row pdatarow = studentsSheet.createRow(rowIndex);
                pdatarow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.payment));
                pdatarow.createCell(cellIndex++);
                pdatarow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.no_s));
                pdatarow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.amount));
                pdatarow.createCell(cellIndex);

                pdatarow.getCell(0).setCellStyle(style);
                pdatarow.getCell(1).setCellStyle(style);
                pdatarow.getCell(2).setCellStyle(style);
                pdatarow.getCell(3).setCellStyle(style);
                pdatarow.getCell(4).setCellStyle(style);

                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 1));
                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 3, 4));


                rowIndex++;

                for (int i = 0; i < vList.size(); i++) {
                    SalesReport salesReport = vList.get(i);
                    cellIndex = 0;
                    Row recrow = studentsSheet.createRow(rowIndex);
                    recrow.createCell(cellIndex++).setCellValue(salesReport.getName());
                    recrow.createCell(cellIndex++);
                    recrow.createCell(cellIndex++).setCellValue(salesReport.getNo() + "");
                    recrow.createCell(cellIndex++).setCellValue(Constant.ConvertToIndianRupeeFormat(salesReport.getDoubleamount(), false, true, false));
                    recrow.createCell(cellIndex);

                    if (i == vList.size() - 1) {
                        for (int j = 0; j < 5; j++)
                            recrow.getCell(j).setCellStyle(style);
                    } else {
                        recrow.getCell(0).setCellStyle(leftborderStyle);
                        recrow.getCell(1).setCellStyle(leftborderStyle);
                        recrow.getCell(2).setCellStyle(borderStyle);
                        recrow.getCell(3).setCellStyle(rightborderStyle);
                        recrow.getCell(4).setCellStyle(rightborderStyle);
                    }

                    studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 1));
                    studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 3, 4));
                    rowIndex++;
                }

            }


            try {
                FileOutputStream fos = new FileOutputStream(FILE_PATH);
                workbook.write(fos);
                fos.close();

                openPdfExcel(FILE_PATH, "excel");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                databaseHelper.addFileNotFoundERROR(e, subject);
            } catch (IOException e) {
                e.printStackTrace();
                databaseHelper.addIOERROR(e, subject);
            }
        } catch (Exception e) {
            e.printStackTrace();
            databaseHelper.addExceptionERROR(e, subject);
        }
    }

    //open created pdf/excel file
    private void openPdfExcel(File file, String type) {
        lytprogress.setVisibility(View.GONE);

        Constant.openPdfExcel(file, type, SalesReportActivity.this);
        btnexcel.setEnabled(true);
        btnpdf.setEnabled(true);
    }

    public interface OnItemClickListener {
        void onItemClick(HashMap<String, String> item);
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        //private ArrayList<HashMap<String,String>> mDataset;

        public ArrayList<SalesReport> mDataset;

        private OnItemClickListener listener;

        // for load more
        private final int VIEW_TYPE_ITEM = 0;
        private final int VIEW_TYPE_LOADING = 1;
        private OnLoadMoreListener onLoadMoreListener;

        // The minimum amount of items to have below your current scroll position
        // before loading more.
        private boolean isLoading;
        private int visibleThreshold = 5;
        private int lastVisibleItem, totalItemCount;


        public void add(int position, SalesReport item) {
            mDataset.add(position, item);
            notifyItemInserted(position);
        }

        public void remove(SalesReport item) {
            int position = mDataset.indexOf(item);
            mDataset.remove(position);
            notifyItemRemoved(position);
        }

        public void clear() {
            int size = mDataset.size();
            if (size != 0) {
                mDataset.clear();
                notifyItemRangeRemoved(0, size);
                lastVisibleItem = 0;
                totalItemCount = 0;
                setLoaded();
            }
        }

        // Provide a suitable constructor (depends on the kind of dataset)
        public RecyclerViewAdapter(ArrayList<SalesReport> myDataset, RecyclerView recyclerView) {

            mDataset = myDataset;

            // load more
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        isLoading = true;
                    }
                }
            });
        }

        // Create new views (invoked by the layout manager)
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == VIEW_TYPE_ITEM) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lyt_salesreport, parent, false);
                return new ViewHolderRow(view);
            } else if (viewType == VIEW_TYPE_LOADING) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_progress, parent, false);
                return new ViewHolderLoading(view);
            }
            return null;
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            // - get element from your dataset at this position
            // - replace the contents of the view with that element

            if (holder instanceof ViewHolderRow) {
                ViewHolderRow userViewHolder = (ViewHolderRow) holder;
                final SalesReport model = mDataset.get(position);


                userViewHolder.txtdate.setText(model.getTrdate2());
                userViewHolder.txttype.setText(model.getTypename());
                userViewHolder.txtvchno.setText(Html.fromHtml("<u><i>" + model.getVchno() + "</i></u>"));

                String data = model.getAmount();
                userViewHolder.txtamount.setText(data);
                int size;
                if (data.length() <= 10) {
                    size = 12;
                } else if (data.length() <= 14) {
                    size = 10;
                } else if (data.length() <= 16) {
                    size = 9;
                } else {
                    size = 8;
                }
                userViewHolder.txtamount.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);

                userViewHolder.txtvchno.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //System.out.println("=================== = = " + model.getVchcode() + " = " + model.getVouchertypecode());
                        if (!model.getVchcode().equals("") && !model.getVouchertypecode().equals("")) {
                            String vchno = model.getVchno().replaceAll("[^0-9]", "").trim();
                            startActivity(new Intent(SalesReportActivity.this, VoucherInfoActivity.class).putExtra("from", "salesreport").putExtra("code", model.getVchcode()).putExtra("vno", vchno).putExtra("typecode", model.getVouchertypecode()));
                        }
                    }
                });


                String name = model.getPartyname();
                if (model.getPartyname() == null || name.equals(""))
                    name = getResources().getString(R.string.nodata_updatemaster);

                userViewHolder.txtname.setText(Html.fromHtml(name));

                userViewHolder.txtname.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (model.getPartyname() == null || model.getPartyname().equals("")) {
                            session.OnUpdateMasterClick(new VolleyCallback() {
                                @Override
                                public void onSuccess(boolean result) {
                                    if (result)
                                        OnShowSalesReportClick(lyttop.getRootView());
                                }

                                @Override
                                public void onSuccessWithMsg(boolean result, String message) {
                                }
                            }, SalesReportActivity.this, session, orgcode, compcode, databaseHelper, subject, findViewById(R.id.activity_main));

                        }
                    }
                });
            } else if (holder instanceof ViewHolderLoading) {
                ViewHolderLoading loadingViewHolder = (ViewHolderLoading) holder;
                loadingViewHolder.progressBar.setIndeterminate(true);
            }

        }

        // Return the size of your dataset (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return mDataset == null ? 0 : mDataset.size();
        }

        public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
            this.onLoadMoreListener = mOnLoadMoreListener;
        }

        public void setOnItemListener(OnItemClickListener listener) {
            this.listener = listener;
        }

        @Override
        public int getItemViewType(int position) {
            return mDataset.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
        }

        public void setLoaded() {
            isLoading = false;
        }

        private class ViewHolderLoading extends RecyclerView.ViewHolder {
            public ProgressBar progressBar;

            public ViewHolderLoading(View view) {
                super(view);
                progressBar = (ProgressBar) view.findViewById(R.id.loadmore_progress);
            }
        }


        public class ViewHolderRow extends RecyclerView.ViewHolder {
            public TextView txtdate, txtname, txttype, txtvchno, txtamount;

            public ViewHolderRow(View v) {
                super(v);
                txtdate = itemView.findViewById(R.id.txtdate);
                txtname = itemView.findViewById(R.id.txtname);
                txttype = itemView.findViewById(R.id.txttype);
                txtvchno = itemView.findViewById(R.id.txtvchno);
                txtamount = itemView.findViewById(R.id.txtamount);
            }

            /*public void bind(final HashMap<String,String> item, final OnItemClickListener listener) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onItemClick(item);
                    }
                });
            }*/
        }

    }
}
