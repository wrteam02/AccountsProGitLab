package com.accountspro.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import com.accountspro.helper.HomeMenuMethods;
import com.accountspro.model.HomeMenu;
import com.google.android.material.navigation.NavigationView;

import androidx.core.content.res.ResourcesCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Configuration;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;

import android.text.Html;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.accountspro.R;
import com.accountspro.helper.AppController;
import com.accountspro.helper.Constant;
import com.accountspro.helper.DatabaseHelper;
import com.accountspro.helper.MyBounceInterpolator;
import com.accountspro.helper.UserSessionManager;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class OwnerHomeActivity extends AppCompatActivity {

    static UserSessionManager session;
    static Activity activity;
    Toolbar toolbar;
    DrawerLayout drawer;
    NavigationView navigationView;
    static TextView txtdemo, txtoffline;
    TextView txtbottom;
    TextView txtcompany;
    TextView txtdrawerheader;
    static DatabaseHelper databaseHelper;
    int[] imglistid = {1, 4, 5};
    Animation myAnim;
    MyBounceInterpolator interpolator;
    static String compcode, orgcode;
    ProgressBar progressbar;
    static GridView gridview;
    static ArrayList<HomeMenu> homeMenuArrayList;
    String subject;
    static HomeMenuMethods homeMenuMethods;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        activity = OwnerHomeActivity.this;
        session = new UserSessionManager(OwnerHomeActivity.this);
        databaseHelper = new DatabaseHelper(OwnerHomeActivity.this);

        orgcode = session.getData(UserSessionManager.KEY_ORG_REGKEY);
        compcode = session.getData(orgcode + UserSessionManager.KEY_COM_CODE);
        subject = Constant.ERRORID + " | " + session.getData(UserSessionManager.KEY_Mobile) + " | " + session.getData(UserSessionManager.KEY_ORG_NAME) + " | " + session.getData(orgcode + UserSessionManager.KEY_COM_NAME);

        try {
            homeMenuMethods = new HomeMenuMethods(OwnerHomeActivity.this, session, orgcode, compcode, databaseHelper, subject, findViewById(R.id.snackbarid));


            //System.out.println("==========com code " + session.getData(UserSessionManager.KEY_Mobile) + " = " + session.getData(UserSessionManager.KEY_IMEI));

            myAnim = AnimationUtils.loadAnimation(this, R.anim.btn_clickanimation);
            interpolator = new MyBounceInterpolator(0.2, 20);
            myAnim.setInterpolator(interpolator);

            gridview = findViewById(R.id.gridview);
            progressbar = findViewById(R.id.progressbar);
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            navigationView = (NavigationView) findViewById(R.id.nav_view);
            drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            txtdemo = (TextView) findViewById(R.id.txtdemo);
            txtbottom = (TextView) findViewById(R.id.txtbottom);
            txtoffline = (TextView) findViewById(R.id.txtoffline);
            txtcompany = (TextView) findViewById(R.id.txtcompany);

            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);

            setupnavigationDrawer();

            View header = navigationView.getHeaderView(0);
            txtdrawerheader = (TextView) header.findViewById(R.id.header_name);


        /*String lastimg = session.getData(UserSessionManager.KEY_LASTIMG);
        if (!lastimg.equals("")) {
            int limg = getResources().getIdentifier("img" + lastimg.trim(), "id", this.getPackageName());
            ImageView imageViewl = (ImageView) findViewById(limg);
            imageViewl.setColorFilter(getResources().getColor(R.color.black));
        }*/

            txtdemo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder alertcleardialog = new AlertDialog.Builder(OwnerHomeActivity.this);
                    alertcleardialog.setTitle(getResources().getString(R.string.app_name));
                    if (session.getData(UserSessionManager.KEY_DEMO).equalsIgnoreCase("Yes"))
                        alertcleardialog.setMessage(getResources().getString(R.string.demotext));
                    else {

                        View view = getLayoutInflater().inflate(R.layout.lyt_userinfo, null);

                        TextView txtuname = (TextView) view.findViewById(R.id.txtusername);
                        //txtuname.setPaintFlags(txtuname.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                        txtuname.setText(session.getData(UserSessionManager.KEY_USERNAME));

                        TextView txtfrom = (TextView) view.findViewById(R.id.txtfromdate);
                        txtfrom.setText(session.getData(UserSessionManager.KEY_STARTDATE));

                        TextView txtto = (TextView) view.findViewById(R.id.txttodate);
                        txtto.setText(session.getData(UserSessionManager.KEY_ENDDATE));

                        TextView txtimei = (TextView) view.findViewById(R.id.txtimei);
                        txtimei.setText(session.getData(UserSessionManager.KEY_DEVICEIMEI));

                        TextView txtfees = (TextView) view.findViewById(R.id.txtfees);
                        txtfees.setText(session.getData(UserSessionManager.KEY_YEARLYFEE));


                        alertcleardialog.setView(view);

                    /*StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("<b>User Name :\t\t\t\t\t\t\t\t\t</b><u>"+session.getData(UserSessionManager.KEY_USERNAME)+"</u><br>");
                    stringBuilder.append("<b>Valid From Date :\t\t\t</b>"+session.getData(UserSessionManager.KEY_STARTDATE)+"<br>");
                    stringBuilder.append("<b>Valid To Date :\t\t\t\t\t</b>"+session.getData(UserSessionManager.KEY_ENDDATE)+"<br>");
                    stringBuilder.append("<b>Device IMEI :\t\t\t\t\t\t\t</b>"+session.getData(UserSessionManager.KEY_DEVICEIMEI)+"<br>");
                    stringBuilder.append("<b>Subscription Fees :\t</b>"+session.getData(UserSessionManager.KEY_YEARLYFEE));
                    alertcleardialog.setMessage(Html.fromHtml(stringBuilder.toString()));*/
                    }

                    alertcleardialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    alertcleardialog.show();
                }
            });


            if (!session.getData(UserSessionManager.KEY_DEMO).equalsIgnoreCase("Yes")) {
                txtdemo.setText(Html.fromHtml("<u>" + session.getData(UserSessionManager.KEY_USERNAME) + "</u>"));
                if (AppController.isNetConnected(OwnerHomeActivity.this)) {
                    SetOfflineVisibility(false);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.UserInfoUrl + session.getData(UserSessionManager.KEY_DEVICECODE), new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            //System.out.println("===================  " + response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                session.setData(UserSessionManager.KEY_USERNAME, jsonObject.getString("username"));
                                session.setData(UserSessionManager.KEY_STARTDATE, jsonObject.getString("startdate"));
                                session.setData(UserSessionManager.KEY_ENDDATE, jsonObject.getString("expdate"));
                                session.setData(UserSessionManager.KEY_DEVICEIMEI, jsonObject.getString("deviceimei"));
                                session.setData(UserSessionManager.KEY_YEARLYFEE, jsonObject.getString("yearlyfees"));
                                txtdemo.setText(Html.fromHtml("<u>" + jsonObject.getString("username") + "</u>"));
                            } catch (Exception e) {
                                e.printStackTrace();
                                String subject = Constant.ERRORID + " | " + session.getData(UserSessionManager.KEY_Mobile) + " | " + session.getData(UserSessionManager.KEY_ORG_NAME) + " | " + session.getData(orgcode + UserSessionManager.KEY_COM_NAME);
                                databaseHelper.addExceptionERROR(e, subject);
                            }

                        }

                    },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                    String message = Constant.VolleyErrorMessage(error);
                           /* if (!message.equals(""))
                                Toast.makeText(OwnerHomeActivity.this, message, Toast.LENGTH_SHORT).show();*/

                                }
                            });
                    AppController.getInstance().getRequestQueue().getCache().clear();
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    AppController.getInstance().addToRequestQueue(stringRequest);
                } else {
                    SetOfflineVisibility(true);
                }
            } else {
                txtdemo.setText("Request : " + session.getIntData(UserSessionManager.KEY_DEMOREQCOUNT) + "/" + session.getIntData(UserSessionManager.KEY_DEMOREQTOTAL));
            }
        } catch (Exception e) {
            e.printStackTrace();

            databaseHelper.addExceptionERROR(e, subject);
        }
    }

    public static void setGridDetails() {
        //function defined in helper folder's HomeMenuMethods file
        homeMenuArrayList = new ArrayList<>();
        homeMenuArrayList.add(new HomeMenu(activity.getResources().getString(R.string.business_view), "OnBusinessviewClick", R.drawable.businessview));
        homeMenuArrayList.add(new HomeMenu(activity.getResources().getString(R.string.receivble), "OnReceivbleClick", R.drawable.receivable));
        homeMenuArrayList.add(new HomeMenu(activity.getResources().getString(R.string.ledger_report), "OnLedgerReportClick", R.drawable.ledgerreport));
        homeMenuArrayList.add(new HomeMenu(activity.getResources().getString(R.string.sales_report), "OnSalesReportClick", R.drawable.salesreport));
        homeMenuArrayList.add(new HomeMenu(activity.getResources().getString(R.string.stock_status), "OnStockClick", R.drawable.stockstatus));
        homeMenuArrayList.add(new HomeMenu(activity.getResources().getString(R.string.voucherinfo), "OnVoucherInfoClick", R.drawable.voucherinfo));
        homeMenuArrayList.add(new HomeMenu(activity.getResources().getString(R.string.update_masters), "OnUpdateMasterClick", R.drawable.updatemaster));
        gridview.setAdapter(new HomeMenuAdapter(homeMenuArrayList));
    }

    private void setupnavigationDrawer() {

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                drawer.closeDrawers();
                Intent intent;
                switch (menuItem.getItemId()) {
                    case R.id.menu_privacy:
                        intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(getResources().getString(R.string.privacyurl)));
                        startActivity(intent);
                        break;
                    case R.id.menu_terms:
                        intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(getResources().getString(R.string.termsurl)));
                        startActivity(intent);
                        break;
                    case R.id.menu_logout:
                        try {
                            session.logoutUser();
                            finish();
                        } catch (Exception e) {
                        }
                        Toast.makeText(OwnerHomeActivity.this, "Logout Successfully..!!", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.menu_company:
                        startActivity(new Intent(OwnerHomeActivity.this, CompanyActivity.class).putExtra("from", "home"));
                        break;
                    case R.id.menu_organization:
                        startActivity(new Intent(OwnerHomeActivity.this, OrganizationActivity.class).putExtra("from", "home"));
                        break;
                    case R.id.menu_clearall:
                        final AlertDialog.Builder alertcleardialog = new AlertDialog.Builder(OwnerHomeActivity.this);
                        alertcleardialog.setTitle("Clear All");
                        alertcleardialog.setMessage("This will clear all reports data saved in your device !!\nAre you sure to clear data ?");
                        alertcleardialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        alertcleardialog.setPositiveButton("ClearAll", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                databaseHelper.ClearAllData();
                                Toast.makeText(OwnerHomeActivity.this, "Successfully Cleared All Data", Toast.LENGTH_SHORT).show();
                            }
                        });

                        alertcleardialog.show();

                        break;
                    case R.id.menu_changepin:
                        if (session.getBooleanData(UserSessionManager.KEY_PINSET)) {
                            final AlertDialog.Builder alertdialog = new AlertDialog.Builder(OwnerHomeActivity.this);
                            alertdialog.setTitle("Change PIN");
                            LinearLayout linearLayout = new LinearLayout(OwnerHomeActivity.this);
                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            layoutParams.setMargins(50, 0, 50, 0);
                            linearLayout.setLayoutParams(layoutParams);
                            linearLayout.setOrientation(LinearLayout.VERTICAL);
                            final EditText oldeditText = new EditText(OwnerHomeActivity.this);
                            oldeditText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                            oldeditText.setTypeface(ResourcesCompat.getFont(OwnerHomeActivity.this, R.font.robotoregular));
                            oldeditText.setHint("Enter Old PIN");
                            linearLayout.addView(oldeditText);

                            final EditText neweditText = new EditText(OwnerHomeActivity.this);
                            neweditText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                            neweditText.setTypeface(ResourcesCompat.getFont(OwnerHomeActivity.this, R.font.robotoregular));
                            neweditText.setHint("Enter New PIN");
                            linearLayout.addView(neweditText);
                            alertdialog.setView(linearLayout);
                            alertdialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                    imm.hideSoftInputFromWindow(oldeditText.getWindowToken(), 0);
                                    dialog.dismiss();
                                }
                            });

                            alertdialog.setCancelable(false);


                            alertdialog.setPositiveButton("Change PIN", null);
                            final AlertDialog dialog = alertdialog.create();
                            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                @Override
                                public void onShow(DialogInterface dialog1) {
                                    Button btnsave = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
                                    btnsave.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            String opin = oldeditText.getText().toString().trim();
                                            final String npin = neweditText.getText().toString().trim();

                                            if (opin.length() == 0)
                                                oldeditText.setError("Enter Old PIN");
                                            else if (npin.length() == 0)
                                                neweditText.setError("Enter New PIN");
                                            else if (!opin.equals(session.getData(UserSessionManager.KEY_PIN))) {
                                                oldeditText.setError("Old Pin not match with Current PIN");
                                            } else if (AppController.isNetConnected(OwnerHomeActivity.this)) {
                                                SetOfflineVisibility(false);
                                                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                                imm.hideSoftInputFromWindow(oldeditText.getWindowToken(), 0);
                                                progressbar.setVisibility(View.VISIBLE);
                                                StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.PINSETUrl + session.getData(UserSessionManager.KEY_Mobile) + "&pin=" + npin, new Response.Listener<String>() {
                                                    @Override
                                                    public void onResponse(String response1) {
                                                        progressbar.setVisibility(View.GONE);
                                                        if (response1.contains("\n"))
                                                            response1 = response1.replace("\n", "").trim();

                                                        //System.out.println("========== = =  =pin " + response1);

                                                        if (response1.equalsIgnoreCase("Yes")) {
                                                            session.setBooleanData(UserSessionManager.KEY_PINSET, true);
                                                            session.setData(UserSessionManager.KEY_PIN, npin);
                                                            session.setData(UserSessionManager.KEY_LoginType, UserSessionManager.OWNER);
                                                            databaseHelper.UpdateUserdata(session.getData(UserSessionManager.KEY_Mobile), npin);
                                                            Toast.makeText(OwnerHomeActivity.this, "PIN Successfully set", Toast.LENGTH_SHORT).show();
                                                            startActivity(new Intent(OwnerHomeActivity.this, OwnerLoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                                        } else {
                                                            Toast.makeText(OwnerHomeActivity.this, "Sorry Your PIN not set,Try Again", Toast.LENGTH_SHORT).show();
                                                        }

                                                    }

                                                },
                                                        new Response.ErrorListener() {
                                                            @Override
                                                            public void onErrorResponse(VolleyError error) {
                                                                progressbar.setVisibility(View.GONE);
                                                                String message = Constant.VolleyErrorMessage(error);
                                                                if (!message.equals(""))
                                                                    Toast.makeText(OwnerHomeActivity.this, message, Toast.LENGTH_SHORT).show();

                                                            }
                                                        });
                                                AppController.getInstance().getRequestQueue().getCache().clear();
                                                AppController.getInstance().addToRequestQueue(stringRequest);
                                            } else {
                                                SetOfflineVisibility(true);
                                            }

                                        }
                                    });
                                }
                            });
                            dialog.show();
                            oldeditText.requestFocus();
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                        } else {
                            Toast.makeText(OwnerHomeActivity.this, "You are not registered with this device", Toast.LENGTH_SHORT).show();
                        }
                        break;

                }
                return true;
            }
        });

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        drawer.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    private static void SetOfflineVisibility(boolean b) {
        if (b)
            txtoffline.setVisibility(View.VISIBLE);
        else
            txtoffline.setVisibility(View.GONE);
    }


    public void OnLogoutClick(View view) {
        try {
            session.logoutUser();
            finish();
        } catch (Exception e) {
        }
        Toast.makeText(OwnerHomeActivity.this, "Logout Successfully..!!", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //ChangeImageColor(0);

        setGridDetails();

        txtcompany.setText(session.getData(orgcode + UserSessionManager.KEY_COM_NAME));
        txtdrawerheader.setText(session.getData(UserSessionManager.KEY_ORG_NAME));
        txtbottom.setText("Last Login : " + session.getData(UserSessionManager.KEY_LastLogin));

        if (session.getData(UserSessionManager.KEY_DEMO).equalsIgnoreCase("Yes"))
            DemoReqCout();

        CheckError();
    }

    private void CheckError() {
        if (AppController.isNetConnected(OwnerHomeActivity.this)) {
            SetOfflineVisibility(false);

            HashMap<String, String> errorlist = databaseHelper.getERROR();

            Iterator it = errorlist.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();

                String url = "";

                try {
                    url = Constant.ErrorReportUrl + URLEncoder.encode(String.valueOf(pair.getValue()), "UTF-8") + "&subject=" + URLEncoder.encode(String.valueOf(pair.getKey()), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();

                    String subject = Constant.ERRORID + " | " + session.getData(UserSessionManager.KEY_Mobile) + " | " + session.getData(UserSessionManager.KEY_ORG_NAME) + " | " + session.getData(orgcode + UserSessionManager.KEY_COM_NAME);
                    databaseHelper.addUnSuEncodingERROR(e, subject);
                }

                //System.out.println("===============err url " + url);
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response.contains("\n"))
                            response = response.replace("\n", "").trim();


                        //System.out.println("===============err res " + response);

                    }

                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                String message = Constant.VolleyErrorMessage(error);
                                /*if (!message.equals(""))
                                    Toast.makeText(OwnerHomeActivity.this, message, Toast.LENGTH_SHORT).show();*/

                            }
                        });
                AppController.getInstance().getRequestQueue().getCache().clear();
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(stringRequest);

            }

            if (errorlist.size() != 0)
                databaseHelper.DeleteData(DatabaseHelper.TABLE_ERROR, "");
        } else {
            SetOfflineVisibility(true);
        }
    }

    public static void DemoReqCout() {
        if (session.getData(UserSessionManager.KEY_DEMO).equalsIgnoreCase("Yes") && AppController.isNetConnected(activity)) {
            SetOfflineVisibility(false);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.DemoReqCountUrl + session.getData(UserSessionManager.KEY_Mobile) + "&imei=" + session.getData(UserSessionManager.KEY_IMEI), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (response.contains("\n"))
                        response = response.replace("\n", "").trim();

                    //System.out.println("==============demo " + response + " == " + session.getData(UserSessionManager.KEY_DEMO));
                    try {
                        String[] demo = response.split(";");
                        session.setIntData(UserSessionManager.KEY_DEMOREQCOUNT, Integer.parseInt(demo[0].trim()));
                        session.setIntData(UserSessionManager.KEY_DEMOREQTOTAL, Integer.parseInt(demo[1].trim()));
                        if (session.getData(UserSessionManager.KEY_DEMO).equalsIgnoreCase("Yes"))
                            txtdemo.setText("Request : " + demo[0] + "/" + demo[1]);
                        else
                            txtdemo.setText(R.string.registeruser);
                    } catch (Exception e) {
                        e.printStackTrace();

                        String subject = Constant.ERRORID + " | " + session.getData(UserSessionManager.KEY_Mobile) + " | " + session.getData(UserSessionManager.KEY_ORG_NAME) + " | " + session.getData(orgcode + UserSessionManager.KEY_COM_NAME);
                        databaseHelper.addExceptionERROR(e, subject);
                    }

                }

            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            String message = Constant.VolleyErrorMessage(error);
                           /* if (!message.equals(""))
                                Toast.makeText(OwnerHomeActivity.this, message, Toast.LENGTH_SHORT).show();*/

                        }
                    });
            AppController.getInstance().getRequestQueue().getCache().clear();
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(stringRequest);
        } else {
            SetOfflineVisibility(true);
        }
    }

    public static class HomeMenuAdapter extends BaseAdapter {
        ArrayList<HomeMenu> homeMenus;
        int iconcolor;
        int orange = activity.getResources().getColor(R.color.orange);
        int blue = activity.getResources().getColor(R.color.blue);


        public HomeMenuAdapter(ArrayList<HomeMenu> homeMenus) {
            this.homeMenus = homeMenus;
            //inflter = (LayoutInflater.from(getApplicationContext()));
        }

        @Override
        public int getCount() {
            return homeMenus.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {
            HomeMenu homeMenu = homeMenus.get(position);

            //view = inflter.inflate(R.layout.lyt_homemenu, null);
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.lyt_homemenu, viewGroup, false);

            ImageView icon = view.findViewById(R.id.image);
            TextView text = view.findViewById(R.id.text);
            LinearLayout layout = view.findViewById(R.id.layout);

            icon.setImageResource(homeMenu.getImage());
            text.setText(homeMenu.getName());


            if (position % 4 == 1 || position % 4 == 2)
                icon.setColorFilter(orange);
            else
                icon.setColorFilter(blue);


            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (position % 4 == 1 || position % 4 == 2)
                            layout.setBackgroundColor(orange);
                        else
                            layout.setBackgroundColor(blue);

                        icon.setColorFilter(activity.getResources().getColor(R.color.white));
                        text.setTextColor(activity.getResources().getColor(R.color.white));


                        homeMenuMethods.getClass().getDeclaredMethod(homeMenu.getFunname()).invoke(homeMenuMethods);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            });

            return view;
        }
    }


    /*
        public void OnUpdateMasterClick(View view) {
        if (AppController.isConnected(OwnerHomeActivity.this)) {
            final AlertDialog.Builder alertdialog = new AlertDialog.Builder(OwnerHomeActivity.this);
            alertdialog.setTitle("Update Masters");
            alertdialog.setMessage("Are you sure , You want to Update Masters ?");


            alertdialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ChangeImageColor(0);
                    dialog.dismiss();
                }
            });
            alertdialog.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    final ProgressDialog progressDialog = new ProgressDialog(OwnerHomeActivity.this);
                    progressDialog.setCancelable(false);
                    progressDialog.setMessage("Updating Masters...");
                    progressDialog.show();
                    System.out.println("============= =url =  " + Constant.GETMASTERNUrl + session.getData(UserSessionManager.KEY_ORG_REGKEY) + "&mobno=" + session.getData(UserSessionManager.KEY_Mobile) + "&imei=" + session.getData(UserSessionManager.KEY_IMEI) + "&compcode=" + session.getData(orgcode + UserSessionManager.KEY_COM_CODE) + "&devicecode=" + session.getData(UserSessionManager.KEY_DEVICECODE));
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.GETMASTERNUrl + session.getData(UserSessionManager.KEY_ORG_REGKEY) + "&compcode=" + session.getData(orgcode + UserSessionManager.KEY_COM_CODE) + "&mobno=" + session.getData(UserSessionManager.KEY_Mobile) + "&imei=" + session.getData(UserSessionManager.KEY_IMEI) + "&devicecode=" + session.getData(UserSessionManager.KEY_DEVICECODE) + "&demo=" + session.getData(UserSessionManager.KEY_DEMO), new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if (response.contains("\n"))
                                response = response.replace("\n", "").trim();

                            if (response.contains("NOREP")) {
                                Toast.makeText(OwnerHomeActivity.this, "Not Connected to server", Toast.LENGTH_SHORT).show();
                            } else if (response.contains("MULTIUSE")) {
                                Toast.makeText(OwnerHomeActivity.this, "This App is Already Running on other device", Toast.LENGTH_SHORT).show();
                            } else {

                                databaseHelper.DeleteMasterData(compcode, orgcode);
                                try {
                                    JSONArray jsonArray = new JSONArray(response);
                                    //System.out.println("========== = len - " + jsonArray.toString().getBytes() + " = " + jsonArray.toString().getBytes().length);

                                    for (int i = 1; i < jsonArray.length() - 1; i++) {
                                        JSONArray masterarray = jsonArray.getJSONArray(i);
                                        Master master = new Master(masterarray.getString(0), masterarray.getString(1), masterarray.getString(2), masterarray.getString(3), masterarray.getString(4), masterarray.getString(5), masterarray.getString(6), masterarray.getString(7), masterarray.getString(8), compcode, orgcode);
                                        databaseHelper.addMasterdata(master);
                                    }

                                    progressDialog.dismiss();
                                    Toast.makeText(OwnerHomeActivity.this, "Masters Successfully Updated", Toast.LENGTH_SHORT).show();

                                } catch (JSONException e) {
                                    e.printStackTrace();

                                    progressDialog.dismiss();
                                    if (e.toString().contains("NOREP"))
                                        Toast.makeText(OwnerHomeActivity.this, "Not Connected to Server", Toast.LENGTH_SHORT).show();
                                    else {
                                        String subject = Constant.ERRORID + " | " + session.getData(UserSessionManager.KEY_Mobile) + " | " + session.getData(UserSessionManager.KEY_ORG_NAME) + " | " + session.getData(orgcode + UserSessionManager.KEY_COM_NAME);
                                        databaseHelper.addJSONERROR(e, subject);
                                    }

                                }
                            }
                            ChangeImageColor(0);

                        }

                    },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    progressDialog.dismiss();
                                    ChangeImageColor(0);
                                    String message = "";
                                    if (error instanceof NetworkError) {
                                        message = "Cannot connect to Internet...Please check your connection!";
                                    } else if (error instanceof ServerError) {
                                        message = "The server could not be found. Please try again after some time!!";
                                    } else if (error instanceof AuthFailureError) {
                                        message = "Cannot connect to Internet...Please check your connection!";
                                    } else if (error instanceof ParseError) {
                                        message = "Parsing error! Please try again after some time!!";
                                    } else if (error instanceof NoConnectionError) {
                                        message = "Cannot connect to Internet...Please check your connection!";
                                    } else if (error instanceof TimeoutError) {
                                        message = "Connection TimeOut! Please check your internet connection.";
                                    }

                                    if (!message.equals(""))
                                        Toast.makeText(OwnerHomeActivity.this, message, Toast.LENGTH_SHORT).show();

                                }
                            });
                    AppController.getInstance().getRequestQueue().getCache().clear();
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    AppController.getInstance().addToRequestQueue(stringRequest);
                }
            });
            final AlertDialog dialog = alertdialog.create();
            dialog.show();
        }
        ChangeImageColor(7);
    }

    public void OnVoucherInfoClick(View view) {
        ChangeImageColor(6);
        startActivity(new Intent(OwnerHomeActivity.this, VoucherInfoActivity.class).putExtra("from", "home").putExtra("code", "0"));
    }

    public void OnStockClick(View view) {
        ChangeImageColor(5);
        startActivity(new Intent(OwnerHomeActivity.this, StockStatusActivity.class).putExtra("from", "home"));
    }

    public void OnSalesReportClick(View view) {
        ChangeImageColor(4);
        startActivity(new Intent(OwnerHomeActivity.this, SalesReportActivity.class).putExtra("from", "home"));
    }

    public void OnLedgerReportClick(View view) {
        ChangeImageColor(3);
        startActivity(new Intent(OwnerHomeActivity.this, LedgerReportActivity.class).putExtra("from", "home"));
    }

    public void OnReceivbleClick(View view) {
        ChangeImageColor(2);
        startActivity(new Intent(OwnerHomeActivity.this, ReceivbleActivity.class));
    }

    public void OnBusinessviewClick(View view) {
        ChangeImageColor(1);
        startActivity(new Intent(OwnerHomeActivity.this, BusinessViewActivity.class));
    }

    public void ChangeImageColor(int changeid) {

        String lastimg = session.getData(UserSessionManager.KEY_LASTIMG);
        if (!lastimg.equals("")) {
            ImageView imageViewl = (ImageView) findViewById(getResources().getIdentifier("img" + lastimg.trim(), "id", this.getPackageName()));
            if (ContainsItemFromList(Integer.parseInt(lastimg.trim())))
                imageViewl.setColorFilter(getResources().getColor(R.color.blue));
            else
                imageViewl.setColorFilter(getResources().getColor(R.color.orange));

            LinearLayout lyt = (LinearLayout) findViewById(getResources().getIdentifier("lyt" + lastimg.trim(), "id", this.getPackageName()));
            lyt.setBackgroundResource(R.drawable.bg_border);
            TextView txt = (TextView) findViewById(getResources().getIdentifier("txt" + lastimg.trim(), "id", this.getPackageName()));
            txt.setTextColor(getResources().getColor(R.color.black));
            lyt.setEnabled(true);
        }

        if (changeid != 0) {
            LinearLayout lyt = (LinearLayout) findViewById(getResources().getIdentifier("lyt" + changeid, "id", this.getPackageName()));
            if (ContainsItemFromList(changeid))
                lyt.setBackgroundResource(R.color.blue);
            else
                lyt.setBackgroundResource(R.color.orange);

            TextView txt = (TextView) findViewById(getResources().getIdentifier("txt" + changeid, "id", this.getPackageName()));
            txt.setTextColor(getResources().getColor(R.color.white));
            ImageView imageView = (ImageView) findViewById(getResources().getIdentifier("img" + changeid, "id", this.getPackageName()));
            imageView.setColorFilter(getResources().getColor(R.color.white));
            session.setData(UserSessionManager.KEY_LASTIMG, changeid + "");
            lyt.setEnabled(false);
        }
    }

    public boolean ContainsItemFromList(int inputStr) {
        for (int item : imglistid) {
            if (item == inputStr) {
                return true;
            }
        }
        return false;
    }
     */
}
