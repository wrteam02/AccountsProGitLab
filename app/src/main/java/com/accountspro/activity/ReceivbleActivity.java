package com.accountspro.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.text.Editable;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.accountspro.R;
import com.accountspro.adapter.AutoCompleteAdapter;
import com.accountspro.helper.AppController;
import com.accountspro.helper.Constant;
import com.accountspro.helper.DatabaseHelper;
import com.accountspro.helper.InstantAutoComplete;
import com.accountspro.helper.UserSessionManager;
import com.accountspro.helper.VolleyCallback;
import com.accountspro.model.Master;
import com.accountspro.model.Receivble;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.apache.poi.hssf.usermodel.HeaderFooter;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Footer;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.PrintSetup;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.ShapeTypes;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFSimpleShape;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.concurrent.TimeUnit;


public class ReceivbleActivity extends AppCompatActivity {

    public int ReqReadPermission = 1;
    public int ReqWritePermission = 2;
    Toolbar toolbar;
    InstantAutoComplete txtsearchvalue;
    Spinner sp_searchby;
    LinearLayout lytmain, lyttotal, lyttop, lytbottom, lytprogress, lytpdfexcel;
    TextView txtnodata, txtnetreceivable, txttotalcredit, txttotaldebit, txtasondate, datedisplay, txtsec;
    UserSessionManager session;
    DatabaseHelper databaseHelper;
    String subject, regionwisereqd, billwisereqd, asondatedisplay, headertext, companyname, compcode, orgcode, searchvaluename, searchbyval, path, filename, address, pdfheadertext, asondate = "", searchvalue = "";
    String defaultname = null, defaultcode = null;
    int searchbypos = 0;
    Master mastersearchvalue;
    ArrayList<Master> ledgernamelist, ledgergrouplist, ledgerregionlist;
    ImageView imgdown, imgclear;
    Button btnsync, btnshow;
    ArrayList<String> spsearchbyarray;
    SimpleDateFormat inFormatdate = new SimpleDateFormat("yyyyMMdd");
    ProgressBar progressbar;
    int nCounter = 0;
    private Handler mHandler = new Handler();
    RecyclerView recycleview;
    ArrayList<Receivble> receivbleArrayList, partynamelist, entrylist;
    LinkedHashMap<String, Receivble> receiblelist;
    //NumberFormat indianFormat = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
    ReceivbleAdapter adapter;
    ImageButton btnexcel, btnpdf;
    //SimpleDateFormat input = new SimpleDateFormat("yyyyMMdd");
    SimpleDateFormat output = new SimpleDateFormat("dd/MM/yy");
    AsyncTask<String, String, String> asyntask;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receivble);

        databaseHelper = new DatabaseHelper(ReceivbleActivity.this);
        session = new UserSessionManager(ReceivbleActivity.this);
        orgcode = session.getData(UserSessionManager.KEY_ORG_REGKEY);
        compcode = session.getData(orgcode + UserSessionManager.KEY_COM_CODE);
        companyname = session.getData(orgcode + UserSessionManager.KEY_COM_NAME);
        address = session.getData(orgcode + UserSessionManager.KEY_COM_ADDRESS);
        billwisereqd = session.getData(orgcode + UserSessionManager.KEY_COM_BILLWISEREQD);
        regionwisereqd = session.getData(orgcode + UserSessionManager.KEY_COM_REGIONREQD);

        subject = Constant.ERRORID + " | " + session.getData(UserSessionManager.KEY_Mobile) + " | " + session.getData(UserSessionManager.KEY_ORG_NAME) + " | " + companyname;

        try {
            ledgernamelist = databaseHelper.getMasterRowData(compcode, getResources().getString(R.string.master_ledger_name), orgcode);
            ledgergrouplist = databaseHelper.getMasterRowData(compcode, getResources().getString(R.string.master_ledger_group), orgcode);
            ledgerregionlist = databaseHelper.getMasterRowData(compcode, getResources().getString(R.string.master_ledger_region), orgcode);

            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);


            receivbleArrayList = new ArrayList<>();
            receiblelist = new LinkedHashMap<>();
            partynamelist = new ArrayList<>();
            entrylist = new ArrayList<>();


            btnpdf = findViewById(R.id.btnpdf);
            btnexcel = findViewById(R.id.btnexcel);
            recycleview = findViewById(R.id.recycleview);
            recycleview.setLayoutManager(new LinearLayoutManager(ReceivbleActivity.this));
            sp_searchby = findViewById(R.id.sp_searchby);
            btnsync = findViewById(R.id.btnsync);
            btnshow = findViewById(R.id.btnshow);
            progressbar = findViewById(R.id.progressbar);
            txtsec = findViewById(R.id.txtsec);
            datedisplay = findViewById(R.id.datedisplay);
            txtnodata = findViewById(R.id.txtnodata);

            spsearchbyarray = new ArrayList<>();

            spsearchbyarray.add(getResources().getString(R.string.ledger_name));
            spsearchbyarray.add(getResources().getString(R.string.ledger_group));

            if (regionwisereqd.equalsIgnoreCase("Yes") && ledgerregionlist.size() > 1)
                spsearchbyarray.add(getResources().getString(R.string.ledger_region));

            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spsearchbyarray);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sp_searchby.setAdapter(spinnerArrayAdapter);


            txtsearchvalue = findViewById(R.id.txtsearchvalue);
            lyttop = findViewById(R.id.lyttop);
            lyttotal = findViewById(R.id.lyttotal);
            lytmain = findViewById(R.id.lytmain);

            lytbottom = findViewById(R.id.lytbottom);
            lytprogress = findViewById(R.id.lytprogress);
            lytpdfexcel = findViewById(R.id.lytpdfexcel);
            txtasondate = findViewById(R.id.txtasondate);
            imgclear = findViewById(R.id.imgclear);
            imgdown = findViewById(R.id.imgdown);
            txttotaldebit = findViewById(R.id.txttotaldebit);
            txttotalcredit = findViewById(R.id.txttotalcredit);
            txtnetreceivable = findViewById(R.id.txtnetreceivable);

            final Calendar calendar = Calendar.getInstance();
            /*String dateString = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
            String monthString = String.valueOf(calendar.get(Calendar.MONTH) + 1);
            if (monthString.length() == 1) {
                monthString = "0" + monthString;
            }
            if (dateString.length() == 1) {
                dateString = "0" + dateString;
            }*/

            String[] datestring = Constant.GetMonthDateString(calendar).split(";");
            String dateString = datestring[0];
            String monthString = datestring[1];

            txtasondate.setText(dateString + "-" + monthString + "-" + calendar.get(Calendar.YEAR));
            asondate = calendar.get(Calendar.YEAR) + monthString + dateString;

        /*final ArrayAdapter<Master> nameadapter = new ArrayAdapter<Master>(ReceivbleActivity.this, R.layout.lyt_sptext, ledgernamelist);
        final ArrayAdapter<Master> groupadapter = new ArrayAdapter<Master>(ReceivbleActivity.this, R.layout.lyt_sptext, ledgergrouplist);
        final ArrayAdapter<Master> regionadapter = new ArrayAdapter<Master>(ReceivbleActivity.this, R.layout.lyt_sptext, ledgerregionlist);*/

            final AutoCompleteAdapter nameadapter = new AutoCompleteAdapter(ReceivbleActivity.this, R.layout.lyt_sptext, ledgernamelist);
            final AutoCompleteAdapter groupadapter = new AutoCompleteAdapter(ReceivbleActivity.this, R.layout.lyt_sptext, ledgergrouplist);
            final AutoCompleteAdapter regionadapter = new AutoCompleteAdapter(ReceivbleActivity.this, R.layout.lyt_sptext, ledgerregionlist);

            nameadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            groupadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            regionadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


            sp_searchby.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        Master defaultmaster = null;
                        if (sp_searchby.getSelectedItem().toString().equals(getResources().getString(R.string.ledger_name))) {
                            txtsearchvalue.setAdapter(nameadapter);
                            defaultmaster = ledgernamelist.get(0);

                            //defaultname = ledgernamelist.get(0).getMastername();
                            //defaultcode = ledgernamelist.get(0).getMastercode();
                        } else if (sp_searchby.getSelectedItem().toString().equals(getResources().getString(R.string.ledger_group))) {
                            txtsearchvalue.setAdapter(groupadapter);
                            defaultmaster = containNameCode(Constant.DefaultLedgerGroupSearchValue);
                            if (defaultmaster == null)
                                defaultmaster = ledgergrouplist.get(0);

                            //defaultname = ledgergrouplist.get(0).getMastername();
                            //defaultcode = ledgergrouplist.get(0).getMastercode();
                        } else if (sp_searchby.getSelectedItem().toString().equals(getResources().getString(R.string.ledger_region))) {
                            txtsearchvalue.setAdapter(regionadapter);
                            defaultmaster = ledgerregionlist.get(0);

                            //defaultname = ledgerregionlist.get(0).getMastername();
                            //defaultcode = ledgerregionlist.get(0).getMastercode();
                        }

                        searchbyval = sp_searchby.getSelectedItem().toString().trim();
                        searchbypos = position;
                        ((TextView) sp_searchby.getSelectedView()).setTextColor(getResources().getColor(R.color.white));

                        if (Integer.parseInt(session.getData("resby" + compcode + orgcode, "0")) == position && !session.getData("resval" + compcode + orgcode).equals("") && !session.getData("resvalname" + compcode + orgcode).equals("")) {
                            searchvalue = session.getData("resval" + compcode + orgcode);
                            txtsearchvalue.setText(session.getData("resvalname" + compcode + orgcode));
                        } else {
                    /*txtsearchvalue.setText("");
                    searchvalue = "";
                    searchvaluename = "";*/

                            mastersearchvalue = defaultmaster;
                            txtsearchvalue.setText(mastersearchvalue.getMastername());
                            searchvalue = mastersearchvalue.getMastercode();
                            searchvaluename = mastersearchvalue.getMastername();
                        }


                        BtnSyncVisibility();
                    } catch (Exception e) {
                        e.printStackTrace();
                        databaseHelper.addExceptionERROR(e, subject);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });


            txtasondate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new DatePickerDialog(ReceivbleActivity.this, pickerListenerfrom, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
                }
            });

            txtsearchvalue.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    //txtsearchvalue.requestFocus();
                    if (s.length() == 0) {
                        imgclear.setVisibility(View.GONE);
                    } else
                        imgclear.setVisibility(View.VISIBLE);
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });

            txtsearchvalue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    txtsearchvalue.requestFocus();
                    txtsearchvalue.showDropDown();
                }
            });

            txtsearchvalue.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        mastersearchvalue = (Master) parent.getItemAtPosition(position);
                        searchvalue = mastersearchvalue.getMastercode();
                        searchvaluename = mastersearchvalue.getMastername();

                        //System.out.println("==================== ==1 "+searchvalue+" = "+searchvaluename);

                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(txtsearchvalue.getWindowToken(), 0);

                        BtnSyncVisibility();
                    } catch (Exception e) {
                        e.printStackTrace();
                        databaseHelper.addExceptionERROR(e, subject);
                    }
                }
            });


            //System.out.println("==================== ==1 " + session.getData("resvalname" + compcode + orgcode) + " == " + session.getData("resval" + compcode + orgcode) + " = " + session.getData("resvalpos" + compcode + orgcode));

            try {
                if (session.getBooleanData("resdata" + compcode + orgcode)) {
                    if (!session.getData("resby" + compcode + orgcode).equals("")) {
                        searchbypos = Integer.parseInt(session.getData("resby" + compcode + orgcode, "0"));
                        sp_searchby.setSelection(searchbypos);
                        searchbyval = sp_searchby.getSelectedItem().toString().trim();
                    }
                    if (!session.getData("resdate" + compcode + orgcode).equals("")) {
                        asondate = session.getData("resdate" + compcode + orgcode);
                        txtasondate.setText(asondate.substring(6, 8) + "-" + asondate.substring(4, 6) + "-" + asondate.substring(0, 4));
                    }
                    if (!session.getData("resvalname" + compcode + orgcode).equals("")) {
                        searchvaluename = session.getData("resvalname" + compcode + orgcode);
                    }
                    if (!session.getData("resval" + compcode + orgcode).equals("") && !session.getData("resvalname" + compcode + orgcode).equals("")) {
                        searchvalue = session.getData("resval" + compcode + orgcode);
                        txtsearchvalue.setText(session.getData("resvalname" + compcode + orgcode));
                    } else {
                        txtsearchvalue.setText("");
                        searchvalue = "";
                        searchvaluename = "";
                    }


                } else {
                    imgdown.setVisibility(View.GONE);
                    sp_searchby.setSelection(1);

                    if (ledgergrouplist.size() == 0)
                        Toast.makeText(ReceivbleActivity.this, getResources().getString(R.string.updatemaster_msg), Toast.LENGTH_SHORT).show();

                    /*if (ledgergrouplist.size() != 0) {
                     *//* mastersearchvalue = containNameCode(Constant.DefaultLedgerGroupSearchValue);
                    if (mastersearchvalue == null)
                        mastersearchvalue = ledgergrouplist.get(0);

                    txtsearchvalue.setText(mastersearchvalue.getMastername());
                    searchvalue = mastersearchvalue.getMastercode();
                    searchvaluename = mastersearchvalue.getMastername();*//*
                } else
                    Toast.makeText(ReceivbleActivity.this, getResources().getString(R.string.updatemaster_msg), Toast.LENGTH_SHORT).show();
                */

                }
            } catch (Exception e) {
                e.printStackTrace();
                sp_searchby.setSelection(1);

                if (ledgergrouplist.size() == 0)
                    Toast.makeText(ReceivbleActivity.this, getResources().getString(R.string.updatemaster_msg), Toast.LENGTH_SHORT).show();

                /*if (ledgergrouplist.size() != 0) {
                 *//* mastersearchvalue = containNameCode(Constant.DefaultLedgerGroupSearchValue);
                if (mastersearchvalue == null)
                    mastersearchvalue = ledgergrouplist.get(0);

                txtsearchvalue.setText(mastersearchvalue.getMastername());
                searchvalue = mastersearchvalue.getMastercode();
                searchvaluename = mastersearchvalue.getMastername();*//*
            } else
                Toast.makeText(ReceivbleActivity.this, getResources().getString(R.string.updatemaster_msg), Toast.LENGTH_SHORT).show();
            */
            }

            try {
                String localdata = databaseHelper.getReceivableData(orgcode, compcode, asondate, String.valueOf(searchbypos), searchvalue);
                if (!localdata.equals("")) {

                    //StartHandlerProcess();
                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            OnShowRecDataClick(lyttop.getRootView());
                        }
                    }, 150);

                } else if (AppController.isNetConnected(ReceivbleActivity.this)) {
                    SyncVisibility();
                    imgdown.setVisibility(View.GONE);
                    btnshow.setVisibility(View.GONE);
                } else {
                    //lyttop.setVisibility(View.GONE);
                    imgdown.setVisibility(View.GONE);
                    Toast.makeText(ReceivbleActivity.this, getResources().getString(R.string.checkinternet), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                databaseHelper.addExceptionERROR(e, subject);
            }
        } catch (Exception e) {
            e.printStackTrace();

            databaseHelper.addExceptionERROR(e, subject);
        }
    }

    private DatePickerDialog.OnDateSetListener pickerListenerfrom = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            /*String monthString = String.valueOf(selectedMonth + 1);
            String dateString = String.valueOf(selectedDay);
            if (monthString.length() == 1) {
                monthString = "0" + monthString;
            }
            if (dateString.length() == 1) {
                dateString = "0" + dateString;
            }*/
            String[] datestring = Constant.GetMonthDateOfDatePicker(selectedMonth,selectedDay).split(";");
            String dateString = datestring[0];
            String monthString = datestring[1];

            txtasondate.setText(dateString + "-" + monthString + "-" + selectedYear);
            asondate = selectedYear + monthString + dateString;
            BtnSyncVisibility();
        }
    };

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    private void StopHandlerProcess() {
        Constant.BtnEnableDisable(true, btnsync, btnshow);
        progressbar.setVisibility(View.GONE);
        txtsec.setVisibility(View.GONE);
        mHandler.removeCallbacks(hMyTimeTask);
    }

    private void StartHandlerProcess() {
        Constant.BtnEnableDisable(false, btnsync, btnshow);
        progressbar.setVisibility(View.VISIBLE);
        txtsec.setVisibility(View.VISIBLE);
        txtsec.setText("");
        nCounter = 0;
        mHandler.postDelayed(hMyTimeTask, 1000);
    }

    private Runnable hMyTimeTask = new Runnable() {
        public void run() {
            nCounter++;
            txtsec.setText("" + nCounter);
            mHandler.postDelayed(hMyTimeTask, 1000);
        }
    };

    public void OnSyncRecDataClick(View view) {
        try {

            if (!CheckValidation() && AppController.isConnected(ReceivbleActivity.this, findViewById(R.id.activity_main))) {

                lytmain.setVisibility(View.GONE);
                lytbottom.setVisibility(View.GONE);
                StartHandlerProcess();

                String url = Constant.ReceivbleUrl + orgcode + "&compcode=" + compcode + "&demo=" + session.getData(UserSessionManager.KEY_DEMO) + "&date2=" + asondate + "&searchby=" + searchbyval + "&searchValue=" + searchvalue + "&mobno=" + session.getData(UserSessionManager.KEY_Mobile) + "&imei=" + session.getData(UserSessionManager.KEY_IMEI) + "&devicecode=" + session.getData(UserSessionManager.KEY_DEVICECODE);
                url = url.replaceAll(" ", "%20");

                //System.out.println("=============url -- " + url);
                StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        if (session.getData(UserSessionManager.KEY_DEMO).equalsIgnoreCase("Yes"))
                            OwnerHomeActivity.DemoReqCout();

                        if (response.contains("\n"))
                            response = response.replace("\n", "").trim();


                        /*if (response.contains(getResources().getString(R.string.noresponse))) {
                            StopHandlerProcess();
                            Toast.makeText(ReceivbleActivity.this, getResources().getString(R.string.notconnectedserver), Toast.LENGTH_SHORT).show();
                        } else if (response.contains(getResources().getString(R.string.multiuseword))) {
                            StopHandlerProcess();
                            Toast.makeText(ReceivbleActivity.this, getResources().getString(R.string.multiuse), Toast.LENGTH_SHORT).show();
                        }*/
                        if (Constant.ResponseErrorCheck(ReceivbleActivity.this, response, true)) {
                            StopHandlerProcess();
                        } else {
                            String localdata = databaseHelper.getReceivableData(orgcode, compcode, asondate, String.valueOf(searchbypos), searchvalue);
                            if (localdata.equals("")) {
                                databaseHelper.addReceivabledata(compcode, asondate, String.valueOf(searchbypos), searchvalue, response, orgcode);
                            } else {
                                databaseHelper.UpdateReceivableData(orgcode, compcode, asondate, String.valueOf(searchbypos), searchvalue, response);
                            }
                            GetReceivableData(response);
                        }
                        //StopHandlerProcess();
                    }

                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                StopHandlerProcess();
                                String message = Constant.VolleyErrorMessage(error);

                                if (!message.equals(""))
                                    Toast.makeText(ReceivbleActivity.this, message, Toast.LENGTH_SHORT).show();

                            }
                        });

                AppController.getInstance().getRequestQueue().getCache().clear();
                stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                AppController.getInstance().addToRequestQueue(stringRequest);
            }
        } catch (Exception e) {
            e.printStackTrace();

            databaseHelper.addExceptionERROR(e, subject);
        }
    }

    private boolean CheckValidation() {
        boolean iserr = false;
        Date asondt = Constant.FormateDate(inFormatdate, asondate, subject, databaseHelper);
        String searchdata = txtsearchvalue.getText().toString().trim();

        if (billwisereqd.equalsIgnoreCase("No")) {
            StopHandlerProcess();
            Toast.makeText(ReceivbleActivity.this, getResources().getString(R.string.billwiseacmsg), Toast.LENGTH_SHORT).show();
            iserr = true;
        } else if (asondt.after(new Date())) {
            StopHandlerProcess();
            Toast.makeText(ReceivbleActivity.this, getResources().getString(R.string.asondateerr), Toast.LENGTH_SHORT).show();
            iserr = true;
        } else if (searchvalue.equals("")) {
            StopHandlerProcess();
            Toast.makeText(ReceivbleActivity.this, "Search Value not found", Toast.LENGTH_SHORT).show();
            iserr = true;
        } else if (searchdata.length() == 0) {
            StopHandlerProcess();
            Toast.makeText(ReceivbleActivity.this, "Please Add Search Value", Toast.LENGTH_SHORT).show();
            iserr = true;
        } else if (containNameCode(searchdata) == null) {
            StopHandlerProcess();
            Toast.makeText(ReceivbleActivity.this, "Please Add Correct Search Value", Toast.LENGTH_SHORT).show();
            iserr = true;
        }
        return iserr;
    }

    private Master containNameCode(String search) {
        ArrayList<Master> list = null;
        if (sp_searchby.getSelectedItem().toString().equals(getResources().getString(R.string.ledger_name))) {
            list = ledgernamelist;
        } else if (sp_searchby.getSelectedItem().toString().equals(getResources().getString(R.string.ledger_group))) {
            list = ledgergrouplist;
        } else if (sp_searchby.getSelectedItem().toString().equals(getResources().getString(R.string.ledger_region))) {
            list = ledgerregionlist;
        }

        for (final Master master : list) {
            if (master.getMastername().trim().equalsIgnoreCase(search)) {
                return master;
            }
        }

        return null;
    }

    public void OnShowRecDataClick(View view) {
        try {

            if (!CheckValidation()) {
                String localdata = databaseHelper.getReceivableData(orgcode, compcode, asondate, String.valueOf(searchbypos), searchvalue);
                if (localdata.equals("")) {
                    Toast.makeText(ReceivbleActivity.this, getResources().getString(R.string.trytosync), Toast.LENGTH_SHORT).show();
                } else {
                    SyncVisibility();
                    StartHandlerProcess();
                    GetReceivableData(localdata);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            databaseHelper.addExceptionERROR(e, subject);
        }
    }

    private void GetReceivableData(String response) {
        //StopHandlerProcess();
        if (databaseHelper.getAllMasterData(compcode, orgcode).size() == 0) {
            StopHandlerProcess();
            Toast.makeText(ReceivbleActivity.this, getResources().getString(R.string.master_updatemsg), Toast.LENGTH_SHORT).show();
            return;
        }

        session.editor.putString("resby" + compcode + orgcode, "" + searchbypos);
        session.editor.putString("resdate" + compcode + orgcode, asondate);
        session.editor.putString("resvalname" + compcode + orgcode, searchvaluename);
        session.editor.putString("resval" + compcode + orgcode, searchvalue);
        session.editor.putBoolean("resdata" + compcode + orgcode, true);
        session.editor.commit();

        try {
            JSONArray jsonArray = new JSONArray(response);

            receivbleArrayList = new ArrayList<>();
            partynamelist = new ArrayList<>();
            entrylist = new ArrayList<>();

            if (jsonArray.length() == 2) {
                lytbottom.setVisibility(View.GONE);
                lytpdfexcel.setVisibility(View.GONE);
                lyttotal.setVisibility(View.GONE);
                lytmain.setVisibility(View.GONE);
                txtnodata.setVisibility(View.VISIBLE);
                imgdown.setVisibility(View.GONE);
                StopHandlerProcess();
            } else {


                asyntask = new AsyncTaskRunner(response).execute();

                /*asondatedisplay = txtasondate.getText().toString().trim();
                String header = companyname + "\n Receivable report on " + asondatedisplay;
                SpannableString ssdate = new SpannableString(header);
                ssdate.setSpan(new RelativeSizeSpan(1.3f), 0, companyname.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ssdate.setSpan(new StyleSpan(Typeface.BOLD), 0, companyname.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ssdate.setSpan(new UnderlineSpan(), header.indexOf("Receivable"), header.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                datedisplay.setText(ssdate);
                filename = companyname + "-Receivblereport-" + searchvalue + " on " + asondatedisplay;

                pdfheadertext = "Receivble report on " + asondatedisplay;
                headertext = "\n" + companyname + "\n" + address + "\n\nReceivble report on " + asondatedisplay + "\n";


                double totaldb = 0, totalcr = 0;

                //System.out.println("=========== = = = " + response);
                for (int i = 1; i < jsonArray.length() - 1; i++) {
                    JSONArray receivablearray = jsonArray.getJSONArray(i);
                    String regionname = databaseHelper.getMasterValue(compcode, DatabaseHelper.MASTER_MASTERCODE, receivablearray.getString(2), getResources().getString(R.string.master_ledger_region), DatabaseHelper.MASTER_MASTERNAME, orgcode);
                    String partyname = databaseHelper.getMasterValue(compcode, DatabaseHelper.MASTER_MASTERCODE, receivablearray.getString(1), getResources().getString(R.string.master_ledger_name), DatabaseHelper.MASTER_MASTERNAME, orgcode);
                    String mobileno = databaseHelper.getMasterValue(compcode, DatabaseHelper.MASTER_MASTERCODE, receivablearray.getString(1), getResources().getString(R.string.master_ledger_name), DatabaseHelper.MASTER_DTL2, orgcode);


                    long days = TimeUnit.DAYS.convert((new Date().getTime() - Constant.FormateDate(inFormatdate, receivablearray.getString(9), subject, databaseHelper).getTime()), TimeUnit.MILLISECONDS);

                    if (receivablearray.getString(8).equalsIgnoreCase("dr"))
                        totaldb = totaldb + Constant.ConvertToDouble(receivablearray.getString(7));
                    else
                        totalcr = totalcr + Constant.ConvertToDouble(receivablearray.getString(7));

                    Receivble listdata = new Receivble(receivablearray.getString(0), receivablearray.getString(1), receivablearray.getString(2), receivablearray.getString(3), receivablearray.getString(4), receivablearray.getString(5), receivablearray.getString(6), receivablearray.getString(7), receivablearray.getString(8), receivablearray.getString(9), String.valueOf(days), regionname, partyname, mobileno);
                    //

                    String due = listdata.getDue(), bill = listdata.getBillamt(), trdate = listdata.getTrdate2(), duedate = listdata.getDuedate2();

                    if (due != null && !due.equals("") && due.matches("[-+]?[0-9.]+")) {
                        due = Constant.ConvertToIndianRupeeFormat(Constant.ConvertToDouble(due), false, true, false);
                    }
                    if (bill != null && !bill.equals("") && bill.matches("[-+]?[0-9.]+")) {
                        bill = Constant.ConvertToIndianRupeeFormat(Constant.ConvertToDouble(bill), false, true, false);
                    }
                    duedate = Constant.FormateDate(output, Constant.FormateDate(inFormatdate, duedate, subject, databaseHelper));
                    trdate = Constant.FormateDate(output, Constant.FormateDate(inFormatdate, trdate, subject, databaseHelper));

                    listdata.setBillamt(bill);
                    listdata.setDue(due);
                    listdata.setDuedate2(duedate);
                    listdata.setTrdate2(trdate);

                    //

                    Receivble receivbledata;
                    String code = receivablearray.getString(1);
                    double duedouble = 0;

                    if (receivablearray.getString(7).equals("0") || receivablearray.getString(7).equals(""))
                        duedouble = 0;
                    else
                        duedouble = Constant.ConvertToDouble(receivablearray.getString(7));

                    if (receiblelist.containsKey(code)) {
                        receivbledata = receiblelist.get(code);
                        receivbledata.setDue(String.valueOf(Constant.ConvertToDouble(receivbledata.getDue()) + duedouble));
                        ArrayList<Receivble> stockStatusArrayList = receivbledata.getReceivbleArrayList();
                        stockStatusArrayList.add(listdata);
                        receivbledata.setReceivbleArrayList(stockStatusArrayList);

                    } else {
                        ArrayList<Receivble> stockStatusArrayList = new ArrayList<>();
                        stockStatusArrayList.add(listdata);
                        receivbledata = new Receivble(receivablearray.getString(1), partyname, regionname, receivablearray.getString(7), mobileno, stockStatusArrayList);
                    }

                    receiblelist.put(code, receivbledata);

                }

                totalcr = Math.abs(totalcr);
                totaldb = Math.abs(totaldb);

                txttotalcredit.setText(Constant.ConvertToIndianRupeeFormat(totalcr, false, false, false));
                txttotaldebit.setText(Constant.ConvertToIndianRupeeFormat(totaldb, false, false, false));
                txtnetreceivable.setText(Constant.ConvertToIndianRupeeFormat((totaldb - totalcr), false, false, false));

                SetViewVisiblility();
                BtnSyncVisibility();
                entrylist = new ArrayList<Receivble>(receiblelist.values());
                adapter = new ReceivbleAdapter(entrylist);
                recycleview.setAdapter(adapter);
                StopHandlerProcess();*/
            }

        } catch (JSONException e) {
            e.printStackTrace();
            StopHandlerProcess();
            databaseHelper.addJSONERROR(e, subject);
        }

    }

    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        double totaldb = 0, totalcr = 0;
        JSONArray jsonArray;

        public AsyncTaskRunner(String response) {
            try {
                jsonArray = new JSONArray(response);
            } catch (JSONException e) {
                e.printStackTrace();

                databaseHelper.addJSONERROR(e, subject);
            }
        }

        @Override
        protected String doInBackground(String... params) {

            try {

                //System.out.println("================ = = " + jsonArray.length());
                for (int i = 1; i < jsonArray.length() - 1; i++) {
                    JSONArray receivablearray = jsonArray.getJSONArray(i);
                    String regionname = databaseHelper.getMasterValue(compcode, DatabaseHelper.MASTER_MASTERCODE, receivablearray.getString(2), getResources().getString(R.string.master_ledger_region), DatabaseHelper.MASTER_MASTERNAME, orgcode);
                    String partyname = databaseHelper.getMasterValue(compcode, DatabaseHelper.MASTER_MASTERCODE, receivablearray.getString(1), getResources().getString(R.string.master_ledger_name), DatabaseHelper.MASTER_MASTERNAME, orgcode);
                    String mobileno = databaseHelper.getMasterValue(compcode, DatabaseHelper.MASTER_MASTERCODE, receivablearray.getString(1), getResources().getString(R.string.master_ledger_name), DatabaseHelper.MASTER_DTL2, orgcode);


                    long days = TimeUnit.DAYS.convert((new Date().getTime() - Constant.FormateDate(inFormatdate, receivablearray.getString(9), subject, databaseHelper).getTime()), TimeUnit.MILLISECONDS);

                    if (receivablearray.getString(8).equalsIgnoreCase("dr"))
                        totaldb = totaldb + Constant.ConvertToDouble(receivablearray.getString(7));
                    else
                        totalcr = totalcr + Constant.ConvertToDouble(receivablearray.getString(7));

                    Receivble listdata = new Receivble(receivablearray.getString(0), receivablearray.getString(1), receivablearray.getString(2), receivablearray.getString(3), receivablearray.getString(4), receivablearray.getString(5), receivablearray.getString(6), receivablearray.getString(7), receivablearray.getString(8), receivablearray.getString(9), String.valueOf(days), regionname, partyname, mobileno);
                    //

                    String due = listdata.getDue(), bill = listdata.getBillamt(), trdate = listdata.getTrdate2(), duedate = listdata.getDuedate2();

                    if (due != null && !due.equals("") && due.matches("[-+]?[0-9.]+")) {
                        due = Constant.ConvertToIndianRupeeFormat(Constant.ConvertToDouble(due), false, true, false);
                    }
                    if (bill != null && !bill.equals("") && bill.matches("[-+]?[0-9.]+")) {
                        bill = Constant.ConvertToIndianRupeeFormat(Constant.ConvertToDouble(bill), false, true, false);
                    }
                    duedate = Constant.FormateDate(output, Constant.FormateDate(inFormatdate, duedate, subject, databaseHelper));
                    trdate = Constant.FormateDate(output, Constant.FormateDate(inFormatdate, trdate, subject, databaseHelper));

                    listdata.setBillamt(bill);
                    listdata.setDue(due);
                    listdata.setDuedate2(duedate);
                    listdata.setTrdate2(trdate);

                    //

                    Receivble receivbledata;
                    String code = receivablearray.getString(1);
                    double duedouble = 0;

                    if (receivablearray.getString(7).equals("0") || receivablearray.getString(7).equals(""))
                        duedouble = 0;
                    else
                        duedouble = Constant.ConvertToDouble(receivablearray.getString(7));

                    if (receiblelist.containsKey(code)) {
                        receivbledata = receiblelist.get(code);
                        receivbledata.setDue(String.valueOf(Constant.ConvertToDouble(receivbledata.getDue()) + duedouble));
                        ArrayList<Receivble> stockStatusArrayList = receivbledata.getReceivbleArrayList();
                        stockStatusArrayList.add(listdata);
                        receivbledata.setReceivbleArrayList(stockStatusArrayList);

                    } else {
                        ArrayList<Receivble> stockStatusArrayList = new ArrayList<>();
                        stockStatusArrayList.add(listdata);
                        receivbledata = new Receivble(receivablearray.getString(1), partyname, regionname, receivablearray.getString(7), mobileno, stockStatusArrayList);
                    }

                    receiblelist.put(code, receivbledata);

                }

            } catch (JSONException e) {
                e.printStackTrace();
                databaseHelper.addJSONERROR(e, subject);
            }

            return null;
        }


        @Override
        protected void onPostExecute(String result) {

            try {

                asondatedisplay = txtasondate.getText().toString().trim();
                String header = companyname + "\n Receivable report on " + asondatedisplay;
                SpannableString ssdate = new SpannableString(header);
                ssdate.setSpan(new RelativeSizeSpan(1.3f), 0, companyname.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ssdate.setSpan(new StyleSpan(Typeface.BOLD), 0, companyname.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                ssdate.setSpan(new UnderlineSpan(), header.indexOf("Receivable"), header.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                datedisplay.setText(ssdate);
                filename = companyname + "-Receivblereport-" + searchvalue + " on " + asondatedisplay;

                pdfheadertext = "Receivble report on " + asondatedisplay;
                headertext = "\n" + companyname + "\n" + address + "\n\nReceivble report on " + asondatedisplay + "\n";

                totalcr = Math.abs(totalcr);
                totaldb = Math.abs(totaldb);

                txttotalcredit.setText(Constant.ConvertToIndianRupeeFormat(totalcr, false, false, false));
                txttotaldebit.setText(Constant.ConvertToIndianRupeeFormat(totaldb, false, false, false));
                txtnetreceivable.setText(Constant.ConvertToIndianRupeeFormat((totaldb - totalcr), false, false, false));

                Constant.SetViewVisiblility(imgdown, lyttop);
                BtnSyncVisibility();
                StopHandlerProcess();

                txtnodata.setVisibility(View.GONE);
                lytbottom.setVisibility(View.VISIBLE);
                lytmain.setVisibility(View.VISIBLE);
                lytpdfexcel.setVisibility(View.VISIBLE);
                lyttotal.setVisibility(View.VISIBLE);


                entrylist = new ArrayList<Receivble>(receiblelist.values());
                adapter = new ReceivbleAdapter(entrylist);
                recycleview.setAdapter(adapter);

            } catch (Exception e) {
                e.printStackTrace();
                databaseHelper.addExceptionERROR(e, subject);
            }

        }


        @Override
        protected void onPreExecute() {
            receivbleArrayList = new ArrayList<>();
            partynamelist = new ArrayList<>();
            entrylist = new ArrayList<>();
            receiblelist.clear();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (asyntask != null)
            asyntask.cancel(true);
    }

    private Receivble containLedgerCode(final ArrayList<Receivble> receivbleArrayList, final String search) {
        for (final Receivble receivble : receivbleArrayList) {
            if (receivble.getLedgercode().equals(search)) {
                return receivble;
            }
        }

        return null;
    }

    public void BtnSyncVisibility() {

        String localdata = "";
        try {
            localdata = databaseHelper.getReceivableData(orgcode, compcode, asondate, String.valueOf(searchbypos), searchvalue);
        } catch (Exception e) {
            e.printStackTrace();
            databaseHelper.addExceptionERROR(e, subject);
        }

        if (localdata.equals("")) {
            btnshow.setVisibility(View.GONE);
        } else {
            btnshow.setVisibility(View.VISIBLE);
        }
        SyncVisibility();
    }

    public void SyncVisibility() {
        if (session.getData(UserSessionManager.KEY_DEMO).equalsIgnoreCase("Yes") && session.getIntData(UserSessionManager.KEY_DEMOREQCOUNT) > (session.getIntData(UserSessionManager.KEY_DEMOREQTOTAL)))
            btnsync.setVisibility(View.GONE);
        else
            btnsync.setVisibility(View.VISIBLE);
    }

    public void OnHideRecClick(View view) {
        Constant.SetViewVisiblility(imgdown, lyttop);
    }

   /* public void SetViewVisiblility() {
        imgdown.setVisibility(View.VISIBLE);
        if (lyttop.getVisibility() == View.GONE) {
            imgdown.setImageResource(R.drawable.ic_bup);
            lyttop.setVisibility(View.VISIBLE);
        } else {
            imgdown.setImageResource(R.drawable.ic_bdown);
            lyttop.setVisibility(View.GONE);
        }

    }*/

    public void OnRePdfClick(View view) {
        lytprogress.setVisibility(View.VISIBLE);
        btnexcel.setEnabled(false);
        btnpdf.setEnabled(false);
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                //takeScreenShot();

                /*File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getResources().getString(R.string.app_name) + "/");

                if (!folder.exists()) {
                    boolean success = folder.mkdir();
                }

                path = folder.getAbsolutePath();
                path = path + "/" + filename + ".pdf";
                File filepath = new File(path);
                if (filepath.exists()) {
                    filepath.delete();
                }*/

                File filepath = Constant.FilePathPdfExcel(true, ".pdf", ReceivbleActivity.this, filename);

                UserSessionManager.CreateReceivblePdf(filename, address, companyname, pdfheadertext, entrylist, recycleview, txttotaldebit.getText().toString().trim(), txttotalcredit.getText().toString().trim(), txtnetreceivable.getText().toString().trim());

                openPdfExcel(filepath, "pdf");
            }
        }, 300);

    }

    private void openPdfExcel(File file, String type) {
        /*if (ContextCompat.checkSelfPermission(ReceivbleActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ReceivbleActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, ReqReadPermission);
        } else if (ContextCompat.checkSelfPermission(ReceivbleActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ReceivbleActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, ReqWritePermission);
        } else {*/
        if (Constant.CheckReadWritePermissionGranted(ReceivbleActivity.this)) {
            lytprogress.setVisibility(View.GONE);

            /*Uri uri = FileProvider.getUriForFile(ReceivbleActivity.this, getResources().getString(R.string.authority), file);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            if (type.equals("pdf"))
                intent.setDataAndType(uri, "application/pdf");
            else
                intent.setDataAndType(uri, "application/vnd.ms-excel");

            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(intent);*/
            Constant.openPdfExcel(file, type, ReceivbleActivity.this);
            btnexcel.setEnabled(true);
            btnpdf.setEnabled(true);
        }
    }

    public void OnReExcelClick(View view) {
        /*if (ContextCompat.checkSelfPermission(ReceivbleActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ReceivbleActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, ReqReadPermission);
        } else if (ContextCompat.checkSelfPermission(ReceivbleActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ReceivbleActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, ReqWritePermission);
        } else {*/
        if (Constant.CheckReadWritePermissionGranted(ReceivbleActivity.this)) {
            lytprogress.setVisibility(View.VISIBLE);
            btnexcel.setEnabled(false);
            btnpdf.setEnabled(false);
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    writeStudentsListToExcel();
                }
            }, 300);
        }
    }

    public void writeStudentsListToExcel() {

        try {
            /*File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getResources().getString(R.string.app_name) + "/");

            if (!folder.exists()) {
                boolean success = folder.mkdir();
            }
            String path = folder.getAbsolutePath();
            path = path + "/" + filename + ".xlsx";

            File FILE_PATH = new File(path);
            if (FILE_PATH.exists()) {
                FILE_PATH.delete();
            }*/

            File FILE_PATH = Constant.FilePathPdfExcel(true, ".xlsx", ReceivbleActivity.this, filename);

            XSSFWorkbook workbook = new XSSFWorkbook();

            XSSFSheet studentsSheet = workbook.createSheet("Receivble");
            studentsSheet.getPrintSetup().setPaperSize(PrintSetup.A4_PAPERSIZE);
            studentsSheet.setHorizontallyCenter(true);

            Footer footer = studentsSheet.getFooter();
            footer.setRight("Page " + HeaderFooter.page() + " of " + HeaderFooter.numPages());


            studentsSheet.setColumnWidth(0, (15 * 200));
            studentsSheet.setColumnWidth(1, (15 * 200));
            studentsSheet.setColumnWidth(2, (15 * 200));
            studentsSheet.setColumnWidth(3, (15 * 200));
            studentsSheet.setColumnWidth(4, (15 * 200));
            studentsSheet.setColumnWidth(5, (15 * 200));
            studentsSheet.setColumnWidth(6, (15 * 200));


            int rowIndex = 0, nextrow = 0;
            //Row row = studentsSheet.createRow(rowIndex++);
            int cellIndex = 0;
            CellStyle style = workbook.createCellStyle();

            style.setBorderBottom(CellStyle.BORDER_THIN);
            style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            style.setBorderLeft(CellStyle.BORDER_THIN);
            style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            style.setBorderRight(CellStyle.BORDER_THIN);
            style.setRightBorderColor(IndexedColors.BLACK.getIndex());
            style.setBorderTop(CellStyle.BORDER_THIN);
            style.setTopBorderColor(IndexedColors.BLACK.getIndex());

            CellStyle borderStyle = workbook.createCellStyle();
            borderStyle.setBorderBottom(CellStyle.BORDER_THIN);
            borderStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            borderStyle.setBorderLeft(CellStyle.BORDER_THIN);
            borderStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            borderStyle.setBorderRight(CellStyle.BORDER_THIN);
            borderStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            borderStyle.setBorderTop(CellStyle.BORDER_THIN);
            borderStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
            ((XSSFCellStyle) borderStyle).setAlignment(HorizontalAlignment.CENTER);

            XSSFFont font = workbook.createFont();
            font.setBold(true);
            style.setFont(font);
            ((XSSFCellStyle) style).setAlignment(HorizontalAlignment.CENTER);

            //row.createCell(cellIndex);
            CellStyle cs = workbook.createCellStyle();
            cs.setFont(font);
            ((XSSFCellStyle) cs).setAlignment(HorizontalAlignment.CENTER);
            ((XSSFCellStyle) cs).setVerticalAlignment(VerticalAlignment.CENTER);
            cs.setWrapText(true);


            XSSFFont comFont = workbook.createFont();
            comFont.setFontHeightInPoints((short) 20);
            CellStyle cellstyle = workbook.createCellStyle();
            cellstyle.setFont(comFont);
            ((XSSFCellStyle) cellstyle).setAlignment(HorizontalAlignment.CENTER);
            //richString.applyFont(headertext.indexOf(companyname), companyname.length() + 1, comFont);

            Row row = studentsSheet.createRow(rowIndex);
            row.createCell(cellIndex).setCellValue(companyname);
            row.getCell(cellIndex).setCellStyle(cellstyle);
            studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 6));

            rowIndex++;
            cellIndex = 0;

            Row addrow = studentsSheet.createRow(rowIndex);
            addrow.createCell(cellIndex).setCellValue(address);
            addrow.getCell(cellIndex).setCellStyle(cs);
        /*int sindext = rowIndex;

        Row addrow1 = studentsSheet.createRow(rowIndex++);
        Row addrow2 = studentsSheet.createRow(rowIndex++);
        Row addrow3 = studentsSheet.createRow(rowIndex);*/
            studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex + 3, 0, 6));

            rowIndex = rowIndex + 4;
            //rowIndex++;
            cellIndex = 0;

            Drawing drawing = studentsSheet.createDrawingPatriarch();
            CreationHelper creationHelper = workbook.getCreationHelper();
            ClientAnchor anchor = creationHelper.createClientAnchor();
            anchor.setDx1(0);
            anchor.setCol1(1);
            anchor.setRow1(rowIndex);
            anchor.setRow2(rowIndex);
            anchor.setCol2(5);
            XSSFDrawing xssfdrawing = (XSSFDrawing) drawing;
            XSSFClientAnchor xssfanchor = (XSSFClientAnchor) anchor;
            XSSFSimpleShape xssfshape = xssfdrawing.createSimpleShape(xssfanchor);
            xssfshape.setShapeType(ShapeTypes.LINE);
            xssfshape.setLineWidth(1);
            xssfshape.setLineStyle(0);
            xssfshape.setLineStyleColor(0, 0, 0);

            Row daterow = studentsSheet.createRow(rowIndex);
            daterow.createCell(cellIndex).setCellValue("Receivble report on " + asondatedisplay);
            daterow.getCell(cellIndex).setCellStyle(cs);
            studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 6));

            rowIndex++;
            Row blankrow = studentsSheet.createRow(rowIndex);

            rowIndex++;
            nextrow = rowIndex;

            //rowIndex = rowIndex + 9;
            cellIndex = 0;
            Row srow = studentsSheet.createRow(rowIndex);
            srow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.date));
            srow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.vch_no));
            srow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.bill_amt));
            srow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.due_amt));
            srow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.dr_cr));
            srow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.due_date));
            srow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.due_days));

            studentsSheet.setRepeatingRows(CellRangeAddress.valueOf("1:" + (rowIndex + 1)));

            for (int i = 0; i < 7; i++) {
                srow.getCell(i).setCellStyle(style);
            }

            int bottomrow = rowIndex;
            for (int i = 0; i < entrylist.size(); i++) {
                rowIndex++;
                bottomrow = rowIndex;
                Receivble receivble = entrylist.get(i);

                String mobile = receivble.getMobileno().trim();
                String datadisplay;
                if (mobile.length() != 0)
                    mobile = "M." + mobile;

                String region = receivble.getRegionname().trim();
                if (mobile.length() != 0)
                    region = region + "\n" + mobile;


                if (receivble.getRegionname().contains("NOT APPLICABLE"))
                    datadisplay = mobile;
                else
                    datadisplay = region;


                cellIndex = 0;
                Row nrow = studentsSheet.createRow(rowIndex);
                nrow.createCell(cellIndex++).setCellValue(receivble.getPartyname());
                nrow.createCell(cellIndex++);
                nrow.createCell(cellIndex++);
                nrow.createCell(cellIndex++).setCellValue(datadisplay);
                nrow.createCell(cellIndex++);
                nrow.createCell(cellIndex++).setCellValue(receivble.getDue());
                nrow.createCell(cellIndex++);


                for (int j = 0; j < 7; j++) {
                    nrow.getCell(j).setCellStyle(style);
                }
                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 2));
                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 3, 4));
                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 5, 6));


                View rowlist = recycleview.getLayoutManager().findViewByPosition(i);
                RecyclerView recycleviewdata = rowlist.findViewById(R.id.recycleview);
                //rowIndex++;
                if (recycleviewdata.getVisibility() == View.VISIBLE) {
                    ArrayList<Receivble> datalist = receivble.getReceivbleArrayList();

                    for (int j = 0; j < datalist.size(); j++) {
                        rowIndex++;
                        bottomrow = rowIndex;
                        Receivble receivbledata = datalist.get(j);
                        //System.out.println("============ = =  =" + receivbledata.getDuedays());

                        Row drow = studentsSheet.createRow(rowIndex);
                        cellIndex = 0;
                        drow.createCell(cellIndex++).setCellValue(receivbledata.getTrdate2());
                        drow.createCell(cellIndex++).setCellValue(receivbledata.getVchno());
                        drow.createCell(cellIndex++).setCellValue(receivbledata.getBillamt());
                        drow.createCell(cellIndex++).setCellValue(receivbledata.getDue());
                        drow.createCell(cellIndex++).setCellValue(receivbledata.getDc());
                        drow.createCell(cellIndex++).setCellValue(receivbledata.getDuedate2());
                        drow.createCell(cellIndex++).setCellValue(receivbledata.getDuedays());
                        for (int k = 0; k < 7; k++) {
                            drow.getCell(k).setCellStyle(borderStyle);
                        }

                    }
                }
            }

            bottomrow++;
            cellIndex = 0;
            Row drow = studentsSheet.createRow(bottomrow++);
            drow.createCell(cellIndex++).setCellValue("");
            drow.createCell(cellIndex++).setCellValue("");
            drow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.total_debit).trim());
            drow.createCell(cellIndex++).setCellValue(txttotaldebit.getText().toString().trim());
            drow.createCell(cellIndex++).setCellValue("");
            drow.createCell(cellIndex++).setCellValue("");
            drow.createCell(cellIndex++).setCellValue("");

            for (int j = 2; j <= 3; j++) {
                drow.getCell(j).setCellStyle(style);
            }


            cellIndex = 0;
            Row crow = studentsSheet.createRow(bottomrow++);
            crow.createCell(cellIndex++).setCellValue("");
            crow.createCell(cellIndex++).setCellValue("");
            crow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.total_credit).trim());
            crow.createCell(cellIndex++).setCellValue(txttotalcredit.getText().toString().trim());
            crow.createCell(cellIndex++).setCellValue("");
            crow.createCell(cellIndex++).setCellValue("");
            crow.createCell(cellIndex++).setCellValue("");

            for (int j = 2; j <= 3; j++) {
                crow.getCell(j).setCellStyle(style);
            }


            cellIndex = 0;
            Row netrow = studentsSheet.createRow(bottomrow++);
            netrow.createCell(cellIndex++).setCellValue("");
            netrow.createCell(cellIndex++).setCellValue("");
            netrow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.net_receivble).trim());
            netrow.createCell(cellIndex++).setCellValue(txtnetreceivable.getText().toString().trim());
            netrow.createCell(cellIndex++).setCellValue("");
            netrow.createCell(cellIndex++).setCellValue("");
            netrow.createCell(cellIndex++).setCellValue("");

            for (int j = 2; j <= 3; j++) {
                netrow.getCell(j).setCellStyle(style);
            }


            try {
                FileOutputStream fos = new FileOutputStream(FILE_PATH);
            /*studentsSheet.setPrintGridlines(true);
            Header header = studentsSheet.getEvenHeader();
            header.setCenter(headertext);

            Footer footer = studentsSheet.getFooter();
            footer.setRight( "Page " + HeaderFooter.page() + " of " + HeaderFooter.numPages() );

            studentsSheet.getPrintSetup().setPaperSize(PrintSetup.A4_PAPERSIZE);*/

                workbook.write(fos);
                fos.close();


                //System.out.println(FILE_PATH + " is successfully written");
                openPdfExcel(FILE_PATH, "excel");
            } catch (FileNotFoundException e) {
                e.printStackTrace();

                databaseHelper.addFileNotFoundERROR(e, subject);
            } catch (IOException e) {
                e.printStackTrace();

                databaseHelper.addIOERROR(e, subject);
            }
        } catch (Exception e) {
            e.printStackTrace();
            databaseHelper.addExceptionERROR(e, subject);
        }
    }

    public void OnClearClick(View view) {
        txtsearchvalue.setText("");
        searchvalue = "";
        searchvaluename = "";
        txtsearchvalue.performClick();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }


    public class ReceivbleAdapter extends RecyclerView.Adapter<ReceivbleAdapter.ViewHolder> {

        public ArrayList<Receivble> receivableArrayList;

        public ReceivbleAdapter(ArrayList<Receivble> receivableArrayList) {
            this.receivableArrayList = receivableArrayList;
        }

        @NonNull
        @Override
        public ReceivbleAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lyt_receivable, parent, false);
            return new ReceivbleAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final ReceivbleAdapter.ViewHolder holder, int position) {

            final Receivble model = receivableArrayList.get(position);
            String name;
            if (model.getRegionname().contains(getResources().getString(R.string.notapplicable)))
                name = model.getPartyname();
            else
                name = model.getPartyname() + "-" + model.getRegionname();

            if (name == null || name.length() == 0)
                name = getResources().getString(R.string.nodata_updatemaster);

            holder.txtname.setText(Html.fromHtml(name));

            String finalName = name;
            holder.txtname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (finalName.equals(getResources().getString(R.string.nodata_updatemaster))) {
                        session.OnUpdateMasterClick(new VolleyCallback() {
                            @Override
                            public void onSuccess(boolean result) {
                                if (result)
                                    OnShowRecDataClick(lyttop.getRootView());
                            }

                            public void onSuccessWithMsg(boolean result, String message) {
                            }
                        }, ReceivbleActivity.this, session, orgcode, compcode, databaseHelper, subject, findViewById(R.id.activity_main));

                    }
                }
            });


            holder.txtamt.setText(Constant.ConvertToIndianRupeeFormat(Constant.ConvertToDouble(model.getDue()), false, true, false));

            String mobile = model.getMobileno().trim();
            if (mobile.length() != 0)
                mobile = "M." + mobile;

            holder.txtreligion.setText(mobile);
            holder.recycleview.setLayoutManager(new LinearLayoutManager(ReceivbleActivity.this));
            holder.recycleview.setAdapter(new ReceivbleEntryAdapter(model.getReceivbleArrayList()));

            holder.imgarrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.recycleview.getVisibility() == View.VISIBLE) {
                        holder.recycleview.setVisibility(View.GONE);
                        holder.imgarrow.setImageResource(R.drawable.ic_downarrow);
                    } else {
                        holder.recycleview.setVisibility(View.VISIBLE);
                        holder.imgarrow.setImageResource(R.drawable.ic_up);
                    }
                }
            });


            if (position % 2 == 0)
                holder.lytmain.setBackgroundColor(getResources().getColor(R.color.receivble3));
            else
                holder.lytmain.setBackgroundColor(getResources().getColor(R.color.receivble2));


            if (receivableArrayList.size() == 1) {
                holder.imgarrow.performClick();
            }
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return receivableArrayList.size();
        }


        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView txtname, txtamt, txtreligion;
            RecyclerView recycleview;
            ImageView imgarrow;
            LinearLayout lytmain;

            public ViewHolder(View itemView) {
                super(itemView);
                txtname = itemView.findViewById(R.id.txtname);
                txtamt = itemView.findViewById(R.id.txtamt);
                txtreligion = itemView.findViewById(R.id.txtreligion);
                recycleview = itemView.findViewById(R.id.recycleview);
                imgarrow = itemView.findViewById(R.id.imgarrow);
                lytmain = itemView.findViewById(R.id.lytmain);
            }

        }
    }

    public class ReceivbleEntryAdapter extends RecyclerView.Adapter<ReceivbleEntryAdapter.ViewHolder> {

        public ArrayList<Receivble> receivableArrayList;

        public ReceivbleEntryAdapter(ArrayList<Receivble> receivableArrayList) {
            this.receivableArrayList = receivableArrayList;
        }

        @NonNull
        @Override
        public ReceivbleEntryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lyt_receivable_entry, parent, false);
            return new ReceivbleEntryAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final ReceivbleEntryAdapter.ViewHolder holder, int position) {

            final Receivble model = receivableArrayList.get(position);
            holder.txttrdate.setText(model.getTrdate2());
            holder.txtvrno.setText(Html.fromHtml("<u><i>" + model.getVchno() + "</i></u>"));
            holder.txtbillamt.setText(model.getBillamt().replaceAll(",", "").trim());
            holder.txtdueamt.setText(model.getDue().replaceAll(",", "").trim() + " " + model.getDc().trim());
            holder.txtdueday.setText(model.getDuedays());

            //holder.txtdrcr.setText(model.getDc());
            //holder.txtduedate.setText(model.getDuedate2());

            holder.txtvrno.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //System.out.println("=================== = = " + model.getVchcode() + " = " + model.getVouchertypecode());
                    if (!model.getVchcode().equals("") && !model.getVouchertypecode().equals(""))
                        startActivity(new Intent(ReceivbleActivity.this, VoucherInfoActivity.class).putExtra("from", "Receivble").putExtra("code", model.getVchcode()).putExtra("typecode", model.getVouchertypecode()));
                }
            });
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return receivableArrayList.size();
        }


        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView txttrdate, txtvrno, txtbillamt, txtdueamt, txtdrcr, txtduedate, txtdueday;
            LinearLayout lytmain;

            public ViewHolder(View itemView) {
                super(itemView);
                txttrdate = itemView.findViewById(R.id.txttrdate);
                txtvrno = itemView.findViewById(R.id.txtvrno);
                txtbillamt = itemView.findViewById(R.id.txtbillamt);
                txtdueamt = itemView.findViewById(R.id.txtdueamt);
                //txtdrcr = itemView.findViewById(R.id.txtdrcr);
                //txtduedate = itemView.findViewById(R.id.txtduedate);
                txtdueday = itemView.findViewById(R.id.txtdueday);
                lytmain = itemView.findViewById(R.id.lytmain);
            }

        }
    }

    /*public void BtnEnableDisable(boolean isenable) {
        btnsync.setEnabled(isenable);
        btnshow.setEnabled(isenable);
    }*/
}
