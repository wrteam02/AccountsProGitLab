package com.accountspro.activity;

import android.content.Intent;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.accountspro.R;
import com.accountspro.helper.DatabaseHelper;
import com.accountspro.helper.UserSessionManager;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 2000;
    UserSessionManager session;
    DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Window window = getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_splash);

        session = new UserSessionManager(SplashActivity.this);
        databaseHelper = new DatabaseHelper(SplashActivity.this);



        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent intent;
                /*if(session.isUserLoggedIn() && session.getData(UserSessionManager.KEY_LoginType).equalsIgnoreCase(UserSessionManager.OWNER)) {
                    if(session.getBooleanData(UserSessionManager.KEY_COMSET) && databaseHelper.getAllCompany().size() != 0)
                        intent = new Intent(SplashActivity.this, OwnerHomeActivity.class);
                    *//*else if(databaseHelper.getAllCompany().size() != 0 && session.getBooleanData(UserSessionManager.KEY_ORGSET)){
                        intent = new Intent(SplashActivity.this, CompanyActivity.class);
                    }*//*else if(databaseHelper.getAllOrganization().size() != 0){
                        intent = new Intent(SplashActivity.this, OrganizationActivity.class);
                    }else {
                        intent = new Intent(SplashActivity.this, OwnerLoginActivity.class);
                    }
                }*/
                if (session.isUserLoggedIn() && session.getData(UserSessionManager.KEY_LoginType).equalsIgnoreCase(UserSessionManager.OWNER)) {

                    if (session.getBooleanData(session.getData(UserSessionManager.KEY_ORG_REGKEY)+UserSessionManager.KEY_COMSET) && databaseHelper.getAllCompany(session.getData(UserSessionManager.KEY_ORG_REGKEY)).size() != 0)
                        intent = new Intent(SplashActivity.this, PinGenerationActivity.class).putExtra("from", "splash");
                    else if (databaseHelper.getAllOrganization().size() != 0) {
                        intent = new Intent(SplashActivity.this, OrganizationActivity.class);
                    } else {
                        intent = new Intent(SplashActivity.this, OwnerLoginActivity.class);
                    }

                } else {
                    intent = new Intent(SplashActivity.this, LoginActivity.class);
                }

                startActivity(intent);

                finish();
            }

        }, SPLASH_TIME_OUT);
    }



}
/*
if(session.getBooleanData(UserSessionManager.KEY_COMSET) && databaseHelper.getAllCompany().size() != 0)
        intent = new Intent(SplashActivity.this, OwnerHomeActivity.class);
                    else if(databaseHelper.getAllOrganization().size() != 0){
        intent = new Intent(SplashActivity.this, OrganizationActivity.class);
        }else {
        intent = new Intent(SplashActivity.this, OwnerLoginActivity.class);
        }
        */
