package com.accountspro.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Environment;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.accountspro.R;
import com.accountspro.helper.AppController;
import com.accountspro.helper.Constant;
import com.accountspro.helper.DatabaseHelper;
import com.accountspro.helper.UserSessionManager;
import com.accountspro.model.Master;
import com.accountspro.model.VoucherInfo;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.apache.poi.hssf.usermodel.HeaderFooter;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Footer;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.PrintSetup;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;


public class VoucherInfoActivity extends AppCompatActivity {

    String sessionname = Constant.VoucherInfoSessionName;
    public int ReqReadPermission = 1;
    public int ReqWritePermission = 2;
    Toolbar toolbar;
    UserSessionManager session;
    DatabaseHelper databaseHelper;
    String vno, subject, companyname, compcode, orgcode, address, headertext, path, filename, pdfheadertext, vouchertypename, from, vouchertypecode = "0", voucherno = "0", vchcode = "0";
    Spinner spvouchertype;
    ArrayList<Master> vouchertypelist;
    ArrayList<VoucherInfo> voucherInfoArrayList, inventroylist, otherchargelist, transactionlist;
    EditText edtvcno;
    ProgressBar progressbar;
    Button btnsync, btnshow;
    ImageView imgdown;
    ImageButton btnexcel, btnpdf;
    int nCounter = 0;
    private Handler mHandler = new Handler();
    RecyclerView recycleviewtransaction, recycleviewothercharge, recycleviewInv;
    LinearLayout lytremark, lytref, lytinput, lytvehicle, lytpdfexcel, lytprogress, lytbottom, lytaccounting, lytothercharge, lytinvetoryinfo, lytvoucherinfo, lytmain, lytdisplay, lytcontain, lyttop;
    TextView txtremark, txtothertotal, txtinvtotal, txteditedby, txtcreatedby, txtvehicleno, txtvehicle, txtparty, txttype, txtrefno, txtvchdate, txtvchno, datedisplay, txtnodata, txtsec;

    SimpleDateFormat input = new SimpleDateFormat("yyyyMMdd");
    //NumberFormat indianFormat = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
    SimpleDateFormat outputdatetime = new SimpleDateFormat("dd/MM/yy HH:mm");
    SimpleDateFormat output = new SimpleDateFormat("dd/MM/yyyy");
    SimpleDateFormat inputdatetime = new SimpleDateFormat("yyyyMMdd HH:mm");

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voucher_info);

        Intent intent = getIntent();
        from = intent.getStringExtra("from");
        vchcode = intent.getStringExtra("code");
        vouchertypecode = intent.getStringExtra("typecode");
        vno = intent.getStringExtra("vno");

        if (vouchertypecode == null || vouchertypecode.equals(""))
            vouchertypecode = "0";
        if (vchcode == null || vchcode.equals(""))
            vchcode = "0";

        databaseHelper = new DatabaseHelper(VoucherInfoActivity.this);
        session = new UserSessionManager(VoucherInfoActivity.this);
        orgcode = session.getData(UserSessionManager.KEY_ORG_REGKEY);
        compcode = session.getData(orgcode + UserSessionManager.KEY_COM_CODE);
        companyname = session.getData(orgcode + UserSessionManager.KEY_COM_NAME);
        address = session.getData(orgcode + UserSessionManager.KEY_COM_ADDRESS);
        subject = Constant.ERRORID + " | " + session.getData(UserSessionManager.KEY_Mobile) + " | " + session.getData(UserSessionManager.KEY_ORG_NAME) + " | " + companyname;

        try {
            vouchertypelist = databaseHelper.getMasterRowData(compcode, getResources().getString(R.string.master_voucher_type), orgcode);
            toolbar = findViewById(R.id.toolbar);
            spvouchertype = findViewById(R.id.spvouchertype);
            edtvcno = findViewById(R.id.edtvcno);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);

            final ArrayAdapter<Master> voucheradapter = new ArrayAdapter<Master>(VoucherInfoActivity.this, R.layout.lyt_sptext, vouchertypelist);
            voucheradapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spvouchertype.setAdapter(voucheradapter);


            voucherInfoArrayList = new ArrayList<>();
            inventroylist = new ArrayList<>();
            otherchargelist = new ArrayList<>();
            transactionlist = new ArrayList<>();


            btnexcel = findViewById(R.id.btnexcel);
            btnpdf = findViewById(R.id.btnpdf);
            lytremark = findViewById(R.id.lytremark);
            lytref = findViewById(R.id.lytref);
            lytinput = findViewById(R.id.lytinput);
            lytvehicle = findViewById(R.id.lytvehicle);
            lytpdfexcel = findViewById(R.id.lytpdfexcel);
            lytprogress = findViewById(R.id.lytprogress);
            lytbottom = findViewById(R.id.lytbottom);
            lytaccounting = findViewById(R.id.lytaccounting);
            recycleviewtransaction = findViewById(R.id.recycleviewtransaction);
            recycleviewothercharge = findViewById(R.id.recycleviewothercharge);
            lytothercharge = findViewById(R.id.lytothercharge);
            txtinvtotal = findViewById(R.id.txtinvtotal);
            txtremark = findViewById(R.id.txtremark);
            txtothertotal = findViewById(R.id.txtothertotal);
            recycleviewInv = findViewById(R.id.recycleviewInv);
            btnsync = findViewById(R.id.btnsync);
            btnshow = findViewById(R.id.btnshow);
            progressbar = findViewById(R.id.progressbar);
            txteditedby = findViewById(R.id.txteditedby);
            txtcreatedby = findViewById(R.id.txtcreatedby);
            txtvehicleno = findViewById(R.id.txtvehicleno);
            txtvehicle = findViewById(R.id.txtvehicle);
            txtparty = findViewById(R.id.txtparty);
            txttype = findViewById(R.id.txttype);
            txtrefno = findViewById(R.id.txtrefno);
            txtvchdate = findViewById(R.id.txtvchdate);
            txtvchno = findViewById(R.id.txtvchno);
            datedisplay = findViewById(R.id.datedisplay);
            txtnodata = findViewById(R.id.txtnodata);
            txtsec = findViewById(R.id.txtsec);
            imgdown = findViewById(R.id.imgdown);
            lyttop = findViewById(R.id.lyttop);
            lytcontain = findViewById(R.id.lytcontain);
            lytdisplay = findViewById(R.id.lytdisplay);
            lytvoucherinfo = findViewById(R.id.lytvoucherinfo);
            lytinvetoryinfo = findViewById(R.id.lytinvetoryinfo);
            lytmain = findViewById(R.id.lytmain);

            recycleviewtransaction.setLayoutManager(new LinearLayoutManager(VoucherInfoActivity.this));
            recycleviewothercharge.setLayoutManager(new LinearLayoutManager(VoucherInfoActivity.this));
            recycleviewInv.setLayoutManager(new LinearLayoutManager(VoucherInfoActivity.this));

            spvouchertype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        ((TextView) spvouchertype.getSelectedView()).setTextColor(getResources().getColor(R.color.white));
                        Master master = (Master) parent.getItemAtPosition(position);
                        vouchertypecode = master.getMastercode();
                        vouchertypename = master.getMastername();
                        //System.out.println("=====================typecode" + vouchertypecode);
                    } catch (Exception e) {
                        e.printStackTrace();
                        databaseHelper.addExceptionERROR(e, subject);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });


            //spvouchertype.setSelection(0);
            try {
                if (vouchertypelist.size() == 0) {
                    Toast.makeText(VoucherInfoActivity.this, getResources().getString(R.string.novoucherfound), Toast.LENGTH_SHORT).show();
                } else if (from != null && !from.equals("home")) {
                    SpinnerVoucherTypeSelection(vouchertypecode);

                    int vchcodesales = Integer.parseInt(vchcode);
                    if (from.equals("salesreport") && vchcodesales <= 0) {
                        voucherno = vno;
                        vchcode = "0";
                    } else {
                        voucherno = "0";
                    }

                    //System.out.println("============== = = ="+voucherno+" = "+vchcode);
                    //OnShowVInfoDataClick(lyttop.getRootView());

                    boolean issyncable = SyncVisibility();
                    if (issyncable) {
                        OnSyncVInfoDataClick(lyttop.getRootView());
                    } else
                        Toast.makeText(VoucherInfoActivity.this, getResources().getString(R.string.datalimit), Toast.LENGTH_SHORT).show();


                } else if (session.getBooleanData(sessionname + "set" + compcode + orgcode)) {
                    vouchertypecode = session.getData(sessionname + "typecode" + compcode + orgcode);

                    SpinnerVoucherTypeSelection(vouchertypecode);

                    if (from.equals("home")) {
                        voucherno = session.getData(sessionname + "vno" + compcode + orgcode);
                        edtvcno.setText(voucherno);
                        vchcode = "0";
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();

                databaseHelper.addExceptionERROR(e, subject);
            }

            if (from.equals("home")) {
                lyttop.setVisibility(View.VISIBLE);
                imgdown.setVisibility(View.VISIBLE);
            }


            try {
                String localdata = databaseHelper.getVoucherInfoData(compcode, orgcode, voucherno, vchcode, vouchertypecode);

                if (!localdata.equals("")) {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            OnShowVInfoDataClick(lyttop.getRootView());
                        }
                    }, 150);

                } else if (AppController.isNetConnected(VoucherInfoActivity.this)) {
                    SyncVisibility();
                    btnshow.setVisibility(View.GONE);
                    imgdown.setVisibility(View.GONE);
                } else {
                    //lyttop.setVisibility(View.GONE);
                    imgdown.setVisibility(View.GONE);
                    Toast.makeText(VoucherInfoActivity.this, getResources().getString(R.string.checkinternet), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
                databaseHelper.addExceptionERROR(e, subject);
            }
        } catch (Exception e) {
            e.printStackTrace();

            databaseHelper.addExceptionERROR(e, subject);
        }
    }

    private void SpinnerVoucherTypeSelection(String vouchertypecode) {
        int i = 0;
        for (Master master : vouchertypelist) {
            if (master.getMastercode().equals(vouchertypecode)) {
                spvouchertype.setSelection(i);
                break;
            }
            i++;
        }
    }

    /*public void BtnEnableDisable(boolean isenable) {
        btnsync.setEnabled(isenable);
        btnshow.setEnabled(isenable);
    }*/

    public void BtnSyncVisibility() {

        SyncVisibility();
    }

    public boolean SyncVisibility() {
        String localdata = "";
        try {
            localdata = databaseHelper.getVoucherInfoData(compcode, orgcode, voucherno, vchcode, vouchertypecode);
        } catch (Exception e) {
            e.printStackTrace();
            databaseHelper.addExceptionERROR(e, subject);
        }

        if (localdata.equals("")) {
            btnshow.setVisibility(View.GONE);
        } else {
            btnshow.setVisibility(View.VISIBLE);
        }

        boolean issyncenable = false;
        if (session.getData(UserSessionManager.KEY_DEMO).equalsIgnoreCase("Yes") && session.getIntData(UserSessionManager.KEY_DEMOREQCOUNT) > (session.getIntData(UserSessionManager.KEY_DEMOREQTOTAL))) {
            btnsync.setVisibility(View.GONE);
        } else {
            btnsync.setVisibility(View.VISIBLE);
            issyncenable = true;
        }
        return issyncenable;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    private void StopHandlerProcess() {
        Constant.BtnEnableDisable(true, btnsync, btnshow);
        progressbar.setVisibility(View.GONE);
        txtsec.setVisibility(View.GONE);
        mHandler.removeCallbacks(hMyTimeTask);
    }

    private void StartHandlerProcess() {
        Constant.BtnEnableDisable(false, btnsync, btnshow);
        progressbar.setVisibility(View.VISIBLE);
        txtsec.setVisibility(View.VISIBLE);
        txtsec.setText("");
        nCounter = 0;
        mHandler.postDelayed(hMyTimeTask, 1000);
    }

    private Runnable hMyTimeTask = new Runnable() {
        public void run() {
            nCounter++;
            txtsec.setText("" + nCounter);
            mHandler.postDelayed(hMyTimeTask, 1000);
        }
    };

    public void OnVoucherPdfClick(View view) {
        /*if (ContextCompat.checkSelfPermission(VoucherInfoActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(VoucherInfoActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, ReqReadPermission);
        } else if (ContextCompat.checkSelfPermission(VoucherInfoActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(VoucherInfoActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, ReqWritePermission);
        } else {*/
        if (Constant.CheckReadWritePermissionGranted(VoucherInfoActivity.this)) {
            lytprogress.setVisibility(View.VISIBLE);
            btnexcel.setEnabled(false);
            btnpdf.setEnabled(false);
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {

                    /*File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getResources().getString(R.string.app_name) + "/");

                    if (!folder.exists()) {
                        boolean success = folder.mkdir();
                    }

                    path = folder.getAbsolutePath();
                    path = path + "/" + filename + ".pdf";
                    File filepath = new File(path);
                    if (filepath.exists()) {
                        filepath.delete();
                    }*/

                    File filepath = Constant.FilePathPdfExcel(true, ".pdf", VoucherInfoActivity.this, filename);
                    UserSessionManager.CreateVoucherInfoPdf(filename, address, companyname, pdfheadertext, txtvchno.getText().toString().trim(), txtvchdate.getText().toString().trim(), txtrefno.getText().toString().trim(), txttype.getText().toString().trim(), txtparty.getText().toString().trim(), txtvehicleno.getText().toString().trim(), txtcreatedby.getText().toString().trim(), txteditedby.getText().toString().trim(), txtinvtotal.getText().toString().trim(), inventroylist, txtothertotal.getText().toString(), otherchargelist, transactionlist);
                    openPdfExcel(filepath, "pdf");

                }
            }, 300);
        }
    }

    private void openPdfExcel(File file, String type) {
        lytprogress.setVisibility(View.GONE);

       /* Uri uri = FileProvider.getUriForFile(VoucherInfoActivity.this, getResources().getString(R.string.authority), file);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        if (type.equals("pdf"))
            intent.setDataAndType(uri, "application/pdf");
        else
            intent.setDataAndType(uri, "application/vnd.ms-excel");

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivity(intent);*/
        Constant.openPdfExcel(file, type, VoucherInfoActivity.this);
        btnexcel.setEnabled(true);
        btnpdf.setEnabled(true);
    }

    public void OnVoucherExcelClick(View view) {
        /*if (ContextCompat.checkSelfPermission(VoucherInfoActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(VoucherInfoActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, ReqReadPermission);
        } else if (ContextCompat.checkSelfPermission(VoucherInfoActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(VoucherInfoActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, ReqWritePermission);
        } else {*/
        if (Constant.CheckReadWritePermissionGranted(VoucherInfoActivity.this)) {
            lytprogress.setVisibility(View.VISIBLE);
            btnexcel.setEnabled(false);
            btnpdf.setEnabled(false);
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    writeStudentsListToExcel();
                }
            }, 300);
        }
    }

    public void writeStudentsListToExcel() {
        try {
            /*File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getResources().getString(R.string.app_name) + "/");

            if (!folder.exists()) {
                boolean success = folder.mkdir();
            }
            String path = folder.getAbsolutePath();
            path = path + "/" + filename + ".xlsx";

            File FILE_PATH = new File(path);

            if (FILE_PATH.exists()) {
                FILE_PATH.delete();
            }*/
            File FILE_PATH = Constant.FilePathPdfExcel(true, ".xlsx", VoucherInfoActivity.this, filename);

            XSSFWorkbook workbook = new XSSFWorkbook();

            XSSFSheet studentsSheet = workbook.createSheet("VoucherInfo");
            studentsSheet.getPrintSetup().setPaperSize(PrintSetup.A4_PAPERSIZE);
            studentsSheet.setHorizontallyCenter(true);

            Footer footer = studentsSheet.getFooter();
            footer.setRight("Page " + HeaderFooter.page() + " of " + HeaderFooter.numPages());

            studentsSheet.setColumnWidth(0, (15 * 500));
            studentsSheet.setColumnWidth(1, (15 * 200));
            studentsSheet.setColumnWidth(2, (15 * 200));
            studentsSheet.setColumnWidth(3, (15 * 400));

            int rowIndex = 0, nextrow = 0;
            //Row row = studentsSheet.createRow(rowIndex++);
            int cellIndex = 0;
            CellStyle style = workbook.createCellStyle();

            style.setBorderBottom(CellStyle.BORDER_THIN);
            style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            style.setBorderLeft(CellStyle.BORDER_THIN);
            style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            style.setBorderRight(CellStyle.BORDER_THIN);
            style.setRightBorderColor(IndexedColors.BLACK.getIndex());
            style.setBorderTop(CellStyle.BORDER_THIN);
            style.setTopBorderColor(IndexedColors.BLACK.getIndex());

            CellStyle borderStyle = workbook.createCellStyle();
            borderStyle.setBorderBottom(CellStyle.BORDER_THIN);
            borderStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            borderStyle.setBorderLeft(CellStyle.BORDER_THIN);
            borderStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            borderStyle.setBorderRight(CellStyle.BORDER_THIN);
            borderStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            borderStyle.setBorderTop(CellStyle.BORDER_THIN);
            borderStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
            ((XSSFCellStyle) borderStyle).setAlignment(HorizontalAlignment.CENTER);
            ((XSSFCellStyle) borderStyle).setVerticalAlignment(VerticalAlignment.CENTER);

            CellStyle rightborderStyle = workbook.createCellStyle();
            rightborderStyle.setBorderBottom(CellStyle.BORDER_THIN);
            rightborderStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            rightborderStyle.setBorderLeft(CellStyle.BORDER_THIN);
            rightborderStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            rightborderStyle.setBorderRight(CellStyle.BORDER_THIN);
            rightborderStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            rightborderStyle.setBorderTop(CellStyle.BORDER_THIN);
            rightborderStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
            ((XSSFCellStyle) rightborderStyle).setAlignment(HorizontalAlignment.RIGHT);
            ((XSSFCellStyle) rightborderStyle).setVerticalAlignment(VerticalAlignment.CENTER);

            XSSFFont font = workbook.createFont();
            font.setBold(true);
            style.setFont(font);
            ((XSSFCellStyle) style).setAlignment(HorizontalAlignment.CENTER);

            //row.createCell(cellIndex);
            CellStyle cs = workbook.createCellStyle();
            cs.setFont(font);
            ((XSSFCellStyle) cs).setAlignment(HorizontalAlignment.CENTER);
            ((XSSFCellStyle) cs).setVerticalAlignment(VerticalAlignment.CENTER);
            cs.setWrapText(true);

            XSSFFont comFont = workbook.createFont();
            comFont.setFontHeightInPoints((short) 20);
            CellStyle cellstyle = workbook.createCellStyle();
            cellstyle.setFont(comFont);
            ((XSSFCellStyle) cellstyle).setAlignment(HorizontalAlignment.CENTER);

            Row row = studentsSheet.createRow(rowIndex);
            row.createCell(cellIndex).setCellValue(companyname);
            row.getCell(cellIndex).setCellStyle(cellstyle);
            studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 3));

            rowIndex++;
            cellIndex = 0;

            Row addrow = studentsSheet.createRow(rowIndex);
            addrow.createCell(cellIndex).setCellValue(address);
            addrow.getCell(cellIndex).setCellStyle(cs);

            studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex + 3, 0, 3));

            rowIndex = rowIndex + 4;

            cellIndex = 0;

        /*Drawing drawing = studentsSheet.createDrawingPatriarch();
        CreationHelper creationHelper = workbook.getCreationHelper();
        ClientAnchor anchor = creationHelper.createClientAnchor();
        anchor.setDx1(0);
        anchor.setCol1(1);
        anchor.setRow1(rowIndex);
        anchor.setRow2(rowIndex);
        anchor.setCol2(3);
        XSSFDrawing xssfdrawing = (XSSFDrawing) drawing;
        XSSFClientAnchor xssfanchor = (XSSFClientAnchor) anchor;
        XSSFSimpleShape xssfshape = xssfdrawing.createSimpleShape(xssfanchor);
        xssfshape.setShapeType(ShapeTypes.LINE);
        xssfshape.setLineWidth(1);
        xssfshape.setLineStyle(0);
        xssfshape.setLineStyleColor(0, 0, 0);

        Row daterow = studentsSheet.createRow(rowIndex);
        daterow.createCell(cellIndex).setCellValue(pdfheadertext);
        daterow.getCell(cellIndex).setCellStyle(cs);

        studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 3));

        rowIndex++;*/

            Row blankrow = studentsSheet.createRow(rowIndex);
        /*rowIndex++;
        Row blankrow1 = studentsSheet.createRow(rowIndex);*/

            studentsSheet.setRepeatingRows(CellRangeAddress.valueOf("1:" + rowIndex));

            rowIndex++;

            //rowIndex = rowIndex + 5;
            nextrow = rowIndex;
            if (lytvoucherinfo.getVisibility() == View.VISIBLE) {

                cellIndex = 0;
                Row srow = studentsSheet.createRow(rowIndex);
                srow.createCell(cellIndex).setCellValue(getResources().getString(R.string.voucher_info));
                srow.getCell(cellIndex).setCellStyle(style);
                srow.createCell(1).setCellStyle(borderStyle);
                srow.createCell(2).setCellStyle(borderStyle);
                srow.createCell(3).setCellStyle(borderStyle);
                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 3));
                rowIndex++;


                cellIndex = 0;
                Row sdatarow = studentsSheet.createRow(rowIndex++);
                sdatarow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.vch_no));
                sdatarow.createCell(cellIndex++).setCellValue(txtvchno.getText().toString().trim());
                sdatarow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.date));
                sdatarow.createCell(cellIndex).setCellValue(txtvchdate.getText().toString());

                sdatarow.getCell(0).setCellStyle(style);
                sdatarow.getCell(1).setCellStyle(borderStyle);
                sdatarow.getCell(2).setCellStyle(style);
                sdatarow.getCell(3).setCellStyle(borderStyle);

                if (inventroylist.size() != 0) {
                    cellIndex = 0;
                    Row sdatarow1 = studentsSheet.createRow(rowIndex++);
                    sdatarow1.createCell(cellIndex++).setCellValue(getResources().getString(R.string.ref_no));
                    sdatarow1.createCell(cellIndex++).setCellValue(txtrefno.getText().toString().trim());
                    sdatarow1.createCell(cellIndex++).setCellValue(getResources().getString(R.string.type));
                    sdatarow1.createCell(cellIndex).setCellValue(txttype.getText().toString());
                    //System.out.println("=================== = = " + txttype.getText().toString().trim() + " = " + txtvchdate.getText().toString());

                    sdatarow1.getCell(0).setCellStyle(style);
                    sdatarow1.getCell(1).setCellStyle(borderStyle);
                    sdatarow1.getCell(2).setCellStyle(style);
                    sdatarow1.getCell(3).setCellStyle(borderStyle);
                }

                cellIndex = 0;
                Row sdatarow2 = studentsSheet.createRow(rowIndex);
                sdatarow2.createCell(cellIndex++).setCellValue(getResources().getString(R.string.party));
                sdatarow2.createCell(cellIndex).setCellValue(txtparty.getText().toString().trim());
                sdatarow2.getCell(0).setCellStyle(style);
                sdatarow2.getCell(1).setCellStyle(borderStyle);
                sdatarow2.createCell(2).setCellStyle(style);
                sdatarow2.createCell(3).setCellStyle(borderStyle);


                if (txtvehicleno.getText().toString().trim().length() == 0) {
                    studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 1, 3));
                } else {
                    sdatarow2.getCell(2).setCellValue(getResources().getString(R.string.vehi_no));
                    sdatarow2.getCell(3).setCellValue(txtvehicleno.getText().toString());
                }

                rowIndex++;

                Row stotalrow = studentsSheet.createRow(rowIndex);
                cellIndex = 0;
                stotalrow.createCell(cellIndex).setCellValue(txtcreatedby.getText().toString().trim());
                stotalrow.getCell(cellIndex++).setCellStyle(style);
                stotalrow.createCell(cellIndex++).setCellStyle(borderStyle);
                stotalrow.createCell(cellIndex).setCellValue(txteditedby.getText().toString().trim());
                stotalrow.getCell(cellIndex++).setCellStyle(style);
                stotalrow.createCell(cellIndex).setCellStyle(borderStyle);

                if (txteditedby.getText().toString().length() == 0) {
                    studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 3));
                } else {
                    studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 1));
                    studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 2, 3));
                }

                rowIndex++;
                nextrow = rowIndex;
            }

            if (inventroylist.size() != 0) {
                cellIndex = 0;
                Row prow = studentsSheet.createRow(rowIndex);
                prow.createCell(cellIndex).setCellValue(getResources().getString(R.string.inventory_info));
                prow.getCell(cellIndex).setCellStyle(style);
                prow.createCell(1).setCellStyle(borderStyle);
                prow.createCell(2).setCellStyle(borderStyle);
                prow.createCell(3).setCellStyle(borderStyle);
                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 3));
                rowIndex++;

                cellIndex = 0;
                Row pdatarow = studentsSheet.createRow(rowIndex);
                pdatarow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.stock_item));
                pdatarow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.qty));
                pdatarow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.rate));
                pdatarow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.total));
                pdatarow.getCell(0).setCellStyle(style);
                pdatarow.getCell(1).setCellStyle(style);
                pdatarow.getCell(2).setCellStyle(style);
                pdatarow.getCell(3).setCellStyle(style);
                rowIndex++;

                int i = 1;
                for (VoucherInfo voucherInfo : inventroylist) {
                    cellIndex = 0;
                    Row recrow = studentsSheet.createRow(rowIndex++);
                    recrow.createCell(cellIndex++).setCellValue(i + ". " + voucherInfo.getStockitemname());
                    recrow.createCell(cellIndex++).setCellValue(voucherInfo.getDtl2());
                    recrow.createCell(cellIndex++).setCellValue(voucherInfo.getDtl3());
                    recrow.createCell(cellIndex).setCellValue(voucherInfo.getDtl6());
                    recrow.getCell(0).setCellStyle(borderStyle);
                    recrow.getCell(1).setCellStyle(borderStyle);
                    recrow.getCell(2).setCellStyle(rightborderStyle);
                    recrow.getCell(3).setCellStyle(rightborderStyle);

                    String disc = voucherInfo.getDtl4().trim(), gst = voucherInfo.getDtl5().trim();

                    if ((disc.length() != 0 || !disc.equals("0")) && (gst.length() != 0 || !gst.equals("0"))) {

                        Row recrow1 = studentsSheet.createRow(rowIndex);

                        int cellIndex1 = 0;
                        recrow1.createCell(cellIndex1++).setCellValue("Disc: " + disc + "%, GST: " + gst + "%");
                        recrow1.createCell(cellIndex1++);
                        studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 1));
                        recrow1.createCell(cellIndex1++).setCellValue(voucherInfo.getDisgstrate());
                        recrow1.createCell(cellIndex1++);

                        studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex - 1, rowIndex, 3, 3));
                        recrow1.getCell(0).setCellStyle(borderStyle);
                        recrow1.getCell(1).setCellStyle(borderStyle);
                        recrow1.getCell(2).setCellStyle(rightborderStyle);
                        recrow1.getCell(3).setCellStyle(rightborderStyle);
                        rowIndex++;

                    }

                    if (voucherInfo.getDtl7().trim().length() != 0) {
                        cellIndex = 0;
                        //System.out.println("================= " + rowIndex + " ** ");
                        Row psrow = studentsSheet.createRow(rowIndex);
                        psrow.createCell(cellIndex).setCellValue(voucherInfo.getDtl7().trim());
                        psrow.getCell(cellIndex).setCellStyle(style);
                        psrow.createCell(1).setCellStyle(borderStyle);
                        psrow.createCell(2).setCellStyle(borderStyle);
                        psrow.createCell(3).setCellStyle(borderStyle);
                        studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 3));
                        rowIndex++;

                    }

                    i++;
                    //System.out.println("================= " + rowIndex);
                }


                //System.out.println("================= " + rowIndex + " === ");
                Row psrow = studentsSheet.createRow(rowIndex);
                cellIndex = 0;
                psrow.createCell(cellIndex).setCellValue(getResources().getString(R.string.total));
                psrow.getCell(cellIndex++).setCellStyle(style);
                psrow.createCell(cellIndex++).setCellStyle(borderStyle);
                psrow.createCell(cellIndex).setCellValue(txtinvtotal.getText().toString().trim());
                psrow.getCell(cellIndex++).setCellStyle(style);
                psrow.createCell(cellIndex).setCellStyle(rightborderStyle);
                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 1));
                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 2, 3));

                rowIndex++;

            }

            if (otherchargelist.size() != 0) {
                cellIndex = 0;
                Row prow = studentsSheet.createRow(rowIndex);
                prow.createCell(cellIndex).setCellValue(getResources().getString(R.string.other_charge));
                prow.getCell(cellIndex).setCellStyle(style);
                prow.createCell(1).setCellStyle(borderStyle);
                prow.createCell(2).setCellStyle(borderStyle);
                prow.createCell(3).setCellStyle(borderStyle);
                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 3));
                rowIndex++;

                cellIndex = 0;
                Row pdatarow = studentsSheet.createRow(rowIndex);
                pdatarow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.particulars));
                pdatarow.createCell(cellIndex++);
                pdatarow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.amount));
                pdatarow.createCell(cellIndex++);
                pdatarow.getCell(0).setCellStyle(style);
                pdatarow.getCell(1).setCellStyle(borderStyle);
                pdatarow.getCell(2).setCellStyle(style);
                pdatarow.getCell(3).setCellStyle(borderStyle);
                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 1));
                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 2, 3));

                rowIndex++;


                for (VoucherInfo voucherInfo : otherchargelist) {
                    cellIndex = 0;
                    Row recrow = studentsSheet.createRow(rowIndex);
                    recrow.createCell(cellIndex++).setCellValue(voucherInfo.getDtl1());
                    recrow.createCell(cellIndex++);
                    recrow.createCell(cellIndex++).setCellValue(voucherInfo.getDtl3());
                    recrow.createCell(cellIndex);
                    recrow.getCell(0).setCellStyle(borderStyle);
                    recrow.getCell(1).setCellStyle(borderStyle);
                    recrow.getCell(2).setCellStyle(rightborderStyle);
                    recrow.getCell(3).setCellStyle(rightborderStyle);
                    studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 1));
                    studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 2, 3));
                    rowIndex++;
                }

                Row psrow = studentsSheet.createRow(rowIndex);
                cellIndex = 0;
                psrow.createCell(cellIndex).setCellValue(getResources().getString(R.string.total_invoice_amount));
                psrow.getCell(cellIndex++).setCellStyle(style);
                psrow.createCell(cellIndex++).setCellStyle(borderStyle);
                psrow.createCell(cellIndex).setCellValue(txtothertotal.getText().toString().trim());
                psrow.getCell(cellIndex++).setCellStyle(style);
                psrow.createCell(cellIndex).setCellStyle(borderStyle);
                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 1));
                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 2, 3));

                rowIndex++;
            }

            if (transactionlist.size() != 0) {
                cellIndex = 0;
                Row prow = studentsSheet.createRow(rowIndex);
                prow.createCell(cellIndex).setCellValue(getResources().getString(R.string.accounting_transactions));
                prow.getCell(cellIndex).setCellStyle(style);
                prow.createCell(1).setCellStyle(borderStyle);
                prow.createCell(2).setCellStyle(borderStyle);
                prow.createCell(3).setCellStyle(borderStyle);
                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 3));
                rowIndex++;

                cellIndex = 0;
                Row pdatarow = studentsSheet.createRow(rowIndex);
                pdatarow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.particulars));
                pdatarow.createCell(cellIndex++);
                pdatarow.createCell(cellIndex++).setCellValue(getResources().getString(R.string.amount));
                pdatarow.createCell(cellIndex++);
                pdatarow.getCell(0).setCellStyle(style);
                pdatarow.getCell(1).setCellStyle(borderStyle);
                pdatarow.getCell(2).setCellStyle(style);
                pdatarow.getCell(3).setCellStyle(borderStyle);
                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 1));
                studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 2, 3));

                rowIndex++;

                for (VoucherInfo voucherInfo : transactionlist) {
                    cellIndex = 0;
                    Row recrow = studentsSheet.createRow(rowIndex);
                    recrow.createCell(cellIndex++).setCellValue(voucherInfo.getParticulars());
                    recrow.createCell(cellIndex++);
                    recrow.createCell(cellIndex++).setCellValue(voucherInfo.getDisplayamount());
                    recrow.createCell(cellIndex);
                    recrow.getCell(0).setCellStyle(borderStyle);
                    recrow.getCell(1).setCellStyle(borderStyle);
                    recrow.getCell(2).setCellStyle(rightborderStyle);
                    recrow.getCell(3).setCellStyle(rightborderStyle);
                    studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 0, 1));
                    studentsSheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, 2, 3));

                    rowIndex++;
                }

            }


            try {
                FileOutputStream fos = new FileOutputStream(FILE_PATH);
                workbook.write(fos);
                fos.close();

                openPdfExcel(FILE_PATH, "excel");
            } catch (FileNotFoundException e) {
                e.printStackTrace();

                databaseHelper.addFileNotFoundERROR(e, subject);
            } catch (IOException e) {
                e.printStackTrace();

                databaseHelper.addIOERROR(e, subject);
            }
        } catch (Exception e) {
            e.printStackTrace();
            databaseHelper.addExceptionERROR(e, subject);
        }
    }

    public void OnShowVInfoDataClick(View view) {
        try {
            hideKeyboard();
            if (from != null && from.equals("home")) {

                voucherno = edtvcno.getText().toString().trim();

                if (voucherno.length() == 0) {
                    voucherno = "0";
                    if (from.equals("home")) {
                        edtvcno.setError("Enter Voucher No");
                        return;
                    }
                }
            }

            //System.out.println("=================== = = " + vchcode + " = " + vouchertypecode);


            boolean issyncable = SyncVisibility();

            if (from != null && !from.equals("home")) {
                if (issyncable) {
                    OnSyncVInfoDataClick(lyttop.getRootView());
                } else
                    Toast.makeText(VoucherInfoActivity.this, getResources().getString(R.string.datalimit), Toast.LENGTH_SHORT).show();
            } else {
                String localdata = databaseHelper.getVoucherInfoData(compcode, orgcode, voucherno, vchcode, vouchertypecode);
                if (localdata.equals("")) {
                    if (from != null && from.equals("home")) {
                        Toast.makeText(VoucherInfoActivity.this, getResources().getString(R.string.trytosync), Toast.LENGTH_SHORT).show();
                    } else if (issyncable)
                        OnSyncVInfoDataClick(lyttop.getRootView());
                    else
                        Toast.makeText(VoucherInfoActivity.this, getResources().getString(R.string.datalimit), Toast.LENGTH_SHORT).show();
                } else {
                    StartHandlerProcess();
                    GetVoucherInfoData(localdata);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            databaseHelper.addExceptionERROR(e, subject);
        }
    }

    public void OnSyncVInfoDataClick(View view) {
        try {

            hideKeyboard();
            voucherno = edtvcno.getText().toString().trim();

            if (voucherno.length() == 0) {
                voucherno = "0";
                if (from.equals("home")) {
                    edtvcno.setError("Enter Voucher No");
                    return;
                }
            }

            if (AppController.isConnected(VoucherInfoActivity.this, null)) {
                lytbottom.setVisibility(View.GONE);
                lytmain.setVisibility(View.GONE);
                StartHandlerProcess();
                String url = Constant.VoucherInfoUrl + orgcode + "&mobno=" + session.getData(UserSessionManager.KEY_Mobile) + "&imei=" + session.getData(UserSessionManager.KEY_IMEI) + "&devicecode=" + session.getData(UserSessionManager.KEY_DEVICECODE) + "&compcode=" + compcode + "&demo=" + session.getData(UserSessionManager.KEY_DEMO) + "&vouchertypecode=" + vouchertypecode + "&vchno=" + voucherno + "&vchcode=" + vchcode;

                url = url.replaceAll(" ", "%20");

                //System.out.println("=============url -- " + url);
                try {
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            if (session.getData(UserSessionManager.KEY_DEMO).equalsIgnoreCase("Yes"))
                                OwnerHomeActivity.DemoReqCout();

                            if (response.contains("\n"))
                                response = response.replace("\n", "").trim();

                            System.out.println("=============url --res voucher " + response);

                            /*if (response.contains(getResources().getString(R.string.noresponse))) {
                                StopHandlerProcess();
                                Toast.makeText(VoucherInfoActivity.this, getResources().getString(R.string.notconnectedserver), Toast.LENGTH_SHORT).show();
                            } else if (response.contains(getResources().getString(R.string.NOVCH))) {
                                StopHandlerProcess();
                                Toast.makeText(VoucherInfoActivity.this, getResources().getString(R.string.nodatafound), Toast.LENGTH_SHORT).show();
                            } else if (response.contains(getResources().getString(R.string.multiuseword))) {
                                StopHandlerProcess();
                                Toast.makeText(VoucherInfoActivity.this, getResources().getString(R.string.multiuse), Toast.LENGTH_SHORT).show();
                            }*/

                            if (Constant.ResponseErrorCheck(VoucherInfoActivity.this, response, true)) {
                                StopHandlerProcess();
                            } else {
                                if (from != null && from.equals("home")) {
                                    String localdata = databaseHelper.getVoucherInfoData(compcode, orgcode, voucherno, vchcode, vouchertypecode);
                                    if (localdata.equals("")) {
                                        databaseHelper.addVoucherInfodata(compcode, orgcode, response, voucherno, vchcode, vouchertypecode);
                                    } else {
                                        databaseHelper.UpdateVoucherInfoData(compcode, orgcode, response, voucherno, vchcode, vouchertypecode);
                                    }
                                }

                                GetVoucherInfoData(response);

                            }
                            //StopHandlerProcess();
                        }

                    },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    StopHandlerProcess();
                                    String message = Constant.VolleyErrorMessage(error);
                                    if (!message.equals(""))
                                        Toast.makeText(VoucherInfoActivity.this, message, Toast.LENGTH_SHORT).show();

                                }
                            });

                    AppController.getInstance().getRequestQueue().getCache().clear();
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    AppController.getInstance().addToRequestQueue(stringRequest);
                } catch (Exception e) {
                    e.printStackTrace();
                    databaseHelper.addExceptionERROR(e, subject);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            databaseHelper.addExceptionERROR(e, subject);
        }
    }

    public void OnHideClick(View view) {
        Constant.SetViewVisiblility(imgdown, lyttop);
    }

    /*public void SetViewVisiblility() {
        imgdown.setVisibility(View.VISIBLE);
        if (lyttop.getVisibility() == View.GONE) {
            imgdown.setImageResource(R.drawable.ic_bup);
            lyttop.setVisibility(View.VISIBLE);
        } else {
            imgdown.setImageResource(R.drawable.ic_bdown);
            lyttop.setVisibility(View.GONE);
        }
    }*/

    private void GetVoucherInfoData(String response) {
        try {
            StopHandlerProcess();
            if (databaseHelper.getAllMasterData(compcode, orgcode).size() == 0) {
                Toast.makeText(VoucherInfoActivity.this, getResources().getString(R.string.master_updatemsg), Toast.LENGTH_SHORT).show();
                return;
            }

            if (from.equals("home")) {
                session.setBooleanData(sessionname + "set" + compcode + orgcode, true);
                session.setData(sessionname + "typecode" + compcode + orgcode, vouchertypecode);
                session.setData(sessionname + "typename" + compcode + orgcode, spvouchertype.getSelectedItem().toString().trim());
                session.setData(sessionname + "vno" + compcode + orgcode, voucherno);
                session.setData(sessionname + "vcode" + compcode + orgcode, vchcode);
            }

            try {
                if (response.contains(getResources().getString(R.string.NOVCH))) {
                    lytbottom.setVisibility(View.GONE);
                    lytmain.setVisibility(View.GONE);
                    lytpdfexcel.setVisibility(View.GONE);
                    txtnodata.setVisibility(View.VISIBLE);
                    imgdown.setVisibility(View.GONE);
                } else {
                    JSONArray jsonArray = new JSONArray(response);
                    voucherInfoArrayList = new ArrayList<>();


                    lytvoucherinfo.setVisibility(View.GONE);
                    lytinvetoryinfo.setVisibility(View.GONE);
                    lytothercharge.setVisibility(View.GONE);
                    lytaccounting.setVisibility(View.GONE);

                    if (jsonArray.length() == 2) {
                        lytbottom.setVisibility(View.GONE);
                        lytpdfexcel.setVisibility(View.GONE);
                        lytmain.setVisibility(View.GONE);
                        txtnodata.setVisibility(View.VISIBLE);
                    } else {

                        vouchertypename = session.getData(sessionname + "typename" + compcode + orgcode);
                        String header = companyname;//+ "\n ";//Voucher Info of type " + vouchertypename;
                        SpannableString ssdate = new SpannableString(header);
                        ssdate.setSpan(new RelativeSizeSpan(1.3f), 0, companyname.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        ssdate.setSpan(new StyleSpan(Typeface.BOLD), 0, companyname.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        //ssdate.setSpan(new UnderlineSpan(), header.indexOf("Voucher"), header.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        datedisplay.setText(ssdate);
                        filename = companyname + "-voucherinfo-" + vouchertypename + " of " + vchcode + voucherno;

                        pdfheadertext = "Voucher Info of type " + vouchertypename;
                        headertext = "\n" + companyname + "\n" + address + "\n\nVoucher Info of type " + vouchertypename + "\n";

                        //System.out.println("=========== = = =voucher response " + response);

                        inventroylist.clear();
                        otherchargelist.clear();
                        transactionlist.clear();

                        double inventorytotal = 0, otherchargetotal = 0;
                        for (int i = 1; i < jsonArray.length() - 1; i++) {
                            JSONArray voucherarray = jsonArray.getJSONArray(i);

                            String dtype = voucherarray.getString(0);
                            if (dtype.equalsIgnoreCase(getResources().getString(R.string.vch))) {
                                //System.out.println("=========== = = =voucher vch " + voucherarray.toString());
                                lytvoucherinfo.setVisibility(View.VISIBLE);
                                txtvchno.setText(voucherarray.getString(1));
                                txtvchdate.setText(Constant.FormateDate(output, Constant.FormateDate(input, voucherarray.getString(2), subject, databaseHelper)));
                                txtrefno.setText(voucherarray.getString(3));

                                txtparty.setText(voucherarray.getString(4));

                                if (voucherarray.getString(7).trim().length() != 0) {
                                    lytremark.setVisibility(View.VISIBLE);
                                    txtremark.setText(voucherarray.getString(7).trim());
                                } else {
                                    lytremark.setVisibility(View.GONE);
                                }


                                LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT);
                                if (!voucherarray.getString(5).equals("")) {
                                    lytvehicle.setVisibility(View.VISIBLE);
                                    txtvehicleno.setText(voucherarray.getString(5));
                                    //lytvehicle.setBackground(null);
                                    p.weight = (float) 1.3;
                                } else {
                                    lytvehicle.setVisibility(View.GONE);
                                    //lytvehicle.setBackground(getResources().getDrawable(R.drawable.border_black));
                                    p.weight = (float) 3.3;
                                }
                                txtparty.setLayoutParams(p);
                                String type = databaseHelper.getMasterValue(compcode, DatabaseHelper.MASTER_MASTERCODE, voucherarray.getString(6), getResources().getString(R.string.master_paym_type), DatabaseHelper.MASTER_MASTERNAME, orgcode);
                                txttype.setText(type);

                            } else if (dtype.equalsIgnoreCase(getResources().getString(R.string.vch2))) {
                                LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                LinearLayout.LayoutParams q = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                                System.out.println("============= = = =" + voucherarray.getString(1) + " = " + voucherarray.getString(2) + " = " + voucherarray.getString(3) + " = " + voucherarray.getString(4));

                                if ((!voucherarray.getString(1).trim().equals("") && !voucherarray.getString(2).trim().equals("")) && (!voucherarray.getString(3).trim().equals("") && !voucherarray.getString(4).trim().equals(""))) {
                                    txtcreatedby.setVisibility(View.VISIBLE);
                                    txteditedby.setVisibility(View.VISIBLE);

                                    p.weight = 2;
                                    q.weight = 2;
                                    txtcreatedby.setLayoutParams(p);
                                    txteditedby.setLayoutParams(q);
                                    txtcreatedby.setText("Created by " + voucherarray.getString(1) + " on " + Constant.FormateDate(outputdatetime, Constant.FormateDate(inputdatetime, voucherarray.getString(2), subject, databaseHelper)));
                                    txteditedby.setText("Edited by " + voucherarray.getString(3) + " on " + Constant.FormateDate(outputdatetime, Constant.FormateDate(inputdatetime, voucherarray.getString(4), subject, databaseHelper)));
                                } else if (!voucherarray.getString(1).trim().equals("") && !voucherarray.getString(2).trim().equals("")) {
                                    txtcreatedby.setVisibility(View.VISIBLE);
                                    txteditedby.setVisibility(View.GONE);
                                    p.weight = 4;
                                    //q.weight = 0;
                                    txtcreatedby.setLayoutParams(p);
                                    txtcreatedby.setText("Created by " + voucherarray.getString(1) + " on " + Constant.FormateDate(outputdatetime, Constant.FormateDate(inputdatetime, voucherarray.getString(2), subject, databaseHelper)));
                                } else {
                                    txteditedby.setVisibility(View.GONE);
                                    txtcreatedby.setVisibility(View.GONE);
                                }


                            } else if (dtype.equalsIgnoreCase(getResources().getString(R.string.inv))) {
                                lytinvetoryinfo.setVisibility(View.VISIBLE);
                                String stockitem = databaseHelper.getMasterValue(compcode, DatabaseHelper.MASTER_MASTERCODE, voucherarray.getString(1), getResources().getString(R.string.master_item), DatabaseHelper.MASTER_MASTERNAME, orgcode);


                                double rate = Constant.ConvertToDouble(voucherarray.getString(3));
                                double qty = Constant.ConvertToDouble(voucherarray.getString(2));
                                double totalamount = 0;
                                double discount, gst, nettotal;

                            /*int qty = 1;
                            if (voucherarray.getString(4).equals(""))
                                discount = 0;
                            else
                                discount = Double.parseDouble(voucherarray.getString(4));


                            if (voucherarray.getString(5).equals(""))
                                gst = 0;
                            else
                                gst = Double.parseDouble(voucherarray.getString(5));*/

                                if (voucherarray.getString(6).equals(""))
                                    nettotal = 0;
                                else
                                    nettotal = Constant.ConvertToDouble(voucherarray.getString(6));


                            /*double basicamt = qty * rate;
                            double discountamount = 0;
                            double gstamount = 0;
                            double totalamount = 0;

                            if (discount != 0) {
                                discountamount = (basicamt * discount) / 100;
                            }
                            if (gst != 0) {
                                gstamount = ((basicamt - discountamount) * gst) / 100;
                            }

                            totalamount = basicamt - discountamount + gstamount;*/

                                totalamount = (nettotal / qty);


                                inventorytotal = inventorytotal + nettotal;
                                otherchargetotal = inventorytotal;


                                txtinvtotal.setText(Constant.ConvertToIndianRupeeFormat(inventorytotal, false, true, false));
                                inventroylist.add(new VoucherInfo(voucherarray.getString(0), voucherarray.getString(1), voucherarray.getString(2), Constant.ConvertToIndianRupeeFormat(rate, false, true, false), voucherarray.getString(4), voucherarray.getString(5), Constant.ConvertToIndianRupeeFormat(nettotal, false, true, false), voucherarray.getString(7), Constant.ConvertToIndianRupeeFormat(totalamount, false, true, false), stockitem));
                            } else if (dtype.equalsIgnoreCase(getResources().getString(R.string.oth))) {
                                lytothercharge.setVisibility(View.VISIBLE);

                                double chargeamt;

                                if (voucherarray.getString(3).equals(""))
                                    chargeamt = 0;
                                else
                                    chargeamt = Constant.ConvertToDouble(voucherarray.getString(3));

                                otherchargetotal = otherchargetotal + chargeamt;
                                txtothertotal.setText(Constant.ConvertToIndianRupeeFormat(otherchargetotal, false, true, false));
                                otherchargelist.add(new VoucherInfo(voucherarray.getString(0), voucherarray.getString(1), voucherarray.getString(2), Constant.ConvertToIndianRupeeFormat(chargeamt, false, true, false), voucherarray.getString(4), voucherarray.getString(5), voucherarray.getString(6), voucherarray.getString(7)));
                            } else if (dtype.equalsIgnoreCase(getResources().getString(R.string.acct))) {
                                lytaccounting.setVisibility(View.VISIBLE);
                                String particulars = databaseHelper.getMasterValue(compcode, DatabaseHelper.MASTER_MASTERCODE, voucherarray.getString(1), getResources().getString(R.string.master_ledger_name), DatabaseHelper.MASTER_MASTERNAME, orgcode);
                                String displayamount;
                                String dtl2 = Constant.ConvertToIndianRupeeFormat(Constant.ConvertToDouble(voucherarray.getString(2)), false, true, false);
                                String dtl3 = Constant.ConvertToIndianRupeeFormat(Constant.ConvertToDouble(voucherarray.getString(3)), false, true, false);
                                if (voucherarray.getString(2).equals("0") || voucherarray.getString(2).equalsIgnoreCase("")) {
                                    displayamount = dtl3 + " Cr";
                                } else {
                                    displayamount = dtl2 + " Dr";
                                }
                                transactionlist.add(new VoucherInfo(voucherarray.getString(0), voucherarray.getString(1), dtl2, dtl3, voucherarray.getString(4), voucherarray.getString(5), voucherarray.getString(6), voucherarray.getString(7), particulars, displayamount, ""));
                            }
                        }

                        if (inventroylist.size() == 0)
                            lytref.setVisibility(View.GONE);
                        else
                            lytref.setVisibility(View.VISIBLE);

                        if (from.equals("home")) {
                            Constant.SetViewVisiblility(imgdown, lyttop);
                            SyncVisibility();
                        }

                        txtnodata.setVisibility(View.GONE);
                        lytbottom.setVisibility(View.VISIBLE);
                        lytmain.setVisibility(View.VISIBLE);
                        lytpdfexcel.setVisibility(View.VISIBLE);


                        recycleviewInv.setAdapter(new VoucherAdapter(inventroylist, getResources().getString(R.string.inventory_info)));
                        recycleviewothercharge.setAdapter(new VoucherAdapter(otherchargelist, getResources().getString(R.string.other_charge)));
                        recycleviewtransaction.setAdapter(new VoucherAdapter(transactionlist, getResources().getString(R.string.accounting_transactions)));

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                StopHandlerProcess();
                databaseHelper.addExceptionERROR(e, subject);
            }
        } catch (Exception e) {
            e.printStackTrace();
            databaseHelper.addExceptionERROR(e, subject);
        }
    }

    public void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        View currentFocusedView = getCurrentFocus();
        if (currentFocusedView != null) {
            inputManager.hideSoftInputFromWindow(currentFocusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public class VoucherAdapter extends RecyclerView.Adapter<VoucherAdapter.ViewHolder> {

        public ArrayList<VoucherInfo> voucherArrayList;

        String from;

        public VoucherAdapter(ArrayList<VoucherInfo> voucherArrayList, String from) {
            this.voucherArrayList = voucherArrayList;

            this.from = from;
        }

        @NonNull
        @Override
        public VoucherAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view;
            if (from.equals(getResources().getString(R.string.inventory_info)))
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lyt_inventoryinfo, parent, false);
            else if (from.equals(getResources().getString(R.string.other_charge)))
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lyt_othercharges, parent, false);
            else
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lyt_accounting_transaction, parent, false);

            return new VoucherAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

            final VoucherInfo model = voucherArrayList.get(position);

            if (from.equals(getResources().getString(R.string.inventory_info))) {
                holder.txtno.setText((position + 1) + ". ");
                holder.txtitem.setText(model.getStockitemname());
                holder.txtqty.setText(model.getDtl2());
                holder.txtrate.setText(model.getDtl3());

                String data = model.getDtl6();
                holder.txttotal.setText(data);
                int size;
                if (data.length() <= 10) {
                    size = 12;
                } else if (data.length() <= 14) {
                    size = 10;
                } else if (data.length() <= 16) {
                    size = 9;
                } else {
                    size = 8;
                }
                holder.txttotal.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);

                String disc = model.getDtl4().trim(), gst = model.getDtl5().trim();

                if ((disc.length() == 0 || disc.equals("0")) && (gst.length() == 0 || gst.equals("0"))) {
                    //holder.lytdiscount.setVisibility(View.GONE);
                    holder.txtdis.setVisibility(View.GONE);
                    holder.txtdicamt.setVisibility(View.GONE
                    );
                } else {
                    //holder.lytdiscount.setVisibility(View.VISIBLE);
                    holder.txtdis.setVisibility(View.VISIBLE);
                    holder.txtdicamt.setVisibility(View.VISIBLE);
                    holder.txtdis.setText("Disc: " + disc + "%, GST: " + gst + "%");
                    holder.txtdicamt.setText(model.getDisgstrate());
                }

                if (model.getDtl7().trim().length() == 0) {
                    holder.txtsr.setVisibility(View.GONE);
                } else {
                    holder.txtsr.setVisibility(View.VISIBLE);
                    holder.txtsr.setText(model.getDtl7().trim());
                }

            } else if (from.equals(getResources().getString(R.string.other_charge))) {
                holder.txtparticulars.setText(model.getDtl1());
                holder.txtamount.setText(model.getDtl3());
            } else {
                holder.txtparticulars.setText(model.getParticulars());

                //System.out.println("=============url->" + model.getDtl2().replace(" ", "") + " = " + model.getDtl3().trim());
                holder.txtdebitcredit.setText(model.getDisplayamount());
            }
        }


        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public int getItemCount() {
            return voucherArrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            //LinearLayout lytdiscount;
            public TextView txtno, txtitem, txtqty, txtrate, txtdis, txtdicamt, txttotal, txtsr, txtparticulars, txtamount, txtdebitcredit;

            public ViewHolder(View itemView) {
                super(itemView);

                if (from.equals(getResources().getString(R.string.inventory_info))) {
                    txtitem = itemView.findViewById(R.id.txtitem);
                    txtqty = itemView.findViewById(R.id.txtqty);
                    txtrate = itemView.findViewById(R.id.txtrate);
                    txtdis = itemView.findViewById(R.id.txtdis);
                    txtdicamt = itemView.findViewById(R.id.txtdicamt);
                    txttotal = itemView.findViewById(R.id.txttotal);
                    txtsr = itemView.findViewById(R.id.txtsr);
                    //lytdiscount = itemView.findViewById(R.id.lytdiscount);
                    txtno = itemView.findViewById(R.id.txtno);
                } else if (from.equals(getResources().getString(R.string.other_charge))) {
                    txtparticulars = itemView.findViewById(R.id.txtparticulars);
                    txtamount = itemView.findViewById(R.id.txtamount);
                } else {
                    txtparticulars = itemView.findViewById(R.id.txtparticulars);
                    txtdebitcredit = itemView.findViewById(R.id.txtdebitcredit);
                }

            }

        }
    }
}
