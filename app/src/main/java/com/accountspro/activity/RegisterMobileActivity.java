package com.accountspro.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.accountspro.R;
import com.accountspro.helper.AppController;
import com.accountspro.helper.Constant;
import com.accountspro.helper.DatabaseHelper;
import com.accountspro.helper.UserSessionManager;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;

public class RegisterMobileActivity extends AppCompatActivity {

    EditText edtmobileno;
    ProgressBar progressbar;
    UserSessionManager session;
    Button btnsend;
    boolean fromlogin;
    TextView txttoolbar, txtinst;
    public final int ReadPhoneStatemain1 = 0;
    DatabaseHelper databaseHelper;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_mobile);

        fromlogin = getIntent().getBooleanExtra("fromlogin", false);

        databaseHelper = new DatabaseHelper(RegisterMobileActivity.this);
        session = new UserSessionManager(RegisterMobileActivity.this);

        txttoolbar = (TextView) findViewById(R.id.txttoolbar);
        txtinst = (TextView) findViewById(R.id.txtinst);

        if (fromlogin) {
            /*txttoolbar.setText(getResources().getString(R.string.login));
            txtinst.setText(getResources().getString(R.string.regi_logintext));*/
            txttoolbar.setText(getResources().getString(R.string.forgot));
            txtinst.setText(getResources().getString(R.string.regi_forgottext));
        }
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        edtmobileno = (EditText) findViewById(R.id.edtmobileno);
        btnsend = (Button) findViewById(R.id.btnsend);

        if (ContextCompat.checkSelfPermission(RegisterMobileActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(RegisterMobileActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, ReadPhoneStatemain1);
        } else if (session.getData(UserSessionManager.KEY_IMEI).equals("")) {
            AppController.getUniqueIMEIId(session, RegisterMobileActivity.this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (session.getData(UserSessionManager.KEY_IMEI).equals(""))
            AppController.getUniqueIMEIId(session, RegisterMobileActivity.this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case ReadPhoneStatemain1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (session.getData(UserSessionManager.KEY_IMEI).equals(""))
                        AppController.getUniqueIMEIId(session, RegisterMobileActivity.this);
                }
                break;
        }
    }

    public void OnRegisterMobClick(View view) {

        final String mobileno = edtmobileno.getText().toString().trim();
        if (mobileno.length() == 0)
            edtmobileno.setError("Enter Mobile Number");
        else if (mobileno.length() < 10 || mobileno.length() > 12)
            edtmobileno.setError("Invalid Mobile Number");
        else if (AppController.isConnected(RegisterMobileActivity.this, findViewById(R.id.activity_main))) {
            btnsend.setEnabled(false);
            btnsend.setClickable(false);
            progressbar.setVisibility(View.VISIBLE);

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(edtmobileno.getWindowToken(), 0);

            if (session.getData(UserSessionManager.KEY_IMEI).equals("")) {
                Toast.makeText(RegisterMobileActivity.this, getResources().getString(R.string.phonereadpermission), Toast.LENGTH_SHORT).show();
            }


            String url = Constant.MobRegiUrl + mobileno + "&imei=" + session.getData(UserSessionManager.KEY_IMEI);
            //System.out.println("======================= = =" + url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressbar.setVisibility(View.GONE);
                            if (response.contains("\n"))
                                response = response.replace("\n", "").trim();

                            //System.out.println("======================= = = " + response);

                            if (response.contains(getResources().getString(R.string.qstringerr))) {
                                btnsend.setEnabled(true);
                                btnsend.setClickable(true);
                                Toast.makeText(RegisterMobileActivity.this, getResources().getString(R.string.trylater), Toast.LENGTH_SHORT).show();
                                return;
                            } else if (response.equalsIgnoreCase("Reg;Reg;0")) {
                                btnsend.setEnabled(true);
                                btnsend.setClickable(true);
                                Toast.makeText(RegisterMobileActivity.this, getResources().getString(R.string.multiuse), Toast.LENGTH_SHORT).show();
                                return;
                            }

                            //System.out.println("============= = regi " + response);

                            String[] data = response.split(";");
                            try {
                                session.setData(UserSessionManager.KEY_DEVICECODE, data[2]);
                            } catch (Exception e) {
                                e.printStackTrace();

                                String subject = Constant.ERRORID + " | " + mobileno + " | " + Constant.ERRORNOORG + " | " + Constant.ERRORNOCOMP;
                                databaseHelper.addExceptionERROR(e, subject);
                            }

                            boolean isregister = false;
                            String status = data[0] + ";" + data[1];
                            if (status.equalsIgnoreCase("Yes;No")) {
                                //Yes;No  -> Registered & Not a Demo
                                session.setData(UserSessionManager.KEY_REGI, "Yes");
                                session.setData(UserSessionManager.KEY_DEMO, "No");
                                isregister = true;
                            } else if (status.equalsIgnoreCase("Yes;Yes")) {
                                //Yes;Yes  -> Registered & Demo
                                session.setData(UserSessionManager.KEY_REGI, "Yes");
                                session.setData(UserSessionManager.KEY_DEMO, "Yes");
                                isregister = true;
                            } else if (status.equalsIgnoreCase("No;No")) {
                                //No;No  -> Not Registered & Not a Demo
                                session.setData(UserSessionManager.KEY_REGI, "No");
                                session.setData(UserSessionManager.KEY_DEMO, "No");
                                isregister = false;
                            } else if (status.equalsIgnoreCase("No;Yes")) {
                                //No;Yes  -> Not Registered & Demo
                                session.setData(UserSessionManager.KEY_REGI, "No");
                                session.setData(UserSessionManager.KEY_DEMO, "Yes");
                                isregister = false;
                            }

                            if (!isregister) {
                                btnsend.setEnabled(true);
                                btnsend.setClickable(true);
                                Toast.makeText(RegisterMobileActivity.this, "Sorry...You are not Registered with " + getResources().getString(R.string.app_name), Toast.LENGTH_SHORT).show();
                                return;
                            }

                            final String OTP = String.format("%04d", new SecureRandom().nextInt(10000));
                            //System.out.println("======== = = ==otp " + OTP);
                            String smstext = null;
                            try {
                                smstext = URLEncoder.encode("<#> Your Verification OTP for " + getResources().getString(R.string.app_name) + " is : " + OTP + "\n" + Constant.SMSHashCode, "UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();

                                String subject = Constant.ERRORID + " | " + mobileno + " | " + Constant.ERRORNOORG + " | " + Constant.ERRORNOCOMP;
                                databaseHelper.addUnSuEncodingERROR(e, subject);
                            }

                            String otpurl = Constant.OTPUrl + "mobile=" + mobileno + "&message=" + smstext;
                            //System.out.println("==========otp " + otpurl);
                            StringRequest stringRequest = new StringRequest(Request.Method.POST, otpurl, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response1) {
                                    progressbar.setVisibility(View.GONE);

                                    if (response1.contains("success")) {
                                        startActivity(new Intent(RegisterMobileActivity.this, OTPVarificationActivity.class).putExtra("mobileno", mobileno).putExtra("OTP", OTP));
                                    } else {
                                        Toast.makeText(RegisterMobileActivity.this, "Failed...Try Again", Toast.LENGTH_SHORT).show();
                                    }
                                    new Handler().postDelayed(new Runnable() {

                                        @Override
                                        public void run() {
                                            btnsend.setEnabled(true);
                                            btnsend.setClickable(true);
                                        }

                                    }, 1000);
                                }

                            },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            progressbar.setVisibility(View.GONE);
                                            btnsend.setEnabled(true);
                                            btnsend.setClickable(true);
                                            String message = Constant.VolleyErrorMessage(error);
                                            if (!message.equals(""))
                                                Toast.makeText(RegisterMobileActivity.this, message, Toast.LENGTH_SHORT).show();
                                        }
                                    });
                            AppController.getInstance().getRequestQueue().getCache().clear();
                            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            AppController.getInstance().addToRequestQueue(stringRequest);

                            //

                        }


                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            btnsend.setEnabled(true);
                            btnsend.setClickable(true);
                            progressbar.setVisibility(View.GONE);
                            String message = Constant.VolleyErrorMessage(error);

                            if (!message.equals(""))
                                Toast.makeText(RegisterMobileActivity.this, message, Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    return params;
                }

            };
            AppController.getInstance().getRequestQueue().getCache().clear();
            AppController.getInstance().addToRequestQueue(stringRequest);

        }
    }


}
