package com.accountspro.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.accountspro.R;
import com.accountspro.helper.AppController;
import com.accountspro.helper.UserSessionManager;

public class LoginActivity extends AppCompatActivity {

    UserSessionManager session;
    public final int ReadPhoneState = 0;
    public int ReqReadPermission = 1;
    public int ReqWritePermission = 2;
    private static final int PERMISSION_REQUEST_CODE = 200;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Window window = getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);

        session = new UserSessionManager(LoginActivity.this);

        /*if (ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, ReqReadPermission);
        } else if (ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, ReqWritePermission);
        }
        if (ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, ReadPhoneState);
        }*/

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);


        /*else {
            if (session.getData(UserSessionManager.KEY_IMEI).equals(""))
                AppController.getUniqueIMEIId(session, LoginActivity.this);
        }*/

    }

    public void OnOwnerLoginClick(View view) {
        Intent intent;
        if (session.isUserLoggedIn()) {
            intent = new Intent(LoginActivity.this, OrganizationActivity.class).putExtra("from", "login");
        } else if (session.getBooleanData(UserSessionManager.KEY_PINSET)) {
            intent = new Intent(LoginActivity.this, OwnerLoginActivity.class);
        } else if (session.getBooleanData(UserSessionManager.KEY_VERIFIED)) {
            intent = new Intent(LoginActivity.this, PinGenerationActivity.class).putExtra("from", "login");
        } else if (session.getBooleanData(UserSessionManager.KEY_OTPSEND)) {
            intent = new Intent(LoginActivity.this, OTPVarificationActivity.class);
            intent.putExtra("mobileno", session.getData(UserSessionManager.KEY_Mobile));
            intent.putExtra("OTP", session.getData(UserSessionManager.KEY_OTP));
        } else {
            intent = new Intent(LoginActivity.this, RegisterMobileActivity.class);
            /*if (session.getData(UserSessionManager.KEY_Mobile).equals(""))
                intent = new Intent(LoginActivity.this, RegisterMobileActivity.class);
            else
                intent = new Intent(LoginActivity.this, OwnerLoginActivity.class);*/
        }
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (session.getData(UserSessionManager.KEY_IMEI).equals(""))
            AppController.getUniqueIMEIId(session, LoginActivity.this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean readAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean writeAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean phoneAccepted = grantResults[2] == PackageManager.PERMISSION_GRANTED;

                    if (!phoneAccepted) {
                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, ReadPhoneState);
                    } else {
                        AppController.getUniqueIMEIId(session, LoginActivity.this);
                    }

                }
                break;
            case ReadPhoneState:
                if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, ReadPhoneState);
                } else {
                    AppController.getUniqueIMEIId(session, LoginActivity.this);
                }
                break;
        }
    }

    public void OnVendorLoginClick(View view) {
        AlertDialog.Builder alertcleardialog = new AlertDialog.Builder(LoginActivity.this);
        alertcleardialog.setTitle("Vendor Login");
        alertcleardialog.setMessage(getResources().getString(R.string.vendorlogininfo));

        alertcleardialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
               dialog.dismiss();
            }
        });

        alertcleardialog.show();
    }
}
